package main

import (
	"context"
	"fmt"
	"net"
	"os"
	"strings"
	"text/tabwriter"

	log "github.com/sirupsen/logrus"

	"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"
	facility "gitlab.com/mergetb/api/facility/v1/go"
	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/facility/mars/pkg/dance"
	"gitlab.com/mergetb/tech/shared/cli"
	clientv3 "go.etcd.io/etcd/client/v3"
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

var root = &cobra.Command{
	Use:   "dancecmd",
	Short: "The dance CLI admin tool",
}

var list = &cobra.Command{
	Use:   "list",
	Short: "List things",
}

var add = &cobra.Command{
	Use:   "add",
	Short: "Add things",
}

var del = &cobra.Command{
	Use:   "del",
	Short: "Delete things",
}

func main() {

	err := storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatal(err)
	}

	root.AddCommand(list)
	root.AddCommand(add)
	root.AddCommand(del)

	networks()
	entries()
	dns4()

	root.Execute()
}

func networks() {

	listNets := &cobra.Command{
		Use:   "networks",
		Short: "List dance networks",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			listNets()
		},
	}
	list.AddCommand(listNets)

	addNet := &cobra.Command{
		Use:   "network domain ipv4 search search ...",
		Short: "Add a dance network given network name, domain, gateway, and searches. If no search is given network name is used.",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			addNetwork(args[0], args[1], args[2], args[3:])
		},
	}
	add.AddCommand(addNet)

	delNet := &cobra.Command{
		Use:   "network domain",
		Short: "Delete a dance domain",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			delNetwork(args[0], args[1])
		},
	}
	del.AddCommand(delNet)
}

func entries() {

	listEnts := &cobra.Command{
		Use:   "entries <domain>",
		Short: "List dance entries for the given domain",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			listEntries(args[0])
		},
	}
	list.AddCommand(listEnts)

	addEnt := &cobra.Command{
		Use:   "entry <network> <domain> <mac> <ipv4> <name>",
		Short: "Add dance entry for the given domain",
		Args:  cobra.ExactArgs(5),
		Run: func(cmd *cobra.Command, args []string) {
			addEntry(args[0], args[1], args[2], args[3], args[4])
		},
	}
	add.AddCommand(addEnt)

	delEnt := &cobra.Command{
		Use:   "entry <network> <domain> <mac>",
		Short: "Delete the dance entry for the given domain and mac",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			delEntry(args[0], args[1], args[2])
		},
	}
	del.AddCommand(delEnt)
}

func dns4() {

	listDns4 := &cobra.Command{
		Use:   "dns4 <domain>",
		Short: "List dance dns4 entries for the given domain",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			listDns4(args[0])
		},
	}
	list.AddCommand(listDns4)

	addDns4 := &cobra.Command{
		Use:   "dns4 <network> <domain> <ipv4> <name>",
		Short: "Add dance dns4 entry for the given domain",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			addDns4(args[0], args[1], args[2], args[3])
		},
	}
	add.AddCommand(addDns4)

	delDns4 := &cobra.Command{
		Use:   "dns4 <network> <domain> <name>",
		Short: "Delete the dance dns4 entry for the given domain and name",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			delDns4(args[0], args[1], args[2])
		},
	}
	del.AddCommand(delDns4)
}

func listNets() {

	resp, err := storage.EtcdClient.Get(context.TODO(), "/dance", clientv3.WithPrefix())
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(cli.TableTitle(cli.Blue, "Dance Networks"))

	fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "domain", "ipv4", "gateway", "search"))
	for _, kv := range resp.Kvs {
		xk := irec.ParseKey(string(kv.Key))

		if xk == nil {
			continue
		}

		if xk.Type != irec.DanceNetKey {
			continue
		}

		net := &facility.DanceNetwork{}
		err = proto.Unmarshal(kv.Value, net)
		if err != nil {
			log.Fatalf("Network parse error: %s", err)
		}

		fmt.Fprintf(tw, cli.TableRow(
			cli.TableItem{Value: net.Domain},
			cli.TableItem{Value: dance.Uint32AsIpv4(net.DanceAddrV4).String()},
			cli.TableItem{Value: dance.Uint32AsIpv4(net.Gateway).String()},
			cli.TableItem{Value: strings.Join(net.Search, ", ")},
		))
	}
	tw.Flush()
}

func listEntries(network string) {

	resp, err := storage.EtcdClient.Get(
		context.TODO(),
		fmt.Sprintf("/dance/%s", network),
		clientv3.WithPrefix(),
	)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(cli.TableTitle(
		cli.Blue,
		fmt.Sprintf("Dance Entries for %s", network),
	))

	fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "mac", "ipv4", "names"))

	for _, kv := range resp.Kvs {
		xk := irec.ParseKey(string(kv.Key))

		if xk == nil {
			continue
		}

		if xk.Type != irec.DanceEntryKey {
			continue
		}

		ent := &facility.DanceEntry{}
		err = proto.Unmarshal(kv.Value, ent)
		if err != nil {
			log.Fatalf("dance entry parse error: %s", err)
		}

		fmt.Fprintf(tw, cli.TableRow(
			cli.TableItem{Value: xk.Name2},
			cli.TableItem{Value: dance.Uint32AsIpv4(ent.Ipv4).String()},
			cli.TableItem{Value: strings.Join(ent.Names, ", ")},
		))
	}
	tw.Flush()
}

func listDns4(network string) {

	resp, err := storage.EtcdClient.Get(
		context.TODO(),
		fmt.Sprintf("/dance/%s", network),
		clientv3.WithPrefix(),
	)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(cli.TableTitle(
		cli.Blue,
		fmt.Sprintf("Dance Entries for %s", network),
	))

	fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "ipv4", "names"))

	for _, kv := range resp.Kvs {
		xk := irec.ParseKey(string(kv.Key))

		if xk == nil {
			continue
		}

		if xk.Type != irec.DanceDns4Key {
			continue
		}

		ent := &facility.DanceDns4Entry{}
		err = proto.Unmarshal(kv.Value, ent)
		if err != nil {
			log.Fatalf("dance dns4 parse error: %s", err)
		}

		addrs := []string{}
		for _, ip := range ent.Ipv4 {
			addrs = append(addrs, dance.Uint32AsIpv4(ip).String())
		}

		fmt.Fprintf(tw, cli.TableRow(
			cli.TableItem{Value: xk.Name2},
			cli.TableItem{Value: strings.Join(addrs, ", ")},
		))
	}
	tw.Flush()
}

func addNetwork(network, domain, addr string, search []string) {

	ip := "8.8.8.8"
	cfg, err := config.GetConfig()
	if err != nil || cfg.Network.ForwardDNS == "" {
		log.Debugf("Using default DNS of 8.8.8.8..., err: %+v", err)
	} else {
		ip = cfg.Network.ForwardDNS
	}
	forward := dance.Ipv4AsUint32(net.ParseIP(ip))

	// netAddr := dance.Ipv4AsUint32(net.IPv4(172, 30, 0, 1))
	netAddr := dance.Ipv4AsUint32(net.ParseIP(addr))

	ss := []string{}
	if len(search) == 0 {
		ss = append(ss, network)
	} else {
		ss = search
	}

	// Infra DanceNetwork
	entry := &facility.DanceNetwork{
		DanceAddrV4:      netAddr,
		SubnetMask:       16,
		Domain:           network,
		Gateway:          netAddr,
		Search:           ss,
		NextServer:       netAddr,
		ServiceInterface: "eth0", // infrapod invariant
		NoBindDns:        true,   // to allow for queries from wg interfaces
		ForwardDns:       forward,
	}

	out, err := proto.Marshal(entry)
	if err != nil {
		log.Fatalf("marshal dancenetwork: %v", err)
	}

	_, err = storage.EtcdClient.Put(
		context.TODO(),
		fmt.Sprintf("/dance/%s/network/%s", network, domain),
		string(out),
	)
	if err != nil {
		log.Fatal(err)
	}
}

func addEntry(network, domain, mac, ipv4, name string) {
	// danceEntryKey = regexp.MustCompile(`^/dance/(` + namerx + `)/entry/(` + namerx + `)/(` + macrx + `)$`)

	// TODO confirm domain exists as a name

	ip := net.ParseIP(ipv4)
	if ip == nil {
		log.Fatalf("Bad ip4v address %s", ipv4)
	}

	ent, err := proto.Marshal(&facility.DanceEntry{
		Ipv4:  dance.Ipv4AsUint32(ip),
		Names: []string{name},
	})
	if err != nil {
		log.Fatalf("Entry marshal error: %s", err)
	}

	_, err = storage.EtcdClient.Put(
		context.TODO(),
		fmt.Sprintf("/dance/%s/entry/%s/%s", network, domain, mac),
		string(ent),
	)
	if err != nil {
		log.Fatal(err)
	}
}

func addDns4(network, domain, ipv4, name string) {

	ip := net.ParseIP(ipv4)
	if ip == nil {
		log.Fatalf("Bad ip4v address %s", ipv4)
	}

	ent, err := proto.Marshal(&facility.DanceDns4Entry{
		Ipv4: []uint32{dance.Ipv4AsUint32(ip)}, // todo supoport for array of names
	})
	if err != nil {
		log.Fatalf("Dns4 entry marshal error: %s", err)
	}

	_, err = storage.EtcdClient.Put(
		context.TODO(),
		fmt.Sprintf("/dance/%s/dns4/%s/%s", network, domain, name),
		string(ent),
	)
	if err != nil {
		log.Fatal(err)
	}
}

func delNetwork(network, domain string) {

	_, err := storage.EtcdClient.Delete(
		context.TODO(),
		fmt.Sprintf("/dance/%s/network/%s", network, domain),
	)

	if err != nil {
		log.Fatal(err)
	}
}

func delEntry(network, domain, mac string) {

	_, err := storage.EtcdClient.Delete(
		context.TODO(),
		fmt.Sprintf("/dance/%s/entry/%s/%s", network, domain, mac),
	)

	if err != nil {
		log.Fatal(err)
	}
}

func delDns4(network, domain, name string) {

	_, err := storage.EtcdClient.Delete(
		context.TODO(),
		fmt.Sprintf("/dance/%s/dns4/%s/%s", network, domain, name),
	)

	if err != nil {
		log.Fatal(err)
	}
}
