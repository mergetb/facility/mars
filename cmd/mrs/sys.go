package main

import (
	"context"
	"fmt"
	"os"

	"github.com/minio/minio-go/v7"
	iampolicy "github.com/minio/minio/pkg/iam/policy"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"google.golang.org/protobuf/encoding/protojson"
)

func sys() {

	initMinio := &cobra.Command{
		Use:   "minio",
		Short: "Initialize minio",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			err := initMinio()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	initialize.AddCommand(initMinio)

	updateModel := &cobra.Command{
		Use:   "model <model.xir>",
		Short: "Update the facility model",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			err := updateModel(args[0])
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	update.AddCommand(updateModel)

	showModel := &cobra.Command{
		Use:   "model",
		Short: "Show the current model",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			err := showModel()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	show.AddCommand(showModel)

}

// pull minio credentials from the config so the user doesn't need to set the proper ENV vars
func initMinioClient() error {

	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %w", err)
	}

	cred := &cfg.Services.Infraserver.Minio.Credentials
	err = storage.InitMarsMinIOClientCreds(cred.Username, cred.Password)
	if err != nil {
		return fmt.Errorf("init minio client: %w", err)
	}
	return nil

}

func initMinio() error {

	err := initMinioClient()
	if err != nil {
		return err
	}

	mc := storage.MinIOClient

	err = initializeImageBucket(mc)
	if err != nil {
		return err
	}

	err = createBucket("sys", mc)
	if err != nil {
		return err
	}

	err = provisionModel(modelsrc)
	if err != nil {
		log.Fatalf("provision model: %v", err)
	}

	return nil

}

func initializeImageBucket(mc *minio.Client) error {

	err := createBucket("images", mc)
	if err != nil {
		return err
	}

	err = createImageBucketPolicy()
	if err != nil {
		return err
	}

	err = createImageBucketReaderUser()
	if err != nil {
		return err
	}

	return nil
}

func createBucket(bucket string, mc *minio.Client) error {

	found, err := mc.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("%s bucket exists check: %v", bucket, err)
	}

	if found {
		// nothing to do
		return nil
	}

	err = mc.MakeBucket(context.TODO(), bucket, minio.MakeBucketOptions{})
	if err != nil {
		return fmt.Errorf("make image bucket: %v", err)
	}

	return nil

}

func createImageBucketPolicy() error {

	ma := storage.MinIOAdminClient

	p := &iampolicy.Policy{
		Version: "2012-10-17",
		Statements: []iampolicy.Statement{
			{
				Actions: iampolicy.ActionSet{"s3:GetObject": {}},
				Effect:  "Allow",
				Resources: iampolicy.ResourceSet{iampolicy.Resource{
					Pattern: "images/*",
				}: {}},
			},
		},
	}

	err := ma.AddCannedPolicy(context.TODO(), "image-read", p)
	if err != nil {
		return fmt.Errorf("add image read policy: %v", err)
	}

	return nil

}

func createImageBucketReaderUser() error {

	ma := storage.MinIOAdminClient

	// not intended to be an access control at this time, just a well known
	// credential.
	err := ma.AddUser(context.TODO(), "image", "imageread")
	if err != nil {
		return fmt.Errorf("create minio image user: %v", err)
	}

	err = ma.SetPolicy(context.TODO(), "image-read", "image", false)
	if err != nil {
		return fmt.Errorf("set image-read policy to image user: %v", err)
	}

	return nil

}

func provisionModel(modelsrc string) error {

	f, err := os.Open(modelsrc)
	if err != nil {
		return fmt.Errorf("open model: %v", err)
	}

	st, err := f.Stat()
	if err != nil {
		return fmt.Errorf("state model file: %v", err)
	}

	_, err = storage.MinIOClient.PutObject(
		context.TODO(),
		"sys",
		"tbxir.pbuf",
		f,
		st.Size(),
		minio.PutObjectOptions{ContentType: "application/octet-stream"},
	)
	if err != nil {
		return fmt.Errorf("minio put model: %v", err)
	}

	return nil

}

func updateModel(modelsrc string) error {

	err := initMinioClient()
	if err != nil {
		return err
	}

	return provisionModel(modelsrc)

}

func showModel() error {

	err := initMinioClient()
	if err != nil {
		return err
	}

	tbx, err := storage.GetModel()
	if err != nil {
		return fmt.Errorf("get model: %v", err)
	}

	//TODO a nice printout
	fmt.Println(protojson.Format(tbx))

	return nil

}
