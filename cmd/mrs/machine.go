package main

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
)

func stateOptions() []string {
	m := []string{}

	keys := []int{}
	for k := range state.MachineState_name {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)

	for key := range keys {
		// k==0: unknown
		if key > 0 {
			v := state.MachineState_name[int32(key)]
			m = append(m, v)
		}
	}

	return m
}

func machineCmds() {

	machine := &cobra.Command{
		Use:   "machine",
		Short: "Set machine power states and images",
	}

	powerstate := &cobra.Command{
		Use:       fmt.Sprintf("powerstate <powerstate> <node> [<node> ...]. (<powerstate> must be one of: %v)", stateOptions()),
		Short:     "Set machine power state",
		ValidArgs: stateOptions(),
		Args:      cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {

			// case insenstive match
			for _, s := range stateOptions() {
				if strings.EqualFold(s, args[0]) {
					setPowerstate(state.MachineState(state.MachineState_value[s]), args[1:])
					return
				}
			}

			log.Fatalf("'%s' is not a valid powerstate. States: %+v",
				args[0], stateOptions(),
			)
		},
	}

	image := &cobra.Command{
		Use:   "image <image> <node> [<node> ...]",
		Short: "Set machine image",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			setImage(args[0], args[1:])
		},
	}

	machine.AddCommand(powerstate)
	machine.AddCommand(image)
	set.AddCommand(machine)
}

func setPowerstate(powerstate state.MachineState, resources []string) error {
	err := withApi(func(mc facility.FacilityClient) error {
		var err error

		states := make(map[string]state.MachineState)
		for _, r := range resources {
			states[r] = powerstate
		}

		_, err = mc.SetMachinePowerState(context.TODO(), &facility.SetMachineStateRequest{
			States: states,
		})

		return err
	})
	if err != nil {
		log.Fatal(err)
	}

	return nil
}

func setImage(image string, resources []string) error {
	err := withApi(func(mc facility.FacilityClient) error {
		var err error

		images := make(map[string]*state.MachineImage)
		for _, r := range resources {
			images[r] = &state.MachineImage{
				Image: image,
			}
		}

		_, err = mc.SetMachineImage(context.TODO(), &facility.SetMachineImageRequest{
			Images: images,
		})

		return err
	})
	if err != nil {
		log.Fatal(err)
	}

	return nil
}
