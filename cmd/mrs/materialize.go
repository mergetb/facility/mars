package main

import (
	"context"
	//"fmt"
	//"io/ioutil"
	"log"
	"strings"

	//"github.com/golang/protobuf/proto"
	"github.com/spf13/cobra"

	"gitlab.com/mergetb/api/facility/v1/go"
	//"gitlab.com/mergetb/api/portal/v1/go"
	// "gitlab.com/mergetb/portal/services/pkg/materialize XXX no services imports
	// "gitlab.com/mergetb/portal/services/pkg/model" XXX no services imports
	// "gitlab.com/mergetb/portal/services/pkg/realize" XXX no services imports
	//"gitlab.com/mergetb/xir/v0.3/go"
)

func mzctl() {

	mat := &cobra.Command{
		Use:   "materialize <topo file>",
		Short: "Materialize a topology on this testbed",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { runMat(args[0]) },
	}
	root.AddCommand(mat)

	demat := &cobra.Command{
		Use:   "dematerialize <mzid>",
		Short: "Dematerialize an experiment",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { runDemat(args[0]) },
	}
	root.AddCommand(demat)

}

func runMat(filename string) {

	/* TODO XXX

	tbx, err := xir.FacilityFromFile(modelsrc)
	if err != nil {
		log.Fatal("load facility xir: %v", err)
	}
	tbt := tbx.Lift()

	src, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalf("read file: %v", err)
	}

	xnet, err := model.CompileToXir(string(src))
	if err != nil {
		log.Fatalf("compile: %v", err)
	}

	gws := tbt.Select(func(d *xir.Device) bool {
		return d.Resource().HasRole(xir.Role_Gateway)
	})
	if len(gws) == 0 {
		log.Fatal("testbed as no gateways!")
	}

	mzid := fmt.Sprintf("%s.local.marstb", xnet.Id)

	rz, ds, err := realize.Realize(
		gws[0],
		tbt,
		xnet.Lift(),
		portal.NewAllocationTable(),
		realize.RealizeParameters{
			Mzid: mzid,
		},
	)

	log.Println(ds.ToString())

	mz, err := materialize.RzToMz(rz, nil)
	if err != nil {
		log.Fatalf("create materialization: %v", err)
	}

	// XXX hardocde
	if mz.Params == nil {
		mz.Params = new(portal.MzParameters)
	}
	//mz.Params.InfranetVid = 3

	// XXX hardcode
	mz.Infranet.InfrapodConfigs["ifr"].Vid = 3

	if verbose {
		log.Printf("%s", proto.MarshalTextString(mz))
	}
	*/

}

func runDemat(mzid string) {

	parts := strings.Split(mzid, ".")
	if len(parts) != 3 {
		log.Fatalf(
			"invalid mzid '%s', expected <realization>.<experiment>.<project>",
			mzid,
		)
	}
	rid := parts[0]
	eid := parts[1]
	pid := parts[2]

	withApi(func(c facility.FacilityClient) error {

		_, err := c.Dematerialize(context.TODO(), &facility.DematerializeRequest{
			Pid: pid,
			Eid: eid,
			Rid: rid,
		})
		if err != nil {
			log.Fatalf("dematerialization error: %v", err)
		}

		log.Printf("dematerialization requested")

		return nil

	})

}
