package main

import (
	"context"
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/tech/shared/cli"
)

func infrapod() {

	listInfrapods := &cobra.Command{
		Use:   "infrapods",
		Short: "List infrapods",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { listInfrapods() },
	}
	list.AddCommand(listInfrapods)

	clearInfrapodError := &cobra.Command{
		Use:   "infrapod <mzid> <host>",
		Short: "Clear infrapod reconciler error",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			clearInfrapodError(args[0], args[1])
		},
	}
	clear.AddCommand(clearInfrapodError)

}

func listInfrapods() {

	// TODO use podman API to verify pod is actually running

	var resp *facility.ListInfrapodsResponse

	err := withApi(func(mc facility.FacilityClient) error {

		var err error

		resp, err = mc.ListInfrapods(context.TODO(), &facility.ListInfrapodsRequest{})
		if err != nil {
			return err
		}

		return nil

	})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(tw, cli.TableColumns(
		cli.Blue, "Host", "Phy", "Addr", "Subnet", "Gateway", "VLAN", "MZID", "Status"))

	for _, x := range resp.Infrapods {

		if x.State == nil {
			log.Printf("infrapod status with no state: %+v", x)
			continue
		}

		if x.State.Config == nil {
			log.Printf("infrapod state with no config: %+v", x)
			continue
		}

		tiHost := cli.TableItem{Value: x.State.Host}
		tiPhy := cli.TableItem{Value: x.State.Config.Phy}
		tiAddr := cli.TableItem{Value: x.State.Config.Addr}
		tiSubnet := cli.TableItem{Value: x.State.Config.Subnet}
		tiGateway := cli.TableItem{Value: x.State.Config.Gw}
		tiVlan := cli.TableItem{Value: x.State.Config.Vid}
		tiMzid := cli.TableItem{Value: x.State.Mzid}
		tiStatus := cli.TableItem{Value: x.Status}

		fmt.Fprintf(tw, cli.TableRow(
			tiHost,
			tiPhy,
			tiAddr,
			tiSubnet,
			tiGateway,
			tiVlan,
			tiMzid,
			tiStatus,
		))

	}

	tw.Flush()

}

func clearInfrapodError(mzid, host string) {

	err := withApi(func(mc facility.FacilityClient) error {

		_, err := mc.ClearReconcilerStatus(
			context.TODO(),
			&facility.ClearReconcilerStatusRequest{
				Keys: []string{
					fmt.Sprintf("infrapod/%s/%s", mzid, host),
				},
			},
		)

		return err

	})

	if err != nil {
		log.Fatal(err)
	}

}
