package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"reflect"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/facility/mars/pkg/config"
)

func conf() {
	conf := &cobra.Command{
		Use:   "config",
		Short: "Manage Facility Configuration",
	}
	root.AddCommand(conf)

	var overwrite bool
	configInit := &cobra.Command{
		Use:   "init [config.yml]",
		Short: "Init configuration properties",
		Long:  fmt.Sprintf("Init configuration properties from a file (default: %s)", config.DefaultConfigPath),
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 1 {
				confInit(args[0], overwrite)
			} else {
				confInit(config.DefaultConfigPath, overwrite)
			}
		},
	}
	configInit.Flags().BoolVarP(&overwrite, "overwrite", "o", false, "Overwrite existing configuration")
	conf.AddCommand(configInit)

	// add configInit to the init top-level command as well
	initConfig := &cobra.Command{
		Use:   "config [config.yml]",
		Short: "Init configuration properties",
		Long:  fmt.Sprintf("Init configuration properties from a file (default: %s)", config.DefaultConfigPath),
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 1 {
				confInit(args[0], overwrite)
			} else {
				confInit(config.DefaultConfigPath, overwrite)
			}
		},
	}
	initConfig.Flags().BoolVarP(&overwrite, "overwrite", "o", false, "Overwrite existing configuration")
	initialize.AddCommand(initConfig)

	var fromfile bool
	configSet := &cobra.Command{
		Use:   "set <key> <value>",
		Short: "Set a configuration property",
		Long:  "Set a property, where <key> is one of:\n" + strings.Join(keys(), "\n"),
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			confSet(args[0], args[1], fromfile)
		},
	}
	configSet.Flags().BoolVarP(&fromfile, "from-file", "f", false, "File with configuration data")
	conf.AddCommand(configSet)

	configGet := &cobra.Command{
		Use:   "get [key]",
		Short: "Get current configuration. If no key given show all configuration.",
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 1 {
				confGet(args[0])
			} else {
				confGet("")
			}
		},
	}
	conf.AddCommand(configGet)

	configClear := &cobra.Command{
		Use:   "clear <key>",
		Short: "Clear a configuration option",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			confClear(args[0])
		},
	}
	conf.AddCommand(configClear)
}

type CfgKey struct {
	Key     string
	Subkeys []*CfgKey
}

// convert CfgKey structure to period delimited keys
func (cfg *CfgKey) Keys() []string {
	if cfg == nil {
		return nil
	}

	// return if this is a leaf
	if len(cfg.Subkeys) == 0 {
		return []string{cfg.Key}
	}

	var keys []string
	for _, k := range cfg.Subkeys {
		for _, sk := range k.Keys() {
			// do not prepend root
			if cfg.Key == "root" {
				keys = append(keys, sk)
			} else {
				keys = append(keys, fmt.Sprintf("%s.%s", cfg.Key, sk))
			}
		}
	}

	// add self to whatever we have
	return keys
}

func KeyListRecursive(key string, val reflect.Value) *CfgKey {
	switch val.Kind() {
	case reflect.Struct:
		var subkeys []*CfgKey
		for i := 0; i < val.NumField(); i++ {
			subkeys = append(subkeys, KeyListRecursive(val.Type().Field(i).Name, val.Field(i)))
		}
		return &CfgKey{
			Key:     key,
			Subkeys: subkeys,
		}
	default:
		return &CfgKey{
			Key:     key,
			Subkeys: nil,
		}
	}

	return nil
}

func KeyGetRecursive(key string, val reflect.Value) (interface{}, error) {
	tokens := strings.Split(key, ".")

	switch val.Kind() {
	case reflect.Struct:
		for i := 0; i < val.NumField(); i++ {
			f := val.Type().Field(i)

			if f.Name != tokens[0] {
				continue
			}

			switch f.Type.Kind() {
			case reflect.Struct:
				return KeyGetRecursive(strings.Join(tokens[1:], "."), val.Field(i))
			case reflect.String:
				return val.Field(i).String(), nil
			case reflect.Int:
				return val.Field(i).Int(), nil
			case reflect.Bool:
				return val.Field(i).Bool(), nil
			}
		}
	}

	return "", fmt.Errorf("no such key")
}

func KeySetRecursive(key string, value interface{}, val reflect.Value) error {
	if key == "" {
		return fmt.Errorf("no such key")
	}

	tokens := strings.Split(key, ".")

	switch val.Kind() {
	case reflect.Struct:
		for i := 0; i < val.NumField(); i++ {
			f := val.Type().Field(i)

			if f.Name != tokens[0] {
				continue
			}

			switch f.Type.Kind() {
			case reflect.Struct:
				return KeySetRecursive(strings.Join(tokens[1:], "."), value, val.Field(i))
			case reflect.String:
				if value == nil {
					val.Field(i).SetString("")
				} else {
					val.Field(i).SetString(value.(string))
				}
				return nil
			case reflect.Int:
				if value == nil {
					val.Field(i).SetInt(int64(0))
				} else {
					vint, err := strconv.Atoi(value.(string))
					if err != nil {
						log.Fatalf("strconv %s: %v", value.(string), err)
					}
					val.Field(i).SetInt(int64(vint))
				}
				return nil
			case reflect.Bool:
				if value == nil {
					val.Field(i).SetBool(false)
				} else {
					vbool, err := strconv.ParseBool(value.(string))
					if err != nil {
						log.Fatalf("strconv %s: %v", value.(string), err)
					}
					val.Field(i).SetBool(vbool)
				}
				return nil
			}
		}
	}

	return fmt.Errorf("no such key")
}

func KeyGet(c *config.Config, key string) (interface{}, error) {
	return KeyGetRecursive(key, reflect.ValueOf(*c))
}

func KeySet(c *config.Config, key string, value interface{}) error {
	return KeySetRecursive(key, value, reflect.ValueOf(c).Elem())
}

func KeyList(c *config.Config) *CfgKey {
	return KeyListRecursive("root", reflect.ValueOf(*c))
}

func keys() []string {
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatalf("error: %s", err)
	}
	return KeyList(cfg).Keys()
}

func confInit(filename string, overwrite bool) {
	err := config.InitializeConfig(filename, overwrite)
	if err != nil {
		log.Fatalf("initialize config: %v", err)
	}
}

func confSet(key string, val interface{}, fromfile bool) {
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatalf("get config: %v", err)
	}

	if fromfile {
		buf, err := ioutil.ReadFile(val.(string))
		if err != nil {
			log.Fatalf("file read: %s", err)
		}
		val = string(buf)
	}

	err = KeySet(cfg, key, val)
	if err != nil {
		log.Fatalf("set key: %v", err)
	}

	err = config.SetConfig(cfg)
	if err != nil {
		log.Fatalf("set config: %v", err)
	}
}

func confGet(key string) {
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatalf("get config: %v", err)
	}

	fmt.Fprint(tw, "key\tvalue\n")
	fmt.Fprint(tw, "---\t-----\n")

	if key != "" {
		val, err := KeyGet(cfg, key)
		if err != nil {
			log.Fatalf("get key %s: %v", key, err)
		}

		fmt.Fprintf(tw, "%s\t%v\n", key, val)
	} else {
		for _, key := range keys() {
			val, err := KeyGet(cfg, key)
			if err != nil {
				log.Fatalf("get key %s: %v", key, err)
			}

			fmt.Fprintf(tw, "%s\t%v\n", key, val)
		}
	}

	tw.Flush()
}

func confClear(key string) {
	confSet(key, nil, false)
}
