package main

import (
	"context"
	"fmt"
	"log"
	"strings"

	"github.com/spf13/cobra"
	facility "gitlab.com/mergetb/api/facility/v1/go"
)

func wg() {

	showWg := &cobra.Command{
		Use:   "wg [mzid]",
		Short: "Show wireguard configuration for the given materialization",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { showWg(args[0]) },
	}
	show.AddCommand(showWg)

	addr := ""
	initWg := &cobra.Command{
		Use:   "wg [mzid]",
		Short: "Create the wireguard interface for the given materialization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			initWg(args[0], addr)
		},
	}
	initWg.Flags().StringVarP(&addr, "address", "", "192.168.254.1", "Address of thw WG interface")
	initialize.AddCommand(initWg)

	deinitWg := &cobra.Command{
		Use:   "wg [mzid]",
		Short: "Remove the wireguard interface from the given materialization",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { deinitWg(args[0]) },
	}
	deinitialize.AddCommand(deinitWg)
}

func showWg(mzid string) {

	withWireguard(
		func(cli facility.WireguardClient) error {

			wg, err := cli.GetWgInterface(
				context.TODO(),
				&facility.GetWgInterfaceRequest{
					Enclaveid: mzid,
				},
			)
			if err != nil {
				log.Fatalf("get interface: %s", err)
			}

			fmt.Printf("Gateway\n-------\n")
			fmt.Printf("Address: %s\n", wg.Address)
			fmt.Printf("Public Key: %s\n", wg.Publickey)
			fmt.Printf("Private Key: (hidden)\n")

			fmt.Printf("\nPeers\n-----\n")
			for _, p := range wg.Peers {
				fmt.Printf("Key: %s\n", p.Key)
				fmt.Printf("Allowed IPs: %s\n", strings.Join(p.Allowedips, ", "))
			}

			return nil
		},
	)
}

func initWg(mzid, addr string) {

	if addr == "" {
		log.Fatalf("Please specify address.")
	}

	withWireguard(
		func(cli facility.WireguardClient) error {

			_, err := cli.CreateWgInterface(
				context.TODO(),
				&facility.CreateWgInterfaceRequest{
					Enclaveid:  mzid,
					Accessaddr: addr,
				},
			)
			if err != nil {
				log.Fatalf("create wg interface: %s", err)
			}

			return nil
		},
	)
}

func deinitWg(mzid string) {

	withWireguard(
		func(cli facility.WireguardClient) error {

			_, err := cli.DeleteWgInterface(
				context.TODO(),
				&facility.DeleteWgInterfaceRequest{
					Enclaveid: mzid,
				},
			)
			if err != nil {
				log.Fatalf("create wg interface: %s", err)
			}

			return nil
		},
	)
}
