package main

import (
	"log"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"

	"gitlab.com/mergetb/facility/mars/internal/storage"
)

var root = &cobra.Command{
	Use:   "mrs",
	Short: "The Mars CLI admin tool",
}

var list = &cobra.Command{
	Use:   "list",
	Short: "List things",
}

var set = &cobra.Command{
	Use:   "set",
	Short: "Set things",
}

var portalCmd = &cobra.Command{
	Use:   "portal",
	Short: "Act on the facility's portal",
}

var initialize = &cobra.Command{
	Use:   "init",
	Short: "Initialize things",
}

var deinitialize = &cobra.Command{
	Use:   "deinit",
	Short: "Deinitialize things",
}

var update = &cobra.Command{
	Use:   "update",
	Short: "Update things",
}

var show = &cobra.Command{
	Use:   "show",
	Short: "Show things",
}

var clear = &cobra.Command{
	Use:   "clear",
	Short: "Clear reconciler errors",
}

var reset = &cobra.Command{
	Use:   "reset",
	Short: "Reset things",
}

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

var (
	modelsrc      string
	verbose       bool
	apiserverHost string
	jsonOutput    bool
	colorless     bool
)

func main() {

	root.PersistentFlags().StringVarP(
		&apiserverHost,
		"apiserver", "s",
		"apiserver",
		"The hostname or address of the apiserver to contact",
	)

	root.PersistentFlags().StringVarP(
		&modelsrc,
		"model", "m",
		"/etc/tbxir.pbuf",
		"The XIR model of the testbed to use",
	)
	root.PersistentFlags().BoolVarP(
		&verbose,
		"verbose", "v",
		false,
		"Enable verbose output",
	)
	root.PersistentFlags().BoolVarP(
		&jsonOutput,
		"json", "j",
		false,
		"Output as JSON",
	)
	root.PersistentFlags().BoolVarP(
		&colorless,
		"colorless", "c",
		false,
		"Disable color output",
	)
	err := storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("init etcd client: %v", err)
	}

	cobra.EnablePrefixMatching = true

	log.SetFlags(0)

	root.AddCommand(list)
	root.AddCommand(set)
	root.AddCommand(initialize)
	root.AddCommand(deinitialize)
	root.AddCommand(update)
	root.AddCommand(show)
	root.AddCommand(clear)
	root.AddCommand(portalCmd)

	metal()
	vm()
	ntwk()
	mz()
	canopy()
	mzctl()
	sys()
	image()
	infrapod()
	wg()
	conf()
	portalCmds()
	emu()
	statusCmds()
	resetCmds()
	ingress()
	machineCmds()

	root.Execute()

}
