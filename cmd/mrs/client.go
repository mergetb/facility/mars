package main

import (
	"crypto/tls"
	"fmt"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/service"
)

func apiserverClient() (*grpc.ClientConn, facility.FacilityClient) {

	creds := credentials.NewTLS(&tls.Config{
		//XXX XXX XXX
		InsecureSkipVerify: true,
	})
	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", apiserverHost, service.ApiserverGRPC),
		grpc.WithTransportCredentials(creds),
		storage.GRPCMaxMessage,
	)
	if err != nil {
		log.Fatalf("apiserver dial: %v", err)
	}

	return conn, facility.NewFacilityClient(conn)

}

func withApi(f func(c facility.FacilityClient) error) error {

	conn, cli := apiserverClient()
	defer conn.Close()

	return f(cli)

}

func canopyClient(host string) (*grpc.ClientConn, facility.CanopyClient) {

	creds := credentials.NewTLS(&tls.Config{
		//XXX XXX XXX
		InsecureSkipVerify: true,
	})
	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", host, service.CanopyGRPC),
		grpc.WithTransportCredentials(creds),
		storage.GRPCMaxMessage,
	)
	if err != nil {
		log.Fatalf("canopy dial: %v", err)
	}

	return conn, facility.NewCanopyClient(conn)

}

func withCanopy(host string, f func(c facility.CanopyClient) error) error {

	conn, cli := canopyClient(host)
	defer conn.Close()

	return f(cli)

}

func sledClient(host string) (*grpc.ClientConn, facility.SledClient) {

	creds := credentials.NewTLS(&tls.Config{
		//XXX XXX XXX
		InsecureSkipVerify: true,
	})
	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", host, service.SledGRPC),
		grpc.WithTransportCredentials(creds),
		storage.GRPCMaxMessage,
	)
	if err != nil {
		log.Fatalf("sled dial: %v", err)
	}

	return conn, facility.NewSledClient(conn)

}

func withSled(host string, f func(c facility.SledClient) error) error {

	conn, cli := sledClient(host)
	defer conn.Close()

	return f(cli)

}

func wireguardClient() (*grpc.ClientConn, facility.WireguardClient) {

	creds := credentials.NewTLS(&tls.Config{
		//XXX XXX XXX
		InsecureSkipVerify: true,
	})
	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", apiserverHost, service.ApiserverGRPC),
		grpc.WithTransportCredentials(creds),
		storage.GRPCMaxMessage,
	)
	if err != nil {
		log.Fatalf("apiserver dial: %v", err)
	}

	return conn, facility.NewWireguardClient(conn)
}

func withWireguard(f func(c facility.WireguardClient) error) error {

	conn, cli := wireguardClient()
	defer conn.Close()

	return f(cli)
}

func moaClient() (*grpc.ClientConn, facility.MoaClient) {
	creds := credentials.NewTLS(&tls.Config{
		//XXX XXX XXX
		InsecureSkipVerify: true,
	})
	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", apiserverHost, service.ApiserverGRPC),
		grpc.WithTransportCredentials(creds),
		storage.GRPCMaxMessage,
	)
	if err != nil {
		log.Fatalf("apiserver dial: %v", err)
	}

	return conn, facility.NewMoaClient(conn)
}

func withMoa(f func(c facility.MoaClient) error) error {
	conn, cli := moaClient()
	defer conn.Close()

	return f(cli)
}
