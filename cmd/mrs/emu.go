package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	facility "gitlab.com/mergetb/api/facility/v1/go"
)

// the maximum number of threads that can be requested
const maxThreads = 64

// emu configures commands related to the network emulation subsystem
func emu() {
	emuCmd := &cobra.Command{
		Use:   "emu",
		Short: "Configure network emulation",
	}
	root.AddCommand(emuCmd)

	// mrs emu threads default 8
	// mrs emu threads foo.bar.baz 32
	thrCmd := &cobra.Command{
		Use:   "threads",
		Short: "configure number of threads used for emulations",
	}
	emuCmd.AddCommand(thrCmd)

	getCmd := &cobra.Command{
		Use:   "get [ <mzid> ]",
		Short: "get thread configuration",
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if err := listThreads(args); err != nil {
				log.Fatal(err)
			}
		},
	}
	thrCmd.AddCommand(getCmd)

	setCmd := &cobra.Command{
		Use:   "set <mzid> <integer>",
		Short: "set thread configuration",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			if err := setThreads(args); err != nil {
				log.Fatal(err)
			}
		},
	}
	thrCmd.AddCommand(setCmd)
}

// listThreads queries the number of threads to use for a materialization.
// An empty string returns the complete list of thread configuration
func listThreads(args []string) error {
	if !validateMzid(args) {
		return errors.New("unable to parse mzid. must be 'default' or mid.eid.pid")
	}
	mzid := ""
	if len(args) > 0 {
		mzid = args[0]
	}
	err := withMoa(func(c facility.MoaClient) error {
		ret, err := c.GetThreads(context.TODO(), &facility.GetThreadsRequest{Mzid: mzid})
		if err != nil {
			return fmt.Errorf("get threads: %w", err)
		}
		for _, req := range ret.Req {
			fmt.Println(req.Mzid, req.Threads)
		}
		return nil
	})
	return err
}

func validateMzid(args []string) bool {
	if len(args) == 0 || args[0] == "default" {
		return true
	}
	if len(strings.Split(args[0], ".")) == 3 {
		return true
	}
	return false
}

// emuThreads handles the mrs threads subcommand
func setThreads(args []string) error {
	if !validateMzid(args) {
		return errors.New("unable to parse mzid. must be 'default' or mid.eid.pid")
	}

	val, err := strconv.Atoi(args[1])
	if err != nil {
		return fmt.Errorf("parse integer value: %s: %w", args[1], err)
	}
	if val < 0 {
		return errors.New("must be a positive integer")
	}
	if val > maxThreads {
		return fmt.Errorf("max threads is %d", maxThreads)
	}
	return withMoa(func(c facility.MoaClient) error {
		_, err := c.SetThreads(context.TODO(), &facility.SetThreadsRequest{Mzid: args[0], Threads: uint32(val)})
		if err != nil {
			return fmt.Errorf("set threads: %w", err)
		}
		return nil
	})
}
