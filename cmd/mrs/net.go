package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/util/sets"


	"github.com/spf13/cobra"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/portal/v1/go"
	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/shared/cli"
	"gitlab.com/mergetb/xir/v0.3/go"

	"gitlab.com/mergetb/facility/mars/internal/storage"
)

func ntwk() {

	listNet := &cobra.Command{
		Use:   "net",
		Short: "List network elements",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { listNet() },
	}
	list.AddCommand(listNet)

}

func listNet() {

	var resp *facility.ListNetResponse

	err := withApi(func(mc facility.FacilityClient) error {

		var err error

		resp, err = mc.ListNet(context.TODO(), &facility.ListNetRequest{})
		if err != nil {
			return err
		}

		return nil

	})
	if err != nil {
		log.Fatal(err)
	}

	canopyresps := make(map[string]*facility.CanopyConfig)

	// initialize everything with 'nil', which we will interpret to mean that
	// we could not communicate with the host
	for _, x := range resp.Phys {
		canopyresps[x.Host] = nil
	}
	for _, x := range resp.Vlans {
		canopyresps[x.Host] = nil
	}
	for _, x := range resp.Vteps {
		canopyresps[x.Host] = nil
	}
	for _, x := range resp.Access {
		canopyresps[x.Host] = nil
	}
	for _, x := range resp.Trunks {
		canopyresps[x.Host] = nil
	}
	for _, x := range resp.Peers {
		canopyresps[x.Host] = nil
	}

	tbx, err := storage.GetModel()
	if err != nil {
		log.Fatalf("get model: %v", err)
	}

	// a map of resource names to their current image
	images := make(map[string]string)

	metal, sled_status, ms_err := getMetalAndSledStatus()
	if ms_err != nil {
		log.Warnf("could not get metal and sled status, skipping nodes: %+v", ms_err)
	} else {
		for _, x := range metal {
			images[x.Metal.Resource] = x.Metal.Model.Image.Value
		}
	}

	// a map of hosts that don't have canopy on them
	canopyless := make(sets.String)

	// ping every host that canopy lives on for data
	for host := range canopyresps {

		r := tbx.Resource(host)
		if r == nil {
			log.Printf("warn: host %s not found", host)
			canopyless[host] = struct{}{}
			continue
		}

		// if this node can be bare metal materialized,
		// we need to check if canopy is actually on it
		// if it isn't:
		//  - note that canopy isn't on it
		//  - don't bother checking in with canopy (which is what continue does here)
		if r.HasRole(xir.Role_TbNode) {

			// if we had an error getting the metal and sled status,
			// assume that the node doesn't have canopy on it
			if ms_err != nil {
				canopyless.Insert(host)
				continue
			}

			// this node is not a hypervisor -> canopy isn't on the node
			if !r.HasRole(xir.Role_Hypervisor) {
				canopyless.Insert(host)
				continue
			}

			// no target image on the node -> canopy isn't on the node
			img, ok := images[host]
			if !ok {
				canopyless.Insert(host)
				continue
			}

			// does not have the hypervisor -> canopy isn't on the node
			// TODO: this may not be true if we have dynamically provisioned moa nodes
			if img != "hypervisor" {
				canopyless.Insert(host)
				continue
			}

			// at this point, the image on the node is supposed to be a hypervisor image,
			// and canopy is supposed to be on the image
			//
			// however, if we haven't completed sled yet,
			//   (because there's no sled status OR sled hasn't succeeded yet)
			// then we know canopy isn't running yet, so there's no need to try to communicate with canopy
			s, ok := sled_status[host]
			if !ok || s.GetCurrentStatus() != rec.TaskStatus_Success {
				continue
			}
		}

		// check in with canopy to get the interface configuration
		// a nil config means that there was an error trying to talk to the node's canopy
		var config *facility.CanopyConfig

		err = withCanopy(host, func(cc facility.CanopyClient) error {

			resp, err := cc.ListAll(
				context.TODO(), &facility.CanopyListAllRequest{})
			if err != nil {
				return fmt.Errorf("[%s] canopy list all %v", host, err)
			}

			config = resp.Config

			return nil

		})

		if err != nil {
			log.Printf("%v", err)
		} else {
			canopyresps[host] = config
		}

	}

	if len(resp.Phys) > 0 {

		fmt.Printf(cli.TableTitle(cli.Blue, "Physical Interfaces"))

		fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "Host", "Port", "MZID"))

		for _, phy := range resp.Phys {

			tiHost := cli.TableItem{Value: phy.Host}
			tiName := cli.TableItem{Value: phy.Phy.Name}
			tiMzid := cli.TableItem{Value: phy.Mzid}

			if !canopyless.Has(phy.Host) {
				config, ok := canopyresps[phy.Host]

				if !ok || config == nil {
					tiHost.Status = cli.ItemStatusError
					tiName.Status = cli.ItemStatusWarn
				} else {
					tiHost.Status = cli.ItemStatusOK

					_, ok = config.Phys[phy.Phy.Name]
					if !ok {
						tiName.Status = cli.ItemStatusError
					} else {
						tiName.Status = cli.ItemStatusOK
					}

				}
			}

			fmt.Fprintf(tw, cli.TableRow(
				tiHost,
				tiName,
				tiMzid,
			))

		}
		tw.Flush()
		fmt.Println("")
	}

	if len(resp.Vlans) > 0 {

		fmt.Printf(cli.TableTitle(cli.Blue, "Vlan Interfaces"))

		fmt.Fprintf(tw, cli.TableColumns(
			cli.Blue, "Host", "Name", "VID", "Parent", "Addrs", "MZID"))

		for _, vlan := range resp.Vlans {

			tiHost := cli.TableItem{Value: vlan.Host}
			tiName := cli.TableItem{Value: vlan.Vlan.Name}
			tiVid := cli.TableItem{Value: vlan.Vlan.Vid}
			tiParent := cli.TableItem{Value: vlan.Vlan.Parent}
			tiAddrs := cli.TableItem{Value: vlan.Vlan.Addrs}
			tiMzid := cli.TableItem{Value: vlan.Mzid}

			if !canopyless.Has(vlan.Host) {
				config, ok := canopyresps[vlan.Host]

				if !ok || config == nil {
					tiHost.Status = cli.ItemStatusError
					tiName.Status = cli.ItemStatusWarn
					tiVid.Status = cli.ItemStatusWarn
					tiParent.Status = cli.ItemStatusWarn
				} else {
					tiHost.Status = cli.ItemStatusOK

					_vlan, ok := config.Vlans[vlan.Vlan.Name]
					if !ok || _vlan == nil {
						tiName.Status = cli.ItemStatusError
					} else {
						tiName.Status = cli.ItemStatusOK

						if _vlan.Info.Vid != vlan.Vlan.Vid {
							tiVid.Status = cli.ItemStatusError
						} else {
							tiVid.Status = cli.ItemStatusOK
						}

						if !physicalInterfaceOk(_vlan.Info.Parent, vlan.Vlan.Parent) {
							tiParent.Status = cli.ItemStatusError
						} else {
							tiParent.Status = cli.ItemStatusOK
						}

					}

				}

			}

			fmt.Fprintf(tw, cli.TableRow(
				tiHost,
				tiName,
				tiVid,
				tiParent,
				tiAddrs,
				tiMzid,
			))

		}
		tw.Flush()
		fmt.Println("")

	}

	if len(resp.Vteps) > 0 {

		fmt.Printf(cli.TableTitle(cli.Blue, "Vtep Interfaces"))

		fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "Host", "Name", "VNI", "Parent", "Tunnel IP", "MZID"))

		for _, vtep := range resp.Vteps {

			tiHost := cli.TableItem{Value: vtep.Host}
			tiName := cli.TableItem{Value: vtep.Vtep.Name}
			tiVni := cli.TableItem{Value: vtep.Vtep.Vni}
			tiParent := cli.TableItem{Value: vtep.Vtep.Parent}
			tiTunnel := cli.TableItem{Value: vtep.Vtep.TunnelIp}
			tiMzid := cli.TableItem{Value: vtep.Mzid}

			if !canopyless.Has(vtep.Host) {
				config, ok := canopyresps[vtep.Host]

				if !ok || config == nil {
					tiHost.Status = cli.ItemStatusError
					tiName.Status = cli.ItemStatusWarn
					tiVni.Status = cli.ItemStatusWarn
					tiParent.Status = cli.ItemStatusWarn
					tiTunnel.Status = cli.ItemStatusWarn
				} else {
					tiHost.Status = cli.ItemStatusOK

					_vtep, ok := config.Vteps[vtep.Vtep.Name]
					if !ok || _vtep == nil {
						tiName.Status = cli.ItemStatusError
					} else {
						tiName.Status = cli.ItemStatusOK

						if _vtep.Info.Vni != vtep.Vtep.Vni {
							tiVni.Status = cli.ItemStatusError
						} else {
							tiVni.Status = cli.ItemStatusOK
						}

						if !physicalInterfaceOk(_vtep.Info.Parent, vtep.Vtep.Parent) {
							tiParent.Status = cli.ItemStatusError
						} else {
							tiParent.Status = cli.ItemStatusOK
						}

						if _vtep.Info.TunnelIp != vtep.Vtep.TunnelIp {
							tiTunnel.Value = fmt.Sprintf(
								"%s != %s", _vtep.Info.TunnelIp, vtep.Vtep.TunnelIp)
							tiTunnel.Status = cli.ItemStatusError
						} else {
							tiTunnel.Status = cli.ItemStatusOK
						}

					}

				}

			}

			fmt.Fprintf(tw, cli.TableRow(
				tiHost,
				tiName,
				tiVni,
				tiParent,
				tiTunnel,
				tiMzid,
			))

		}
		tw.Flush()
		fmt.Println("")

	}

	if len(resp.Access) > 0 {

		fmt.Printf(cli.TableTitle(cli.Blue, "Access Interfaces"))

		fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "Host", "Port", "VID", "MZID"))

		for _, access := range resp.Access {

			tiHost := cli.TableItem{Value: access.Host}
			tiPort := cli.TableItem{Value: access.Access.Port}
			tiVid := cli.TableItem{Value: access.Access.Vid}
			tiMzid := cli.TableItem{Value: access.Mzid}

			if !canopyless.Has(access.Host) {
				config, ok := canopyresps[access.Host]

				if !ok || config == nil {
					tiHost.Status = cli.ItemStatusError
					tiPort.Status = cli.ItemStatusWarn
					tiVid.Status = cli.ItemStatusWarn
				} else {
					tiHost.Status = cli.ItemStatusOK

					_access, ok := config.Access[access.Access.Port.Name]
					if !ok || _access == nil {
						tiPort.Status = cli.ItemStatusError
					} else {

						if !physicalInterfaceOk(_access.Info.Port, access.Access.Port) {
							tiPort.Status = cli.ItemStatusError
						} else {
							tiPort.Status = cli.ItemStatusOK
						}

						if _access.Info.Vid != access.Access.Vid {
							tiVid.Status = cli.ItemStatusError
						} else {
							tiVid.Status = cli.ItemStatusOK
						}

					}
				}

			}

			fmt.Fprintf(tw, cli.TableRow(
				tiHost,
				tiPort,
				tiVid,
				tiMzid,
			))

		}
		tw.Flush()
		fmt.Println("")

	}

	if len(resp.Trunks) > 0 {

		fmt.Printf(cli.TableTitle(cli.Blue, "Trunk Interfaces"))

		fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "Host", "Port", "VIDs", "MZID"))

		for _, trunk := range resp.Trunks {

			tiHost := cli.TableItem{Value: trunk.Host}
			tiPort := cli.TableItem{Value: trunk.Trunk.Port}
			tiVids := cli.TableItem{Value: trunk.Trunk.Vids}
			tiMzid := cli.TableItem{Value: trunk.Mzid}

			if !canopyless.Has(trunk.Host) {
				config, ok := canopyresps[trunk.Host]

				if !ok || config == nil {
					tiHost.Status = cli.ItemStatusError
					tiPort.Status = cli.ItemStatusWarn
					tiVids.Status = cli.ItemStatusWarn
				} else {
					tiHost.Status = cli.ItemStatusOK

					_trunk, ok := config.Trunks[trunk.Trunk.Port.Name]
					if !ok || _trunk == nil {
						tiPort.Status = cli.ItemStatusError
					} else {
						if !physicalInterfaceOk(_trunk.Info.Port, trunk.Trunk.Port) {
							tiPort.Status = cli.ItemStatusError
						} else {
							tiPort.Status = cli.ItemStatusOK
						}

						tiVids.Status = cli.ItemStatusOK

						for _, t := range trunk.Trunk.Vids {
							found := false
							for _, _t := range _trunk.Info.Vids {
								if t == _t {
									found = true
									break
								}
							}
							if !found {
								tiVids.Status = cli.ItemStatusError
								break
							}
						}

					}

				}

			}

			fmt.Fprintf(tw, cli.TableRow(
				tiHost,
				tiPort,
				tiVids,
				tiMzid,
			))

		}
		tw.Flush()
		fmt.Println("")

	}

	if len(resp.Peers) > 0 {

		fmt.Printf(cli.TableTitle(cli.Blue, "BGP Peers"))

		fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "Host", "Interface", "ASN", "First MZID"))

		for _, peer := range resp.Peers {

			tiHost := cli.TableItem{Value: peer.Host}
			tiInterface := cli.TableItem{Value: peer.Peer.Interface}
			tiAsn := cli.TableItem{Value: fmt.Sprintf("%x", peer.Peer.RemoteAsn)}
			tiMzid := cli.TableItem{Value: peer.Mzid}

			if !canopyless.Has(peer.Host) {
				config, ok := canopyresps[peer.Host]

				if !ok || config == nil {
					tiHost.Status = cli.ItemStatusError
					tiInterface.Status = cli.ItemStatusWarn
					tiAsn.Status = cli.ItemStatusWarn
				} else {
					tiHost.Status = cli.ItemStatusOK

					_peer, ok := config.Peers[peer.Peer.Interface.Name]
					if !ok || _peer == nil {
						tiInterface.Status = cli.ItemStatusError
					} else {

						if !physicalInterfaceOk(_peer.Interface, peer.Peer.Interface) {
							tiInterface.Status = cli.ItemStatusError
						} else {
							tiInterface.Status = cli.ItemStatusOK
						}

						if _peer.RemoteAsn != peer.Peer.RemoteAsn {
							tiAsn.Value = fmt.Sprintf(
								"%x != %x", _peer.RemoteAsn, peer.Peer.RemoteAsn)
							tiAsn.Status = cli.ItemStatusError
						} else {
							tiAsn.Status = cli.ItemStatusOK
						}

					}

				}

			}

			fmt.Fprintf(tw, cli.TableRow(
				tiHost,
				tiInterface,
				tiAsn,
				tiMzid,
			))

		}
		tw.Flush()
		fmt.Println("")

	}
}

// Returns if the physical interface meets the requirements, which isn't the same as equality
func physicalInterfaceOk(actual, desired *portal.PhysicalInterface) bool {

	if (actual != nil && desired == nil) || (actual == nil && desired != nil) {
		return false
	}

	if actual == nil && desired == nil {
		return true
	}

	// When the physicalInterface is a bond, we don't write its mac address
	// into the desired state, so don't compare macs if the desired mac is empty,
	// since the canopy api will always return a mac for all physical interfaces,
	// including bonds
	if desired.Mac == "" && actual.Name == desired.Name {
		return true
	}

	// When the physical interface is on a hypervisor, the names may not match up,
	// as canopy will get the actual interface name associated with the mac, so
	// don't compare names, just the macs of the interfaces if at least 1 is present
	if actual.Mac != "" || desired.Mac != "" {
		return actual.Mac == desired.Mac
	}

	// Default to both fields matching
	return actual.Equals(desired)

}
