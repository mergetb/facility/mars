package main

import (
	"context"
	"fmt"
	"log"

	"google.golang.org/protobuf/encoding/protojson"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/tech/shared/cli"
	rviews "gitlab.com/mergetb/tech/shared/cli/reconcile"
)

func mz() {

	var withStatus bool

	listMz := &cobra.Command{
		Use:   "mz",
		Short: "List materializations",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { listMz() },
	}
	list.AddCommand(listMz)

	showMz := &cobra.Command{
		Use:   "mz",
		Short: "Show materialization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			if withStatus {
				showMzStatus(args[0])
			} else {
				showMz(args[0])
			}
		},
	}
	showMz.Flags().BoolVarP(&withStatus, "with-status", "S", false, "Show current status of the materialization")
	show.AddCommand(showMz)
}

func listMz() {

	var mzm map[string]*portal.MzParameters

	err := withApi(func(mc facility.FacilityClient) error {

		resp, err := mc.ListMz(context.TODO(), &facility.ListMzRequest{})
		if err != nil {
			return err
		}
		mzm = resp.Result

		return nil

	})
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(tw, "Name\n")
	fmt.Fprintf(tw, "----\n")

	for name := range mzm {
		fmt.Fprintf(tw, "%s\n", name)
	}
	tw.Flush()

}

// Dump the materialization info from the facility etcd
func showMz(mzid string) {

	pid, eid, rid, err := parseMzId(mzid)
	if err != nil {
		log.Fatal(err)
	}

	var resp *facility.GetMaterializationResponse

	err = withApi(func(mc facility.FacilityClient) error {
		resp, err = mc.GetMaterialization(context.TODO(), &facility.GetMaterializationRequest{
			Pid: pid,
			Eid: eid,
			Rid: rid,
		})

		if err != nil {
			log.Fatal(err)
		}

		return err
	})

	if jsonOutput {
		fmt.Printf("%s\n", protojson.Format(resp))
		return
	}

	fmt.Print("Parameters:\n")
	fmt.Fprint(tw, "Server\tPhy\tAddr\tSubnet\tGw\tVni\n")
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%d\n",
		resp.Materialization.Params.InfrapodServer,
		resp.Materialization.Params.InfrapodPhy,
		resp.Materialization.Params.InfranetAddr,
		resp.Materialization.Params.InfranetSubnet,
		resp.Materialization.Params.InfranetGw,
		resp.Materialization.Params.InfranetVni)
	tw.Flush()

	fmt.Print("\nMetal:\n")
	fmt.Fprint(tw, "ID\tResource\tMAC\tVID\tPort\tAddr\n")
	for _, n := range resp.Materialization.Metal {
		fmt.Fprintf(tw, "%s\t%s\t%s\t%d\t%s\t%s\n",
			n.Model.Id,
			n.Resource,
			n.Inframac,
			n.Infravid,
			n.Infraport,
			n.InfranetAddr)
	}
	tw.Flush()

	fmt.Print("\nVms:\n")
	fmt.Fprint(tw, "Id\tResource\tAddress\n")
	for _, vm := range resp.Materialization.Vms {
		fmt.Fprintf(tw, "%s\t%s\t%s\n",
			vm.VmAlloc.Model.Id,
			vm.VmAlloc.Resource,
			vm.InfranetAddr)
	}
	tw.Flush()

	fmt.Print("\nLinks:\n")
	for _, lnk := range resp.Materialization.Links {
		fmt.Printf("%s\n", lnk.Realization.String()) // TODO what do we want to see here?
	}

	return
}

func showMzStatus(mzid string) {

	pid, eid, rid, err := parseMzId(mzid)
	if err != nil {
		log.Fatal(err)
	}

	var resp *facility.GetMaterializationStatusResponse

	err = withApi(func(mc facility.FacilityClient) error {
		resp, err = mc.GetMaterializationStatus(context.TODO(), &facility.GetMaterializationStatusRequest{
			Pid: pid,
			Eid: eid,
			Rid: rid,
		})

		if err != nil {
			log.Fatal(err)
		}

		return err
	})

	if jsonOutput {
		fmt.Printf("%s\n", protojson.Format(resp))
		return
	}

	if colorless {
		cli.DisableColor()
	}

	rviews.WriteTaskForest(resp.Status, tw)
}
