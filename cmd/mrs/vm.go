package main

import (
	"context"
	"fmt"
	"log"

	"github.com/dustin/go-humanize"
	"github.com/spf13/cobra"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
)

func vm() {

	var all bool

	listVMs := &cobra.Command{
		Use:   "vms",
		Short: "List virtual machine resources",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { listVMs() },
	}
	listVMs.Flags().BoolVarP(&all, "all", "a", false, "Include unallocated resources")
	list.AddCommand(listVMs)

}

func listVMs() {

	var vms []*state.Vm

	err := withApi(func(mc facility.FacilityClient) error {

		resp, err := mc.ListVM(context.TODO(), &facility.ListVMRequest{
			All: true,
		})
		if err != nil {
			return err
		}

		vms = resp.Machines
		return nil

	})

	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprint(tw, "hypervisor\tmzid\tnode\tcores\tmemory\tdisk\timage\n")
	fmt.Fprint(tw, "----------\t----\t----\t-----\t------\t----\t-----\n")

	for _, x := range vms {

		img := "default"

		if x.Vm.VmAlloc == nil {
			log.Printf("invalid VM in mzid %s", x.Mzid)
			continue
		}
		if x.Vm.VmAlloc.Model == nil {
			log.Printf("invalid VM %s/%s", x.Vm.VmAlloc.Resource, x.Vm.VmAlloc.Node)
			continue
		}

		if x.Vm.VmAlloc.Model.Image != nil {
			img = x.Vm.VmAlloc.Model.Image.Value
		}

		var cores, mem, disk string
		if x.Vm.VmAlloc.Procs != nil {
			cores = fmt.Sprintf("%d", x.Vm.VmAlloc.Procs.TotalCores())
		}
		if x.Vm.VmAlloc.Memory != nil {
			mem = humanize.IBytes(x.Vm.VmAlloc.Memory.TotalCapacity())
		}
		if x.Vm.VmAlloc.Disks != nil {
			disk = humanize.IBytes(x.Vm.VmAlloc.Disks.TotalCapacity())
		}

		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
			x.Vm.VmAlloc.Resource,
			x.Mzid,
			x.Vm.VmAlloc.Node,
			cores,
			mem,
			disk,
			img,
		)

		//log.Printf("%+v", x)
	}
	tw.Flush()

}
