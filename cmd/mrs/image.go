package main

import (
	"archive/zip"
	"bytes"
	"context"
	"fmt"
	"io/fs"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/minio/minio-go/v7"
	miniocredentials "github.com/minio/minio-go/v7/pkg/credentials"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/zenthangplus/goccm"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	//"gitlab.com/mergetb/facility/mars/internal/storage"
)

// TODO: use factory
const (
	bucket = "images"
)

var cia_fields = []func(*CIArtifact) string{
	func(art *CIArtifact) string { return art.Distro },
	func(art *CIArtifact) string { return art.Version },
	func(art *CIArtifact) string { return art.Variant },
}

var (
	repository      = "https://gitlab.com/mergetb/tech/images"
	tag             = "main"
	prefetched      = ""
	parallel        = 1
	dry_run         = false
	only_strings    = ""
	exclude_strings = ""
	padding         []int
	big_pad         = 0
)

type CIArtifact struct {
	Repo    string
	Ref     string
	Job     string
	Distro  string
	Version string
	Variant string
}

func (c *CIArtifact) RootfsPath() string {
	return fmt.Sprintf("%s/build/%s/%s/%s-%s.zst", c.Distro, c.Version, c.Variant, c.Distro, c.Version)
}

func (c *CIArtifact) InitramfsPath() string {
	return fmt.Sprintf("%s/build/%s/%s/collect/sled-initramfs", c.Distro, c.Version, c.Variant)
}

func (c *CIArtifact) KernelPath() string {
	return fmt.Sprintf("%s/build/%s/%s/collect/sled-kernel", c.Distro, c.Version, c.Variant)
}

func (c *CIArtifact) ArtifactName() string {
	return strings.Join(c.ArtifactStrings(), "/")
}

func (c *CIArtifact) ArtifactNameWithPadding() string {
	ans := ""
	for i, f := range cia_fields {
		ans += fmt.Sprintf("%-*s", padding[i]+2, f(c))
	}
	return ans
}

func (c *CIArtifact) ArtifactStrings() []string {
	ans := make([]string, len(cia_fields))
	for i, f := range cia_fields {
		ans[i] = f(c)
	}
	return ans
}

func parseFilters() ([]string, []string) {
	// parse only_strings
	filter_strings := []string{
		only_strings,
		exclude_strings,
	}

	ans := make([][]string, len(filter_strings))

	for i, f := range filter_strings {
		for _, m := range strings.Split(f, ",") {
			if m != "" {
				ans[i] = append(ans[i], strings.ToLower(m))
			}
		}
	}

	return ans[0], ans[1]

}

func filterArtifacts(artifacts []*CIArtifact) ([]*CIArtifact, error) {
	must_match, no_match := parseFilters()

	if len(must_match) == 0 {
		return artifacts, nil
	}

	// filter artifacts if they contain all of the things we must match

	var ans []*CIArtifact

	for _, art := range artifacts {
		ok := isArtifactOkay(
			art.ArtifactStrings(),
			must_match, no_match,
			func(a, b string) bool {
				return strings.ToLower(a) == strings.ToLower(b)
			})

		if ok {
			ans = append(ans, art)
		}
	}

	if len(ans) == 0 {
		return nil, fmt.Errorf("Nothing to do. Every artifact was filtered.")

	}

	return ans, nil
}

func isArtifactOkay(art_strings, must_match, no_match []string, comp func(string, string) bool) bool {
	add := true

	// add only if we match:
	//   everything in must_match AND
	//   nothing in no_match
	for _, m := range must_match {
		found := false

		for _, f := range art_strings {
			if comp(m, f) {
				found = true
				break
			}
		}

		if !found {
			add = false
			break
		}
	}

	for _, m := range no_match {
		for _, f := range art_strings {
			if comp(m, f) {
				add = false
				break
			}
		}
	}

	return add
}

func calculatePadding(artifacts []*CIArtifact) ([]int, int) {

	// calculate the padding of each field by the biggest one we find
	max_fields := make([]int, len(cia_fields))
	max_file := 0

	for _, art := range artifacts {
		// check the length of each field
		for i, f := range cia_fields {
			l := len(f(art))

			if l > max_fields[i] {
				max_fields[i] = l
			}
		}

		// check the length of each file
		lens := []int{
			len(art.RootfsPath()),
			len(art.InitramfsPath()),
			len(art.KernelPath()),
		}

		for _, l := range lens {
			if l > max_file {
				max_file = l
			}
		}

	}

	return max_fields, max_file

}

func image() {
	initImages := &cobra.Command{
		Use:   "images",
		Short: "Initializing base images",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			err := initImages()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	initImages.Flags().IntVarP(&parallel, "parallel", "p", parallel, "Number of parallel artifacts to process")
	initImages.Flags().StringVarP(&repository, "repository", "r", repository, "Repository to pull images from")
	initImages.Flags().StringVarP(&tag, "tag", "t", tag, "Tag to pull images from")
	initImages.Flags().StringVarP(&prefetched, "prefetched", "f", "", "Upload from the prefetched directory instead")
	initImages.Flags().StringVarP(&only_strings, "include", "i", "", "Only download artifacts that matches ALL of the specified strings in a comma separated list")
	initImages.Flags().StringVarP(&exclude_strings, "exclude", "e", "", "Do not download artifacts that match ANY of the specified strings in a comma separated list")
	initImages.Flags().BoolVarP(&dry_run, "dry-run", "d", dry_run, "Don't actually download any artifacts; just print the artifacts")
	initialize.AddCommand(initImages)
}

func loadDir(cli *minio.Client) error {

	must_match, no_match := parseFilters()

	abs, err := filepath.Abs(prefetched)
	if err != nil {
		return fmt.Errorf("abs %s: %v", prefetched, err)
	}

	abs = strings.TrimRight(abs, "/")

	walk := func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() {
			return nil
		}

		relpath := strings.TrimPrefix(path, abs)
		relpath = strings.TrimLeft(relpath, "/")

		if relpath == "" {
			return nil
		}

		ok := isArtifactOkay(
			[]string{relpath},
			must_match, no_match,
			func(match, test string) bool {
				return strings.Contains(strings.ToLower(test), strings.ToLower(match))
			})

		if !ok {
			return nil
		}

		log.Printf("+ uploading: %s -> %s", path, relpath)

		if dry_run {
			return nil
		}

		file, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("open %s: %v", path, err)
		}
		defer file.Close()

		item, err := os.Stat(path)
		if err != nil {
			return fmt.Errorf("stat %s: %v", path, err)
		}

		_, err = cli.PutObject(
			context.TODO(),
			bucket,
			relpath,
			file,
			int64(item.Size()),
			minio.PutObjectOptions{ContentType: "application/octet-stream"},
		)
		if err != nil {
			return fmt.Errorf("put minio object: %v", err)
		}

		return nil
	}

	err = filepath.WalkDir(abs, walk)
	if err != nil {
		return fmt.Errorf("walkdir %s: %v", prefetched, err)
	}

	return nil
}

func loadImage(cli *minio.Client, srcfile *zip.File, dst string, rootfs bool) error {
	reader, err := srcfile.Open()
	if err != nil {
		return fmt.Errorf("zip reader: %v", err)
	}

	log.Printf("  uploading: %-*s  -> %s", big_pad, srcfile.Name, dst)

	_, err = cli.PutObject(
		context.TODO(),
		bucket,
		dst,
		reader,
		int64(srcfile.UncompressedSize64),
		minio.PutObjectOptions{ContentType: "application/octet-stream"},
	)
	if err != nil {
		return fmt.Errorf("put minio object: %v", err)
	}

	return nil
}

func fetchArtifact(cli *minio.Client, art *CIArtifact) {
	url := fmt.Sprintf("%s/-/jobs/artifacts/%s/download?job=%s",
		art.Repo, art.Ref, art.Job,
	)

	resp, err := http.Get(url)
	if err != nil {
		log.Fatalf("image fetch: %v", err)
	}

	log.Infof("downloading: %s%s", art.ArtifactNameWithPadding(), url)

	if dry_run {
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalf("read artifact: %v", err)
	}

	zr, err := zip.NewReader(bytes.NewReader(body), int64(len(body)))
	if err != nil {
		log.Fatalf("zip reader: %v", err)
	}

	for _, zf := range zr.File {
		switch zf.Name {
		case art.RootfsPath():
			dst := fmt.Sprintf("%s/%s-rootfs", art.Variant, art.Version)
			err = loadImage(cli, zf, dst, true)
			if err != nil {
				log.Fatalf("%v", err)
			}

		case art.InitramfsPath():
			dst := fmt.Sprintf("%s/%s-initramfs", art.Variant, art.Version)
			err = loadImage(cli, zf, dst, false)
			if err != nil {
				log.Fatalf("%v", err)
			}

		case art.KernelPath():
			dst := fmt.Sprintf("%s/%s-kernel", art.Variant, art.Version)
			err = loadImage(cli, zf, dst, false)
			if err != nil {
				log.Fatalf("%v", err)
			}
		}
	}
}

func loadImages(cli *minio.Client, artifacts []*CIArtifact) error {
	if artifacts == nil {
		return nil
	}

	c := goccm.New(parallel)

	for _, art := range artifacts {
		c.Wait()
		go func(a *CIArtifact) {
			defer c.Done()
			fetchArtifact(cli, a)
		}(art)
	}

	c.WaitAllDone()

	return nil
}

func OSArtifacts(distro, version string) []*CIArtifact {
	var artifacts []*CIArtifact

	variants := []string{"efi", "bios"}
	for _, variant := range variants {
		artifacts = append(artifacts,
			&CIArtifact{
				Repo:    repository,
				Ref:     tag,
				Job:     fmt.Sprintf("%s-%s", version, variant),
				Distro:  distro,
				Version: version,
				Variant: variant,
			},
		)
	}

	return artifacts
}

func initImages() error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %w", err)
	}

	// note: we want the credentials for the minio running in the harbor pod, not the main infraserver
	cred := &cfg.Services.Infrapod.Minio.Credentials

	// single infrapod server for now
	mc, err := minio.New("infrapod.harbor.system.marstb:9000", &minio.Options{
		Creds: miniocredentials.NewStaticV4(
			cred.Username,
			cred.Password,
			"",
		),
		//TODO Secure: true, // implies ssl
	})
	if err != nil {
		return fmt.Errorf("new minio client: %v", err)
	}

	if prefetched == "" {
		var artifacts []*CIArtifact
		artifacts = append(artifacts, OSArtifacts("fedora", "hypervisor")...)
		artifacts = append(artifacts, OSArtifacts("tinycore", "tc12")...)
		artifacts = append(artifacts, OSArtifacts("tinycore", "tc13")...)
		artifacts = append(artifacts, OSArtifacts("debian", "bullseye")...)
		artifacts = append(artifacts, OSArtifacts("debian", "buster")...)
		artifacts = append(artifacts, OSArtifacts("ubuntu", "2004")...)
		artifacts = append(artifacts, OSArtifacts("ubuntu", "1804")...)

		artifacts, err = filterArtifacts(artifacts)

		if err != nil {
			return err
		}

		padding, big_pad = calculatePadding(artifacts)

		return loadImages(mc, artifacts)

	} else {
		return loadDir(mc)
	}

}
