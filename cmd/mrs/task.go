package main

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/encoding/protojson"

	"github.com/spf13/cobra"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/shared/cli"
	rviews "gitlab.com/mergetb/tech/shared/cli/reconcile"
)

func statusCmds() {

	goal := &cobra.Command{
		Use:   "goal [key]",
		Short: "Display a task goal (by reading keys directly from etcd)",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			readgoal(args[0])
		},
	}
	show.AddCommand(goal)

	status := &cobra.Command{
		Use:   "status [key]",
		Short: "Display a task status (by reading keys directly from etcd)",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			readstatus(args[0])
		},
	}
	show.AddCommand(status)

}

func readstatus(k string) {
	tt, err := storage.ReadTaskTree(k)
	if err != nil {
		log.Error(err)
		return
	}

	if jsonOutput {
		fmt.Println(protojson.Format(tt))
		return
	}

	if colorless {
		cli.DisableColor()
	}

	rviews.WriteTaskTree(tt, tw, cli.Blue)

	if tt.HighestStatus != reconcile.TaskStatus_Success {
		os.Exit(1)
	}
}

func readgoal(k string) {
	tf, err := storage.ReadTaskForest(k, 0)
	if err != nil {
		log.Error(err)
		return
	}

	if jsonOutput {
		fmt.Println(protojson.Format(tf))
		return
	}

	if colorless {
		cli.DisableColor()
	}

	rviews.WriteTaskForest(tf, tw)

	if tf.HighestStatus != reconcile.TaskStatus_Success {
		os.Exit(1)
	}
}
