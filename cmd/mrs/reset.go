package main

import (
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/tech/reconcile"
)

func resetCmds() {

	root.AddCommand(reset)

	prefix := false
	delete := false
	stale := false
	wait := 1500
	rawkey := &cobra.Command{
		Use:   "key [key...]",
		Short: "Reset a raw etcd key by rewriting it with the same value",
		Run: func(cmd *cobra.Command, args []string) {
			err := resetrawkey(prefix, delete, stale, wait, args...)

			if err != nil {
				log.Fatal(err)
			}
		},
	}
	rawkey.Flags().BoolVarP(&prefix, "prefix", "p", false, "Treat the keys as a prefix space")
	rawkey.Flags().BoolVarP(&delete, "delete", "d", false, "Delete the key before rewriting it")
	rawkey.Flags().BoolVarP(&stale, "stale", "o", false, "Set the associated statuses to stale")
	rawkey.Flags().IntVarP(&wait, "wait", "w", 1500, "How long to wait after deleting to write the keys again (in ms)")
	reset.AddCommand(rawkey)

	rawgoal := &cobra.Command{
		Use:   "goal [key...]",
		Short: "Reset a raw etcd goal by removing all unread task keys from it",
		Run: func(cmd *cobra.Command, args []string) {
			err := resetrawgoal(delete, args...)

			if err != nil {
				log.Fatal(err)
			}
		},
	}
	rawgoal.Flags().BoolVarP(&delete, "delete", "d", false, "Delete all task keys from it without checking to see if task keys and statuses exist for it")
	reset.AddCommand(rawgoal)
}

func resetrawkey(prefix, delete, stale bool, wait int, key ...string) error {
	if len(key) == 0 {
		return nil
	}

	wait_time := time.Duration(wait) * time.Millisecond

	return reconcile.ForceRehandleKey(storage.EtcdClient, prefix, delete, stale, wait_time, key...)
}

func resetrawgoal(delete bool, goals ...string) error {
	return reconcile.ResetGoals(storage.EtcdClient, delete, goals...)
}
