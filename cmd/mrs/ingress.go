package main

import (
	"context"
	"fmt"
	"log"

	"github.com/spf13/cobra"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/tech/shared/cli"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func ingress() {

	listIng := &cobra.Command{
		Use:   "ingresses",
		Short: "List ingresses",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { listIngresses() },
	}
	list.AddCommand(listIng)
}

func listIngresses() {

	err := withApi(func(c facility.FacilityClient) error {
		resp, err := c.ListIngresses(
			context.TODO(),
			&facility.ListIngressesRequest{})
		if err != nil {
			log.Fatal(err)
		}

		if len(resp.Ingresses) == 0 {
			return nil
		}

		fmt.Printf(cli.TableColumns(cli.Blue, "Ingresses"))
		fmt.Fprintf(tw, cli.TableColumns(cli.Blue, "mzid", "protocol", "host", "port", "address", "gwport", "ingress"))

		for _, ing := range resp.Ingresses {

			var ingpath string
			switch ing.Protocol {
			case xir.Protocol_tcp, xir.Protocol_udp:
				ingpath = fmt.Sprintf("%s:%d", ing.Gateway, ing.Externalport)
			case xir.Protocol_http:
				ingpath = fmt.Sprintf(
					"http://%s:80/%s/%s/%d",
					ing.Gateway,
					ing.Mzid,
					ing.Hostname,
					ing.Hostport,
				)
			case xir.Protocol_https:
				ingpath = fmt.Sprintf(
					"https://%s:443/%s/%s/%d",
					ing.Gateway,
					ing.Mzid,
					ing.Hostname,
					ing.Hostport,
				)
			}

			fmt.Fprintf(tw, cli.TableRow(
				cli.TableItem{Value: ing.Mzid, Status: cli.ItemStatusOK},
				cli.TableItem{Value: ing.Protocol.String(), Status: cli.ItemStatusOK},
				cli.TableItem{Value: ing.Hostname, Status: cli.ItemStatusOK},
				cli.TableItem{Value: ing.Hostport, Status: cli.ItemStatusOK},
				cli.TableItem{Value: ing.Hostaddr, Status: cli.ItemStatusOK},
				cli.TableItem{Value: ing.Gwnetport, Status: cli.ItemStatusOK},
				cli.TableItem{Value: ingpath, Status: cli.ItemStatusOK},
			))
		}

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	tw.Flush()
}
