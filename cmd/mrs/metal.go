package main

import (
	"context"
	"fmt"
	"time"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/facility/mars/internal/storage"

	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/shared/cli"
	rviews "gitlab.com/mergetb/tech/shared/cli/reconcile"
)

func metal() {

	var all bool

	listMetal := &cobra.Command{
		Use:   "metal",
		Short: "List bare metal resources",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { listMetal() },
	}
	listMetal.Flags().BoolVarP(&all, "with-unallocated", "u", false, "Include unallocated resources")
	list.AddCommand(listMetal)

	resetHarborCmd := &cobra.Command{
		Use:   "harbor <machines...>",
		Short: "Manually reset the harbor metal resources",
		Args:  cobra.MinimumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			return resetHarbor(args)
		},
	}
	reset.AddCommand(resetHarborCmd)

}

func listMetal() {

	metal, status, err := getMetalAndSledStatus()

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(cli.TableTitle(cli.Blue, "Baremetal Status"))

	fmt.Fprint(tw, cli.TableColumns(cli.Blue, "machine", "addr", "mzid", "node", "image", "timestamp", "info"))

	for _, x := range metal {

		img := "unknown"

		if x.Metal.Model.Image != nil {
			img = x.Metal.Model.Image.Value
		}

		s := status[x.Metal.Resource]

		cli_status := cli.ItemStatusWarn

		if s != nil {
			switch s.CurrentStatus {
			case rec.TaskStatus_Success:
				cli_status = cli.ItemStatusOK
			case rec.TaskStatus_Error:
				cli_status = cli.ItemStatusError
			}
		}

		tiResource := cli.TableItem{Value: x.Metal.Resource, Status: cli_status}
		tiAddr := cli.TableItem{Value: x.InfranetBootAddr, Status: cli_status}
		tiMzid := cli.TableItem{Value: x.Mzid, Status: cli_status}
		tiNode := cli.TableItem{Value: x.Metal.Model.Id, Status: cli_status}
		tiImage := cli.TableItem{Value: img, Status: cli_status}
		tiInfo := cli.TableItem{Value: "", Status: cli_status}
		tiTimestamp := cli.TableItem{Value: "", Status: cli_status}

		if s != nil {
			tiInfo = rviews.TaskMessagesToItem(s.CurrentStatus, s.Messages)

			tiTimestamp.Value = s.When.AsTime().Format(time.UnixDate)
		}

		fmt.Fprintf(tw, cli.TableRow(
			tiResource,
			tiAddr,
			tiMzid,
			tiNode,
			tiImage,
			tiTimestamp,
			tiInfo,
		))

	}
	tw.Flush()

}

func resetHarbor(machines []string) (err error) {
	var resp *facility.GetMaterializationResponse

	err = withApi(func(mc facility.FacilityClient) error {
		resp, err = mc.GetMaterialization(context.TODO(), &facility.GetMaterializationRequest{
			Pid: "marstb",
			Eid: "system",
			Rid: "harbor",
		})

		return err
	})

	if err != nil {
		return err
	}

	for _, t := range machines {
		found := false
		for _, m := range resp.Materialization.Metal {
			if m.Resource == t {
				log.Infof("resetting harbor key for %s", t)

				found = true

				x := &state.Metal{
					Mzid:             "harbor.system.marstb",
					Metal:            m,
					State:            state.MachineState_Reimaged,
					InfranetBootAddr: m.InfranetAddr,
				}

				b, err := proto.Marshal(x)
				if err != nil {
					return err
				}

				_, err = storage.EtcdClient.KV.Put(
					context.TODO(),
					fmt.Sprintf("/harbor.metal/%s", m.Resource),
					string(b),
				)

				if err != nil {
					return err
				}

				break
			}
		}

		if !found {
			log.Warnf("did not find harbor key for %s", t)
		}

	}

	return nil
}
