package main

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"

	"github.com/spf13/cobra"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/connect"
)

func portalCmds() {

	// mrs portal alloc ...
	allocCmds := &cobra.Command{
		Use:   "alloc",
		Short: "edit resource alloc settings",
	}

	// example: mrs portal alloc add phobos NoAlloc x0 x2 x1
	addAlloc := &cobra.Command{
		Use:   "add <facility> <alloc mode> <resource> ... <resource>",
		Short: "Add the alloc mode on model resources on the Merge Portal. This DOES NOT update the model locally.",
		Long:  "Valid alloc modes are " + strings.Join(knownAllocModes(), ", "),
		Args:  cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {

			rs := allocResources(args[2:], args[1])
			updateResources(portal.PatchStrategy_expand, args[0], rs)
		},
	}
	allocCmds.AddCommand(addAlloc)

	// example: mrs portal alloc delete phobos NoAlloc x0 x2 x1
	delAlloc := &cobra.Command{
		Use:   "delete <facility> <alloc mode> <resource> ... <resource>",
		Short: "Remove the alloc mode on model resources on the Merge Portal. This DOES NOT update the model locally.",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {

			rs := allocResources(args[2:], args[1])
			updateResources(portal.PatchStrategy_subtract, args[0], rs)
		},
	}
	allocCmds.AddCommand(delAlloc)

	// resource roles

	// mrs portal role ...
	roleCmds := &cobra.Command{
		Use:   "role",
		Short: "edit resource role settings",
	}

	// example: mrs portal role add phobos NoAlloc x0 x2 x1
	addRole := &cobra.Command{
		Use:   "add <facility> <role> <resource> ... <resource>",
		Short: "Add the rolde on model resources on the Merge Portal. This DOES NOT update the model locally.",
		Long:  "Valid roles are " + strings.Join(knownRoles(), ", "),
		Args:  cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {

			rs := roleResources(args[2:], args[1])
			updateResources(portal.PatchStrategy_expand, args[0], rs)
		},
	}
	roleCmds.AddCommand(addRole)

	// example: mrs portal role delete phobos NoAlloc x0 x2 x1
	delRole := &cobra.Command{
		Use:   "del <facility> <role> <resource> ... <resource>",
		Short: "Delete the role on model resources on the Merge Portal. This DOES NOT update the model locally.",
		Long:  "Valid roles are " + strings.Join(knownRoles(), ", "),
		Args:  cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {

			rs := roleResources(args[2:], args[1])
			updateResources(portal.PatchStrategy_subtract, args[0], rs)
		},
	}
	roleCmds.AddCommand(delRole)

	listCmd := &cobra.Command{
		Use:   "show",
		Short: "Show portal information",
	}

	res := &cobra.Command{
		Use:   "resources <facility>",
		Short: "Show resources according to the portal",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { portalResources(args[0]) },
	}
	listCmd.AddCommand(res)

	harborCmds := &cobra.Command{
		Use:   "harbor",
		Short: "Manage harbor materialization",
	}

	initHarbor := &cobra.Command{
		Use:   "init",
		Short: "Initialize the harbor",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { doInitHarbor() },
	}
	harborCmds.AddCommand(initHarbor)

	deinitHarbor := &cobra.Command{
		Use:   "deinit",
		Short: "Deinitialize the harbor",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { doDeinitHarbor() },
	}
	harborCmds.AddCommand(deinitHarbor)

	portalCmd.AddCommand(allocCmds)
	portalCmd.AddCommand(roleCmds)
	portalCmd.AddCommand(listCmd)
	portalCmd.AddCommand(harborCmds)

	// legacy compat
	initHarborLegacy := &cobra.Command{
		Use:   "harbor",
		Short: "Initialize the harbor",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { doInitHarbor() },
	}

	deinitHarborLegacy := &cobra.Command{
		Use:   "harbor",
		Short: "Deinitialize the harbor",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { doDeinitHarbor() },
	}
	initialize.AddCommand(initHarborLegacy)
	deinitialize.AddCommand(deinitHarborLegacy)
}

func updateResources(strat portal.PatchStrategy_Strategy, facility string, resources []*xir.Resource) {

	err := connect.PortalCommission(
		func(cli portal.CommissionClient) error {

			rq := &portal.UpdateFacilityRequest{
				Name: facility,
				Model: &xir.Facility{
					Resources: resources,
				},
				ModelPatchStrategy: &portal.PatchStrategy{
					Strategy: strat,
				},
			}

			_, err := cli.UpdateFacility(context.TODO(), rq)
			if err != nil {
				log.Fatal(err)
			}

			return nil
		},
	)

	if err != nil {
		log.Fatal(err)
	}
}

func allocResources(resources []string, mode string) []*xir.Resource {

	res := []*xir.Resource{}
	m := toAllocMode(mode)

	for _, r := range resources {
		res = append(res, &xir.Resource{
			Id:    r,
			Alloc: []xir.AllocMode{m},
		})
	}

	return res
}

func roleResources(resources []string, role string) []*xir.Resource {

	res := []*xir.Resource{}
	xr := toRole(role)

	for _, r := range resources {
		res = append(res, &xir.Resource{
			Id:    r,
			Roles: []xir.Role{xr},
		})
	}

	return res
}

func toAllocMode(mode string) xir.AllocMode {

	m, ok := xir.AllocMode_value[mode]
	if !ok {
		log.Fatal(
			fmt.Errorf("Unknown mode %s. Must be one of %s", mode, strings.Join(knownAllocModes(), ", ")),
		)
	}

	return xir.AllocMode(m)
}

func knownAllocModes() []string {

	modes := []string{}
	for i, mode := range xir.AllocMode_name {
		if i == 0 { // skip undefined
			continue
		}
		modes = append(modes, mode)
	}

	return modes
}

func toRole(role string) xir.Role {

	r, ok := xir.Role_value[role]
	if !ok {
		log.Fatal(
			fmt.Errorf("Unknown role %s. Must be one of %s", role, strings.Join(knownRoles(), ", ")),
		)
	}

	return xir.Role(r)
}

func knownRoles() []string {

	roles := []string{}
	for i, role := range xir.Role_name {
		if i == 0 { // skip undefined
			continue
		}
		roles = append(roles, role)
	}

	return roles
}

func portalResources(facility string) {

	var m *xir.Facility

	err := connect.PortalCommission(

		func(cli portal.CommissionClient) error {

			resp, err := cli.GetFacility(
				context.TODO(),
				&portal.GetFacilityRequest{
					Name:      facility,
					WithModel: true,
				},
			)

			if err != nil {
				log.Fatalf("get facility: %v", err)
			}

			m = resp.Model

			return nil
		},
	)

	if err != nil {
		log.Fatal(err)
	}

	active := []string{}
	inactive := []string{}
	roles := make(map[string][]string)

	for _, r := range m.Resources {

		for _, role := range r.Roles {

			roles[r.Id] = append(roles[r.Id], xir.Role_name[int32(role)])

			if role == xir.Role_TbNode {

				a := true
				for _, am := range r.Alloc {
					if am == xir.AllocMode_NoAlloc {
						a = false
					}
				}

				if a {
					active = append(active, r.Id)
				} else {
					inactive = append(inactive, r.Id)
				}
				break
			}
		}
	}

	if len(active) > 0 {
		sort.Strings(active)
		fmt.Println("Active Nodes:")
		fmt.Println(strings.Join(active, ", "))
		fmt.Println()
	}
	if len(inactive) > 0 {
		sort.Strings(inactive)
		fmt.Println("Inactive Nodes:")
		fmt.Println(strings.Join(inactive, ", "))
		fmt.Println()
	}

	if len(roles) > 0 {
		keys := []string{}
		for id := range roles {
			keys = append(keys, id)
			sort.Strings(roles[id])
		}

		sort.Strings(keys)

		fmt.Println()
		fmt.Println("Roles:")
		for _, id := range keys {
			fmt.Printf("%s: %s\n", id, strings.Join(roles[id], ", "))
		}
	}
}

// Initialize the harbor through the portal apiserver
func doInitHarbor() {

	tbx, err := storage.GetModel()
	if err != nil {
		log.Fatalf("load facility xir: %v", err)
	}

	err = connect.PortalCommission(
		func(cli portal.CommissionClient) error {
			_, err := cli.InitializeHarbor(
				context.TODO(),
				&portal.InitHarborRequest{
					Facility: tbx.Id,
					Rid:      "harbor",
					Eid:      "system",
					Pid:      "marstb",
				},
			)
			return err
		},
	)
	if err != nil {
		log.Fatalf("portal commission: %v", err)
	}

}

// Deinitialize the harbor through the portal apiserver
func doDeinitHarbor() {

	tbx, err := storage.GetModel()
	if err != nil {
		log.Fatalf("load facility xir: %v", err)
	}

	err = connect.PortalCommission(
		func(cli portal.CommissionClient) error {
			_, err := cli.DeinitializeHarbor(
				context.TODO(),
				&portal.DeinitHarborRequest{
					Facility: tbx.Id,
					Rid:      "harbor",
					Eid:      "system",
					Pid:      "marstb",
				},
			)
			return err
		},
	)
	if err != nil {
		log.Fatalf("portal commission: %v", err)
	}

}
