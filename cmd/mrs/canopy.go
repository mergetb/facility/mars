package main

import (
	"context"
	"fmt"
	"log"
	"strconv"

	"github.com/spf13/cobra"
	//"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	//"gitlab.com/mergetb/api/facility/v1/go/state"
)

func canopy() {

	// List
	listPhys := &cobra.Command{
		Use:   "phys <host>",
		Short: "List physical interfaces on a host",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { listPhys(args[0]) },
	}
	list.AddCommand(listPhys)

	listVteps := &cobra.Command{
		Use:   "vteps <host>",
		Short: "List vteps on a host",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { listVteps(args[0]) },
	}
	list.AddCommand(listVteps)

	listVlans := &cobra.Command{
		Use:   "vlans <host>",
		Short: "List vlans on a host",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { listVlans(args[0]) },
	}
	list.AddCommand(listVlans)

	listAccess := &cobra.Command{
		Use:   "access <host>",
		Short: "List vlan access ports on a host",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { listAccess(args[0]) },
	}
	list.AddCommand(listAccess)

	listTrunks := &cobra.Command{
		Use:   "trunks <host>",
		Short: "List vlan trunk ports on a host",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { listTrunks(args[0]) },
	}
	list.AddCommand(listTrunks)

	listPeers := &cobra.Command{
		Use:   "peers <host>",
		Short: "List BGP peers on a host",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { listPeers(args[0]) },
	}
	list.AddCommand(listPeers)

	// Initialize
	initAccess := &cobra.Command{
		Use:   "access <mzid> <host> <port> <vid>",
		Short: "Create vlan access port on a host",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			vid, err := strconv.Atoi(args[3])
			if err != nil {
				log.Fatal(err)
			}
			initAccess(args[0], args[1], args[2], uint32(vid))
		},
	}
	initialize.AddCommand(initAccess)

	initTrunk := &cobra.Command{
		Use:   "trunk <mzid> <host> <port> <vid>",
		Short: "Create vlan trunk port on a host",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {
			vid, err := strconv.Atoi(args[3])
			if err != nil {
				log.Fatal(err)
			}
			initTrunk(args[0], args[1], args[2], uint32(vid))
		},
	}
	initialize.AddCommand(initTrunk)

	// Deinitialize
	deinitAccess := &cobra.Command{
		Use:   "access <host> <port>",
		Short: "Delete vlan access port on a host",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			deinitAccess(args[0], args[1])
		},
	}
	deinitialize.AddCommand(deinitAccess)

	deinitTrunk := &cobra.Command{
		Use:   "trunk <host> <port> <vid>",
		Short: "Delete vlan trunk port on a host",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			vid, err := strconv.Atoi(args[2])
			if err != nil {
				log.Fatal(err)
			}
			deinitTrunk(args[0], args[1], uint32(vid))
		},
	}
	deinitialize.AddCommand(deinitTrunk)

}

func listPhys(host string) {

	var phys map[string]*facility.Phy

	withCanopy(host, func(cc facility.CanopyClient) error {

		resp, err := cc.ListPhys(context.TODO(), &facility.ListPhyRequest{})
		if err != nil {
			log.Fatal(err)
		}

		phys = resp.Phys
		return nil

	})

	for _, x := range phys {
		log.Printf("%+v", x)
	}

}

func listVteps(host string) {

	for _, x := range listHostVteps(host) {
		log.Printf("%+v", x)
	}

}

func listHostVteps(host string) map[string]*facility.Vtep {

	var vteps map[string]*facility.Vtep

	withCanopy(host, func(cc facility.CanopyClient) error {

		resp, err := cc.ListVteps(context.TODO(), &facility.ListVtepRequest{})
		if err != nil {
			log.Fatal(err)
		}

		vteps = resp.Vteps
		return nil

	})

	return vteps

}

func listVlans(host string) {

	var vlans map[string]*facility.Vlan

	withCanopy(host, func(cc facility.CanopyClient) error {

		resp, err := cc.ListVlans(context.TODO(), &facility.ListVlanRequest{})
		if err != nil {
			log.Fatal(err)
		}

		vlans = resp.Vlans
		return nil

	})

	for _, x := range vlans {
		log.Printf("%+v", x)
	}

}

func listAccess(host string) {

	var access map[string]*facility.Access

	withCanopy(host, func(cc facility.CanopyClient) error {

		resp, err := cc.ListAccess(context.TODO(), &facility.ListAccessRequest{})
		if err != nil {
			log.Fatal(err)
		}

		access = resp.Access
		return nil

	})

	for _, x := range access {
		log.Printf("%+v", x)
	}

}

func listTrunks(host string) {

	var trunks map[string]*facility.Trunk

	withCanopy(host, func(cc facility.CanopyClient) error {

		resp, err := cc.ListTrunks(context.TODO(), &facility.ListTrunkRequest{})
		if err != nil {
			log.Fatal(err)
		}

		trunks = resp.Trunks
		return nil

	})

	for _, x := range trunks {
		log.Printf("%+v", x)
	}

}

func listPeers(host string) {

	var peers map[string]*portal.BGPPeer

	withCanopy(host, func(cc facility.CanopyClient) error {

		resp, err := cc.ListPeers(context.TODO(), &facility.ListPeerRequest{})
		if err != nil {
			log.Fatal(err)
		}

		peers = resp.Peers
		return nil

	})

	for _, x := range peers {
		log.Printf("%+v", x)
	}

}

func initAccess(mzid, host, port string, vid uint32) {

	out, err := proto.Marshal(&state.Access{
		Mzid: mzid,
		Host: host,
		Access: &portal.VLANAccessPort{
			Port: &portal.PhysicalInterface{
				Name: port,
			},
			Vid: vid,
		},
		Bridge: &portal.BridgeMember{
			Bridge:   "bridge",
			Untagged: []uint32{vid},
			Pvid:     vid,
		},
	})
	if err != nil {
		log.Fatalf("failed to marshal access port: %v", err)
	}

	txo := new(storage.Txo)
	txo.Put(fmt.Sprintf("/net/%s/access/%s", host, port), out)

	_, err = txo.Exec()
	if err != nil {
		log.Fatalf("access txo: %v", err)
	}

}

func initTrunk(mzid, host, port string, vid uint32) {

	out, err := proto.Marshal(&state.Trunk{
		Mzid: mzid,
		Host: host,
		Trunk: &portal.VLANTrunkPort{
			Port: &portal.PhysicalInterface{
				Name: port,
			},
			Vids: []uint32{vid},
		},
		Bridge: &portal.BridgeMember{
			Bridge: "bridge",
			Tagged: []uint32{vid},
			Pvid:   vid,
		},
	})
	if err != nil {
		log.Fatalf("failed to marshal trunk port: %v", err)
	}

	txo := new(storage.Txo)
	txo.Put(fmt.Sprintf("/net/%s/trunk/%s.%d", host, port, vid), out)

	_, err = txo.Exec()
	if err != nil {
		log.Fatalf("trunk txo: %v", err)
	}

}

func deinitAccess(host, port string) {

	txo := new(storage.Txo)
	txo.Del(fmt.Sprintf("/net/%s/access/%s", host, port))

	_, err := txo.Exec()
	if err != nil {
		log.Fatalf("access txo: %v", err)
	}

}

func deinitTrunk(host, port string, vid uint32) {

	txo := new(storage.Txo)
	txo.Del(fmt.Sprintf("/net/%s/trunk/%s.%d", host, port, vid))

	_, err := txo.Exec()
	if err != nil {
		log.Fatalf("trunk txo: %v", err)
	}

}
