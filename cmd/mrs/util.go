package main

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	rec "gitlab.com/mergetb/tech/reconcile"
)

// parse a user-supplied string of the form <project name>.<experiment name>.<realization name> and return the parts separately
// returns nil on success, or error object
func parseMzId(mzid string) (pid string, eid string, rid string, err error) {

	parts := strings.Split(mzid, ".")
	if len(parts) != 3 {
		err = fmt.Errorf(
			"invalid mzid '%s', expected <realization>.<experiment>.<project>",
			mzid,
		)
		return
	}
	rid = parts[0]
	eid = parts[1]
	pid = parts[2]

	return
}

func getMetalAndSledStatus() ([]*state.Metal, map[string]*rec.TaskStatus, error) {
	var metal []*state.Metal

	err := withApi(func(mc facility.FacilityClient) error {

		resp, err := mc.ListMetal(context.TODO(), &facility.ListMetalRequest{
			All: true,
		})
		if err != nil {
			return err
		}

		metal = resp.Machines
		return nil

	})

	if err != nil {
		return nil, nil, fmt.Errorf("list metal: %v", err)
	}

	var hosts []string
	for _, m := range metal {
		hosts = append(hosts, m.Metal.Resource)
	}

	status, err := storage.SledStatus(hosts)
	if err != nil {
		return nil, nil, fmt.Errorf("sled status: %v", err)
	}

	return metal, status, nil
}
