package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"

	moa "gitlab.com/mergetb/api/moa/v1/go"
	mars "gitlab.com/mergetb/facility/mars/pkg"
	"gitlab.com/mergetb/facility/mars/pkg/moautil"
)

var (
	host string
)

func parseCapacity(s string) uint64 {

	var v string
	var base uint64

	if strings.HasSuffix(s, "Gbps") || strings.HasSuffix(s, "gbps") {
		// gbits/sec
		v = s[0 : len(s)-4]
		base = 1000 * 1000 * 1000
	} else if strings.HasSuffix(s, "G") || strings.HasSuffix(s, "g") {
		v = s[0 : len(s)-1]
		base = 1000 * 1000 * 1000
	} else if strings.HasSuffix(s, "Mbps") || strings.HasSuffix(s, "mbps") {
		// mbits/sec
		v = s[0 : len(s)-4]
		base = 1000 * 1000
	} else if strings.HasSuffix(s, "M") || strings.HasSuffix(s, "m") {
		v = s[0 : len(s)-1]
		base = 1000 * 1000
	} else if strings.HasSuffix(s, "Kbps") || strings.HasSuffix(s, "kbps") {
		// kbits/sec
		v = s[0 : len(s)-4]
		base = 1000
	} else if strings.HasSuffix(s, "K") || strings.HasSuffix(s, "k") {
		v = s[0 : len(s)-1]
		base = 1000
	} else if strings.HasSuffix(s, "bps") {
		// bits/sec
		v = s[0 : len(s)-3]
		base = 1
	} else {
		// bits/sec
		v = s
		base = 1
	}

	r, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", v, err)
		os.Exit(1)
	}
	if r < 0 {
		fmt.Fprintf(os.Stderr, "%d: invalid capacity value\n", r)
	}
	return uint64(r) * base
}

func parseLatency(s string) uint64 {
	var v string
	var base uint64
	if strings.HasSuffix(s, "ns") {
		base = 1
		v = s[0 : len(s)-2]
	} else if strings.HasSuffix(s, "us") {
		base = 1000
		v = s[0 : len(s)-2]
	} else if strings.HasSuffix(s, "ms") {
		base = 1000 * 1000
		v = s[0 : len(s)-2]
	} else if strings.HasSuffix(s, "s") {
		base = 1000 * 1000 * 1000
		v = s[0 : len(s)-1]
	} else {
		// assume ms
		v = s
		base = 1000 * 1000
	}
	r, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", v, err)
		os.Exit(1)
	}
	if r < 0 {
		fmt.Fprintf(os.Stderr, "%d: invalid latency value\n", r)
	}
	return uint64(r) * base
}

func doSet(values []string) {
	upd := &moa.LinkUpdate{
		Tags: []string{values[0]},
	}

	req := &moa.UpdateRequest{
		Mzid:    "unused",
		Updates: []*moa.LinkUpdate{upd},
	}

	vlen := len(values)
	for j := 1; j < vlen; j = j + 2 {
		param := values[j]
		if j == vlen-1 {
			fmt.Fprintf(os.Stdout, "missing value for parameter %s\n", param)
			os.Exit(1)
		}
		value := values[j+1]

		if param == "capacity" || param == "bandwidth" || param == "bw" || param == "cap" {
			upd.Capacity = parseCapacity(value)
		} else if param == "latency" || param == "delay" || param == "dly" || param == "lat" {
			upd.Latency = parseLatency(value)
		} else if param == "loss" {
			v, err := strconv.ParseFloat(value, 32)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s: %s\n", value, err)
				os.Exit(1)
			}
			if v < 0.0 || v > 1.0 {
				fmt.Fprintf(os.Stderr, "%f: invalid loss value (must be betweeen 0.0 and 1.0)\n", v)
				os.Exit(1)
			}
			upd.Loss = float32(v)
			upd.LossEnforce = true
		} else {
			fmt.Fprintf(os.Stderr, "%s: invalid link parameter\n", param)
			os.Exit(1)
		}
	}

	err := moautil.WithMoaControl(host, func(c moa.ControlClient) error {
		_, r := c.Update(context.TODO(), req)
		return r
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "rpc to moactld failed: %v\n", err)
		os.Exit(1)
	}
}

func cols(items ...string) string {
	return strings.Join(items, "\t")
}

func doShow() {
	err := moautil.WithMoaControl(host, func(c moa.ControlClient) error {
		resp, err := c.Show(context.TODO(), &moa.EmulationRequest{Mzid: "unused"})
		if err != nil {
			fmt.Fprintf(os.Stderr, "rpc to moactld failed: %v\n", err)
			os.Exit(1)
		}

		tw := tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
		fmt.Fprint(tw, cols("NODES", "CAPACITY", "LATENCY", "LOSS", "TAGS\n"))
		for _, lnk := range resp.LinkEmulation.Links {
			fmt.Fprintf(tw,
				cols("%v", "%s", "%s", "%f", "%v\n"),
				lnk.Nodes,
				moautil.PrettyCapacity(lnk.Capacity),
				moautil.PrettyLatency(lnk.Latency),
				lnk.Loss,
				lnk.Tags,
			)

		}

		tw.Flush()

		return nil
	})

	if err != nil {
		fmt.Fprintf(os.Stderr, "rpc to moactld failed: %v\n", err)
		os.Exit(1)
	}
}

func main() {
	var rootCmd = &cobra.Command{
		Use:     "moacmd",
		Short:   "moacmd is a tool for dynamically configuring the moa emulator",
		Version: mars.Version,
	}
	rootCmd.PersistentFlags().StringVarP(&host, "host", "s", "moactl", "specify the host for moactld")

	var setCmd = &cobra.Command{
		Use:   "set <tag> <param> <value> [ <param> <value> ...]",
		Short: "set a parameter on the set of links by tag",
		Long: `Changes the named link parameter.

[tag] refers to the user tags applied to the links in your model. All links with [tag] will be updated by this command.

[param] may be any of:
  capacity, cap, bandwidth, bw
  latency, delay, lat, dly
  loss

[value] can be of the following forms:
  capacity: 1G 1Gbps 1gbps 10M 10Mbps 10mbps 500K 500Kbps 500kbps (if no unit is specified, it is interpretted as bits/sec)
  latency:  1s 500us 50ms 20000ns (if no unit is specified, it is interpretted as ms)
  loss:     range from 0.0 to 1.0 (packet loss probability)`,
		Args: cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			doSet(args)
		},
	}
	rootCmd.AddCommand(setCmd)

	var showCmd = &cobra.Command{
		Use:   "show",
		Short: "show current link configurations",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			doShow()
		},
	}
	rootCmd.AddCommand(showCmd)

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
