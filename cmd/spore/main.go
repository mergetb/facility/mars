package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/coreos/go-systemd/v22/dbus"
	"github.com/coreos/go-systemd/v22/unit"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/facility/mars/pkg/spore"
	"gitlab.com/mergetb/facility/mars/service"
)

type spr struct{}

func main() {
	creds, err := credentials.NewServerTLSFromFile(
		"/var/vol/certs/spore.pem",
		"/var/vol/certs/spore-key.pem",
	)
	if err != nil {
		log.Fatalf("failed to read TLS cert: %v", err)
	}

	grpcServer := grpc.NewServer(
		grpc.Creds(creds),
		grpc.MaxRecvMsgSize(1024*1024*1024*4), // 4GB
	)
	spore.RegisterSporeServer(grpcServer, &spr{})

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", service.SporeGRPC))
	if err != nil {
		log.Fatalf("failed to listen: %#v", err)
	}

	log.Info("Starting the Mars Spore apiserver")
	log.Infof("Listening on tcp://:%d", service.SporeGRPC)
	grpcServer.Serve(l)
}

func (s *spr) SetHostname(
	ctx context.Context, rq *spore.SetHostnameRequest,
) (*spore.SetHostnameResponse, error) {

	out, err := exec.Command(
		"hostnamectl", "set-hostname", rq.Hostname,
	).CombinedOutput()

	if err != nil {
		return nil, fmt.Errorf("%v: %s", err, string(out))
	}

	return &spore.SetHostnameResponse{}, nil

}

func (s *spr) AddService(
	ctx context.Context, rq *spore.AddServiceRequest,
) (*spore.AddServiceResponse, error) {

	err := createService(rq.Name)
	if err != nil {
		return nil, err
	}

	err = initService(rq.Name)
	if err != nil {
		return nil, err
	}

	return &spore.AddServiceResponse{}, nil

}

func (s *spr) RemoveService(
	ctx context.Context, rq *spore.RemoveServiceRequest,
) (*spore.RemoveServiceResponse, error) {

	err := deleteServiceFiles(rq.Name)
	if err != nil {
		return nil, err
	}

	return &spore.RemoveServiceResponse{}, nil
}

func (s *spr) AddDisk(
	ctx context.Context, rq *spore.AddDiskRequest,
) (*spore.AddDiskResponse, error) {

	// format

	out, err := exec.Command("mkfs.ext4", rq.Dev).CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("mkfs.ext4: %v: %s", err, string(out))
	}

	// create systemd mount spec

	sections := []*unit.UnitSection{{
		Section: "Unit",
		Entries: []*unit.UnitEntry{
			{Name: "Description", Value: fmt.Sprintf("%s mount", rq.Name)},
		},
	}, {
		Section: "Mount",
		Entries: []*unit.UnitEntry{
			{Name: "What", Value: rq.Dev},
			{Name: "Where", Value: fmt.Sprintf("/var/mnt/%s", rq.Name)},
			{Name: "Type", Value: "ext4"},
			{Name: "Options", Value: "defaults"},
		},
	}, {
		Section: "Install",
		Entries: []*unit.UnitEntry{
			{Name: "WantedBy", Value: "multi-user.target"},
		},
	}}

	f, err := os.Create(fmt.Sprintf("/etc/systemd/system/var-mnt-%s.mount", rq.Name))
	if err != nil {
		return nil, fmt.Errorf("create mount unit file: %v", err)
	}
	_, err = io.Copy(f, unit.SerializeSections(sections))
	if err != nil {
		return nil, fmt.Errorf("write mount unit file: %v", err)
	}

	// setup the mount

	err = os.MkdirAll(fmt.Sprintf("/var/mnt/%s", rq.Name), 0755)
	if err != nil {
		return nil, fmt.Errorf("create mount point: %v", err)
	}

	sysd, err := dbus.New()
	if err != nil {
		return nil, fmt.Errorf("failed to get dbus connection: %v", err)
	}
	err = sysd.Reload()
	if err != nil {
		return nil, fmt.Errorf("systemd reload failed: %v", err)
	}
	_, _, err = sysd.EnableUnitFiles([]string{
		fmt.Sprintf("var-mnt-%s.mount", rq.Name),
	}, false, true)
	if err != nil {
		return nil, fmt.Errorf("failed to enable %s mount: %v", rq.Name, err)
	}
	_, err = sysd.StartUnit(
		fmt.Sprintf("var-mnt-%s.mount", rq.Name),
		"replace",
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to start %s mount: %v", rq.Name, err)
	}

	return &spore.AddDiskResponse{}, nil

}

func (s *spr) AddDirectory(
	ctx context.Context, rq *spore.AddDirectoryRequest,
) (*spore.AddDirectoryResponse, error) {

	err := os.MkdirAll(rq.Path, os.FileMode(rq.Mode))
	if err != nil {
		return nil, err
	}

	err = os.Chown(rq.Path, int(rq.Uid), int(rq.Gid))
	if err != nil {
		return nil, err
	}

	return &spore.AddDirectoryResponse{}, nil

}

func (s *spr) AddFile(
	ctx context.Context, rq *spore.AddFileRequest,
) (*spore.AddFileResponse, error) {

	err := os.MkdirAll(filepath.Dir(rq.Path), os.FileMode(rq.Mode))
	if err != nil {
		return nil, err
	}

	err = os.Chown(filepath.Dir(rq.Path), int(rq.Uid), int(rq.Gid))
	if err != nil {
		return nil, err
	}

	err = ioutil.WriteFile(rq.Path, []byte(rq.Content), os.FileMode(rq.Mode))
	if err != nil {
		return nil, err
	}

	err = os.Chown(rq.Path, int(rq.Uid), int(rq.Gid))
	if err != nil {
		return nil, err
	}

	return &spore.AddFileResponse{}, nil

}

func createService(name string) error {

	// Have the libpod API generate a set of systemd service descriptions for
	// this service

	t := &http.Transport{
		DialContext: func(context.Context, string, string) (net.Conn, error) {
			return net.Dial("unix", "/var/run/podman/podman.sock")
		},
	}
	c := http.Client{
		Transport: t,
	}
	defer t.CloseIdleConnections()

	resp, err := c.Get(
		fmt.Sprintf(
			"http://d/v1.0.0/libpod/generate/%s/systemd?useName=true",
			name,
		),
	)
	if err != nil {
		return fmt.Errorf("create pod service: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {

		var msg string
		buf, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			msg = string(buf)
		}

		return fmt.Errorf("failed to generate systemd service: %s: %v", resp.Status, msg)

	}

	var services map[string]string
	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to read generate systemd service response: %v", err)
	}

	err = json.Unmarshal(buf, &services)
	if err != nil {
		return fmt.Errorf("failed to unmarshal systemd service response: %v", err)
	}

	for name, svc := range services {
		err = ioutil.WriteFile(
			fmt.Sprintf("/etc/systemd/system/%s.service", name),
			[]byte(svc),
			0644,
		)
		if err != nil {
			return fmt.Errorf("write out service %s: %v", name, err)
		}

	}

	return nil

}

/* basically the following

systemctl daemon-reload
systemctl enable pod-$name.service
systemctl restart $name
*/
func initService(name string) error {

	sysd, err := dbus.New()
	if err != nil {
		return fmt.Errorf("failed to get dbus connection: %v", err)
	}

	err = sysd.Reload()
	if err != nil {
		return fmt.Errorf("systemd reload failed: %v", err)
	}

	_, _, err = sysd.EnableUnitFiles([]string{
		fmt.Sprintf("pod-%s.service", name),
	}, false, true)
	if err != nil {
		return fmt.Errorf("failed to enable %s service: %v", name, err)
	}

	_, err = sysd.RestartUnit(
		fmt.Sprintf("pod-%s.service", name),
		"replace",
		nil,
	)
	if err != nil {
		return fmt.Errorf("failed to restart %s service: %v", name, err)
	}

	return nil

}

// clean up systemd files on dematerialization. opposite of initService.
func deleteServiceFiles(name string) error {

	sysd, err := dbus.New()
	if err != nil {
		return fmt.Errorf("failed to get dbus connection: %v", err)
	}

	_, err = sysd.StopUnit(
		fmt.Sprintf("pod-%s.service", name),
		"replace",
		nil,
	)
	if err != nil {
		return fmt.Errorf("failed to stop %s service: %v", name, err)
	}

	_, err = sysd.DisableUnitFiles([]string{
		fmt.Sprintf("pod-%s.service", name),
	}, false)
	if err != nil {
		return fmt.Errorf("failed to disable %s service: %v", name, err)
	}

	/*
		err = os.Remove(fmt.Sprintf("/etc/systemd/system/%s.service", name))
		if err != nil {
			return fmt.Errorf("failed to remove systemd service %s: %v", name, err)
		}

		err = sysd.Reload()
		if err != nil {
			return fmt.Errorf("systemd reload failed: %v", err)
		}
	*/

	return nil

}
