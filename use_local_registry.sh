#!/bin/bash

export \
    REGISTRY=localhost:5000 \
    REGISTRY_PATH=mergetb/facility/mars \
    TAG=main \
    DOCKER=podman \
    DOCKER_PUSH_ARGS=--tls-verify=false \
    BUILD_ARGS='--no-cache --squash-all'
