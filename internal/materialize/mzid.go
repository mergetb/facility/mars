package materialize

import (
	"fmt"
	"strings"
)

type Mzid struct {
	Rid string
	Eid string
	Pid string
}

func (m *Mzid) ToString() string {
	return m.Mid()
}

func (m *Mzid) Mid() string {
	return fmt.Sprintf("%s.%s.%s", m.Rid, m.Eid, m.Pid)
}

func (m *Mzid) ToMzid(rid, eid, pid string) {
	m.Rid = rid
	m.Eid = eid
	m.Pid = pid
}

func NewMzidFromIds(rid, eid, pid string) *Mzid {
	return &Mzid{
		Rid: rid,
		Eid: eid,
		Pid: pid,
	}
}

func NewMzidFromMid(mid string) *Mzid {
	ids := strings.Split(mid, ".")
	if len(ids) != 3 {
		return nil
	}

	return NewMzidFromIds(ids[0], ids[1], ids[2])
}
