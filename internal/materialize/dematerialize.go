package materialize

import (
	"context"
	"fmt"
	"path"
	"time"

	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	rec "gitlab.com/mergetb/tech/reconcile"
	storlib "gitlab.com/mergetb/tech/shared/storage/etcd"
)

func ClearMaterializationState(rid, eid, pid string) error {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	l, err := storlib.NewLock(ctx, storage.EtcdClient, "all.mtz")
	if err != nil {
		return err
	}
	defer l.Unlock()

	mzid := fmt.Sprintf("%s.%s.%s", rid, eid, pid)

	mz, err := storage.FetchMaterialization(mzid)
	if err != nil {
		return err
	}

	txo := new(storage.Txo)

	// delete all goals for the mzid
	tg := rec.NewGenericGoal(mzid)
	err = tg.Read(storage.EtcdClient)
	if err != nil {
		// assume it doesn't exist
		log.Error(err)
	} else {
		for _, k := range tg.GetSubgoals() {
			g := rec.NewGenericGoal(k)
			txo.Del(g.Key())
		}
	}

	txo.Del(tg.Key())

	// remove mzid from all.mtz
	root_mtz_goal := rec.NewGoal("all.mtz", "", "")
	err = root_mtz_goal.Read(storage.EtcdClient)
	if err != nil {
		log.Errorf("failed readtaskgoal: %+v", err)
		// assume it doesn't exist...
		// can't remove mzid from all if it doesn't exist...
	} else {

		if root_mtz_goal.RemoveSubgoal(tg) {
			_, err = root_mtz_goal.Update(storage.EtcdClient)
			if err != nil {
				log.Errorf("failed to write subgoal: %+v", err)
			}
		} else {
			log.Errorf("%s not removed from all.mtz", mzid)
		}

	}

	txo.DumpKeys()

	_, err = txo.Exec()
	if err != nil {
		return fmt.Errorf("goal etcd txn: %v", err)
	}

	txo = new(storage.Txo)

	clearParameters(mz, txo)

	err = metalClearOps(mz, txo)
	if err != nil {
		return err
	}

	err = vmClearOps(mz, txo)
	if err != nil {
		return err
	}

	err = linkClearOps(mz, txo)
	if err != nil {
		return err
	}

	err = danceClearOps(mz, txo)
	if err != nil {
		return err
	}

	err = pathfinderClearOps(mz, txo)
	if err != nil {
		return err
	}

	err = deleteInfrapod(mz, txo)
	if err != nil {
		return err
	}

	err = deleteEmu(mz, txo)
	if err != nil {
		return err
	}

	txo.DumpKeys()

	_, err = txo.Exec()
	if err != nil {
		return fmt.Errorf("delete etcd txn: %v", err)
	}

	// We have to do this after, instead of as part of the other TXNs, for a whole bunch of reasons
	err = linkClearPeers(mz)
	if err != nil {
		return err
	}

	return nil

}

func clearParameters(mz *portal.Materialization, txo *storage.Txo) {

	mzid := mzid(mz)

	txo.Del(fmt.Sprintf("/mzparam/%s", mzid))

}

func deleteInfrapod(mz *portal.Materialization, txo *storage.Txo) error {

	mzid := mzid(mz)
	srv := mz.GetParams().GetInfrapodServer()
	if srv == "" {
		return fmt.Errorf("no infrapod server defined for %s", mzid)
	}
	txo.Del(fmt.Sprintf("/infrapod/%s/%s", mzid, srv))
	return nil

}

func deleteEmu(mz *portal.Materialization, txo *storage.Txo) error {
	if mz.LinkEmulation != nil {
		txo.Del(fmt.Sprintf("/emu/%s/%s", mz.LinkEmulation.Server, mzid(mz)))
	}

	return nil
}

func metalClearOps(mz *portal.Materialization, txo *storage.Txo) error {

	mzid := mzid(mz)
	harbor := is_harbor_mzid(mzid)

	for _, x := range mz.Metal {
		if harbor {
			txo.Del(fmt.Sprintf("/harbor.metal/%s", x.Resource))
		} else {
			// put harbor metal back
			m, err := RetrieveHarborMetal(x.Resource)
			if err != nil {
				return fmt.Errorf("cannot set harbor metal state: %v", err)
			}
			// and signal the need to reimage
			m.State = state.MachineState_Reimaged
			out, err := proto.Marshal(m)
			if err != nil {
				return fmt.Errorf("marshal metal: %s/%s", mzid, x.Resource)
			}
			txo.Put(fmt.Sprintf("/metal/%s", x.Resource), out)
		}
	}

	return nil

}

func vmClearOps(mz *portal.Materialization, txo *storage.Txo) error {

	// VM resource allocations
	for _, x := range mz.Vms {
		txo.Del(
			fmt.Sprintf("/vm/%s/%s", x.VmAlloc.Resource, fqvm(mz, x.VmAlloc.Node)),
		)
	}

	// TunTap waypoints
	for _, lnk := range mz.Links {
		for _, segment := range lnk.Realization.Segments {
			for _, wp := range segment.Waypoints {
				switch ifx := wp.Interface.(type) {
				case *portal.Waypoint_Tap:

					// name is the fqvm with the tap frontend appended
					name := fmt.Sprintf("%s.%s", fqvm(mz, ifx.Tap.Node), ifx.Tap.Frontend.Name)

					txo.Del(
						fmt.Sprintf("/net/%s/tap/%s", wp.Host, name),
					)
				}
			}
		}
	}

	return nil

}

func linkClearOps(mz *portal.Materialization, txo *storage.Txo) error {

	harbor := is_harbor_mzid(mzid(mz))

	for _, lnk := range mz.Links {
		for _, segment := range lnk.Realization.Segments {

			for _, ep := range segment.Endpoints {

				switch ifx := ep.Interface.(type) {

				case *portal.Endpoint_Phy:
					if ep.Virtual {
						continue
					}

					// Deleting physical endpoints during a harbor materialization
					// means that you bring down the management interfaces,
					// which is pretty annoying
					if harbor {
						continue
					}

					txo.Del(
						fmt.Sprintf("/net/%s/phy/%s", ep.Host, ifx.Phy.Name),
					)

				case *portal.Endpoint_Vlan:
					txo.Del(
						fmt.Sprintf("/net/%s/vlan/%s", ep.Host, ifx.Vlan.Name),
					)

				case *portal.Endpoint_Vtep:
					txo.Del(
						fmt.Sprintf("/net/%s/vtep/%s", ep.Host, ifx.Vtep.Name),
					)

				}

			}

			for _, wp := range segment.Waypoints {

				switch ifx := wp.Interface.(type) {

				case *portal.Waypoint_Access:
					txo.Del(
						fmt.Sprintf("/net/%s/access/%s", wp.Host, ifx.Access.Port.Name),
					)

				case *portal.Waypoint_Trunk:
					txo.Del(
						fmt.Sprintf("/net/%s/trunk/%s.%d", wp.Host, ifx.Trunk.Port.Name, ifx.Trunk.Vids[0]),
					)

				case *portal.Waypoint_Vtep:
					txo.Del(
						fmt.Sprintf("/net/%s/vtepw/%s", wp.Host, ifx.Vtep.Name),
					)

				case *portal.Waypoint_BgpPeer:
					// For now, only delete this mzid's peer
					// We will check later to see if we need to clear
					// out the root key as well
					txo.Del(
						path.Join("/net/", wp.Host, "peer", ifx.BgpPeer.Interface.Name, "mzid", mzid(mz)),
					)

				}

			}
		}
	}

	return nil

}

func pathfinderClearOps(mz *portal.Materialization, txo *storage.Txo) error {

	// We can just blow away all ingress requests for this mtz and let the
	// reconciler sort it out.
	mzid := mzid(mz)
	txo.Del(
		fmt.Sprintf("/ingressreq/%s/%s/", mz.Params.InfrapodServer, mzid),
		clientv3.WithPrefix(),
	)

	return nil
}

func danceClearOps(mz *portal.Materialization, txo *storage.Txo) error {

	mzid := mzid(mz)

	txo.Del(
		fmt.Sprintf("/dance/%s/", mzid),
		clientv3.WithPrefix(),
	)

	// delete status too, since it's unlikely to be handled by dance (since it gets deleted)
	txo.Del(
		fmt.Sprintf("/.tasks/status/dance/%s/", mzid),
		clientv3.WithPrefix(),
	)

	return nil

}

func linkClearPeers(mz *portal.Materialization) error {

	for _, lnk := range mz.Links {
		for _, segment := range lnk.Realization.Segments {
			for _, wp := range segment.Waypoints {
				if peer := wp.GetBgpPeer(); peer != nil {

					// We want to delete the root BGP Peer if there are no materializations
					// that are currently using the peer

					txo := new(storage.Txo)

					root := path.Join("/net/", wp.Host, "peer", peer.Interface.Name)
					cmp := clientv3.Version(path.Join(root, "mzid") + "/").WithPrefix()

					// If we don't find any MZID keys, then delete the root
					txo.Ifs = append(txo.Ifs, clientv3.Compare(cmp, "=", 0))
					txo.Del(root)

					txr, err := txo.Exec()

					if err != nil && txr == nil {
						return fmt.Errorf("peer etcd txn: %v", err)
					}
					if txr.Succeeded {
						log.Debugf("Deleted Root Peer Key: %s", root)
					} else {
						log.Debugf("Kept Root Peer Key: %s", root)
					}

				}

			}
		}
	}

	return nil

}
