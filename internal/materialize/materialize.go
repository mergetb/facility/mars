package materialize

import (
	"context"
	"fmt"
	"net"
	"path"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/facility/mars/pkg/dance"
	rec "gitlab.com/mergetb/tech/reconcile"
	storlib "gitlab.com/mergetb/tech/shared/storage/etcd"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func SetMaterializationState(mz *portal.Materialization) error {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	l, err := storlib.NewLock(ctx, storage.EtcdClient, "all.mtz")
	if err != nil {
		return err
	}
	defer l.Unlock()

	mzid := mzid(mz)

	all_mtz_goal := rec.NewGenericGoal("all.mtz")

	err = all_mtz_goal.Read(storage.EtcdClient)
	if err != nil || all_mtz_goal.Type == rec.TaskGoal_Undefined {
		log.Warn(err)
		log.Info("Due to error reading all.mtz, creating...")
		// assume that it doesn't exist
		all_mtz_goal = rec.NewGoal("all.mtz", "all.mtz", "List all materializations")
	}

	cur_mtz_goal := rec.NewGoal(mzid, mzid, fmt.Sprintf("Create materialization %s", mzid))

	// add mzid to all.mtz supergoal
	all_mtz_goal.AddSubgoal(cur_mtz_goal)
	all_mtz_goal.Update(storage.EtcdClient)

	root_txo := new(storage.Txo)

	err = setParameters(mz, cur_mtz_goal, root_txo)
	if err != nil {
		return err
	}

	err = metalCreateOps(mz, cur_mtz_goal, root_txo)
	if err != nil {
		return err
	}

	err = vmCreateOps(mz, cur_mtz_goal, root_txo)
	if err != nil {
		return err
	}

	err = linkCreateOps(mz, cur_mtz_goal, root_txo)
	if err != nil {
		return err
	}

	err = danceCreateOps(mz, cur_mtz_goal, root_txo)
	if err != nil {
		return err
	}

	err = pathfinderCreateOps(mz, cur_mtz_goal, root_txo)
	if err != nil {
		return err
	}

	err = createInfrapod(mz, cur_mtz_goal, root_txo)
	if err != nil {
		return err
	}

	// add wireguard ops to the goal, so later wireguard operations can read from this goal
	wg_goal := rec.NewGoal(mzid+"/wireguardOps", "wireguardOps", "Facility-side wireguard operations")
	// we need to manually write the timestamp because we're not using the storage library
	wg_goal.WriteTimestamp()
	cur_mtz_goal.AddSubgoal(wg_goal)

	cur_mtz_goal.WriteTimestamp()

	if b, err := proto.Marshal(wg_goal); err == nil {
		root_txo.Put(wg_goal.GetPrefixedKey(), b)
	} else {
		log.Errorf("marshal wireguardops: %+v", err)
	}

	root_txo.DumpKeys()

	if b, err := proto.Marshal(cur_mtz_goal); err == nil {
		root_txo.Put(cur_mtz_goal.GetPrefixedKey(), b)
	} else {
		log.Errorf("marshal mzid goal: %+v", err)
	}

	_, err = root_txo.Exec()
	if err != nil {
		return fmt.Errorf("etcd txn: %v", err)
	}

	return nil

}

func setParameters(mz *portal.Materialization, root_goal *rec.TaskGoal, root_txo *storage.Txo) error {

	mzid := mzid(mz)

	txo, goal, subtask_filter := createTxoGoalAndFilter(
		root_goal,
		path.Join(mzid, "setParameters"),
		"setParameters",
		"Set parameters",
	)

	defer registerTasksAndGoal(goal, txo, root_txo, subtask_filter)

	out, err := proto.Marshal(mz.Params)
	if err != nil {
		return fmt.Errorf("marshal mz params: %s: %v", mzid, err)
	}

	k := fmt.Sprintf("/mzparam/%s", mzid)
	subtask_filter.Add(k)
	txo.Put(k, out)

	return nil
}

func metalCreateOps(mz *portal.Materialization, root_goal *rec.TaskGoal, root_txo *storage.Txo) error {

	mzid := mzid(mz)
	harbor := is_harbor_mzid(mzid)

	txo, goal, subtask_filter := createTxoGoalAndFilter(
		root_goal,
		path.Join(mzid, "metalCreateOps"),
		"metalCreateOps",
		"Image the baremetal resources",
	)

	defer registerTasksAndGoal(goal, txo, root_txo, subtask_filter)

	for _, x := range mz.Metal {

		var st *state.Metal

		// The boot address is always on the harbor network, _not_ the infranet
		// for the mz.Metal node. Therefore we query the harbor metal key for
		// this resource
		bootAddr := x.InfranetAddr
		if !harbor {
			m, err := RetrieveHarborMetal(x.Resource)
			if err != nil {
				return fmt.Errorf("cannot get harbor metal state for resource %s: %v", x.Resource, err)
			}
			bootAddr = m.Metal.InfranetAddr
		}

		st = &state.Metal{
			Mzid:             mzid,
			Metal:            x,
			State:            state.MachineState_Reimaged,
			InfranetBootAddr: bootAddr,
		}

		if harbor {
			// Special case: Harbor mtz request, when an mtz is already actively running on this
			// resource. Update the infranet address, but do not reimage
			prev, p_err := RetrieveMetal(x.Resource)
			if p_err == nil {
				log.Infof("maintaining metal state for %s", x.Resource)

				prev.InfranetBootAddr = bootAddr
				prev.State = state.MachineState_On
				st = prev
			}
		}

		out, err := proto.Marshal(st)
		if err != nil {
			return fmt.Errorf("marshal metal: %s/%s: %v", mzid, x.Resource, err)
		}

		txo.Put(fmt.Sprintf("/metal/%s", x.Resource), out)

		if harbor {
			// Save harbor state of subsequent retrieval on demtz
			hk := fmt.Sprintf("/harbor.metal/%s", x.Resource)
			subtask_filter.Add(hk)
			txo.Put(hk, out)
		}
	}

	for _, x := range mz.Vms {

		// add the bare metal keys found in the vms to the metal goal,
		// as we need to put the hypervisor image onto machines first
		goal.AddTaskRecord(&rec.TaskRecord{
			Task: fmt.Sprintf("/metal/%s", x.VmAlloc.Resource),
		})
	}

	return nil

}

func vmCreateOps(mz *portal.Materialization, root_goal *rec.TaskGoal, root_txo *storage.Txo) error {

	mzid := mzid(mz)

	txo, goal, subtask_filter := createTxoGoalAndFilter(
		root_goal,
		path.Join(mzid, "vmCreateOps"),
		"vmCreateOps",
		"Create virtual machines",
	)

	defer registerTasksAndGoal(goal, txo, root_txo, subtask_filter)

	// VM resource allocations
	for _, x := range mz.Vms {
		out, err := proto.Marshal(&state.Vm{
			Mzid:  mzid,
			Vm:    x,
			State: state.MachineState_Reimaged,
		})
		if err != nil {
			return fmt.Errorf("marshal vm: %s/%s: %+v", mzid, x.VmAlloc.Resource, err)
		}

		txo.Put(
			fmt.Sprintf("/vm/%s/%s", x.VmAlloc.Resource, fqvm(mz, x.VmAlloc.Node)),
			out,
		)
	}

	// TunTap waypoints
	for _, lnk := range mz.Links {
		for _, segment := range lnk.Realization.Segments {
			for _, wp := range segment.Waypoints {
				switch ifx := wp.Interface.(type) {
				case *portal.Waypoint_Tap:

					// name is the fqvm with the tap frontend appended
					name := fmt.Sprintf("%s.%s", fqvm(mz, ifx.Tap.Node), ifx.Tap.Frontend.Name)

					out, err := proto.Marshal(&state.Tap{
						Mzid:   mzid,
						Host:   wp.Host,
						Tap:    ifx.Tap,
						Bridge: wp.Bridge,
					})
					if err != nil {
						return fmt.Errorf("marshal tap %s/%s/%s: %+v", mzid, wp.Host, name, err)
					}

					k := fmt.Sprintf("/net/%s/tap/%s", wp.Host, name)
					txo.Put(
						k,
						out,
					)
					subtask_filter.Add(k)
				}
			}
		}
	}

	return nil

}

func linkCreateOps(mz *portal.Materialization, root_goal *rec.TaskGoal, root_txo *storage.Txo) error {

	mzid := mzid(mz)

	txo, goal, subtask_filter := createTxoGoalAndFilter(
		root_goal,
		path.Join(mzid, "linkCreateOps"),
		"linkCreateOps",
		"Configure networking links",
	)

	defer registerTasksAndGoal(goal, txo, root_txo, subtask_filter)

	// Make a bare metal list
	metal := make(map[string]struct{})
	for _, m := range mz.Metal {
		if !isHypervisor(m) {
			metal[m.Resource] = struct{}{}
		}
	}

	for _, lnk := range mz.Links {
		for _, segment := range lnk.Realization.Segments {

			for _, ep := range segment.Endpoints {

				// skip endpoints on bare metal nodes,
				// since they're not created by canopy
				if _, found := metal[ep.Host]; found {
					continue
				}

				switch ifx := ep.Interface.(type) {

				case *portal.Endpoint_Phy:
					if ep.Virtual {
						continue
					}

					out, err := proto.Marshal(&state.Phy{
						Mzid: mzid,
						Host: ep.Host,
						Phy:  ifx.Phy,
						Mtu:  ep.Mtu,
					})
					if err != nil {
						return fmt.Errorf("marshal phy: %s/%s/%s", mzid, ep.Host, ifx.Phy.Name)
					}
					txo.Put(
						fmt.Sprintf("/net/%s/phy/%s", ep.Host, ifx.Phy.Name),
						out,
					)

				case *portal.Endpoint_Vlan:
					out, err := proto.Marshal(&state.Vlan{
						Mzid:   mzid,
						Host:   ep.Host,
						Vlan:   ifx.Vlan,
						Bridge: ep.Bridge,
						Mtu:    ep.Mtu,
					})
					if err != nil {
						return fmt.Errorf("marshal vlan: %s/%s/%s", mzid, ep.Host, ifx.Vlan.Name)
					}
					txo.Put(
						fmt.Sprintf("/net/%s/vlan/%s", ep.Host, ifx.Vlan.Name),
						out,
					)

				case *portal.Endpoint_Vtep:
					out, err := proto.Marshal(&state.Vtep{
						Mzid:   mzid,
						Host:   ep.Host,
						Vtep:   ifx.Vtep,
						Bridge: ep.Bridge,
						Mtu:    ep.Mtu,
					})
					if err != nil {
						return fmt.Errorf("marshal vtep: %s/%s/%s", mzid, ep.Host, ifx.Vtep.Name)
					}
					txo.Put(
						fmt.Sprintf("/net/%s/vtep/%s", ep.Host, ifx.Vtep.Name),
						out,
					)

				}

			}

			for _, wp := range segment.Waypoints {

				switch ifx := wp.Interface.(type) {

				case *portal.Waypoint_Access:
					out, err := proto.Marshal(&state.Access{
						Mzid:   mzid,
						Host:   wp.Host,
						Access: ifx.Access,
						Bridge: wp.Bridge,
						Mtu:    wp.Mtu,
					})
					if err != nil {
						return fmt.Errorf("marshal access: %s/%s/%s", mzid, wp.Host, ifx.Access.Port.Name)
					}
					txo.Put(
						fmt.Sprintf("/net/%s/access/%s", wp.Host, ifx.Access.Port.Name),
						out,
					)

				case *portal.Waypoint_Trunk:
					out, err := proto.Marshal(&state.Trunk{
						Mzid:   mzid,
						Host:   wp.Host,
						Trunk:  ifx.Trunk,
						Bridge: wp.Bridge,
						Mtu:    wp.Mtu,
					})
					if err != nil {
						return fmt.Errorf("marshal trunk: %s/%s/%s", mzid, wp.Host, ifx.Trunk.Port.Name)
					}
					txo.Put(
						fmt.Sprintf("/net/%s/trunk/%s.%d", wp.Host, ifx.Trunk.Port.Name, ifx.Trunk.Vids[0]),
						out,
					)

				case *portal.Waypoint_Vtep:
					out, err := proto.Marshal(&state.Vtep{
						Mzid:   mzid,
						Host:   wp.Host,
						Vtep:   ifx.Vtep,
						Bridge: wp.Bridge,
						Mtu:    wp.Mtu,
					})
					if err != nil {
						return fmt.Errorf("marshal vtepw: %s/%s/%s", mzid, wp.Host, ifx.Vtep.Name)
					}
					txo.Put(
						fmt.Sprintf("/net/%s/vtepw/%s", wp.Host, ifx.Vtep.Name),
						out,
					)

				case *portal.Waypoint_BgpPeer:
					out, err := proto.Marshal(&state.Peer{
						Mzid: mzid,
						Host: wp.Host,
						Peer: ifx.BgpPeer,
						Mtu:  wp.Mtu,
					})
					if err != nil {
						return fmt.Errorf("marshal peer: %s/%s", mzid, wp.Host)
					}

					// Place two keys -- once as the root key,
					// which is the one that actually sets up the BG Peer,
					// and a dummy key to keep track of which
					// materializations need this peer.

					// The root key is going to be conditionally written separately,
					// so we don't have to unnecessarily write it if it already exists
					// It's nice to avoid writing it again, so all mz's "Last Updated" don't get updated
					new_txo := new(storage.Txo)
					key := fmt.Sprintf("/net/%s/peer/%s", wp.Host, ifx.BgpPeer.Interface.Name)
					new_txo.Put(
						key,
						out,
					)

					// Only write this new key if it's not already there
					new_txo.Ifs = append(txo.Ifs, clientv3.Compare(clientv3.Version(key), "=", 0))

					// Commit the key
					// TODO: When using the new storage library, check txr.Succeeded instead
					_, err = new_txo.Exec()
					if err != nil && !strings.Contains(err.Error(), "transaction failed") {
						log.Fatalf("%+v", err)
					}

					// Manually add the key to the goal
					goal.AddTaskRecord(&rec.TaskRecord{
						Task: key,
					})

					// Write the dummy key that keeps track of which mtz need this peer
					dummy := path.Join(key, "mzid", mzid)
					subtask_filter.Add(dummy)

					txo.Put(
						dummy,
						nil,
					)
				}

			}
		}
	}

	return nil
}

func createInfrapod(mz *portal.Materialization, root_goal *rec.TaskGoal, root_txo *storage.Txo) error {

	mzid := mzid(mz)

	txo, goal, subtask_filter := createTxoGoalAndFilter(
		root_goal,
		path.Join(mzid, "createInfrapod"),
		"createInfrapod",
		"Create mtz infrapod",
	)

	defer registerTasksAndGoal(goal, txo, root_txo, subtask_filter)

	mdl, err := storage.GetModel()
	if err != nil {
		return err
	}

	for _, x := range mdl.Resources {

		if x.Id == mz.GetParams().GetInfrapodServer() {

			// sanity check that the resource specified in the Materialization object is actually an infrapod server
			if !x.HasRole(xir.Role_InfrapodServer) {
				return fmt.Errorf(
					"resource %s does not have the InfrapodServer role", x.Id)
			}

			ifx := x.Infranet()
			if ifx == nil {
				return fmt.Errorf(
					"infrapod server %s has no infranet interface", x.Id)
			}

			mifx := x.Mgmt()
			if mifx == nil {
				return fmt.Errorf(
					"infrapod server %s has no management interface", x.Id)
			}

			addr := "172.30.0.1"
			// special case for the harbor network
			if mz.Params.InfranetVni == 3 {
				addr = "172.29.0.1"
			}

			wrapped := &state.InfrapodConfig{
				Mzid: mzid,
				Host: x.Id,
				Config: &portal.InfrapodConfig{
					Addr:    addr,
					Phy:     x.PortName(ifx),
					Vni:     mz.Params.InfranetVni,
					MgmtPhy: x.PortName(mifx),
				},
			}

			out, err := proto.Marshal(wrapped)
			if err != nil {
				return fmt.Errorf("marshal infrapod config entry: %v", err)
			}

			txo.Put(
				fmt.Sprintf("/infrapod/%s/%s", mzid, x.Id),
				out,
			)

			// TODO only single infrapod server supported
			return nil
		}
	}

	return fmt.Errorf("no infrapod servers found")

}

func emuCreateOps(mz *portal.Materialization, root_goal *rec.TaskGoal, root_txo *storage.Txo) error {

	mzid := mzid(mz)

	txo, goal, subtask_filter := createTxoGoalAndFilter(
		root_goal,
		path.Join(mzid, "emuCreateOps"),
		"emuCreateOps",
		"Tell moa servers about mtz",
	)

	defer registerTasksAndGoal(goal, txo, root_txo, subtask_filter)

	if mz.LinkEmulation != nil {

		// all we need to do is set an empty etcd key so that moad knows to read the materialization object out of minio
		txo.Put(fmt.Sprintf("/emu/%s/%s", mz.LinkEmulation.Server, mzid), nil)

	}
	return nil
}

func NewIngressRequest(mzid, host string, port uint32, protocol xir.Protocol) error {

	log.Debugf("new ingress %s %s:%d:%s", mzid, host, port, protocol.String())

	mz, err := storage.FetchMaterialization(mzid)
	if err != nil {
		return err
	}

	// This should be mz.Params.InfranetGw. TOD: fix on portal.
	gateway := mz.Params.InfrapodServer
	key := fmt.Sprintf("/ingressreq/%s/%s/%s:%s:%d", gateway, mzid, protocol.String(), host, port)
	goal := rec.NewGenericGoal(mzid + "/pathfinderOps")
	err = goal.Read(storage.EtcdClient)
	if err != nil {
		log.Errorf("etcd: read %s/pathfinderOps goal, not updating: %v", mzid, err)
		return err
	}
	goal.AddTaskRecord(&rec.TaskRecord{Task: key})

	// find node infraaddr
	var infraaddr string
	for _, node := range mz.GetMetal() {
		if node.Model.Id == host {
			infraaddr = node.InfranetAddr
			break
		}
	}
	if infraaddr == "" {
		for _, node := range mz.GetVms() {
			mdl := node.VmAlloc.GetModel()
			if mdl.Id == host {
				infraaddr = node.InfranetAddr
				break
			}
		}
	}

	if infraaddr == "" {
		return fmt.Errorf("No such node in materialization: %s", host)
	}

	txo := new(storage.Txo)
	value := fmt.Sprintf("%s:%d", protocol.String(), port)
	err = addIngressRequest(mzid, host, infraaddr, gateway, []string{value}, txo)
	if err != nil {
		return err
	}

	_, err = txo.Exec()
	if err != nil {
		return err
	}

	goal.Update(storage.EtcdClient)

	return nil
}

func DeleteIngressRequest(mzid, host string, port uint32, protocol xir.Protocol) error {

	mz, err := storage.FetchMaterialization(mzid)
	if err != nil {
		return err
	}

	// This should be mz.Params.InfranetGw. TOD: fix on portal.
	gateway := mz.Params.InfrapodServer
	key := fmt.Sprintf("/ingressreq/%s/%s/%s:%s:%d", gateway, mzid, protocol.String(), host, port)
	goal := rec.NewGenericGoal(mzid + "/pathfinderOps")
	err = goal.Read(storage.EtcdClient)
	if err != nil {
		log.Errorf("etcd: read %s/pathfinderOps goal, not updating: %v", mzid, err)
		return err
	}

	txo := new(storage.Txo)
	txo.Del(key)
	if err != nil {
		return err
	}

	_, err = txo.Exec()
	if err != nil {
		return err
	}

	goal.RemoveSubtask(&rec.TaskRecord{Task: key})
	goal.Update(storage.EtcdClient)

	return nil
}

func addIngressRequest(mzid, host, infraaddr, gateway string, values []string, txo *storage.Txo) error {

	for _, ing := range values {
		// Format is proto:port
		tokens := strings.Split(ing, ":")
		port, err := strconv.Atoi(tokens[1])
		if err != nil {
			return fmt.Errorf("mars: Unable to parse ingress port as number: %s", tokens[1])
		}

		// create an ingress request. The reconciler will add needed runtime data
		// and create the actual ingress task and ingress implementation.
		out, err := proto.Marshal(&state.IngressReq{
			Mzid:     mzid,
			Protocol: xir.Protocol(xir.Protocol_value[tokens[0]]),
			Hostname: host,
			Hostport: uint32(port),
			Hostaddr: infraaddr,
		})

		txo.Put(
			fmt.Sprintf("/ingressreq/%s/%s/%s:%s:%d", gateway, mzid, tokens[0], host, port),
			out,
		)
	}

	return nil
}

func pathfinderCreateOps(mz *portal.Materialization, root_goal *rec.TaskGoal, root_txo *storage.Txo) error {
	mzid := mzid(mz)

	txo, goal, subtask_filter := createTxoGoalAndFilter(
		root_goal,
		path.Join(mzid, "pathfinderOps"),
		"pathfinderOps",
		"Create experiment ingresses",
	)

	defer registerTasksAndGoal(goal, txo, root_txo, subtask_filter)

	for _, node := range mz.GetMetal() {
		if node.Model.Properties != nil {
			if ings, ok := node.Model.Properties.Keyvalues["ingress"]; ok {
				err := addIngressRequest(mzid, node.Model.Id, node.InfranetAddr, mz.Params.InfrapodServer, ings.Values, txo)
				if err != nil {
					return err
				}
			}
		}
	}

	for _, node := range mz.GetVms() {
		mdl := node.VmAlloc.GetModel()
		if mdl.Properties != nil {
			if ings, ok := mdl.Properties.Keyvalues["ingress"]; ok {
				err := addIngressRequest(mzid, mdl.Id, node.InfranetAddr, mz.Params.InfrapodServer, ings.Values, txo)
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func danceCreateOps(mz *portal.Materialization, root_goal *rec.TaskGoal, root_txo *storage.Txo) error {

	var err error

	mzid := mzid(mz)
	harbor := is_harbor_mzid(mzid)

	txo, goal, subtask_filter := createTxoGoalAndFilter(
		root_goal,
		path.Join(mzid, "danceCreateOps"),
		"danceCreateOps",
		"Allocate ip addresses to materialization resources",
	)

	defer registerTasksAndGoal(goal, txo, root_txo, subtask_filter)

	mdl, err := storage.GetModel()
	if err != nil {
		return err
	}

	var infraDomain string
	if harbor {
		infraDomain = mzid
	} else {
		infraDomain = fmt.Sprintf("infra.%s", mzid)
	}
	expDomain := fmt.Sprintf("exp.%s", mzid)

	// TODO: assumes single infrapod
	harboraddr := dance.Ipv4AsUint32(net.IPv4(172, 29, 0, 1))

	var infrapodaddr uint32
	if harbor {
		infrapodaddr = harboraddr
	} else {
		infrapodaddr = dance.Ipv4AsUint32(net.IPv4(172, 30, 0, 1))
	}

	// DanceDns4Entries. These are always mzid (as mzid is always in the search).
	dns4 := map[string]*facility.DanceDns4Entry{
		fmt.Sprintf("podetcd.%s", mzid): {
			Ipv4: []uint32{infrapodaddr},
		},
		fmt.Sprintf("foundry.%s", mzid): {
			Ipv4: []uint32{infrapodaddr},
		},
		// special case because the xdc doesn't have the bare mzid in the search path
		fmt.Sprintf("moactl.%s", infraDomain): {
			Ipv4: []uint32{infrapodaddr},
		},
		"sled.system.marstb": {
			Ipv4: []uint32{harboraddr},
		},
		"images.system.marstb": {
			Ipv4: []uint32{harboraddr},
		},
	}

	// Add the mars system etcd/minio entries for harbor materializations, which are needed
	// by hypervisors
	if harbor {
		etcdip4, err := lookupMarsIP4("etcd")
		if err != nil {
			return err
		}

		minioip4, err := lookupMarsIP4("minio")
		if err != nil {
			return err
		}

		// Because we are not ground-control, these addresses are intentionally
		// advertised on a different domain than the facility fqdn,
		dns4["etcd.system.marstb"] = &facility.DanceDns4Entry{
			Ipv4: []uint32{dance.Ipv4AsUint32(etcdip4)},
		}
		dns4["minio.system.marstb"] = &facility.DanceDns4Entry{
			Ipv4: []uint32{dance.Ipv4AsUint32(minioip4)},
		}
	}

	// DanceEntry for bare metal nodes
	for _, x := range mz.Metal {
		ip := net.ParseIP(x.InfranetAddr)

		// get XIR resource for this node to determine whether it boots with UEFI or legecy BIOS firmware
		rsrc := mdl.Resource(x.Resource)
		if rsrc == nil {
			return fmt.Errorf("no resource '%s' in facility model!", x.Resource)
		}

		fname := tftpFilename(rsrc)
		log.Debugf("configuring tftp filename %s for resource %s", fname, x.Resource)

		uip := dance.Ipv4AsUint32(ip)
		infra := fmt.Sprintf("%s.%s", x.Model.Id, infraDomain)

		out, err := proto.Marshal(&facility.DanceEntry{
			Ipv4:         uip,
			Names:        []string{infra},
			TftpFilename: fname,
		})
		if err != nil {
			return fmt.Errorf("marshal dance entry: %v", err)
		}

		txo.Put(fmt.Sprintf("/dance/%s/entry/%s/%s", mzid, mzid, x.Inframac), out)

		// infranet dns
		dns4[infra] = &facility.DanceDns4Entry{Ipv4: []uint32{uip}}
	}

	// DanceEntry for vms
	for _, x := range mz.Vms {
		ip := net.ParseIP(x.InfranetAddr)

		uip := dance.Ipv4AsUint32(ip)
		infra := fmt.Sprintf("%s.%s", x.VmAlloc.Node, infraDomain)

		out, err := proto.Marshal(&facility.DanceEntry{
			Ipv4:  uip,
			Names: []string{infra},
		})
		if err != nil {
			return fmt.Errorf("marshal dance entry: %v", err)
		}

		txo.Put(fmt.Sprintf("/dance/%s/entry/%s/%s", mzid, mzid, x.Inframac), out)

		// infranet dns
		dns4[infra] = &facility.DanceDns4Entry{Ipv4: []uint32{uip}}
	}

	// Only register DNS entries for expnet when requested by a user, so that we don't interfere
	// if they want to do something like run their own DNS or install /etc/hosts entries
	if mz.Params.ResolveExpnet {
		err = addExpNetDNS4Entries(expDomain, dns4, mz)
		if err != nil {
			return fmt.Errorf("Adding experiment DNS entries: %v", err)
		}
	}

	for name, entry := range dns4 {
		out, err := proto.Marshal(entry)
		if err != nil {
			return fmt.Errorf("marshal dancedns4 entry: %v", err)
		}

		txo.Put(
			fmt.Sprintf("/dance/%s/dns4/%s/%s", mzid, mzid, name),
			out,
		)
	}

	//
	// Search domains provide the flexibility to customize how short names resolve in experiments.
	// Behavior is customized based on whether users request name resolution to experiment addresses
	// or not.
	//
	// If we are resolving node names on the experiment network, the following is configured:
	// * all exp addresses resolve as <name>.exp.<mzid>
	// * all infranet addresses resolve as <name>.infra.<mzid>
	// * Search domains are:
	//   * exp.<mzid>
	//   * <mzid>
	// Only mzid and not infra.mzid as we want the user to explicitly ask for infra addresses ("ssh
	// name.infra" instead of "ssh name") as there may not be an experiment net name for every
	// infranet name - and we do not want to mix networks when resolving short names. Short names
	// for experiment network, not-as-short names for infra network.
	//
	// If we are not resolving node names on the experiment network, the following is configured:
	// * all infra addresses resolve <name>.infra.<mzid>
	// * exp addresses do not resolve at all
	// * Search domains are:
	//   * infra.<mzid>
	//   * <mzid>.
	// Note that it may seem superfluous to register both domains when only infranet resolution is
	// occuring. We do this so that:
	//  a) short names resolve directly (infra.<mzid> is needed for this); and
	//  b) <name>.infra also resolves (<mzid> is needed for this)
	// We want a) because this was the behavior pre any support for experiment net resolution; e.g.,
	// "ping name" or ssh name" resolve to infranet addresses, and we need to maintain that
	// behavior. We want b) because users that may happen to bounce back and forth between different
	// experiments where some use exp resolution and some do not operate in a consistent
	// environment. They would likely be confused if infranet addresses do not *always* resolve via
	// <node>.infra
	//
	// Lastly, harbor is a special case. Maintain its existing behavior
	//

	var search []string
	if harbor {
		search = append(search, []string{"system.marstb", mzid}...)
	} else {
		if mz.Params.ResolveExpnet == true {
			search = append(search, expDomain, mzid)
		} else {
			search = append(search, infraDomain, mzid)
		}
	}

	ip := "8.8.8.8"
	cfg, err := config.GetConfig()
	if err != nil || cfg.Network.ForwardDNS == "" {
		log.Debugf("Using default DNS of 8.8.8.8..., err: %+v", err)
	} else {
		ip = cfg.Network.ForwardDNS
	}

	forward := dance.Ipv4AsUint32(net.ParseIP(ip))

	// Infra DanceNetwork
	network := &facility.DanceNetwork{
		DanceAddrV4:      infrapodaddr,
		SubnetMask:       16,
		Domain:           mzid,
		Gateway:          infrapodaddr,
		Search:           search,
		NextServer:       infrapodaddr,
		TftpBootloader:   "snponly.efi", // default, is overridden by node XIR resource config
		ServiceInterface: "eth0",        // infrapod invariant
		NoBindDns:        true,          // to allow for queries from wg interfaces
		ForwardDns:       forward,
	}

	out, err := proto.Marshal(network)
	if err != nil {
		return fmt.Errorf("marshal dancenetwork: %v", err)
	}

	txo.Put(
		fmt.Sprintf("/dance/%s/network/%s", mzid, mzid),
		out,
	)

	return nil
}

func addExpNetDNS4Entries(expnet string, dns4 map[string]*facility.DanceDns4Entry, mz *portal.Materialization) error {

	// Each interface with an address gets a "<name>-<addr>.<expnet>" address. The first
	// address on each node also gets a plain "<name>.<expnet>" address as the default.
	// The "addr" in the "<name>-<addr>" format is the IP address with the dots replaced
	// with dashes. This allows experimentors to resolve to the exact interface if they want to.
	//
	// example: node named "fred" with two interfaces with experiment addresses could end up with
	// DNS entries like:
	//    "fred"          --> 10.0.1.1
	//    "fred-10-0-1-1" --> 10.0.1.1
	//    "fred-10-0-2-1" --> 10.0.2.1
	//

	for _, x := range mz.Metal {

		defaultName := false
		for _, socket := range x.GetModel().GetSockets() {

			addrs := []uint32{}
			for _, a := range socket.GetAddrs() {
				ip, _, _ := net.ParseCIDR(a)
				addrs = append(addrs, dance.Ipv4AsUint32(ip))
			}

			if len(addrs) > 0 {
				// first gets the name straight, "name" with all addresses
				name := fmt.Sprintf("%s.%s", x.GetModel().Id, expnet)
				if defaultName == false {
					dns4[name] = &facility.DanceDns4Entry{Ipv4: addrs}
					defaultName = true
				}

				// each address gets its own name as well.
				for _, addr := range addrs {
					tag := strings.ReplaceAll(dance.Uint32AsIpv4(addr).String(), ".", "-")
					name = fmt.Sprintf("%s-%s.%s", x.GetModel().Id, tag, expnet)
					dns4[name] = &facility.DanceDns4Entry{Ipv4: []uint32{addr}}
				}
			}
		}
	}

	for _, x := range mz.Vms {

		defaultName := false
		for _, socket := range x.VmAlloc.GetModel().GetSockets() {

			addrs := []uint32{}
			for _, a := range socket.GetAddrs() {
				ip, _, _ := net.ParseCIDR(a)
				addrs = append(addrs, dance.Ipv4AsUint32(ip))
			}

			if len(addrs) > 0 {

				// first gets the name straight, "name". All addrs get "name-n" names.
				name := fmt.Sprintf("%s.%s", x.VmAlloc.Node, expnet)
				if defaultName == false {
					dns4[name] = &facility.DanceDns4Entry{Ipv4: addrs}
					defaultName = true
				}

				// each address gets its own name-AA-BB-CC-DD as well.
				for _, addr := range addrs {
					tag := strings.ReplaceAll(dance.Uint32AsIpv4(addr).String(), ".", "-")
					name = fmt.Sprintf("%s-%s.%s", x.VmAlloc.Node, tag, expnet)
					dns4[name] = &facility.DanceDns4Entry{Ipv4: []uint32{addr}}
				}
			}
		}
	}

	return nil
}
