package materialize

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/api/portal/v1/go"
	storlib "gitlab.com/mergetb/tech/shared/storage/etcd"

	"gitlab.com/mergetb/facility/mars/internal/storage"
)

func rebootMetal(
	mz *portal.Materialization,
	mzid string,
	txo *storage.Txo,
	x *portal.BareMetal,
	mode portal.RebootMaterializationMode,
) error {

	l := log.WithFields(log.Fields{
		"mzid": mzid,
		"node": x.Model.Id,
		"mode": mode,
	})
	l.Debug("handling metal reboot request")

	m, err := RetrieveHarborMetal(x.Resource)
	if err != nil {
		return fmt.Errorf("cannot get harbor metal state for resource %s: %+v", x.Resource, err)
	}
	bootAddr := m.Metal.InfranetAddr

	// Only stamp if requested explicitly
	var ms state.MachineState
	switch mode {
	case portal.RebootMaterializationMode_Reimage:
		ms = state.MachineState_Reimaged
	case portal.RebootMaterializationMode_Reboot:
		ms = state.MachineState_RestartedWarm
	default:
		ms = state.MachineState_Restarted
	}
	l.Debug("setting machine state to %s", ms.String())

	out, err := proto.Marshal(&state.Metal{
		Mzid:             mzid,
		Metal:            x,
		State:            ms,
		InfranetBootAddr: bootAddr,
	})
	if err != nil {
		return fmt.Errorf("marshal metal: %s/%s: %v", mzid, x.Resource, err)
	}

	txo.Put(fmt.Sprintf("/metal/%s", x.Resource), out)
	return nil
}

func rebootVM(
	mz *portal.Materialization,
	mzid string,
	txo *storage.Txo,
	x *portal.VirtualMachine,
	mode portal.RebootMaterializationMode,
) error {

	l := log.WithFields(log.Fields{
		"mzid": mzid,
		"node": x.VmAlloc.Node,
		"mode": mode,
	})
	l.Debug("handling VM reboot request")

	// Only stamp if requested explicitly
	var ms state.MachineState
	switch mode {
	case portal.RebootMaterializationMode_Reimage:
		ms = state.MachineState_Reimaged
	case portal.RebootMaterializationMode_Reboot:
		ms = state.MachineState_RestartedWarm
	default:
		ms = state.MachineState_Restarted
	}
	l.Debug("setting machine state to %s", ms.String())

	out, err := proto.Marshal(&state.Vm{
		Mzid:  mzid,
		Vm:    x,
		State: ms,
	})
	if err != nil {
		return fmt.Errorf("marshal VM: %s/%s: %v", mzid, x.VmAlloc.Resource, err)
	}

	txo.Put(fmt.Sprintf("/vm/%s/%s", x.VmAlloc.Resource, fqvm(mz, x.VmAlloc.Node)), out)
	return nil
}

func metalRebootOps(
	mz *portal.Materialization,
	mzid string,
	txo *storage.Txo,
	nodes []*portal.BareMetal,
	mode portal.RebootMaterializationMode,
) error {

	for _, node := range nodes {
		err := rebootMetal(mz, mzid, txo, node, mode)
		if err != nil {
			return err
		}
	}

	return nil
}

func vmRebootOps(
	mz *portal.Materialization,
	mzid string,
	txo *storage.Txo,
	nodes []*portal.VirtualMachine,
	mode portal.RebootMaterializationMode,
) error {

	for _, node := range nodes {
		err := rebootVM(mz, mzid, txo, node, mode)
		if err != nil {
			return err
		}
	}

	return nil
}

func RebootMaterialization(rid, eid, pid string, hostnames []string, mode portal.RebootMaterializationMode) error {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// look up the mtz
	l, err := storlib.NewLock(ctx, storage.EtcdClient, "all.mtz")
	if err != nil {
		return err
	}
	defer l.Unlock()

	mzid := fmt.Sprintf("%s.%s.%s", rid, eid, pid)

	mz, err := storage.FetchMaterialization(mzid)
	if err != nil {
		return err
	}

	// map hostnames to metal/vm nodes
	var metals []*portal.BareMetal
	var vms []*portal.VirtualMachine

	for _, host := range hostnames {
		for _, x := range mz.Metal {
			if x.Model.Id == host {
				metals = append(metals, x)
			}
		}

		for _, x := range mz.Vms {
			if x.VmAlloc.Node == host {
				vms = append(vms, x)
			}
		}
	}

	txo := new(storage.Txo)

	err = metalRebootOps(mz, mzid, txo, metals, mode)
	if err != nil {
		return err
	}

	err = vmRebootOps(mz, mzid, txo, vms, mode)
	if err != nil {
		return err
	}

	txo.DumpKeys()

	_, err = txo.Exec()
	if err != nil {
		return fmt.Errorf("etcd txn: %+v", err)
	}

	return nil
}
