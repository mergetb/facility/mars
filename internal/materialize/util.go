package materialize

import (
	"context"
	"fmt"
	"net"
	"strings"

	mapset "github.com/deckarep/golang-set/v2"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	rec "gitlab.com/mergetb/tech/reconcile"
	storlib "gitlab.com/mergetb/tech/shared/storage/etcd"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

const (
	harbor_mzid   string = "harbor.system.marstb"
	uefi_firmware string = "snponly.efi"
	bios_firmware string = "ipxe.pxe"
)

func is_harbor_mzid(mzid string) bool {
	return mzid == harbor_mzid
}

func mzid(mz *portal.Materialization) string {
	return fmt.Sprintf("%s.%s.%s", mz.Rid, mz.Eid, mz.Pid)
}

func tftpFilename(r *xir.Resource) string {

	fw := r.GetFirmware()
	if fw == nil {
		log.Warnf("no firmware specified for resource %s: defaulting to tftp filename %s", r.Id, uefi_firmware)
		return uefi_firmware
	}

	switch fw.GetKind() {
	case xir.Firmware_UEFI:
		return uefi_firmware
	case xir.Firmware_BIOS:
		return bios_firmware
	default:
		break
	}

	log.Warnf("undefined firmware specified for resource %s: defaulting to tftp filename %s", r.Id, uefi_firmware)
	return uefi_firmware
}

// Fully qualified vm name, a reversed mzid with the node tacked on the
// back. This is done in reverse so prefix lookups by mzid are possible.
func fqvm(mz *portal.Materialization, node string) string {
	return fmt.Sprintf("%s.%s.%s.%s", mz.Pid, mz.Eid, mz.Rid, node)
}

func lookupMarsIP4(name string) (net.IP, error) {
	ips, err := net.LookupIP(name)
	if err != nil {
		return nil, fmt.Errorf("lookup '%s': %v", name, err)
	}

	for _, ip := range ips {
		if ip.To4() != nil {
			return ip, nil
		}
	}

	return nil, fmt.Errorf("no IP4 address found for '%s'", name)
}

func RetrieveHarborMetal(id string) (*state.Metal, error) {
	return RetrievePrefixedMetal("harbor.metal", id)
}

func RetrieveMetal(id string) (*state.Metal, error) {
	return RetrievePrefixedMetal("metal", id)
}

func RetrievePrefixedMetal(prefix, id string) (*state.Metal, error) {

	prefix = storlib.SmartJoin("/", prefix)
	key := storlib.SmartJoin(prefix, id)

	kvc := clientv3.NewKV(storage.EtcdClient)

	resp, err := kvc.Get(
		context.TODO(),
		key,
	)
	if err != nil {
		return nil, fmt.Errorf("read %s: %v", key, err)
	}

	if len(resp.Kvs) == 0 {
		return nil, fmt.Errorf("no %s value", key)
	} else if len(resp.Kvs) != 1 {
		log.Warnf("bug: multiple %s values found", key)
	}

	m := new(state.Metal)
	err = proto.Unmarshal(resp.Kvs[0].Value, m)
	if err != nil {
		return nil, fmt.Errorf("unmarshal %s: %v", key, err)
	}

	return m, nil

}

func isHypervisor(m *portal.BareMetal) bool {
	return strings.Contains(strings.ToLower(m.Model.Image.Value), "hypervisor")
}

func createTxoGoalAndFilter(root_goal *rec.TaskGoal, key, name, desc string) (*storage.Txo, *rec.TaskGoal, mapset.Set[string]) {

	txo := new(storage.Txo)
	goal := rec.NewGoal(key, name, desc)
	root_goal.AddSubgoal(goal)

	return txo, goal, mapset.NewSet[string]()

}

func registerTasksAndGoal(goal *rec.TaskGoal, txo, root_txo *storage.Txo, filter mapset.Set[string]) {

	txo.RegisterKeys(goal, filter)

	// we need to manually write the timestamp because we're not using the storage library
	goal.WriteTimestamp()

	if b, err := proto.Marshal(goal); err == nil {
		txo.Put(goal.GetPrefixedKey(), b)
	} else {
		log.Errorf("registerTasksAndGoal: goal.Marshal(): %+v", err)
	}

	root_txo.Append(txo)

}
