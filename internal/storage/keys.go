package storage

import (
	"regexp"
)

var (
	namerx = "([a-zA-Z0-9_]+)"

	/* /infrapod/<mzid>/<host> */
	InfrapodKey = regexp.MustCompile(
		`^/infrapod/` + namerx + `\.` + namerx + `\.` + namerx + `/` + namerx)

	// TODO move other keys here
)
