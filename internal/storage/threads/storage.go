// The threads package provides storage for configuration of number of network emulation threads to use for each materialization
package threads

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"go.etcd.io/etcd/client/v3"
)

// Set configures the number of threads to use for a materialization
func Set(mzid string, n uint32) error {

	cli := clientv3.NewKV(storage.EtcdClient)
	_, err := cli.Put(context.TODO(), "/threads/"+mzid, fmt.Sprintf("%d", n))
	if err != nil {
		return fmt.Errorf("put thread config: %w", err)
	}
	return nil
}

// Unset clears the thread configuration for a materialization
func Unset(mzid string) error {
	cli := clientv3.NewKV(storage.EtcdClient)
	_, err := cli.Delete(context.TODO(), "/threads/"+mzid)
	if err != nil {
		return fmt.Errorf("put thread config: %w", err)
	}
	return nil
}

type ThreadConfig struct {
	Mzid    string
	Threads int32
}

// Returns the mzid associated with the etcd key
// of the form: /threads/rid.eid.pid
// returns an empty string on error
func mzidFromKey(key string) string {
	if !strings.HasPrefix(key, "/threads/") {
		return ""
	}
	return key[9:]
}

// List returns the thread configuration
func List() ([]*ThreadConfig, error) {

	cli := clientv3.NewKV(storage.EtcdClient)
	v, err := cli.Get(context.TODO(), "/threads/", clientv3.WithPrefix())
	if err != nil {
		return nil, fmt.Errorf("get thread config: %w", err)
	}
	ret := make([]*ThreadConfig, len(v.Kvs))
	for i, k := range v.Kvs {
		mzid := mzidFromKey(string(k.Key))
		if mzid == "" {
			return nil, fmt.Errorf("unable to parse etcd key: %s", k.Key)
		}
		n, err := strconv.Atoi(string(k.Value))
		if err != nil {
			return nil, fmt.Errorf("convert to int: %w", err)
		}
		ret[i] = &ThreadConfig{Mzid: mzid, Threads: int32(n)}
	}
	return ret, nil
}

// Get returns the number of threads configured for a materialization
func Get(mzid string) (uint32, error) {
	cli := clientv3.NewKV(storage.EtcdClient)
	v, err := cli.Get(context.TODO(), "/threads/"+mzid)
	if err != nil {
		return 0, fmt.Errorf("get thread config: %w", err)
	}
	if len(v.Kvs) == 0 {
		return 0, nil // not found
	}
	n, err := strconv.Atoi(string(v.Kvs[0].Value))
	if err != nil {
		return 0, fmt.Errorf("convert to int: %w", err)
	}
	return uint32(n), nil
}
