package storage

import (
	"path"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/reconcile"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

func SledStatus(hosts []string) (map[string]*reconcile.TaskStatus, error) {

	ops := new(storage.EtcdTransaction)
	for _, h := range hosts {
		ts := reconcile.NewTaskStatusRaw("sled", "harbor.system.marstb", path.Join("/metal/", h), "")
		err := ts.AppendReadOps(EtcdClient, ops)

		if err != nil {
			return nil, err
		}
	}

	txr, _, err := ops.EtcdTx(EtcdClient, "")
	if err != nil {
		return nil, err
	}

	s_m, _, _, err := reconcile.ReadAllStatusInfoFromResponse(txr)
	if err != nil {
		return nil, err
	}

	ans := make(map[string]*reconcile.TaskStatus)

	for k, tses := range s_m {

		for _, ts := range tses {
			parts := strings.Split(k, "/")
			if len(parts) != 3 {
				log.Errorf(
					"unexpected sled status key format %s, skiping", k)
				continue
			}
			device := parts[2]

			ans[device] = ts
		}

	}

	return ans, nil

}
