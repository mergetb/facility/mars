package storage

import (
	"context"
	"fmt"

	"go.etcd.io/etcd/client/v3"
)

func SetConfig(buf []byte) error {
	kvc := clientv3.NewKV(EtcdClient)
	_, err := kvc.Put(context.TODO(), "/config", string(buf))
	if err != nil {
		return fmt.Errorf("etcd: put config: %v", err)
	}

	return nil
}

func GetConfig() ([]byte, error) {
	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), "/config")
	if err != nil {
		return nil, fmt.Errorf("etcd: get config: %v", err)
	}

	if len(resp.Kvs) == 0 {
		return nil, nil
	} else if len(resp.Kvs) != 1 {
		return nil, fmt.Errorf("error in configuration storage")
	}

	return resp.Kvs[0].Value, nil
}
