package storage

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	_ "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/xir/v0.3/go"
)

func readMetalStates(nodeids []string) (map[string]*state.Metal, error) {
	txo := new(Txo)

	// read machine states
	for _, nodeid := range nodeids {
		txo.Get(fmt.Sprintf("/metal/%s", nodeid))
	}

	txr, err := txo.Exec()
	if err != nil {
		return nil, fmt.Errorf("read txn failed: %v", err)
	}

	metalmap := make(map[string]*state.Metal)
	for _, r := range txr.Responses {
		rr := r.GetResponseRange()
		if rr == nil {
			continue
		}

		for _, kv := range rr.Kvs {

			m := new(state.Metal)
			err := proto.Unmarshal(kv.Value, m)
			if err != nil {
				return nil, fmt.Errorf("unmarshal metal at %s", err)
			}
			metalmap[m.Metal.Resource] = m
		}
	}

	return metalmap, nil
}

func SetMachinePowerState(states map[string]state.MachineState) error {
	nodeids := []string{}
	for nodeid := range states {
		nodeids = append(nodeids, nodeid)
	}

	metalmap, err := readMetalStates(nodeids)
	if err != nil {
		return err
	}

	// assign machine states
	for nodeid, state := range states {
		metal := metalmap[nodeid]
		if metal == nil {
			log.Warnf("powerstate: machine not found %s", nodeid)
			continue
		}
		metal.State = state
	}

	// write machines states
	txo := new(Txo)

	for nodeid, metal := range metalmap {
		buf, err := proto.Marshal(metal)
		if err != nil {
			return fmt.Errorf("marshal metal at %s", metal.Metal.Resource)
		}
		txo.Put(fmt.Sprintf("/metal/%s", nodeid), buf)
	}
	_, err = txo.Exec()
	if err != nil {
		return fmt.Errorf("write txn failed: %v", err)
	}

	return nil
}

func SetMachineImage(images map[string]*state.MachineImage) error {

	nodeids := []string{}
	for nodeid := range images {
		nodeids = append(nodeids, nodeid)
	}

	metalmap, err := readMetalStates(nodeids)
	if err != nil {
		return err
	}

	// assign machine images
	for nodeid, image := range images {
		metal := metalmap[nodeid]
		if metal == nil {
			log.Warnf("powerstate: machine not found %s", nodeid)
			continue
		}

		// set image
		if metal.Metal.Model == nil {
			metal.Metal.Model = new(xir.Node)
		}

		metal.Metal.Model.Image = &xir.StringConstraint{
			Op:    xir.Operator_EQ,
			Value: image.Image,
		}

		// set state to reimaged
		metal.State = state.MachineState_Reimaged
	}

	// write machines states
	txo := new(Txo)

	for nodeid, metal := range metalmap {
		buf, err := proto.Marshal(metal)
		if err != nil {
			return fmt.Errorf("marshal metal at %s", metal.Metal.Resource)
		}
		txo.Put(fmt.Sprintf("/metal/%s", nodeid), buf)
	}
	_, err = txo.Exec()
	if err != nil {
		return fmt.Errorf("write txn failed: %v", err)
	}

	return nil
}
