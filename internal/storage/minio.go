package storage

import (
	"os"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/minio/minio/pkg/madmin"
)

var MinIOClient *minio.Client
var MinIOAdminClient *madmin.AdminClient

func InitMarsMinIOClient() error {
	return InitMarsMinIOClientCreds(os.Getenv("MINIO_ROOT_USER"), os.Getenv("MINIO_ROOT_PASSWORD"))
}

func InitMarsMinIOClientCreds(username, password string) error {
	var err error

	MinIOClient, err = minio.New("minio:9000", &minio.Options{
		Creds: credentials.NewStaticV4(
			username,
			password,
			"",
		),
		//TODO Secure: true, // implies ssl
	})

	if err != nil {
		return err
	}

	//TODO use ssl
	ssl := false

	MinIOAdminClient, err = madmin.New(
		"minio:9000",
		username,
		password,
		ssl,
	)

	return err
}
