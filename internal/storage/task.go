package storage

import (
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/reconcile"
	storlib "gitlab.com/mergetb/tech/shared/storage/etcd"
	"google.golang.org/protobuf/proto"
)

func ReadTaskTree(k string) (*reconcile.TaskTree, error) {

	g := storlib.NewGenericEtcdKey(reconcile.JoinStatusRoot(k), nil)

	err := g.Read(EtcdClient)
	if err != nil {
		return nil, err
	}

	ts := new(reconcile.TaskStatus)
	err = proto.Unmarshal(g.EtcdValue, ts)
	if err != nil {
		return nil, err
	}

	tt, err := ts.GetTaskTree(EtcdClient, nil)
	if err != nil {
		return nil, err
	}

	return tt, nil

}

func ReadTaskForest(k string, timeout time.Duration) (*reconcile.TaskForest, error) {

	tg := reconcile.NewGenericGoal(k)

	err := tg.Read(EtcdClient)
	if err != nil {
		return nil, err
	}

	logrus.Debugf("reading timeout: %s", timeout.String())

	tf, err := tg.WatchTaskForest(EtcdClient, timeout)
	if err != nil {
		return nil, err
	}

	return tf, nil

}
