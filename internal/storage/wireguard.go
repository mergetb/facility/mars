package storage

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	rec "gitlab.com/mergetb/tech/reconcile"
)

var (
	wgifreqkey = "/wg/ifreq" // requests from the portal to establish WG ifx
	wgpeerkey  = "/wg/peer"  // requests from the portal to create peers on ifx
	wgifkey    = "/wg/if"    // state that has been created locally and communicated to the portal
)

// return a key suitable for watching for wg ifreq events.
func WgIfReqKey() string {
	return wgifreqkey + "/"
}

// return a key suitable for watching for wg peer events.
func WgPeerKey() string {
	return wgpeerkey + "/"
}

// return a key suitable for watching wg if events.
func WgIfKey() string {
	return wgifkey + "/"
}

func GetWireguardGoal(enclaveid string) (*rec.TaskGoal, error) {
	wg_goal := rec.NewGenericGoal(enclaveid + "/wireguardOps")
	err := wg_goal.Read(EtcdClient)
	if err != nil {
		return nil, err
	}

	return wg_goal, nil
}

func AddWgPeers(host string, rq *facility.AddWgPeersRequest) error {

	kvc := clientv3.NewKV(EtcdClient)

	wg_goal, wg_err := GetWireguardGoal(rq.Enclaveid)
	if wg_err != nil {
		log.Errorf("etcd: ensure wireguard goal, not updating: %v", wg_err)
	}

	for _, p := range rq.Configs {

		buf, err := proto.Marshal(p)
		if err != nil {
			return fmt.Errorf("bad wg peer config format: %s", err)
		}

		k := wgpeerkey + "/" + rq.Enclaveid + "/" + host + "/" + p.Key

		_, err = kvc.Put(
			context.TODO(),
			k,
			string(buf),
		)
		if err != nil {
			return fmt.Errorf("etcd: put wireguard peer request: %v", err)
		}

		if wg_err == nil {
			wg_goal.AddTaskRecord(&rec.TaskRecord{
				Task: k,
			})
		}
	}

	if wg_err == nil {
		_, err := wg_goal.Update(EtcdClient)
		if err != nil {
			log.Errorf("wg_goal.Update: %v", err)
		}
	}

	return nil
}

func DelWgPeer(host string, rq *facility.DelWgPeerRequest) error {

	k := wgpeerkey + "/" + rq.Enclaveid + "/" + host + "/" + rq.Config.Key

	kvc := clientv3.NewKV(EtcdClient)
	_, err := kvc.Delete(
		context.TODO(),
		k,
	)
	if err != nil {
		return fmt.Errorf("etcd: delete wireguard peer request: %v", err)
	}

	// No goal likely means a demtz has happened; Debug not Error
	wg_goal, wg_err := GetWireguardGoal(rq.Enclaveid)
	if wg_err != nil {
		log.Debugf("etcd: ensure wireguard goal, not updating: %v", wg_err)
		return nil
	}

	if wg_goal.RemoveSubtask(&rec.TaskRecord{Task: k}) {
		_, err := wg_goal.Update(EtcdClient)
		if err != nil {
			log.Errorf("wg_goal.Update: %v", err)
		}
	}

	return nil
}

func GetWgPeers(enclaveid string) ([]*portal.WgIfConfig, error) {

	k := fmt.Sprintf("%s/%s/", wgpeerkey, enclaveid)

	kvc := clientv3.NewKV(EtcdClient)
	kvs, err := kvc.Get(
		context.TODO(),
		k,
		clientv3.WithPrefix(),
	)
	if err != nil {
		return nil, fmt.Errorf("etcd: kvc get %s: %v", k, err)
	}

	peers := []*portal.WgIfConfig{}
	for _, kv := range kvs.Kvs {
		resp := new(portal.WgIfConfig)
		err = proto.Unmarshal(kv.Value, resp)
		if err != nil {
			return nil, fmt.Errorf("bad wg peer format: %v", err)
		}

		peers = append(peers, resp)
	}

	return peers, nil
}

func ClearWgPeers(enclaveid string) error {

	log.Debugf("Clearing wireguard peers for %s", enclaveid)

	k := fmt.Sprintf("%s/%s/", wgpeerkey, enclaveid)

	kvc := clientv3.NewKV(EtcdClient)
	txr, err := kvc.Delete(
		context.TODO(),
		k,
		clientv3.WithPrefix(),
		clientv3.WithPrevKV(),
	)
	if err != nil {
		return fmt.Errorf("etcd: kvc delete %s: %v", k, err)
	}

	wg_goal, wg_err := GetWireguardGoal(enclaveid)
	if wg_err != nil {
		log.Errorf("etcd: ensure wireguard goal, not updating: %v", wg_err)
		return nil
	}

	for _, kv := range txr.PrevKvs {
		wg_goal.RemoveSubtask(&rec.TaskRecord{Task: string(kv.Key)})

	}

	_, err = wg_goal.Update(EtcdClient)
	if err != nil {
		log.Errorf("wg_goal.Update: %v", err)
	}

	return nil
}

func CreateWgInterface(host string, rq *facility.CreateWgInterfaceRequest) error {

	buf, err := proto.Marshal(rq)
	if err != nil {
		return fmt.Errorf("marshal wireguard if request: %v", err)
	}

	k := wgifreqkey + "/" + rq.Enclaveid + "/" + host

	kvc := clientv3.NewKV(EtcdClient)
	_, err = kvc.Put(
		context.TODO(),
		k,
		string(buf),
	)
	if err != nil {
		return fmt.Errorf("etcd: create wireguard interfaces request: %v", err)
	}

	wg_goal, wg_err := GetWireguardGoal(rq.Enclaveid)
	if wg_err != nil {
		log.Errorf("etcd: ensure wireguard goal, not updating: %v", wg_err)
		return nil
	}

	wg_goal.AddTaskRecord(&rec.TaskRecord{
		Task: k,
	})
	_, err = wg_goal.Update(EtcdClient)
	if err != nil {
		log.Errorf("wg_goal.Update: %v", err)
	}

	return nil
}

func DeleteWgInterface(host string, rq *facility.DeleteWgInterfaceRequest) error {

	k := wgifreqkey + "/" + rq.Enclaveid + "/" + host

	kvc := clientv3.NewKV(EtcdClient)
	_, err := kvc.Delete(
		context.TODO(),
		k,
	)
	if err != nil {
		return fmt.Errorf("etcd: delete wireguard interfaces request: %v", err)
	}

	// No goal likely means a demtz has happened; Debug not Error
	wg_goal, wg_err := GetWireguardGoal(rq.Enclaveid)
	if wg_err != nil {
		log.Debugf("etcd: ensure wireguard goal, not updating: %v", wg_err)
		return nil
	}

	if wg_goal.RemoveSubtask(&rec.TaskRecord{Task: k}) {
		_, err := wg_goal.Update(EtcdClient)
		if err != nil {
			log.Errorf("wg_goal.Update: %v", err)
		}
	}

	return nil
}

func StoreWgIf(host, enclaveid string, wgif *facility.GetWgInterfaceResponse) error {

	buf, err := proto.Marshal(wgif)
	if err != nil {
		return fmt.Errorf("marshal wireguard if: %v", err)
	}

	k := wgifkey + "/" + enclaveid + "/" + host

	kvc := clientv3.NewKV(EtcdClient)
	_, err = kvc.Put(
		context.TODO(),
		k,
		string(buf),
	)
	if err != nil {
		return fmt.Errorf("etcd: store wireguard interface: %v", err)
	}

	wg_goal, wg_err := GetWireguardGoal(enclaveid)
	if wg_err != nil {
		log.Errorf("etcd: ensure wireguard goal, not updating: %v", wg_err)
		return nil
	}

	wg_goal.AddTaskRecord(&rec.TaskRecord{Task: k})
	_, err = wg_goal.Update(EtcdClient)
	if err != nil {
		log.Errorf("wg_goal.Update: %v", err)
	}

	return nil

}

func GetWgInterface(host, enclaveid string) (*facility.GetWgInterfaceResponse, error) {

	kvc := clientv3.NewKV(EtcdClient)
	kv, err := kvc.Get(
		context.TODO(),
		wgifkey+"/"+enclaveid+"/"+host,
	)
	if err != nil {
		return nil, fmt.Errorf("etcd: get: %s", err)
	}

	if len(kv.Kvs) == 0 {
		return nil, fmt.Errorf("interface does not exist")
	}

	if len(kv.Kvs) != 1 {
		return nil, fmt.Errorf("internal error: too many interfaces")
	}

	resp := new(facility.GetWgInterfaceResponse)
	err = proto.Unmarshal(kv.Kvs[0].Value, resp)
	if err != nil {
		return nil, fmt.Errorf("bad wg interface format: %s", err)
	}

	return resp, nil
}

func ClearWgState(enclaveid string) error {

	log.Debugf("Clearing wireguard state for %s", enclaveid)

	// No goal likely means a demtz has happened; Debug not Error
	wg_goal, wg_err := GetWireguardGoal(enclaveid)
	if wg_err != nil {
		log.Debugf("etcd: ensure wireguard goal, not updating: %v", wg_err)
	}

	keys := []string{wgifreqkey, wgpeerkey, wgifkey}
	kvc := clientv3.NewKV(EtcdClient)

	for _, k := range keys {

		k = fmt.Sprintf("%s/%s/", k, enclaveid)

		txr, err := kvc.Delete(
			context.TODO(),
			k,
			clientv3.WithPrefix(),
			clientv3.WithPrevKV(),
		)
		if err != nil {
			return fmt.Errorf("etcd: delete key %s: %v", k, err)
		}

		if wg_err == nil {
			for _, kv := range txr.PrevKvs {
				wg_goal.RemoveSubtask(&rec.TaskRecord{Task: string(kv.Key)})
			}
		}
	}

	if wg_err == nil {
		_, err := wg_goal.Update(EtcdClient)
		if err != nil {
			log.Errorf("wg_goal.Update: %v", err)
		}
	}

	return nil
}

func ClearWgIf(enclaveid string) error {

	log.Debugf("Clearing wireguard if for %s", enclaveid)

	k := fmt.Sprintf("%s/%s/", wgifkey, enclaveid)

	kvc := clientv3.NewKV(EtcdClient)
	txr, err := kvc.Delete(
		context.TODO(),
		k,
		clientv3.WithPrefix(),
		clientv3.WithPrevKV(),
	)
	if err != nil {
		log.Errorf("etcd: kvc delete %s: %v", k, err)
		return err
	}

	// No goal likely means a demtz has happened; Debug not Error
	wg_goal, wg_err := GetWireguardGoal(enclaveid)
	if wg_err != nil {
		log.Debugf("etcd: ensure wireguard goal, not updating: %v", wg_err)
		return nil
	}

	for _, kv := range txr.PrevKvs {
		wg_goal.RemoveSubtask(&rec.TaskRecord{Task: string(kv.Key)})
	}

	_, err = wg_goal.Update(EtcdClient)
	if err != nil {
		log.Errorf("wg_goal.Update: %v", err)
	}

	return nil
}

func GetCreateWgInterfaceRequest(host, enclaveid string) (*facility.CreateWgInterfaceRequest, error) {

	kvc := clientv3.NewKV(EtcdClient)
	kv, err := kvc.Get(
		context.TODO(),
		wgifreqkey+"/"+enclaveid+"/"+host,
	)
	if err != nil {
		return nil, fmt.Errorf("etcd: get: %s", err)
	}

	if len(kv.Kvs) == 0 {
		return nil, fmt.Errorf("interface does not exist")
	} else if len(kv.Kvs) > 1 {
		log.Warnf("found %d interface requests for enclave %s on host %s (expected 1)", len(kv.Kvs), enclaveid, host)
	}

	req := new(facility.CreateWgInterfaceRequest)
	err = proto.Unmarshal(kv.Kvs[0].Value, req)
	if err != nil {
		return nil, fmt.Errorf("bad wg interface format: %s", err)
	}

	return req, nil

}
