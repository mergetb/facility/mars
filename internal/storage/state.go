package storage

import (
	"context"
	"fmt"
	"regexp"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
	portal "gitlab.com/mergetb/api/portal/v1/go"

	rec "gitlab.com/mergetb/tech/reconcile"
)

var MetalKey = "/metal/"

func ListMetal(all bool) ([]*state.Metal, error) {

	kvc := clientv3.NewKV(EtcdClient)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kvc.Get(ctx, MetalKey, clientv3.WithPrefix())
	cancel()
	if err != nil {
		log.Errorf("etcd get: '%s': %v", MetalKey, err)
		return nil, fmt.Errorf("db error")
	}

	var result []*state.Metal

	for _, kv := range resp.Kvs {

		m := new(state.Metal)
		err = proto.Unmarshal(kv.Value, m)
		if err != nil {
			log.Errorf("unmarshal metal: %s %s", string(kv.Key), err)
			return nil, fmt.Errorf("data error")
		}
		result = append(result, m)

	}

	return result, nil

}

func ListIngresses() ([]*facility.Ingress, error) {
	kvc := clientv3.NewKV(EtcdClient)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kvc.Get(ctx, "/ingress/", clientv3.WithPrefix())
	cancel()

	if err != nil {
		log.Errorf("etcd get: '/ingress': %v", err)
		return nil, fmt.Errorf("db error")
	}

	var result []*facility.Ingress

	for _, kv := range resp.Kvs {
		ing := new(facility.Ingress)
		err = proto.Unmarshal(kv.Value, ing)
		if err != nil {
			return nil, fmt.Errorf("ingress unmarshal %s: %v", string(kv.Key), err)
		}

		result = append(result, ing)
	}

	return result, nil
}

func ListVM(all bool) ([]*state.Vm, error) {

	kvc := clientv3.NewKV(EtcdClient)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kvc.Get(ctx, "/vm/", clientv3.WithPrefix())
	cancel()
	if err != nil {
		log.Errorf("etcd get: '/vm': %v", err)
		return nil, fmt.Errorf("db error")
	}

	var result []*state.Vm

	for _, kv := range resp.Kvs {

		m := new(state.Vm)
		err = proto.Unmarshal(kv.Value, m)
		if err != nil {
			log.Errorf("unmarshal vm: %s %s", string(kv.Key), err)
			return nil, fmt.Errorf("data error")
		}
		result = append(result, m)

	}

	return result, nil

}

func ListInfrapods() ([]*facility.InfrapodStatus, error) {

	kvc := clientv3.NewKV(EtcdClient)

	// TODO wrap these two up into a single transaction
	// TODO export the keys so they are not hard coded across two repos, import here

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kvc.Get(ctx, "/infrapod/", clientv3.WithPrefix())
	cancel()
	if err != nil {
		log.Errorf("etcd get: '/infrapod: %v", err)
		return nil, fmt.Errorf("db error")
	}

	statusPath := rec.JoinStatusRoot("infrapod") + "/"

	ctx, cancel = context.WithTimeout(context.Background(), time.Second*10)
	sresp, err := kvc.Get(ctx, statusPath, clientv3.WithPrefix())
	cancel()
	if err != nil {
		log.Errorf("etcd get: '%s: %v", statusPath, err)
		return nil, fmt.Errorf("db error")
	}

	// creating a temporary struct to make sure each status was mapped/found
	type FoundStatus struct {
		Status *rec.TaskStatus
		Found  bool
	}

	var statuses = make(map[string]*FoundStatus, 0)
	for _, kv := range sresp.Kvs {

		status := new(rec.TaskStatus)

		err = proto.Unmarshal(kv.Value, status)
		if err != nil {
			log.Errorf("unmarshal infrapod status: %s %s", string(kv.Key), err)
			return nil, fmt.Errorf("data error")
		}

		key := string(kv.Key)

		_, ok := statuses[key]
		if ok {
			log.Errorf("Key already in map, big problemo: %s", kv.Key)
			return nil, fmt.Errorf("etcd key issue: %+v", kv)
		}
		statuses[key] = &FoundStatus{Status: status}
	}

	var results []*facility.InfrapodStatus

	// now for each status key, find the corresponding infrapod config, but do so from
	// the etcd txn get. this will help us find mismatches
	for _, kv := range resp.Kvs {

		state := new(state.InfrapodConfig)
		err = proto.Unmarshal(kv.Value, state)
		if err != nil {
			log.Errorf("unmarshal infrapod: %s %s", string(kv.Key), err)
			return nil, fmt.Errorf("data error")
		}

		// now we check if there is a corresponding status
		val, ok := statuses[state.Mzid]
		if ok {
			results = append(results, &facility.InfrapodStatus{
				State:  state,
				Status: val.Status,
			})
			// mark found, so if at the end our map and slice are not the same size
			// we can easily pinpoint which key is the offender
			val.Found = true
		} else {
			// here we immediately know that we've found a config without a status
			log.Errorf("found dangling infrapod config without status: %s", kv.Key)
		}
	}

	if len(statuses) != len(results) {
		// and here is the other case where there is a status without config
		log.Errorf("found dangling infrapod status without config.")
		for k, v := range statuses {
			if !v.Found {
				log.Errorf("Offending status: %s", k)
			}
		}
	}

	return results, nil

}

func ListMz() (map[string]*portal.MzParameters, error) {

	kvc := clientv3.NewKV(EtcdClient)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kvc.Get(ctx, "/mzparam/", clientv3.WithPrefix())
	cancel()
	if err != nil {
		log.Errorf("etcd get: '/mzparam': %v", err)
		return nil, fmt.Errorf("db error")
	}

	result := make(map[string]*portal.MzParameters)

	for _, kv := range resp.Kvs {

		p := new(portal.MzParameters)
		err = proto.Unmarshal(kv.Value, p)
		if err != nil {
			log.Errorf("unmarshal mz info: %s %s", string(kv.Key), err)
			return nil, fmt.Errorf("data error")
		}

		keyp := strings.Split(string(kv.Key), "/")
		if len(keyp) != 3 {
			log.Errorf("invalid mz info key: %s", string(kv.Key))
			return nil, fmt.Errorf("data error")
		}
		result[keyp[2]] = p

	}

	return result, nil

}

// Query a tap device spec by hostname and virtual device name
func GetTap(hostname, virt_dev string) (*state.Tap, error) {

	key := fmt.Sprintf("/net/%s/tap/%s", hostname, virt_dev)

	log.Debugf("querying etcd key %s", key)

	kvc := clientv3.NewKV(EtcdClient)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kvc.Get(ctx, key)
	cancel()

	if err != nil {
		return nil, fmt.Errorf("etcd get: '%s': %v", key, err)
	}

	if len(resp.Kvs) != 1 {
		return nil, fmt.Errorf("etcd get: '%s': %v responses, expect 1", key, resp.Kvs)
	}

	kv := resp.Kvs[0]
	t := new(state.Tap)
	err = proto.Unmarshal(kv.Value, t)
	if err != nil {
		return nil, fmt.Errorf("unmarshal '%s': %v", key, err)
	}

	return t, nil

}

type NetElementType string

const (
	PhysicalInterface   NetElementType = "phy"
	VxlanTunnelEndpoint                = "vtep"
	VlanSubinterface                   = "vlan"
	AccessPort                         = "access"
	TapPort                            = "tap"
	TrunkPort                          = "trunk"
	VxlanTunnelWaypoint                = "vtepw"
	BgpPeer                            = "peer"
	Routes                             = "routes"
	VrfConfig                          = "vrf"
	PrefixList                         = "prefix-list"
	RouteMap                           = "route-map"
)

type NetKey struct {
	Host string
	Type NetElementType
	Name string
}

func NetKeyFromString(s string) *NetKey {

	r := regexp.MustCompile(
		`^/net/([a-zA-Z][a-zA-Z0-9-_]*)/([a-zA-Z][a-zA-Z0-9-_]*)/([a-zA-Z][a-zA-Z0-9-_.]*)$`,
	)
	sm := r.FindStringSubmatch(s)

	if len(sm) != 4 {
		return nil
	}

	host := sm[1]
	kind := NetElementType(sm[2])
	name := sm[3]

	switch kind {
	case PhysicalInterface:
		return &NetKey{host, PhysicalInterface, name}

	case VxlanTunnelEndpoint:
		return &NetKey{host, VxlanTunnelEndpoint, name}

	case VlanSubinterface:
		return &NetKey{host, VlanSubinterface, name}

	case AccessPort:
		return &NetKey{host, AccessPort, name}

	case TapPort:
		return &NetKey{host, TapPort, name}

	case TrunkPort:
		return &NetKey{host, TrunkPort, name}

	case VxlanTunnelWaypoint:
		return &NetKey{host, VxlanTunnelWaypoint, name}

	case BgpPeer:
		return &NetKey{host, BgpPeer, name}

	case Routes:
		return &NetKey{host, Routes, name}

	case VrfConfig:
		return &NetKey{host, VrfConfig, name}

	case PrefixList:
		return &NetKey{host, PrefixList, name}

	case RouteMap:
		return &NetKey{host, RouteMap, name}
	}

	return nil

}

type NetElements struct {
	Phys   []*state.Phy
	Vlans  []*state.Vlan
	Vteps  []*state.Vtep
	Access []*state.Access
	Taps   []*state.Tap
	Trunks []*state.Trunk
	Peers  []*state.Peer
	// Routes []*state.InfranetRoutes
	// /* XXX Vrfs        []*state.VrfConfig */
	// PrefixLists []*state.PrefixList
	// RouteMaps   []*state.RouteMap
}

func ListNet() (*NetElements, error) {

	kvc := clientv3.NewKV(EtcdClient)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kvc.Get(ctx, "/net/", clientv3.WithPrefix())
	cancel()
	if err != nil {
		log.Errorf("etcd get: '/net': %v", err)
		return nil, fmt.Errorf("db error")
	}

	nes := new(NetElements)

	for _, kv := range resp.Kvs {

		nk := NetKeyFromString(string(kv.Key))
		if nk == nil {
			continue
		}
		switch nk.Type {

		case PhysicalInterface:
			x := new(state.Phy)
			err = proto.Unmarshal(kv.Value, x)
			if err != nil {
				log.Errorf("unmarshal %s %s", string(kv.Key), err)
			}
			nes.Phys = append(nes.Phys, x)

		case VxlanTunnelEndpoint:
			x := new(state.Vtep)
			err = proto.Unmarshal(kv.Value, x)
			if err != nil {
				log.Errorf("unmarshal %s %s", string(kv.Key), err)
			}
			nes.Vteps = append(nes.Vteps, x)

		case VlanSubinterface:
			x := new(state.Vlan)
			err = proto.Unmarshal(kv.Value, x)
			if err != nil {
				log.Errorf("unmarshal %s %s", string(kv.Key), err)
			}
			nes.Vlans = append(nes.Vlans, x)

		case AccessPort:
			x := new(state.Access)
			err = proto.Unmarshal(kv.Value, x)
			if err != nil {
				log.Errorf("unmarshal %s %s", string(kv.Key), err)
			}
			nes.Access = append(nes.Access, x)

		case TapPort:
			x := new(state.Tap)
			err = proto.Unmarshal(kv.Value, x)
			if err != nil {
				log.Errorf("unmarshal %s %s", string(kv.Key), err)
			}
			nes.Taps = append(nes.Taps, x)

		case TrunkPort:
			x := new(state.Trunk)
			err = proto.Unmarshal(kv.Value, x)
			if err != nil {
				log.Errorf("unmarshal %s %s", string(kv.Key), err)
			}
			nes.Trunks = append(nes.Trunks, x)

		case VxlanTunnelWaypoint:
			x := new(state.Vtep)
			err = proto.Unmarshal(kv.Value, x)
			if err != nil {
				log.Errorf("unmarshal %s %s", string(kv.Key), err)
			}
			nes.Vteps = append(nes.Vteps, x)

		case BgpPeer:
			x := new(state.Peer)
			err = proto.Unmarshal(kv.Value, x)
			if err != nil {
				log.Errorf("unmarshal %s %s", string(kv.Key), err)
			}
			nes.Peers = append(nes.Peers, x)

			// case Routes:
			// 	x := new(state.InfranetRoutes)
			// 	err = proto.Unmarshal(kv.Value, x)
			// 	if err != nil {
			// 		log.Errorf("unmarshal %s %s", string(kv.Key), err)
			// 	}
			// 	nes.Routes = append(nes.Routes, x)

			// 	/* XXX
			// 	case VrfConfig:
			// 		x := new(state.VrfConfig)
			// 		err = proto.Unmarshal(kv.Value, x)
			// 		if err != nil {
			// 			log.Errorf("unmarshal %s %s", string(kv.Key), err)
			// 		}
			// 		nes.Vrfs = append(nes.Vrfs, x)
			// 	*/

			// case PrefixList:
			// 	x := new(state.PrefixList)
			// 	err = proto.Unmarshal(kv.Value, x)
			// 	if err != nil {
			// 		log.Errorf("unmarshal %s %s", string(kv.Key), err)
			// 	}
			// 	nes.PrefixLists = append(nes.PrefixLists, x)

			// case RouteMap:
			// 	x := new(state.RouteMap)
			// 	err = proto.Unmarshal(kv.Value, x)
			// 	if err != nil {
			// 		log.Errorf("unmarshal %s %s", string(kv.Key), err)
			// 	}
			// 	nes.RouteMaps = append(nes.RouteMaps, x)

		}

	}

	return nes, nil

}

func ClearReconcilerState(keys []string) error {

	var ops []clientv3.Op

	for _, k := range keys {
		ops = append(ops,
			clientv3.OpDelete(rec.JoinStatusRoot(k)),
		)
	}

	log.Infof("CLEARING %+v", keys)

	kvc := clientv3.NewKV(EtcdClient)

	resp, err := kvc.Txn(context.TODO()).If().Then(ops...).Commit()
	if err != nil {
		return fmt.Errorf("etcd txn: %v", err)
	}
	if !resp.Succeeded {
		return fmt.Errorf("etcd transaction failed")
	}

	return nil

}
