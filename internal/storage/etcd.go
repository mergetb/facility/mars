package storage

import (
	//"crypto/tls"
	//"crypto/x509"
	"context"
	"fmt"

	//"io/ioutil"
	"time"

	mapset "github.com/deckarep/golang-set/v2"
	log "github.com/sirupsen/logrus"
	rec "gitlab.com/mergetb/tech/reconcile"
	"go.etcd.io/etcd/client/v3"
)

var (
	EtcdClient *clientv3.Client
	EtcdHost   = "etcd"
)

func InitMarsEtcdClient() error {

	/* TODO
	capool := x509.NewCertPool()
	capem, err := ioutil.ReadFile("/dbcerts/ca.pem")
	if err != nil {
		return fmt.Errorf("failed to read cacert: %v", err)
	}
	ok := capool.AppendCertsFromPEM(capem)
	if !ok {
		return fmt.Errorf("capem is not ok")
	}

	cert, err := tls.LoadX509KeyPair(
		"/dbcerts/etcd.pem",
		"/dbcerts/etcd-key.pem",
	)
	if err != nil {
		return fmt.Errorf("failed to load cert/key pair: %v", err)
	}

	tlsConfig := &tls.Config{
		RootCAs:      capool,
		Certificates: []tls.Certificate{cert},
	}
	*/

	var err error

	connstr := fmt.Sprintf("%s:%d", EtcdHost, 2379)
	cliconf := clientv3.Config{
		Endpoints:            []string{connstr},
		DialTimeout:          2 * time.Second,
		DialKeepAliveTime:    10 * time.Minute,
		DialKeepAliveTimeout: 10 * time.Second,
		MaxCallSendMsgSize:   MaxMessageSize,
		MaxCallRecvMsgSize:   MaxMessageSize,
		//TLS:         tlsConfig,
	}

	EtcdClient, err = clientv3.New(cliconf)

	return err

}

// TODO: Depreciate all of these with the new storage library

type Txo struct {
	Ifs []clientv3.Cmp
	Ops []clientv3.Op
}

func (txo *Txo) Get(key string, opts ...clientv3.OpOption) {

	txo.Ops = append(txo.Ops, clientv3.OpGet(key, opts...))

}

func (txo *Txo) Put(key string, data []byte, opts ...clientv3.OpOption) {

	txo.Ops = append(txo.Ops, clientv3.OpPut(key, string(data), opts...))

}

func (txo *Txo) Del(key string, opts ...clientv3.OpOption) {

	txo.Ops = append(txo.Ops, clientv3.OpDelete(key, opts...))

}

func (txo *Txo) DumpKeys() {

	for _, x := range txo.Ops {
		log.Debugf("%+v", string(x.KeyBytes()))
	}

}

func (txo *Txo) Exec() (*clientv3.TxnResponse, error) {

	kvc := clientv3.NewKV(EtcdClient)

	txr, err := kvc.Txn(context.TODO()).If(txo.Ifs...).Then(txo.Ops...).Commit()
	if err != nil {
		return nil, fmt.Errorf("giving up on etcd txn: %v", err)
	}
	if !txr.Succeeded {
		return txr, fmt.Errorf("transaction failed")
	}

	return txr, nil

}

func (txo *Txo) Append(txos ...*Txo) *Txo {

	for _, t := range txos {
		if t != nil {
			if t.Ops != nil {
				txo.Ops = append(txo.Ops, t.Ops...)
			}

			if t.Ifs != nil {
				txo.Ifs = append(txo.Ifs, t.Ifs...)
			}
		}
	}

	return txo
}

func (txo *Txo) RegisterKeys(tg *rec.TaskGoal, subtask_filter mapset.Set[string]) {
	if tg == nil {
		log.Error("goal is nil!")
		return
	}

	var keys []string

	for _, x := range txo.Ops {
		k := string(x.KeyBytes())

		if x.IsPut() {
			keys = append(keys, k)
		}
	}

	if len(keys) == 0 {
		return
	}

	for _, k := range keys {
		if !subtask_filter.Contains(k) {
			tg.AddTaskRecord(&rec.TaskRecord{
				Task: k,
			})
		}
	}

	return
}
