package storage

import (
	"google.golang.org/grpc"
)

var (
	// grpcMaxMessage is the maximum message size of a grpc message
	// the option is then passed to the grpc client connection, and
	// is set in bytes.
	MaxMessageSize = 512 * 1024 * 1024
	GRPCMaxMessage = grpc.WithDefaultCallOptions(
		grpc.MaxCallRecvMsgSize(MaxMessageSize),
		grpc.MaxCallSendMsgSize(MaxMessageSize),
	)
	GRPCMaxServer = []grpc.ServerOption{
		grpc.MaxRecvMsgSize(MaxMessageSize),
		grpc.MaxSendMsgSize(MaxMessageSize),
	}
)
