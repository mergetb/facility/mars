package storage

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/facility/mars/pkg/infrapod"
)

func SetInfrapodDNSConfig(host, mzid string, config *infrapod.InfrapodDNSConfig) error {
	key := fmt.Sprintf("/infrapod-dns/%s/%s", host, mzid)

	out, err := proto.Marshal(config)
	if err != nil {
		return err
	}

	_, err = EtcdClient.Put(
		context.TODO(),
		key,
		string(out),
	)

	return err
}

func DeleteInfrapodDNSConfig(host, mzid string) error {
	key := fmt.Sprintf("/infrapod-dns/%s/%s", host, mzid)

	_, err := EtcdClient.Delete(
		context.TODO(),
		key,
	)

	return err
}

func GetInfrapodDNSConfig(host, mzid string) (*infrapod.InfrapodDNSConfig, error) {
	key := fmt.Sprintf("/infrapod-dns/%s/%s", host, mzid)

	resp, err := EtcdClient.Get(
		context.TODO(),
		key,
	)
	if err != nil {
		return nil, err
	}

	// this isn't necessarily an error, so the caller should handle this
	if len(resp.Kvs) == 0 {
		return nil, nil
	}

	config := new(infrapod.InfrapodDNSConfig)
	err = proto.Unmarshal(resp.Kvs[0].Value, config)
	if err != nil {
		return nil, err
	}

	return config, nil
}

func GetInfrapodDNSConfigs(host string) (map[string]*infrapod.InfrapodDNSConfig, error) {
	key := fmt.Sprintf("/infrapod-dns/%s/", host)
	resp, err := EtcdClient.Get(
		context.TODO(),
		key,
		clientv3.WithPrefix(),
	)
	if err != nil {
		return nil, err
	}

	configs := make(map[string]*infrapod.InfrapodDNSConfig)
	for _, kv := range resp.Kvs {
		config := new(infrapod.InfrapodDNSConfig)
		err = proto.Unmarshal(kv.Value, config)
		if err != nil {
			log.Error(err)
			continue
		}

		if config == nil {
			err := fmt.Errorf("%s: config is nil", kv.Key)
			log.Error(err)
			return nil, err
		}

		configs[config.Mzid] = config
	}

	return configs, nil
}
