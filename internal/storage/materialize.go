package storage

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
)

func MaterializationExists(rid, eid, pid string) (bool, error) {

	bucket := "materializations"
	oid := fmt.Sprintf("%s.%s.%s", rid, eid, pid)

	_, err := MinIOClient.StatObject(
		context.TODO(),
		bucket,
		oid,
		minio.StatObjectOptions{},
	)
	if err != nil {
		resp := minio.ToErrorResponse(err)
		if resp.Code == "NoSuchBucket" {
			return false, nil
		}
		if resp.Code == "NoSuchKey" {
			return false, nil
		}
		return false, fmt.Errorf("minio error: %v", err)
	}

	return true, nil

}

func SaveMaterialization(mz *portal.Materialization) error {

	bucket := "materializations"
	oid := fmt.Sprintf("%s.%s.%s", mz.Rid, mz.Eid, mz.Pid)

	found, err := MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("bucket test: %v", err)
	}

	if !found {
		err = MinIOClient.MakeBucket(
			context.TODO(), bucket, minio.MakeBucketOptions{})
		if err != nil {
			return fmt.Errorf("make bucket: %s %v", bucket, err)
		}
	}

	buf, err := proto.Marshal(mz)
	if err != nil {
		return fmt.Errorf("marshal mzn: %v", err)
	}

	_, err = MinIOClient.PutObject(
		context.TODO(),
		bucket,
		oid,
		bytes.NewReader(buf),
		int64(len(buf)),
		minio.PutObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio: put mzn: %s %v", bucket, err)
	}

	return nil

}

func FetchMaterialization(mzid string) (*portal.Materialization, error) {

	bucket := "materializations"

	_, err := MinIOClient.StatObject(
		context.TODO(),
		bucket,
		mzid,
		minio.StatObjectOptions{},
	)
	if err != nil {
		return nil, status.Error(
			codes.NotFound,
			fmt.Errorf("materialization %s does not exist", mzid).Error(),
		)
	}

	obj, err := MinIOClient.GetObject(
		context.TODO(),
		bucket,
		mzid,
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, status.Error(
			codes.Internal,
			fmt.Errorf("minio get %s/%s: %v", bucket, mzid, err).Error(),
		)
	}
	defer obj.Close()

	buf, err := ioutil.ReadAll(obj)
	if err != nil {
		return nil, status.Error(
			codes.Internal,
			fmt.Errorf("minio read obj %s/%s: %v", bucket, mzid, err).Error(),
		)
	}

	mz := new(portal.Materialization)
	err = proto.Unmarshal(buf, mz)
	if err != nil {
		return nil, status.Error(
			codes.Internal,
			fmt.Errorf("mtz unmarshal: %s/%s: %v", bucket, mzid, err).Error(),
		)
	}

	return mz, nil
}

func DeleteMaterialization(rid, eid, pid string) error {

	bucket := "materializations"
	oid := fmt.Sprintf("%s.%s.%s", rid, eid, pid)

	found, err := MinIOClient.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("bucket test: %v", err)
	}

	if !found {
		// nothing to do
		log.Warn("demat requested but materializations bucket does not exist")
		return nil
	}

	_, err = MinIOClient.StatObject(
		context.TODO(),
		bucket,
		oid,
		minio.StatObjectOptions{},
	)
	if err != nil {
		// https://github.com/minio/minio-go/issues/1082#issuecomment-468215014
		resp := minio.ToErrorResponse(err)
		if resp.Code == "NoSuchKey" {
			log.Warn("demat requested bt materialization data not found")
			return nil
		}
		return fmt.Errorf("failed to stat object %s/%s: %v", bucket, oid, err)
	}

	err = MinIOClient.RemoveObject(
		context.TODO(),
		bucket,
		oid,
		minio.RemoveObjectOptions{},
	)
	if err != nil {
		log.Errorf("minio: delete mzn: %s %v", oid, err)
	}

	return nil

}
