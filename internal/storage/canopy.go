package storage

import (
	"context"
	"fmt"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"

	"gitlab.com/mergetb/api/facility/v1/go/state"
)

func GetVtep(host, device string) (*state.Vtep, error) {
	key := fmt.Sprintf("/net/%s/vtep/%s", host, device)
	l := log.WithFields(log.Fields{
		"host":   host,
		"device": device,
		"key":    key,
	})

	kvc := clientv3.NewKV(EtcdClient)
	resp, err := kvc.Get(context.TODO(), key)
	if err != nil {
		l.Error(err)
		return nil, fmt.Errorf("etcd: get vtep key %s: %+v", key, err)
	}

	if len(resp.Kvs) == 0 {
		return nil, fmt.Errorf("etcd: vtep key not present")
	} else if len(resp.Kvs) != 1 {
		l.Warnf("etcd: get vtep key: got %d responses from etcd. Using first", len(resp.Kvs))
	}

	val := resp.Kvs[0].Value

	vtep := new(state.Vtep)
	err = proto.Unmarshal(val, vtep)
	if err != nil {
		l.Error(err)
		return nil, fmt.Errorf("unmarshal %s: %+v", key, err)
	}

	return vtep, nil
}
