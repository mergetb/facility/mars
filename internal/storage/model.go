package storage

import (
	"context"
	"fmt"
	"io/ioutil"

	"github.com/minio/minio-go/v7"

	"gitlab.com/mergetb/xir/v0.3/go"
)

var _facility *xir.Facility

func GetModel() (*xir.Facility, error) {

	if _facility == nil {
		err := loadModel()
		if err != nil {
			return nil, err
		}
	}
	return _facility, nil

}

func loadModel() error {

	if MinIOClient == nil {
		InitMarsMinIOClient()
	}

	obj, err := MinIOClient.GetObject(
		context.TODO(),
		"sys",
		"tbxir.pbuf",
		minio.GetObjectOptions{},
	)
	if err != nil {
		return fmt.Errorf("minio get object: %v", err)
	}
	defer obj.Close()

	buf, err := ioutil.ReadAll(obj)
	if err != nil {
		return fmt.Errorf("read minio object: %v", err)
	}

	_facility, err = xir.FacilityFromB64String(string(buf))
	if err != nil {
		return err
	}

	return nil

}
