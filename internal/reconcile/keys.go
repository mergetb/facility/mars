package reconcile

import (
	"regexp"
)

var (
	namerx = "[a-zA-Z0-9_.-]+"
	macrx  = "(?:[0-9a-fA-F]{2}[:-]){5}(?:[0-9a-fA-F]{2})"

	netPhyKey     = regexp.MustCompile(`^/net/(` + namerx + `)/phy/(` + namerx + `)$`)
	netVtepKey    = regexp.MustCompile(`^/net/(` + namerx + `)/vtep/(` + namerx + `)$`)
	netVlanKey    = regexp.MustCompile(`^/net/(` + namerx + `)/vlan/(` + namerx + `)$`)
	netAccessKey  = regexp.MustCompile(`^/net/(` + namerx + `)/access/(` + namerx + `)$`)
	netTapKey     = regexp.MustCompile(`^/net/(` + namerx + `)/tap/(` + namerx + `)$`)
	netTrunkKey   = regexp.MustCompile(`^/net/(` + namerx + `)/trunk/(` + namerx + `)$`)
	netVtepwKey   = regexp.MustCompile(`^/net/(` + namerx + `)/vtepw/(` + namerx + `)$`)
	netPeerKey    = regexp.MustCompile(`^/net/(` + namerx + `)/peer/(` + namerx + `)$`)
	metalKey      = regexp.MustCompile(`^/metal/(` + namerx + `)$`)
	rebootKey     = regexp.MustCompile(`^/reboot/(` + namerx + `)$`)
	vmKey         = regexp.MustCompile(`^/vm/(` + namerx + `)/(` + namerx + `)$`)
	danceNetKey   = regexp.MustCompile(`^/dance/(` + namerx + `)/network/(` + namerx + `)$`)
	danceEntryKey = regexp.MustCompile(`^/dance/(` + namerx + `)/entry/(` + namerx + `)/(` + macrx + `)$`)
	danceDns4Key  = regexp.MustCompile(`^/dance/(` + namerx + `)/dns4/(` + namerx + `)/(` + namerx + `)$`)
	wgIfreqKey    = regexp.MustCompile(`^/wg/ifreq/(` + namerx + `)/(` + namerx + `)$`)
	wgPeerKey     = regexp.MustCompile(`^/wg/peer/(` + namerx + `)/(` + namerx + `)/(.+)$`)
	wgIfKey       = regexp.MustCompile(`^/wg/if/(` + namerx + `)/(` + namerx + `)$`)
	emuKey        = regexp.MustCompile(`^/mzparam/(` + namerx + `)$`)

	// ingress key -> /net/[ifr]/ingress/[mzid]/[proto]:[host]:[port]
	ingressId  = "([^:]+):([^:]+):([^:]+)"
	ingressKey = regexp.MustCompile(`^/net/(` + namerx + `)/ingress/(` + namerx + `)/` + ingressId + `$`)
)

type KeyType int

// TODO: we've been here before, we may want to enumerate for when we want to add more key types
const (
	NetPhyKey KeyType = iota
	NetVtepKey
	NetVlanKey
	NetAccessKey
	NetTapKey
	NetTrunkKey
	NetVtepwKey
	NetPeerKey
	MetalKey
	RebootKey
	VMKey
	DanceNetKey
	DanceEntryKey
	DanceDns4Key
	WgIfreqKey
	WgPeerKey
	WgIfKey
	EmuKey
	IngressKey
)

type StatusKey struct {
	Type  KeyType
	Host  string
	Name  string
	Name2 string
}

func ParseKey(key string) *StatusKey {
	parts := netPhyKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: NetPhyKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = netVtepKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: NetVtepKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = netVlanKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: NetVlanKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = netAccessKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: NetAccessKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = netTapKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: NetTapKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = netTrunkKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: NetTrunkKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = netVtepwKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: NetVtepwKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = netPeerKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: NetPeerKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = metalKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: MetalKey,
			Host: parts[0][1],
		}
	}

	parts = rebootKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: RebootKey,
			Host: parts[0][1],
		}
	}

	parts = vmKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: VMKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = danceNetKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: DanceNetKey,
			Host: parts[0][1],
			Name: parts[0][2],
		}
	}

	parts = danceEntryKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type:  DanceEntryKey,
			Host:  parts[0][1],
			Name:  parts[0][2],
			Name2: parts[0][3],
		}
	}

	parts = danceDns4Key.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type:  DanceDns4Key,
			Host:  parts[0][1],
			Name:  parts[0][2],
			Name2: parts[0][3],
		}
	}

	parts = wgIfreqKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: WgIfreqKey,
			Name: parts[0][1],
			Host: parts[0][2],
		}
	}

	parts = wgPeerKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type:  WgPeerKey,
			Name:  parts[0][1],
			Host:  parts[0][2],
			Name2: parts[0][3],
		}
	}

	parts = wgIfKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: WgIfKey,
			Name: parts[0][1],
			Host: parts[0][2],
		}
	}

	parts = emuKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type: EmuKey,
			Name: parts[0][1],
		}
	}

	parts = ingressKey.FindAllStringSubmatch(key, -1)
	if len(parts) > 0 {
		return &StatusKey{
			Type:  IngressKey,
			Name:  parts[0][1],
			Host:  parts[0][1],
			Name2: parts[0][1], // name2 == port.
		}
	}

	return nil
}
