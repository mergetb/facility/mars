package container

import (
	"context"
	"fmt"
	"os"

	"github.com/containers/podman/v4/pkg/bindings"
	"github.com/containers/podman/v4/pkg/bindings/containers"
	"github.com/containers/podman/v4/pkg/bindings/images"
	"github.com/containers/podman/v4/pkg/specgen"
	spec "github.com/opencontainers/runtime-spec/specs-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/facility/mars/internal/moa/metrics"
	"gitlab.com/mergetb/facility/mars/pkg/config"
)

const (
	libpodPort = 7474
)

// returns the podman container name for the given Mzid
func getContainerName(mzid string) string {
	return fmt.Sprintf("click-%s", mzid)
}

// returns the URI for the libpod service
func getPodmanURI() string {
	h, err := os.Hostname()
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("get hostname")
		h = "localhost" // fallback
	}
	return fmt.Sprintf("tcp://%s:%d", h, libpodPort)
}

func closePodmanConn(ctx context.Context) {
	if ctx == nil {
		return
	}

	conn, err := bindings.GetClient(ctx)
	if err != nil {
		log.Errorf("error: trying to close podman connection: %+v", err)
		return
	}

	conn.Client.CloseIdleConnections()
}

func Start(mzid string) error {
	// TODO should we open a connection when moa starts up and keep reusing it?
	ctx, err := bindings.NewConnection(context.TODO(), getPodmanURI())
	defer closePodmanConn(ctx)
	if err != nil {
		return fmt.Errorf("podman connection: %w", err)
	}

	name := getContainerName(mzid)
	err = containers.Start(ctx, name, nil)
	if err != nil {
		return fmt.Errorf("start container %s: %w", name, err)
	}

	return nil
}

// Pull the fastclick image specified in the Mars config and create a container for the mzid
func Create(mzid string, nthreads int) error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %w", err)
	}
	image := cfg.Images.Moa.Fastclick.RegUrl()

	// TODO should we open a connection when moa starts up and keep reusing it?
	ctx, err := bindings.NewConnection(context.TODO(), getPodmanURI())
	defer closePodmanConn(ctx)
	if err != nil {
		return fmt.Errorf("podman connection: %w", err)
	}

	opts := new(images.PullOptions).WithPolicy("newer").WithSkipTLSVerify(cfg.Images.Moa.Moa.Registry.NoVerify)
	_, err = images.Pull(ctx, image, opts)
	if err != nil {
		return fmt.Errorf("pull image %s: %w", image, err)
	}

	s := specgen.SpecGenerator{}
	s.Name = getContainerName(mzid)
	s.RestartPolicy = "on-failure"
	restart := uint(5)
	s.RestartRetries = &restart

	s.Image = image
	s.Privileged = true                               // --privileged
	s.NetNS = specgen.Namespace{NSMode: specgen.Host} // --network=host
	s.Mounts = []spec.Mount{
		{
			Type:        "bind",
			Destination: "/click",
			Source:      "/var/vol/moa/emulation/" + mzid,
		},
	}
	s.Command = []string{"/usr/bin/click", "-j", fmt.Sprintf("%d", nthreads), "-u", "/click/control.sock", "-f", "/click/config"}

	resp, err := containers.CreateWithSpec(ctx, &s, nil)
	if err != nil {
		return fmt.Errorf("container create: %w", err)
	}
	if len(resp.Warnings) > 0 {
		log.WithFields(log.Fields{"warnings": resp.Warnings}).Warn("container create")
	}

	err = containers.Start(ctx, s.Name, nil)
	if err != nil {
		//TODO remove container as well
		return fmt.Errorf("start container %s: %w", s.Name, err)
	}

	metrics.AddEmu()

	return nil
}

// possible podman container states
var containerStates = []string{"dead", "restarting", "paused", "running", "oomkilled"}

// Delete the container for the given Mzid
func Stop(mzid string) error {

	ctx, err := bindings.NewConnection(context.TODO(), getPodmanURI())
	defer closePodmanConn(ctx)
	if err != nil {
		return fmt.Errorf("podman connection: %v", err)
	}

	name := getContainerName(mzid)
	opts := new(containers.RemoveOptions).WithForce(true)
	_, err = containers.Remove(ctx, name, opts)
	if err != nil {
		return fmt.Errorf("remove container: %v", err)
	}

	// reset the container state in the metrics since the inspect thread can't
	for _, s := range containerStates {
		metrics.ContainerState(mzid, s, false)
	}

	return nil
}

// Determine if container exists for the given Mzid
func Exists(mzid string) (bool, error) {

	ctx, err := bindings.NewConnection(context.TODO(), getPodmanURI())
	defer closePodmanConn(ctx)
	if err != nil {
		return false, fmt.Errorf("podman connection: %v", err)
	}

	name := getContainerName(mzid)
	opts := new(containers.ExistsOptions).WithExternal(false)
	exists, err := containers.Exists(ctx, name, opts)
	if err != nil {
		return false, fmt.Errorf("container exists: %v", err)
	}

	return exists, nil
}

// Inspect podman containers for each materialization and record their state in the metrics
func CollectMetrics(mzid ...string) error {
	ctx, err := bindings.NewConnection(context.TODO(), getPodmanURI())
	defer closePodmanConn(ctx)
	if err != nil {
		return fmt.Errorf("podman connection: %w", err)
	}

	for _, mz := range mzid {
		name := getContainerName(mz)

		data, err := containers.Inspect(ctx, name, nil)
		if err != nil {
			log.WithFields(log.Fields{"name": name, "err": err}).Error("inspect container")
			continue
		}

		metrics.ContainerState(mz, "dead", data.State.Dead)
		metrics.ContainerState(mz, "restarting", data.State.Restarting)
		metrics.ContainerState(mz, "paused", data.State.Paused)
		metrics.ContainerState(mz, "running", data.State.Running)
		metrics.ContainerState(mz, "oomkilled", data.State.OOMKilled)
	}

	metrics.NumEmulations(len(mzid))

	return nil
}
