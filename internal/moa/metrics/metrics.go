// Provides a Prometheus endpoint with metrics about the Moa server
package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	updatesProcessed = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "moa_updates_processed",
		Help: "The number of link updates processed",
	},
		[]string{"mzid"})

	numLinks = promauto.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "moa_num_links",
			Help: "The number of emulated links in a materialization",
		},
		[]string{"mzid"})

	numEndpoints = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "moa_num_endpoints",
		Help: "total number of endpoints on emulated links",
	},
		[]string{"mzid"})

	numEmlations = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "moa_num_emulations",
		Help: "The number of currently running emulations handled by a server",
	})

	totalEmulations = promauto.NewCounter(prometheus.CounterOpts{
		Name: "moa_total_emulations",
		Help: "The absolute count of emulations run on the server",
	})

	containerState = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "moa_container_state",
		Help: "Tracks the state of the container running click",
	},
		[]string{"mzid", "state"})

	emuErrors = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "moa_emu_errors",
		Help: "The absolute number of emulation setup/destroy errors",
	},
		[]string{"mzid", "op"})

	numThreads = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Name: "moa_num_threads",
		Help: "The number of threads used by an emulation",
	},
		[]string{"mzid"})
)

func AddEmu() {
	totalEmulations.Inc()
}

func LinkUpdate(mzid string) {
	updatesProcessed.With(prometheus.Labels{
		"mzid": mzid,
	}).Inc()
}

func ContainerState(mzid, state string, on bool) {
	v := 0.0
	if on {
		v = 1.0
	}
	containerState.With(prometheus.Labels{"mzid": mzid, "state": state}).Set(v)
}

func AddLinks(mzid string, count int) {
	numLinks.With(prometheus.Labels{"mzid": mzid}).Add(float64(count))
}

func DelLinks(mzid string) {
	numLinks.With(prometheus.Labels{"mzid": mzid}).Set(0.0)
}

func AddEndpoints(mzid string, count int) {
	numEndpoints.With(prometheus.Labels{"mzid": mzid}).Add(float64(count))
}

func DelEndpoints(mzid string) {
	numEndpoints.With(prometheus.Labels{"mzid": mzid}).Set(0.0)
}

func EmuError(mzid, op string) {
	emuErrors.With(prometheus.Labels{"mzid": mzid, "op": op}).Inc()
}

// Set the number of currently running emulation containers
func NumEmulations(count int) {
	numEmlations.Set(float64(count))
}

func AddThreads(mzid string, count int) {
	numThreads.With(prometheus.Labels{"mzid": mzid}).Add(float64(count))
}

func DelThreads(mzid string) {
	numThreads.With(prometheus.Labels{"mzid": mzid}).Set(0.0)
}

func Serve() {
	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":2112", nil)
}
