package internal

import (
	"os"
	"regexp"

	log "github.com/sirupsen/logrus"
)

type LogWriter struct{}

var (
	MonitorEtcdServer bool = true
	levelRegex *regexp.Regexp
)

var (
	LevelError   = log.ErrorLevel.String()
	LevelWarning = log.WarnLevel.String()
	LevelFatal   = log.FatalLevel.String()
	LevelPanic   = log.PanicLevel.String()
	LevelDebug   = log.DebugLevel.String()
	LevelTrace   = log.TraceLevel.String()
)

func init() {
	var err error
	levelRegex, err = regexp.Compile("level=([a-z]+)")

	if err != nil {
		log.WithError(err).Fatal("Cannot setup log level")
	}
}

func (w *LogWriter) detectLogLevel(p []byte) (level string) {
	matches := levelRegex.FindStringSubmatch(string(p))
	if len(matches) > 1 {
		level = matches[1]
	}
	return
}

func (w *LogWriter) Write(p []byte) (n int, err error) {
	level := w.detectLogLevel(p)

	if level == LevelError || level == LevelWarning || level == LevelFatal || level == LevelPanic {
		return os.Stderr.Write(p)
	}
	return os.Stdout.Write(p)
}

func InitLogging() {
	// Support for setting log level via environment
	// default is at info level
	value, ok := os.LookupEnv("LOGLEVEL")
	if ok {
		lvl, err := log.ParseLevel(value)
		if err != nil {
			log.SetLevel(log.InfoLevel)
			log.Errorf("bad LOGLEVEL env var: %v. ignoring", value)
		} else {
			log.SetLevel(lvl)
			log.Infof("setting log level to %s", value)
		}
	}

	// Interpose all logs to ensure we write to stdout/stderr appropriately
	log.SetOutput(&LogWriter{})
}

func initHeartbeatMonitor() {
	// individual reconcilers can disable etcd heartbeat monitoring by setting this value to 1
	value, ok := os.LookupEnv("DISABLE_ETCD_HEARTBEAT_MONITOR")

	if ok && value == "1" {
		log.Info("detected DISABLE_ETCD_HEARTBEAT_MONITOR=1. Disabling etcd heartbeat monitoring")
		MonitorEtcdServer = false
	}
}

func InitReconciler() {
	initHeartbeatMonitor()
}
