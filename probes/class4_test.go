package main

import (
	"context"
	"crypto/tls"
	"testing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/xir/v0.3/go"
)

func TestMz0Mat(t *testing.T) {

	mz := &portal.Materialization{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t0",
		/*
			Params: &portal.MzParameters{
				InfranetVid: 1701,
			},
		*/
		Metal: []*portal.BareMetal{{
			Resource: "x0",
			Model: &xir.Node{
				Id: "n0",
				Image: &xir.StringConstraint{
					Op:    xir.Operator_EQ,
					Value: "fedora:33",
				},
			},
		}},
		Vms: []*portal.VirtualMachine{
			{
				VmAlloc: &xir.ResourceAllocation{
					Resource: "x1",
					Node:     "n1",
					Procs: &xir.ProcAllocation{
						Alloc: map[uint32]*xir.SocketAllocation{
							0: {Cores: 2},
						},
					},
					Memory: &xir.MemoryAllocation{
						Alloc: map[uint32]*xir.DimmAllocation{
							0: {Capacity: xir.GB(2)},
						},
					},
					Disks: &xir.DisksAllocation{
						Alloc: map[uint32]*xir.DiskAllocation{
							0: {Capacity: xir.GB(40)},
						},
					},
					Model: &xir.Node{
						Id: "n1",
						Image: &xir.StringConstraint{
							Op:    xir.Operator_EQ,
							Value: "fedora:33",
						},
					},
				},
			},
			{
				VmAlloc: &xir.ResourceAllocation{
					Resource: "x1",
					Node:     "n2",
					Procs: &xir.ProcAllocation{
						Alloc: map[uint32]*xir.SocketAllocation{
							0: {Cores: 2},
						},
					},
					Memory: &xir.MemoryAllocation{
						Alloc: map[uint32]*xir.DimmAllocation{
							0: {Capacity: xir.GB(2)},
						},
					},
					Disks: &xir.DisksAllocation{
						Alloc: map[uint32]*xir.DiskAllocation{
							0: {Capacity: xir.GB(20)},
						},
					},
					Model: &xir.Node{
						Id: "n1",
						Image: &xir.StringConstraint{
							Op:    xir.Operator_EQ,
							Value: "fedora:33",
						},
					},
				},
			},
		},
		Links: []*portal.Link{{
			Realization: &portal.LinkRealization{
				Link: &xir.Link{Id: "x1~x2"},
				Segments: map[uint64]*portal.LinkSegment{
					47: {
						Endpoints: []*portal.Endpoint{
							{
								Host: "x0",
								Interface: &portal.Endpoint_Phy{&portal.PhysicalInterface{
									Name: "eno1",
								}},
							},
							{
								Host: "x1",
								Interface: &portal.Endpoint_Vtep{
									&portal.Vtep{
										Name: "vtep47",
										Vni:  47,
										Parent: &portal.PhysicalInterface{
											Name: "enp1",
										},
									},
								},
							},
						},
						Waypoints: []*portal.Waypoint{
							{
								Host: "xp",
								Interface: &portal.Waypoint_Access{
									&portal.VLANAccessPort{
										Vid: 10,
										Port: &portal.PhysicalInterface{
											Name: "swp1",
										},
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge:   "bridge",
									Untagged: []uint32{10},
									Pvid:     10,
								},
							},
							{
								Host: "xp",
								Interface: &portal.Waypoint_Vtep{
									&portal.Vtep{
										Name: "vtep47",
										Vni:  47,
										Parent: &portal.PhysicalInterface{
											Name: "swp2",
										},
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge:   "bridge",
									Untagged: []uint32{10},
									Pvid:     10,
								},
							},
						},
					},
				},
			},
		}},
	}

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Materialize(context.TODO(), &facility.MaterializeRequest{
		Materialization: mz,
	})

	if err != nil {
		t.Fatalf("materialize: %v", err)
	}

	t.Log("materialization requested")

}

func TestMz0Demat(t *testing.T) {

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Dematerialize(context.TODO(), &facility.DematerializeRequest{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t0",
	})

	if err != nil {
		t.Fatalf("demat: %v", err)
	}

}

func TestMz1Mat(t *testing.T) {

	mz := &portal.Materialization{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t1",
		Links: []*portal.Link{{
			Realization: &portal.LinkRealization{
				Link: &xir.Link{Id: "x0~x1"},
				Segments: map[uint64]*portal.LinkSegment{
					47: {
						Waypoints: []*portal.Waypoint{
							{
								Host: "xp",
								Interface: &portal.Waypoint_Access{
									&portal.VLANAccessPort{
										Vid: 10,
										Port: &portal.PhysicalInterface{
											Name: "swp1",
										},
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge: "bridge",
								},
							},
							{
								Host: "xp",
								Interface: &portal.Waypoint_Access{
									&portal.VLANAccessPort{
										Vid: 10,
										Port: &portal.PhysicalInterface{
											Name: "swp2",
										},
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge: "bridge",
								},
							},
						},
					},
				},
			},
		}},
	}

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Materialize(context.TODO(), &facility.MaterializeRequest{
		Materialization: mz,
	})

	if err != nil {
		t.Fatalf("materialize: %v", err)
	}

	t.Log("materialization requested")

}

func TestMz1Demat(t *testing.T) {

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Dematerialize(context.TODO(), &facility.DematerializeRequest{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t1",
	})

	if err != nil {
		t.Fatalf("demat: %v", err)
	}

}

func TestMz2Mat(t *testing.T) {

	mz := &portal.Materialization{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t2",
		Links: []*portal.Link{{
			Realization: &portal.LinkRealization{
				Link: &xir.Link{Id: "x0~x1"},
				Segments: map[uint64]*portal.LinkSegment{
					47: {
						Waypoints: []*portal.Waypoint{
							{
								Host: "xp",
								Interface: &portal.Waypoint_Trunk{
									&portal.VLANTrunkPort{
										Vids: []uint32{4, 7},
										Port: &portal.PhysicalInterface{
											Name: "swp1",
										},
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge: "bridge",
								},
							},
							{
								Host: "xp",
								Interface: &portal.Waypoint_Trunk{
									&portal.VLANTrunkPort{
										Vids: []uint32{4, 7},
										Port: &portal.PhysicalInterface{
											Name: "swp2",
										},
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge: "bridge",
								},
							},
						},
					},
				},
			},
		}},
	}

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Materialize(context.TODO(), &facility.MaterializeRequest{
		Materialization: mz,
	})

	if err != nil {
		t.Fatalf("materialize: %v", err)
	}

	t.Log("materialization requested")

}

func TestMz2Demat(t *testing.T) {

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Dematerialize(context.TODO(), &facility.DematerializeRequest{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t2",
	})

	if err != nil {
		t.Fatalf("demat: %v", err)
	}

}

func TestMz3Mat(t *testing.T) {

	mz := &portal.Materialization{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t3",
		Links: []*portal.Link{{
			Realization: &portal.LinkRealization{
				Link: &xir.Link{Id: "x0~x1"},
				Segments: map[uint64]*portal.LinkSegment{
					47: {
						Waypoints: []*portal.Waypoint{
							{
								Host: "xp",
								Interface: &portal.Waypoint_Vtep{
									&portal.Vtep{
										Name:     "vtep47",
										Vni:      47,
										TunnelIp: "10.99.0.9",
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge:   "bridge",
									Untagged: []uint32{4},
									Pvid:     4,
								},
							},
							{
								Host: "xp",
								Interface: &portal.Waypoint_Vtep{
									&portal.Vtep{
										Name:     "vtep74",
										Vni:      74,
										TunnelIp: "10.99.0.9",
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge: "bridge",
									Tagged: []uint32{74},
								},
							},
						},
					},
				},
			},
		}},
	}

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Materialize(context.TODO(), &facility.MaterializeRequest{
		Materialization: mz,
	})

	if err != nil {
		t.Fatalf("materialize: %v", err)
	}

	t.Log("materialization requested")

}

func TestMz3Demat(t *testing.T) {

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Dematerialize(context.TODO(), &facility.DematerializeRequest{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t3",
	})

	if err != nil {
		t.Fatalf("demat: %v", err)
	}

}

func TestMz4Mat(t *testing.T) {

	mz := &portal.Materialization{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t4",
		Links: []*portal.Link{{
			Realization: &portal.LinkRealization{
				Link: &xir.Link{Id: "x0~x1"},
				Segments: map[uint64]*portal.LinkSegment{
					47: {
						Waypoints: []*portal.Waypoint{
							{
								Host: "xp",
								Interface: &portal.Waypoint_BgpPeer{
									&portal.BGPPeer{
										LocalAsn: 0x1701d,
										Interface: &portal.PhysicalInterface{
											Name: "swp1",
										},
									},
								},
							},
							{
								Host: "xp",
								Interface: &portal.Waypoint_BgpPeer{
									&portal.BGPPeer{
										LocalAsn: 0x1701d,
										Interface: &portal.PhysicalInterface{
											Name: "swp2",
										},
									},
								},
							},
						},
					},
				},
			},
		}},
	}

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Materialize(context.TODO(), &facility.MaterializeRequest{
		Materialization: mz,
	})

	if err != nil {
		t.Fatalf("materialize: %v", err)
	}

	t.Log("materialization requested")

}

func TestMz4Demat(t *testing.T) {

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Dematerialize(context.TODO(), &facility.DematerializeRequest{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t4",
	})

	if err != nil {
		t.Fatalf("demat: %v", err)
	}

}

func TestMz5Mat(t *testing.T) {

	mz := &portal.Materialization{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t5",
		Metal: []*portal.BareMetal{{
			Resource: "x0",
			Model: &xir.Node{
				Id: "n0",
				Image: &xir.StringConstraint{
					Op:    xir.Operator_EQ,
					Value: "bullseye",
				},
			},
			Inframac: "04:71:00:00:00:10",
			Rootdev:  "/dev/sda",
		}},
		Links: []*portal.Link{{
			Realization: &portal.LinkRealization{
				Link: &xir.Link{Id: "stornet"},
				Segments: map[uint64]*portal.LinkSegment{
					3: {
						/*XXX in base harbor provisioning
						Endpoints: []*portal.Endpoint{
							{
								Host: "ifr",
								Interface: &portal.Endpoint_Vtep{
									&portal.Vtep{
										Name:      "vtep3",
										Vni:       3,
										TunnelIp:  "10.99.0.2",
										ServiceIp: "172.30.0.1/16",
										Parent:    "ens2",
									},
								},
							},
						},
						*/
						Waypoints: []*portal.Waypoint{
							//// transit
							{
								Host: "ifr",
								Interface: &portal.Waypoint_BgpPeer{
									&portal.BGPPeer{
										Interface: &portal.PhysicalInterface{
											Name: "ens2",
										},
										LocalAsn:  0xfa56ea04,
										RemoteAsn: 0xfa56ea03,
										Network:   "10.99.0.2",
									},
								},
							},
							{
								Host: "infra",
								Interface: &portal.Waypoint_BgpPeer{
									&portal.BGPPeer{
										Interface: &portal.PhysicalInterface{
											Name: "swp1",
										},
										LocalAsn:  0xfa56ea03,
										RemoteAsn: 0xfa56ea04,
										Network:   "10.99.0.1",
									},
								},
							},
							{
								Host: "infra",
								Interface: &portal.Waypoint_Vtep{
									&portal.Vtep{
										Name:     "vtep3",
										Vni:      3,
										TunnelIp: "10.99.0.1",
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge:   "bridge",
									Untagged: []uint32{3},
									Pvid:     3,
								},
							},
							//// x0
							{
								Host: "infra",
								Interface: &portal.Waypoint_Access{
									&portal.VLANAccessPort{
										Vid: 3,
										Port: &portal.PhysicalInterface{
											Name: "swp2",
										},
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge: "bridge",
								},
							},
						},
					},
					/*TODO
					101: {
						Endpoints: []*portal.Endpoint{
							{
								Host: "ifr",
								Interface: &portal.Endpoint_Vtep{
									&portal.Vtep{
										Name:     "vtep101",
										Vni:      101,
										TunnelIp: "10.99.0.2",
										Parent:   "ens2",
									},
								},
							},
						},
						Waypoints: []*portal.Waypoint{
							//// transit
							{
								Host: "ifr",
								Interface: &portal.Waypoint_BgpPeer{
									&portal.BGPPeer{
										Asn:       0xfa56ea05,
										Interface: "ens2",
									},
								},
							},
							{
								Host: "infra",
								Interface: &portal.Waypoint_BgpPeer{
									&portal.BGPPeer{
										Asn:       0xfa56ea01,
										Interface: "swp1",
									},
								},
							},
							{
								Host: "infra",
								Interface: &portal.Waypoint_Vtep{
									&portal.Vtep{
										Name:     "vtep101",
										Vni:      101,
										TunnelIp: "10.99.0.1",
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge:   "bridge",
									Untagged: []uint32{101},
									Pvid:     101,
								},
							},
							//// x0
							{
								Host: "infra",
								Interface: &portal.Waypoint_Access{
									&portal.VLANAccessPort{
										Port: "swp2",
										Vid:  101,
									},
								},
								Bridge: &portal.BridgeMember{
									Bridge: "bridge",
								},
							},
						},
					},
					*/
				},
			},
		}},
	}

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Materialize(context.TODO(), &facility.MaterializeRequest{
		Materialization: mz,
	})

	if err != nil {
		t.Fatalf("materialize: %v", err)
	}

	t.Log("materialization requested")

}

func TestMz5Demat(t *testing.T) {

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Dematerialize(context.TODO(), &facility.DematerializeRequest{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t5",
	})

	if err != nil {
		t.Fatalf("demat: %v", err)
	}

}

func TestMz6Mat(t *testing.T) {

	mz := &portal.Materialization{
		Rid: "harbor",
		Eid: "system",
		Pid: "marstb",
		/*
			Params: &portal.MzParameters{
				InfranetVid: 1701,
			},
		*/
		Metal: []*portal.BareMetal{{
			Resource: "x0",
			Model: &xir.Node{
				Id: "n0",
				Image: &xir.StringConstraint{
					Op:    xir.Operator_EQ,
					Value: "bullseye",
				},
			},
			Inframac: "04:71:00:00:00:10",
			Rootdev:  "/dev/sda",
		}},
	}

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Materialize(context.TODO(), &facility.MaterializeRequest{
		Materialization: mz,
	})

	if err != nil {
		t.Fatalf("materialize: %v", err)
	}

	t.Log("materialization requested")

}

func TestMz6Demat(t *testing.T) {

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.Dematerialize(context.TODO(), &facility.DematerializeRequest{
		Pid: "terraform",
		Eid: "mars",
		Rid: "t6",
	})

	if err != nil {
		t.Fatalf("demat: %v", err)
	}

}

func TestMz6Cycle(t *testing.T) {

	conn, cli := apiserverClient(t)
	defer conn.Close()

	_, err := cli.SetMachinePowerState(
		context.TODO(),
		&facility.SetMachineStateRequest{
			States: map[string]state.MachineState{
				"x0": state.MachineState_Reimaged,
			},
		},
	)
	if err != nil {
		t.Fatalf("set state: %v", err)
	}

}

func apiserverClient(t *testing.T) (*grpc.ClientConn, facility.FacilityClient) {

	creds := credentials.NewTLS(&tls.Config{
		//XXX XXX XXX
		InsecureSkipVerify: true,
	})
	conn, err := grpc.Dial("apiserver:6001",
		grpc.WithTransportCredentials(creds),
	)
	if err != nil {
		t.Fatalf("apiserver dial: %v", err)
	}

	return conn, facility.NewFacilityClient(conn)

}
