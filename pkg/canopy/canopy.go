package canopy

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/tech/rtnl"
)

const (
	VXLAN_DST_PORT     uint16 = 4789 // standard Linux vtep port
	VXLAN_HEADER_BYTES uint32 = 50
	JUMBO_FRAME_MTU    uint32 = 9216
)

func CreateVtep(vtep *state.Vtep) error {
	l := log.WithFields(log.Fields{
		"mzid":   vtep.Mzid,
		"host":   vtep.Host,
		"vtep":   vtep.Vtep,
		"bridge": vtep.Bridge,
		"mtu":    vtep.Mtu,
	})
	l.Debug("handling vtep update")

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	tip := net.ParseIP(vtep.Vtep.TunnelIp)
	if tip == nil {
		return fmt.Errorf("failed to parse tunnel ip '%s'", vtep.Vtep.TunnelIp)
	}

	lnk := rtnl.NewLink()
	lnk.Info.Name = vtep.Vtep.Name
	lnk.Info.Vxlan = &rtnl.Vxlan{
		Vni:     uint32(vtep.Vtep.Vni),
		Local:   tip,
		DstPort: VXLAN_DST_PORT,
	}

	if vtep.Vtep.Parent != nil {
		plink, err := phyLink(rtx, vtep.Vtep.Parent.Name, vtep.Vtep.Parent.Mac)
		if err != nil {
			return err
		}
		lnk.Info.Vxlan.Link = uint32(plink.Msg.Index)

		// ensure parent MTU leaves room for the VxLan header
		if vtep.Mtu != 0 {
			if plink.Info.Mtu < vtep.Mtu+VXLAN_HEADER_BYTES {
				l.Debugf("setting vtep parent device MTU to %d", vtep.Mtu+VXLAN_HEADER_BYTES)
				err = plink.SetMtu(rtx, int(vtep.Mtu+VXLAN_HEADER_BYTES))
				if err != nil {
					return fmt.Errorf("set parent mtu: %v", err)
				}
			}
		}
	}

	err = lnk.Present(rtx)
	if err != nil {
		return err
	}

	if vtep.Bridge != nil {
		// ensure the vtep is on the bridge
		bridge, err := EnsureBridgeMember(rtx, vtep.Bridge)
		if err != nil {
			return err
		}

		if lnk.Info.Master == 0 {
			err = lnk.SetMaster(rtx, int(bridge.Msg.Index))
			if err != nil {
				return err
			}
		}

		for _, vid := range vtep.Bridge.Untagged {
			pvid := vid == vtep.Bridge.Pvid
			lnk.SetUntagged(rtx, uint16(vid), false, pvid, false)
		}

		for _, vid := range vtep.Bridge.Tagged {
			pvid := vid == vtep.Bridge.Pvid
			lnk.SetTagged(rtx, uint16(vid), false, pvid, false)
		}
	}

	if vtep.Mtu != 0 {
		err = lnk.SetMtu(rtx, int(vtep.Mtu))
		if err != nil {
			return fmt.Errorf("set mtu: %v", err)
		}
	}

	err = lnk.Up(rtx)
	if err != nil {
		return err
	}

	err = lnk.Promisc(rtx, true)
	if err != nil {
		return err
	}

	return nil
}

func DeleteVtep(device string) error {
	l := log.WithFields(log.Fields{
		"name": device,
	})
	l.Debug("handling vtep delete")

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	lnk := rtnl.NewLink()
	lnk.Info.Name = device

	err = lnk.Down(rtx)
	if err != nil {
		return err
	}

	err = lnk.Absent(rtx)
	if err != nil {
		return err
	}

	return nil
}

func EnsureBridgeMember(rtx *rtnl.Context, b *portal.BridgeMember) (*rtnl.Link, error) {
	return EnsureBridge(rtx, b.Bridge, bridgeNeedsVlanFiltering(b))
}

func EnsureBridge(rtx *rtnl.Context, name string, vlan_aware bool) (*rtnl.Link, error) {
	bridge := rtnl.NewLink()

	bridge.Info.Name = name
	bridge.Info.Mtu = JUMBO_FRAME_MTU
	bridge.Info.Bridge = &rtnl.Bridge{
		VlanAware: vlan_aware,
	}

	err := bridge.Present(rtx)
	if err != nil {
		return nil, err
	}

	err = bridge.Up(rtx)
	if err != nil {
		return nil, err
	}

	return bridge, nil

}

// Attempt to remove a bridge device. It may fail if other slaves are still attached
func RemoveBridge(rtx *rtnl.Context, name string) error {
	bridge, err := rtnl.GetLink(rtx, name)
	if err != nil {
		// rtnl library doesn't use structured errors, yuck!
		// if the bridge already doesn't exist, it is not an error
		if err.Error() == "not found" {
			return nil
		}
		return fmt.Errorf("unable to get link: %w", err)
	}

	err = bridge.Down(rtx)
	if err != nil {
		return fmt.Errorf("unable to set link down: %w", err)
	}

	err = bridge.Absent(rtx)
	if err != nil {
		return fmt.Errorf("unable to remove link: %w", err)
	}

	return nil
}

// bridge to be made vlan aware if it is configured with a bridge PVID or we have tagged
// or untagged ports to put onto it. This is a heuristic that meets all of the current link
// embedding processes in Merge, but may need to be made explicit in the API at some point
func bridgeNeedsVlanFiltering(b *portal.BridgeMember) bool {
	return b.Pvid != 0 || len(b.Tagged) > 0 || len(b.Untagged) > 0
}
