package canopy

import (
	"bytes"
	"fmt"
	"net"

	"gitlab.com/mergetb/tech/rtnl"
)

func LinkByMac(rtx *rtnl.Context, mac string) (*rtnl.Link, error) {
	addr, err := net.ParseMAC(mac)
	if err != nil {
		return nil, fmt.Errorf("parse MAC %s: %v", mac, err)
	}

	links, err := rtnl.ReadLinks(rtx, nil)
	if err != nil {
		return nil, fmt.Errorf("read links: %v", err)
	}

	for _, link := range links {
		// look for physical links
		if link.Info.Type() == rtnl.PhysicalType && bytes.Equal(addr, link.Info.Address) {
			return link, nil
		}
	}

	return nil, fmt.Errorf("cannot find link with MAC %s", mac)
}

// Find the rtnetlink associated with this port using the MAC if provided,
// and falling back to the name if not
func LinkByMacOrName(rtx *rtnl.Context, name, mac string) (*rtnl.Link, error) {
	if mac != "" {
		return LinkByMac(rtx, mac)
	}

	if name != "" {
		return rtnl.GetLink(rtx, name)
	}

	return nil, fmt.Errorf("no mac or name for port")
}

func phyLink(rtx *rtnl.Context, name, mac string) (*rtnl.Link, error) {
	return LinkByMacOrName(rtx, name, mac)
}
