package moautil

import (
	"strings"
)

// Parse a Mzid into its component parts: rid.eid.pid
func ParseMzid(mzid string) []string {
	r := strings.Split(mzid, ".")
	if len(r) == 3 {
		return r
	}
	return make([]string, 0)
}
