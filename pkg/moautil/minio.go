package moautil

import (
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
)

// pull creds from etcd config
// GoLang doesn't allow circular imports, so this can't live in storage, due to the import of config
func InitMarsMinIOClientFromConfig() error {
	cfg, err := config.GetConfig()
	if err != nil {
		return err
	}
	return storage.InitMarsMinIOClientCreds(
		cfg.Services.Infraserver.Minio.Credentials.Username,
		cfg.Services.Infraserver.Minio.Credentials.Password,
	)
}
