package moautil

import (
	"fmt"

	"google.golang.org/grpc"

	moaapi "gitlab.com/mergetb/api/moa/v1/go"
	"gitlab.com/mergetb/facility/mars/service"
)

// WithMoa executes a Moa API call aganst the provided endpoint
func WithMoa(endpoint string, f func(moaapi.ManageClient) error) error {

	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", endpoint, service.MoaGRPC), grpc.WithInsecure())
	if err != nil {
		return fmt.Errorf("failed to connect to moa service: %v", err)
	}
	client := moaapi.NewManageClient(conn)
	defer conn.Close()

	return f(client)

}

// WithMoa executes a Moa API call aganst the provided endpoint
func WithMoaControl(endpoint string, f func(moaapi.ControlClient) error) error {

	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", endpoint, service.MoactlGRPC), grpc.WithInsecure())
	if err != nil {
		return fmt.Errorf("failed to connect to moa service: %v", err)
	}
	client := moaapi.NewControlClient(conn)
	defer conn.Close()

	return f(client)

}
