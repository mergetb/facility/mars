package volume

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/xir/v0.3/go"
)

const (
	md_chunk_size   int = 512 * 1024 // assumed to be constant
	ext4_block_size int = 4096       // ext4 default
)

type Filesystem struct {
	Target string `json:"target,omitempty"`
}

type FindMnt struct {
	Filesystems []Filesystem `json:"filesystems,omitempty"`
}

func CreateDiskPool(r *xir.Resource, ds []*xir.Disk) error {
	// check if md is present
	md, err := mdPresent()
	if err != nil {
		return err
	}

	// found MD
	if md != "" {
		return nil
	}

	// assemble existing deactivated array
	err = mdAssemble()
	if err == nil {
		return nil
	}

	// wipe all disks
	for _, d := range ds {
		err := diskWipe(r, d)
		if err != nil {
			return err
		}
	}

	// create md using RAID0
	err = mdCreate(r, ds)
	if err != nil {
		return err
	}

	// throw an ext4 on it
	err = ext4Create(len(ds))
	if err != nil {
		return err
	}

	return nil
}

func MountDiskPool(mountpoint string) error {
	md, err := mdPresent()
	if err != nil {
		return err
	}

	if md == "" {
		return fmt.Errorf("mariner disk pool is not present")
	}

	mts, err := mdMountPoints(md)
	if err != nil {
		return err
	}

	for _, mt := range mts {
		if mt == mountpoint {
			log.Infof("mariner md '%s' already mounted on %s", md, mountpoint)
			return nil
		}
	}

	err = ext4Mount(md, mountpoint)
	if err != nil {
		return err
	}

	return nil
}

func mdPresent() (string, error) {
	files, err := ioutil.ReadDir("/dev/md")
	if err != nil {
		log.Infof("/dev/md not present; assume no arrays have been created")
		return "", nil
	}

	md := ""
	for _, file := range files {
		info, err := os.Stat(fmt.Sprintf("/dev/md/%s", file.Name()))
		if err != nil {
			return "", err
		}

		d := fmt.Sprintf("/dev/md/%s", info.Name())
		if !strings.Contains(d, "mariner") {
			continue
		}

		md = d
		log.Infof("discovered md device %s", md)
		break
	}

	if md == "" {
		return "", nil
	}

	args := []string{
		"--detail",
		md,
	}

	log.Infof("mdadm %+v", args)
	cmd := exec.Command("mdadm", args...)
	err = cmd.Run()
	if err != nil {
		return "", fmt.Errorf("mdadm failed: %v", err)
	}

	log.Infof("mariner md '%s' already present", md)
	return md, nil
}

func mdAssemble() error {
	args := []string{
		"--assemble",
		"--scan",
		"--name=mariner",
	}

	log.Infof("mdadm %+v", args)
	cmd := exec.Command("mdadm", args...)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("mdadm failed: %v", err)
	}

	log.Info("mariner md assembled")
	return nil
}

func mdCreate(r *xir.Resource, ds []*xir.Disk) error {
	args := []string{
		"--create",
		"/dev/md/mariner",
		"--run",
		"--level", "0",
		"--name=mariner",
		fmt.Sprintf("--raid-devices=%d", len(ds)),
	}

	// need '--force' for single disk arrays
	if len(ds) == 1 {
		args = append(args, "--force")
	}

	for _, d := range ds {
		path := fmt.Sprintf("/dev/%s", r.DiskName(d))
		args = append(args, path)
	}

	log.Infof("mdadm %+v", args)
	cmd := exec.Command("mdadm", args...)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("mdadm failed: %v", err)
	}

	log.Info("/dev/md/mariner created")
	return nil
}

func mdMountPoints(md string) ([]string, error) {
	args := []string{
		md,
		"--output", "TARGET",
		"--json",
	}

	log.Infof("findmnt %+v", args)
	cmd := exec.Command("findmnt", args...)

	// command failed, no mountpoints
	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, nil
	}

	var mnt FindMnt
	err = json.Unmarshal(out, &mnt)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal findmnt JSON: %v", err)
	}

	fs := []string{}
	for _, f := range mnt.Filesystems {
		fs = append(fs, f.Target)
	}

	return fs, nil
}

func diskWipe(r *xir.Resource, d *xir.Disk) error {
	path := fmt.Sprintf("/dev/%s", r.DiskName(d))
	args := []string{
		"--all",
		"--force",
		path,
	}

	log.Infof("wipefs %+v", args)
	cmd := exec.Command("wipefs", args...)
	err := cmd.Run()
	if err != nil {
		return err
	}

	log.Infof("cleared partition and filesystems on %s", path)
	return nil
}

func ext4Create(num_disks int) error {
	// set stride and stripe_width based on these recommendations:
	// https://gryzli.info/2015/02/26/calculating-filesystem-stride_size-and-stripe_width-for-best-performance-under-raid/

	stride := md_chunk_size / ext4_block_size
	stripe := stride * num_disks

	args := []string{
		"-F",
		"/dev/md/mariner",
		"-E", fmt.Sprintf("stride=%d", stride),
		"-E", fmt.Sprintf("stripe_width=%d", stripe),
	}

	log.Infof("mkfs.ext4 %+v", args)
	cmd := exec.Command("mkfs.ext4", args...)
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("mkfs.ext4 failed: %v", err)
	}

	log.Infof("formatted ext4 on /dev/md/mariner")
	return nil
}

func ext4Mount(md, mountpoint string) error {
	// create mount point
	err := os.MkdirAll(mountpoint, 0755)
	if err != nil && !os.IsExist(err) {
		return fmt.Errorf("could not create %s: %v", mountpoint, err)
	}

	args := []string{
		md,
		mountpoint,
	}

	log.Infof("mount %+v", args)
	cmd := exec.Command("mount", args...)
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("mount failed: %v", err)
	}

	log.Infof("mounted ext4 on /dev/md/mariner:%s", mountpoint)
	return nil
}
