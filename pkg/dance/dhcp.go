package dance

import (
	"net"
	"strings"

	"github.com/insomniacslk/dhcp/dhcpv4"
	"github.com/insomniacslk/dhcp/dhcpv4/server4"
	"github.com/insomniacslk/dhcp/dhcpv6"
	"github.com/insomniacslk/dhcp/dhcpv6/server6"
	log "github.com/sirupsen/logrus"
	facility "gitlab.com/mergetb/api/facility/v1/go"
)

const (
	listen4 = "0.0.0.0"
	listen6 = "::0"
	dfltLeaseSec = 6000 // legacy dflt
)

func (s *Server) dhcp4Server(ifx string) {

	laddr := net.UDPAddr{
		IP:   net.ParseIP(listen4),
		Port: 67,
	}
	server, err := server4.NewServer(ifx, &laddr,
		func(conn net.PacketConn, peer net.Addr, req *dhcpv4.DHCPv4) {
			s.handler4(conn, peer, req)
		},
	)
	if err != nil {
		log.Fatalf("new dhcp4 server: %v", err)
	}

	log.Infof("starting dhcp4 server on %s %s:67", ifx, listen4)

	server.Serve()

}

func (s *Server) dhcp6Server(ifx string) {

	laddr := net.UDPAddr{
		IP:   net.ParseIP(listen6),
		Port: 67,
	}
	server, err := server6.NewServer(ifx, &laddr,
		func(conn net.PacketConn, peer net.Addr, req dhcpv6.DHCPv6) {
			s.handler6(conn, peer, req)
		},
	)
	if err != nil {
		log.Fatalf("new dhcp6 server: %v", err)
	}

	log.Infof("starting dhcp6 server on %s %s:67", ifx, listen6)

	server.Serve()

}

func (s *Server) handler4(
	conn net.PacketConn, peer net.Addr, req *dhcpv4.DHCPv4) {

	//log.Printf("H4: %s", req.Summary())

	switch req.MessageType() {

	case dhcpv4.MessageTypeDiscover:
		s.handleDiscover4(conn, peer, req)

	case dhcpv4.MessageTypeRequest:
		s.handleRequest4(conn, peer, req)

	default:
		// Nothing to do
		return

	}

}

func (s *Server) handler6(
	conn net.PacketConn, peer net.Addr, req dhcpv6.DHCPv6) {

	switch req.Type() {

	case dhcpv6.MessageTypeSolicit:
		s.handleSolicit6(conn, peer, req)

	case dhcpv6.MessageTypeRequest:
		s.handleRequest6(conn, peer, req)

	default:
		// Nothing to do
		return

	}

}

func (s *Server) handleDiscover4(
	conn net.PacketConn, peer net.Addr, req *dhcpv4.DHCPv4) {

	ip := s.Cache.dhcp4Lookup(req.ClientHWAddr)
	if ip == nil {
		log.Warnf("%s not found", req.ClientHWAddr.String())

		if s.Alloc == nil {
			return
		}

		var err error
		ip, err = s.Alloc.Lease(req.HostName(), req.ClientHWAddr)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("get lease")
			return
		}

		log.Infof("allocated %s to %s/%s", ip, req.HostName(), req.ClientHWAddr.String())
	}

	bootfile := s.Cache.dhcp4FilenameLookup(req.ClientHWAddr)
	if bootfile == "" {
		log.Debugf("%s bootfile not found", req.ClientHWAddr.String())
	}

	ntwk := s.Cache.GetNetwork()

	resp, err := dhcpv4.NewReplyFromRequest(req,
		append(
			[]dhcpv4.Modifier{
				dhcpv4.WithMessageType(dhcpv4.MessageTypeOffer),
				dhcpv4.WithYourIP(ip),
			},
			replyOptionsForNet4(&ntwk, bootfile)...,
		)...,
	)
	if err != nil {
		log.Errorf("[%s] new reply from request: %v", req.ClientHWAddr, err)
		return
	}

	conn.WriteTo(resp.ToBytes(), peer)

}

func (s *Server) handleRequest4(
	conn net.PacketConn, peer net.Addr, req *dhcpv4.DHCPv4) {

	ip := s.Cache.dhcp4Lookup(req.ClientHWAddr)
	if ip == nil {
		log.Warnf("%s not found", req.ClientHWAddr.String())
		return
	}

	// attempt to renew a dhcp lease
	if s.Alloc != nil {
		err := s.Alloc.Renew(ip, req.ClientHWAddr)
		if err != nil {
			log.WithFields(log.Fields{"err": err, "ip": ip, "mac": req.ClientHWAddr}).Error("renew lease")
			return
		}
	}

	bootfile := s.Cache.dhcp4FilenameLookup(req.ClientHWAddr)
	if bootfile == "" {
		log.Debugf("%s bootfile not found", req.ClientHWAddr.String())
	}

	ntwk := s.Cache.GetNetwork()

	mods := append(
		[]dhcpv4.Modifier{
			dhcpv4.WithMessageType(dhcpv4.MessageTypeAck),
			dhcpv4.WithYourIP(ip),
		},
		replyOptionsForNet4(&ntwk, bootfile)...,
	)

	name := s.Cache.rdns4Lookup(ip)
	if name != "" {
		short := strings.TrimSuffix(name, "."+ntwk.Domain)
		mods = append(mods, dhcpv4.WithOption(dhcpv4.OptHostName(short)))
	}

	resp, err := dhcpv4.NewReplyFromRequest(req, mods...)
	if err != nil {
		log.Errorf("[%s] new reply from request: %v", req.ClientHWAddr, err)
		return
	}

	log.Infof("request4: acked %s to %s", ip.String(), req.ClientHWAddr.String())

	conn.WriteTo(resp.ToBytes(), peer)

}

type PrivateOptionCode struct {
	code uint8
}

func (z PrivateOptionCode) Code() uint8    { return z.code }
func (z PrivateOptionCode) String() string { return "private" }

func replyOptionsForNet4(n *facility.DanceNetwork, bootfile string) []dhcpv4.Modifier {

	mods := []dhcpv4.Modifier{
		dhcpv4.WithDNS(n.Addrv4()),
		dhcpv4.WithDomainSearchList(n.Search...),
		dhcpv4.WithGatewayIP(n.GatewayAsIP()),
		dhcpv4.WithRouter(n.GatewayAsIP()),
		dhcpv4.WithNetmask(n.SubnetMaskAsIPMask()),
		dhcpv4.WithOption(dhcpv4.OptServerIdentifier(n.Addrv4())),
	}

	for num, value := range n.DhcpOptions {
		mods = append(mods, dhcpv4.WithOption(dhcpv4.OptGeneric(
			PrivateOptionCode{code: uint8(num)}, value)),
		)
	}

	var lease_time uint32
	if n.LeaseTime != 0 {
		lease_time = n.LeaseTime
	} else {
		lease_time = dfltLeaseSec
	}
	mods = append(mods, dhcpv4.WithLeaseTime(lease_time))

	if n.NextServer != 0 {
		ip := Uint32AsIpv4(n.NextServer)
		mods = append(mods, dhcpv4.WithServerIP(ip))
		mods = append(mods,
			dhcpv4.WithOption(dhcpv4.OptTFTPServerName(ip.String())),
		)
	} else if System.network.NextServer != 0 {
		ip := Uint32AsIpv4(System.network.NextServer)
		mods = append(mods, dhcpv4.WithServerIP(ip))
		mods = append(mods,
			dhcpv4.WithOption(dhcpv4.OptTFTPServerName(ip.String())),
		)
	}

	if bootfile != "" {
		mods = append(mods,
			dhcpv4.WithOption(dhcpv4.OptBootFileName(bootfile)),
		)
	} else if n.TftpBootloader != "" {
		mods = append(mods,
			dhcpv4.WithOption(dhcpv4.OptBootFileName(n.TftpBootloader)),
		)
	}

	return mods

}

func (s *Server) handleSolicit6(
	conn net.PacketConn, peer net.Addr, req dhcpv6.DHCPv6) {

	msg, err := req.GetInnerMessage()
	if err != nil {
		log.Errorf("dhcpv6 get inner message: %v", err)
		return
	}

	mac, err := dhcpv6.ExtractMAC(req)
	if err != nil {
		log.Errorf("dhcpv6 extract mac: %v", err)
		return
	}

	ip := s.Cache.dhcp6Lookup(mac)
	if ip == nil {
		return
	}

	ntwk := s.Cache.GetNetwork()

	resp, err := dhcpv6.NewAdvertiseFromSolicit(msg,
		append(
			[]dhcpv6.Modifier{
				dhcpv6.WithIANA(dhcpv6.OptIAAddress{IPv6Addr: ip}),
			},
			replyOptionsForNet6(&ntwk)...,
		)...,
	)
	if err != nil {
		log.Errorf("[%s] new reply from request: %v", mac, err)
		return
	}

	conn.WriteTo(resp.ToBytes(), peer)

}

func (s *Server) handleRequest6(
	conn net.PacketConn, peer net.Addr, req dhcpv6.DHCPv6) {

	msg, err := req.GetInnerMessage()
	if err != nil {
		log.Errorf("dhcpv6 get inner message: %v", err)
		return
	}

	mac, err := dhcpv6.ExtractMAC(req)
	if err != nil {
		log.Errorf("dhcpv6 extract mac: %v", err)
		return
	}

	ip := s.Cache.dhcp6Lookup(mac)
	if ip == nil {
		return
	}

	ntwk := s.Cache.GetNetwork()

	resp, err := dhcpv6.NewReplyFromMessage(msg,
		append(
			[]dhcpv6.Modifier{
				dhcpv6.WithIANA(dhcpv6.OptIAAddress{IPv6Addr: ip}),
			},
			replyOptionsForNet6(&ntwk)...,
		)...,
	)
	if err != nil {
		log.Errorf("[%s] new reply from request: %v", mac, err)
		return
	}

	log.Infof("request6: acked %s to %s", ip.String(), mac.String())

	conn.WriteTo(resp.ToBytes(), peer)

}

func replyOptionsForNet6(n *facility.DanceNetwork) []dhcpv6.Modifier {

	return []dhcpv6.Modifier{
		dhcpv6.WithDNS(n.Addrv6()),
		dhcpv6.WithDomainSearchList(n.Search...),
	}

}
