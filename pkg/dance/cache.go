package dance

import (
	"encoding/binary"
	"net"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"

	facility "gitlab.com/mergetb/api/facility/v1/go"
)

// global cache
var System = NewCache(&facility.DanceNetwork{Domain: "system.marstb"})

type Cache struct {
	// the network configuration for this dance instance
	network *facility.DanceNetwork
	netmtx  sync.RWMutex

	// a set of ipv4 addresses indexed by mac for DHCP lookup
	dhcp4    map[uint64]uint32
	dhcp4mtx sync.RWMutex

	// a set of ipv4 addresses indexed by mac for DHCP lookup
	dhcp6    map[uint64][16]byte
	dhcp6mtx sync.RWMutex

	// a set of boot filenames indexed by mac for DHCP lookup
	boot    map[uint64]string
	bootmtx sync.RWMutex

	// a set of names indexed by ipv4 address for reverse DNS lookup
	rdns4    map[uint32]string
	rdns4mtx sync.RWMutex

	// a set of names indexed by ipv6 address for reverse DNS lookup
	rdns6    map[[16]byte]string
	rdns6mtx sync.RWMutex

	// a set of ipv4 addresses indexed by name for DNS lookup
	dns4    map[string][]uint32
	dns4mtx sync.RWMutex

	// a set of ipv4 addresses indext by wildcard suffix
	wild4    map[string][]uint32
	wild4mtx sync.RWMutex

	// a set of ipv4 addresses indexed by name for DNS lookup
	dns6    map[string][][16]byte
	dns6mtx sync.RWMutex

	// a set of srv records indexed by service fqdn
	srv    map[string][]*facility.DanceService
	srvmtx sync.RWMutex
}

func NewCache(dn *facility.DanceNetwork) *Cache {
	return &Cache{
		network: dn,
		dhcp4:   make(map[uint64]uint32),
		dhcp6:   make(map[uint64][16]byte),
		boot:    make(map[uint64]string),
		rdns4:   make(map[uint32]string),
		rdns6:   make(map[[16]byte]string),
		dns4:    make(map[string][]uint32),
		wild4:   make(map[string][]uint32),
		dns6:    make(map[string][][16]byte),
		srv:     make(map[string][]*facility.DanceService),
	}
}

func (c *Cache) UpdateNetwork(n *facility.DanceNetwork) {

	c.netmtx.Lock()
	c.network = n
	c.netmtx.Unlock()

}

func (c *Cache) GetNetwork() facility.DanceNetwork {

	c.netmtx.RLock()
	n := *c.network
	c.netmtx.RUnlock()

	return n

}

func (c *Cache) UpdateEntries(entries map[uint64]*facility.DanceEntry) {

	// dhcp
	c.dhcp4mtx.Lock()
	c.dhcp6mtx.Lock()
	for k, v := range entries {
		if v.Ipv4 != 0 {
			c.dhcp4[k] = v.Ipv4
		}
		if v.Ipv6 != nil {
			var ipv6 [16]byte
			copy(ipv6[:], v.Ipv6)
			c.dhcp6[k] = ipv6
		}
	}
	c.dhcp4mtx.Unlock()
	c.dhcp6mtx.Unlock()

	// dns
	c.rdns4mtx.Lock()
	c.rdns6mtx.Lock()
	c.dns4mtx.Lock()
	c.dns6mtx.Lock()
	for _, v := range entries {
		if len(v.Names) == 0 {
			continue
		}
		var ipv6 [16]byte
		copy(ipv6[:], v.Ipv6)

		if v.Ipv6 != nil {
			c.rdns6[ipv6] = v.Names[0]
		}
		if v.Ipv4 != 0 {
			c.rdns4[v.Ipv4] = v.Names[0]
		}

		for _, n := range v.Names {
			if v.Ipv4 != 0 {
				c.dns4[n] = append(c.dns4[n], v.Ipv4)
			}

			if v.Ipv6 != nil {
				c.dns6[n] = append(c.dns6[n], ipv6)
			}
		}
	}
	c.rdns4mtx.Unlock()
	c.rdns6mtx.Unlock()
	c.dns4mtx.Unlock()
	c.dns6mtx.Unlock()

	// boot
	c.bootmtx.Lock()
	for k, v := range entries {
		if v.TftpFilename != "" {
			c.boot[k] = v.TftpFilename
		}
	}
	c.bootmtx.Unlock()

}

func ipv6Eq(v1, v2 [16]byte) bool {

	for i, v := range v1 {
		if v != v2[i] {
			return false
		}
	}

	return true

}

func (c *Cache) RemoveEntries(entries map[uint64]*facility.DanceEntry) {

	var ok bool

	// dhcp
	c.dhcp4mtx.Lock()
	c.dhcp6mtx.Lock()
	for k := range entries {
		if _, ok = c.dhcp4[k]; ok {
			delete(c.dhcp4, k)
		}
		if _, ok = c.dhcp6[k]; ok {
			delete(c.dhcp6, k)
		}
	}
	c.dhcp4mtx.Unlock()
	c.dhcp6mtx.Unlock()

	// dns
	c.rdns4mtx.Lock()
	c.rdns6mtx.Lock()
	c.dns4mtx.Lock()
	c.dns6mtx.Lock()
	for _, v := range entries {
		if len(v.Names) == 0 {
			continue
		}

		var ipv6 [16]byte
		copy(ipv6[:], v.Ipv6)

		if _, ok = c.rdns6[ipv6]; ok {
			if c.rdns6[ipv6] != v.Names[0] {
				log.Warnf("name %s not in rdns6 record")
			} else {
				delete(c.rdns6, ipv6)
			}
		}

		if _, ok = c.rdns4[v.Ipv4]; ok {
			if c.rdns4[v.Ipv4] != v.Names[0] {
				log.Warnf("name %s not in rdns4 record")
			} else {
				delete(c.rdns4, v.Ipv4)
			}
		}

		for _, n := range v.Names {
			found4 := false
			found6 := false
			if _, ok = c.dns6[n]; ok {
				// n is tracked -- if we find the ipv6 provided, remove it
				var ipv6s [][16]byte
				for _, exist_ipv6 := range c.dns6[n] {
					if ipv6Eq(ipv6, exist_ipv6) {
						found6 = true
					} else {
						ipv6s = append(ipv6s, exist_ipv6)
					}

				}

				if len(ipv6s) == 0 {
					delete(c.dns6, n)
				} else {
					c.dns6[n] = ipv6s
				}

				if !found6 {
					log.Warnf("no dns6 record for ipv6: %v", ipv6)
				}
			}

			if _, ok = c.dns4[n]; ok {
				// n is tracked -- if we find the ipv4 provided, remove it
				var ipv4s []uint32
				for _, ipv4 := range c.dns4[n] {
					if ipv4 == v.Ipv4 {
						found4 = true
					} else {
						ipv4s = append(ipv4s, ipv4)
					}
				}
				if len(ipv4s) == 0 {
					delete(c.dns4, n)
				} else {
					c.dns4[n] = ipv4s
				}

				if !found4 {
					log.Warnf("no dns4 record for ipv4: %d", v.Ipv4)
				}
			}
		}
	}
	c.rdns4mtx.Unlock()
	c.rdns6mtx.Unlock()
	c.dns4mtx.Unlock()
	c.dns6mtx.Unlock()

	// boot
	c.bootmtx.Lock()
	for k := range entries {
		if _, ok = c.boot[k]; ok {
			delete(c.boot, k)
		}
	}
	c.bootmtx.Unlock()

}

func (c *Cache) UpdateServices(entries map[string][]*facility.DanceService) {

	c.srvmtx.Lock()

	for k, v := range entries {
		c.srv[k] = v
	}

	c.srvmtx.Unlock()

}

func (c *Cache) SetName4(name string, ip net.IP) {

	v := Ipv4AsUint32(ip)

	c.dns4mtx.Lock()
	n := strings.ToLower(name)
	ns := c.dns4[n]
	for _, x := range ns {
		if x == v {
			c.dns4mtx.Unlock()
			return
		}
	}
	c.dns4[name] = append(c.dns4[name], v)
	c.dns4mtx.Unlock()

}

func (c *Cache) SetName4Raw(data map[string][]uint32) {

	c.dns4mtx.Lock()
	for key, v := range data {
		// for all v, if v not already in c.dns4[k], add it
		// generate a map to save repeated list traversals
		k := strings.ToLower(key) // DNS names are case-insentive.
		cipv4 := make(map[uint32]bool)
		for _, ipv4 := range c.dns4[k] {
			cipv4[ipv4] = true
		}

		for _, ipv4 := range v {
			if _, ok := cipv4[ipv4]; !ok {
				c.dns4[k] = append(c.dns4[k], ipv4)
			}
		}
	}
	c.dns4mtx.Unlock()

}

func (c *Cache) RemoveName4Raw(data map[string][]uint32) {

	c.dns4mtx.Lock()
	for key, v := range data {
		// for all v, if v in c.dns4[k], remove it
		// generate a map to save repeated list traversals
		k := strings.ToLower(key) // DNS names are case-insentive.
		vipv4 := make(map[uint32]bool)
		for _, ipv4 := range v {
			vipv4[ipv4] = true
		}

		// generate a new list which is c.dns4 - v, and then shove it
		// into c.dns4[k]
		newdns4 := []uint32{}

		for _, ipv4 := range c.dns4[k] {
			if _, ok := vipv4[ipv4]; !ok {
				newdns4 = append(newdns4, ipv4)
			}
		}
		c.dns4[k] = newdns4
	}
	c.dns4mtx.Unlock()

}

func (c *Cache) SetWild4(name string, ip net.IP) {

	if name[0] != '.' {
		name = "." + name
	}

	v := Ipv4AsUint32(ip)

	c.wild4mtx.Lock()
	ns := c.wild4[name]
	for _, x := range ns {
		if x == v {
			c.wild4mtx.Unlock()
			return
		}
	}
	c.wild4[name] = append(c.wild4[name], v)
	c.wild4mtx.Unlock()

}

func (c *Cache) dhcp4Lookup(mac net.HardwareAddr) net.IP {

	k := MacAsUint64(mac)

	c.dhcp4mtx.RLock()
	addr, ok := c.dhcp4[k]
	if !ok {
		c.dhcp4mtx.RUnlock()
		return nil
	}

	buf := []byte{0, 0, 0, 0}
	binary.BigEndian.PutUint32(buf, addr)
	c.dhcp4mtx.RUnlock()

	return net.IP(buf)
}

func (c *Cache) dhcp6Lookup(mac net.HardwareAddr) net.IP {

	k := MacAsUint64(mac)

	c.dhcp6mtx.RLock()
	addr, ok := c.dhcp6[k]
	if !ok {
		c.dhcp6mtx.RUnlock()
		return nil
	}

	buf := []byte{
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
	}
	copy(buf[:], addr[:])
	c.dhcp6mtx.RUnlock()

	return net.IP(buf)
}

func (c *Cache) dhcp4FilenameLookup(mac net.HardwareAddr) string {

	k := MacAsUint64(mac)
	fname := ""

	c.bootmtx.RLock()
	fname, ok := c.boot[k]
	if !ok {
		c.bootmtx.RUnlock()
		return ""
	}

	c.bootmtx.RUnlock()

	return fname

}

func (c *Cache) dns4Lookup(name string) []net.IP {

	var ips []net.IP

	c.dns4mtx.RLock()

	n := strings.ToLower(name)
	addrs, ok := c.dns4[n]
	if !ok {
		c.dns4mtx.RUnlock()
		return nil
	}

	for _, x := range addrs {
		buf := []byte{0, 0, 0, 0}
		binary.BigEndian.PutUint32(buf, x)
		ips = append(ips, net.IP(buf))
	}
	c.dns4mtx.RUnlock()

	return ips
}

func (c *Cache) wild4Lookup(name string) []net.IP {

	c.wild4mtx.RLock()

	n := strings.ToLower(name)
	for wc, addrs := range c.wild4 {
		if strings.HasSuffix(n, wc) {

			var ips []net.IP

			for _, x := range addrs {
				buf := []byte{0, 0, 0, 0}
				binary.BigEndian.PutUint32(buf, x)
				ips = append(ips, net.IP(buf))
			}
			c.wild4mtx.RUnlock()
			return ips

		}
	}

	c.wild4mtx.RUnlock()
	return nil

}

func (c *Cache) dns6Lookup(name string) []net.IP {

	var ips []net.IP

	c.dns6mtx.RLock()
	n := strings.ToLower(name)
	addrs, ok := c.dns6[n]
	if !ok {
		c.dns6mtx.RUnlock()
		return nil
	}

	for _, x := range addrs {

		buf := []byte{
			0, 0, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 0,
		}
		copy(buf[:], x[:])
		ips = append(ips, net.IP(buf))
	}
	c.dns6mtx.RUnlock()

	return ips

}

func (c *Cache) srvLookup(name string) []*facility.DanceService {

	c.srvmtx.RLock()

	addrs, _ := c.srv[name]

	c.srvmtx.RUnlock()

	return addrs

}

func (c *Cache) rdns4Lookup(addr net.IP) string {

	k := Ipv4AsUint32(addr)

	c.rdns4mtx.RLock()
	name, ok := c.rdns4[k]
	if !ok {
		c.rdns4mtx.RUnlock()
		return ""
	}
	c.rdns4mtx.RUnlock()

	return name

}

func (c *Cache) rdns6Lookup(addr net.IP) string {

	k := [16]byte{
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
		0, 0, 0, 0,
	}
	copy(k[:], addr[:])

	c.rdns6mtx.RLock()
	name, ok := c.rdns6[k]
	if !ok {
		c.rdns6mtx.RUnlock()
		return ""
	}
	c.rdns6mtx.RUnlock()

	return name
}
