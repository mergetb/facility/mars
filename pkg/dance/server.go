package dance

import (
	"net"

	log "github.com/sirupsen/logrus"

	facility "gitlab.com/mergetb/api/facility/v1/go"
)

type Allocator interface {
	// request a dhcp lease and dns entry
	Lease(string, net.HardwareAddr) (net.IP, error)
	Renew(net.IP, net.HardwareAddr) error
}

type Server struct {
	Cache *Cache
	Alloc Allocator

	running bool
	control chan bool
}

func NewServer(dn *facility.DanceNetwork) *Server {
	return &Server{
		Cache:   NewCache(dn),
		running: false,
		control: make(chan bool),
	}
}

func (s *Server) Run() {

	if s.running == true {
		return
	}

	log.Infof("starting server threads for %s", s.Cache.network.Domain)

	bind_dhcp := !s.Cache.network.NoBindDhcp
	bind_dns := !s.Cache.network.NoBindDns
	ifx := s.Cache.network.ServiceInterface

	if bind_dhcp {
		if ifx == "" {
			log.Fatal("cannot bind DHCP server; no ServiceInterface provided")
		}
		go s.dhcp4Server(ifx)
		go s.dhcp6Server(ifx)
	} else {
		// ignore ifx
		go s.dhcp4Server("")
		go s.dhcp6Server("")
	}

	if bind_dns {
		if ifx == "" {
			log.Fatal("cannot bind DNS server; no ServiceInterface provided")
		}
		go s.dnsServer(ifx)
	} else {
		// ignore ifx
		go s.dnsServer("")
	}

	s.running = true
	<-s.control

}

func (s *Server) Stop() {

	if s.running {
		log.Infof("stopping server threads for %s", s.Cache.network.Domain)
		s.control <- true
	}

	s.running = false

}
