package dance

import (
	"math/rand"
	"net"
	"reflect"
	"strings"
	"syscall"

	"github.com/coredns/coredns/plugin/pkg/dnsutil"
	"github.com/coredns/coredns/request"
	"github.com/miekg/dns"
	log "github.com/sirupsen/logrus"
)

const (
	tipv4      = 1
	tipv6      = 2
	defaultTTL = 3600
)

func bindToDevice(conn net.PacketConn, device string) error {

	ptrVal := reflect.ValueOf(conn)
	val := reflect.Indirect(ptrVal)

	fdmember := val.FieldByName("fd")
	val1 := reflect.Indirect(fdmember)
	pfd := val1.FieldByName("pfd")
	val2 := reflect.Indirect(pfd)
	netFdPtr := val2.FieldByName("Sysfd")
	fd := int(netFdPtr.Int())

	return syscall.SetsockoptString(
		fd, syscall.SOL_SOCKET, syscall.SO_BINDTODEVICE, device,
	)

}

func (s *Server) dnsServer(ifx string) {

	addr := "0.0.0.0:53"
	if s.Cache.network.BindDnsAddr {
		addr = Uint32AsIpv4(s.Cache.network.DanceAddrV4).String() + ":53"
	}

	log.Infof("starting dns server; ifx:%s, addr:%s", ifx, addr)

	conn4, err := net.ListenPacket("udp4", addr)
	if err != nil {
		//TODO propagate error
		log.Fatalf("dns4 listen packet: %v", err)
	}

	if ifx != "" {
		err = bindToDevice(conn4, ifx)
		for err != nil {
			//TODO propagate error
			log.Warnf("dns4 bind to device %s: %v", ifx, err)
			err = bindToDevice(conn4, ifx)
		}
	}

	server := &dns.Server{
		PacketConn: conn4,
		Handler:    s,
	}
	server.ActivateAndServe()

}

func (s *Server) ServeDNS(w dns.ResponseWriter, r *dns.Msg) {

	qlen := len(r.Question)
	if qlen != 1 {
		if qlen == 0 {
			log.Warnf("got DNS request with no questions: %s", r)
		} else {
			log.Warnf("got DNS request with more than one question: %s", r)
		}
		m := new(dns.Msg)
		m.SetReply(r)
		m.Authoritative = true
		m.Rcode = dns.RcodeFormatError
		w.WriteMsg(m)
		return
	}

	rq := request.Request{W: w, Req: r}
	name := strings.Trim(rq.QName(), ".")

	var (
		addr net.IP
		rrs  []dns.RR
	)

	switch r.Question[0].Qtype {

	case dns.TypeA:
		addr = s.resolve4(name)
		if addr == nil {

			if s.resolve6(name) != nil {
				// don't forward if we just have an ipv6 address for the name and return an empty record
				// if we forward, it's likely to return NXDOMAIN, possibly causing ipv6 to break resolutions
				// https://www.blackmoreops.com/2014/10/29/ipv6-issues-localized-denial-of-service-caused-by-incorrect-nxdomain-responses-from-aaaa-queries/

				m := new(dns.Msg)
				m.SetReply(r)
				m.Authoritative = true
				m.Rcode = dns.RcodeSuccess
				w.WriteMsg(m)
				return
			}

			if s.forwardDNS() == "" {
				// if the forwarding dns server is empty,
				// don't forward and return NXDOMAIN

				m := new(dns.Msg)
				m.SetReply(r)
				m.Authoritative = true
				m.Rcode = dns.RcodeNameError
				w.WriteMsg(m)
				return
			}

			m1 := new(dns.Msg)
			m1.Id = r.Id
			m1.RecursionDesired = true
			m1.Question = make([]dns.Question, 1)
			m1.Question[0] = dns.Question{rq.QName(), dns.TypeA, dns.ClassINET}

			c := new(dns.Client)

			m2, _, err := c.Exchange(m1, s.forwardDNS())
			if err != nil {
				log.Errorf("forward request %s: %v", name, err)
				m := new(dns.Msg)
				m.SetReply(r)
				m.Authoritative = true
				m.Rcode = dns.RcodeNameError
				w.WriteMsg(m)
				return
			}
			w.WriteMsg(m2)
			return

		}
		rrs = append(rrs, &dns.A{
			Hdr: dns.RR_Header{
				Name:   rq.QName(),
				Rrtype: dns.TypeA,
				Class:  rq.QClass(),
				Ttl:    defaultTTL,
			},
			A: addr,
		})

	case dns.TypeAAAA:
		addr = s.resolve6(name)
		if addr == nil {

			if s.resolve4(name) != nil {
				// don't forward if we have an ipv4 address for the name and return an empty record
				// if we forward, it's likely to return NXDOMAIN, possibly causing ipv4 to break resolutions
				// https://www.blackmoreops.com/2014/10/29/ipv6-issues-localized-denial-of-service-caused-by-incorrect-nxdomain-responses-from-aaaa-queries/

				m := new(dns.Msg)
				m.SetReply(r)
				m.Authoritative = true
				m.Rcode = dns.RcodeSuccess
				w.WriteMsg(m)
				return
			}

			if s.forwardDNS() == "" {
				// if the forwarding dns server is empty,
				// don't forward and return NXDOMAIN

				m := new(dns.Msg)
				m.SetReply(r)
				m.Authoritative = true
				m.Rcode = dns.RcodeNameError
				w.WriteMsg(m)
				return
			}

			m1 := new(dns.Msg)
			m1.Id = r.Id
			m1.RecursionDesired = true
			m1.Question = make([]dns.Question, 1)
			m1.Question[0] = dns.Question{rq.QName(), dns.TypeAAAA, dns.ClassINET}

			c := new(dns.Client)
			m2, _, err := c.Exchange(m1, s.forwardDNS())
			if err != nil {
				//TODO forward
				m := new(dns.Msg)
				m.SetReply(r)
				m.Authoritative = true
				m.Rcode = dns.RcodeNameError
				w.WriteMsg(m)
				return
			}
			w.WriteMsg(m2)
			return

		}
		rrs = append(rrs, &dns.AAAA{
			Hdr: dns.RR_Header{
				Name:   rq.QName(),
				Rrtype: dns.TypeAAAA,
				Class:  rq.QClass(),
				Ttl:    defaultTTL,
			},
			AAAA: addr,
		})

	case dns.TypeMX:

		// no support for local mx records

		m1 := new(dns.Msg)
		m1.Id = r.Id
		m1.RecursionDesired = true
		m1.Question = make([]dns.Question, 1)
		m1.Question[0] = dns.Question{rq.QName(), dns.TypeMX, dns.ClassINET}

		c := new(dns.Client)
		m2, _, err := c.Exchange(m1, s.forwardDNS())
		if err != nil {
			//TODO forward
			m := new(dns.Msg)
			m.SetReply(r)
			m.Authoritative = false
			m.Rcode = dns.RcodeNameError
			w.WriteMsg(m)
			return
		}
		w.WriteMsg(m2)
		return

	case dns.TypePTR:

		bad_query := true
		if ip_str := dnsutil.ExtractAddressFromReverse(rq.QName()); ip_str != "" {
			if ip := net.ParseIP(ip_str); ip != nil {
				bad_query = false
				if name := s.resolveRdns(ip); name != "" {
					rrs = append(rrs, &dns.PTR{
						Hdr: dns.RR_Header{
							Name:   rq.QName(),
							Rrtype: dns.TypePTR,
							Class:  rq.QClass(),
							Ttl:    defaultTTL,
						},
						Ptr: name + ".", // must be fully qualified
					})
					break
				}
			}
		}
		if bad_query {
			log.Debugf("bad PTR query: %s", rq.QName())
			// could return an NXDOMAIN right now
			m := new(dns.Msg)
			m.SetReply(r)
			m.Authoritative = false
			m.Rcode = dns.RcodeNameError
			w.WriteMsg(m)
			return
		}

		m1 := new(dns.Msg)
		m1.Id = r.Id
		m1.RecursionDesired = true
		m1.Question = make([]dns.Question, 1)
		m1.Question[0] = dns.Question{rq.QName(), dns.TypePTR, dns.ClassINET}

		c := new(dns.Client)
		m2, _, err := c.Exchange(m1, s.forwardDNS())
		if err != nil {
			//TODO forward
			m := new(dns.Msg)
			m.SetReply(r)
			m.Authoritative = false
			m.Rcode = dns.RcodeNameError
			w.WriteMsg(m)
			return
		}
		w.WriteMsg(m2)
		return

	case dns.TypeSRV:

		srvs := s.Cache.srvLookup(name)

		if len(srvs) > 0 {
			for _, x := range srvs {

				srv := &dns.SRV{
					Hdr: dns.RR_Header{
						Name:   rq.QName(),
						Rrtype: dns.TypeSRV,
						Class:  rq.QClass(),
					},
					Priority: uint16(x.Priority),
					Weight:   uint16(x.Weight),
					Port:     uint16(x.Port),
					Target:   x.Target + ".",
				}

				rrs = append(rrs, srv)

			}
			a := &dns.Msg{}
			a.SetReply(r)
			a.Compress = true
			a.Authoritative = true
			a.Answer = rrs
			rq.SizeAndDo(a)
			err := w.WriteMsg(a)
			if err != nil {
				log.Error(err)
			}

		} else {
			m1 := new(dns.Msg)
			m1.Id = r.Id
			m1.RecursionDesired = true
			m1.Question = make([]dns.Question, 1)
			m1.Question[0] = dns.Question{rq.QName(), dns.TypeSRV, dns.ClassINET}

			c := new(dns.Client)
			m2, _, err := c.Exchange(m1, s.forwardDNS())
			if err != nil {
				//TODO forward
				m := new(dns.Msg)
				m.SetReply(r)
				m.Authoritative = true
				m.Rcode = dns.RcodeNameError
				w.WriteMsg(m)
				return
			}
			w.WriteMsg(m2)
			return
		}

	}

	if len(rrs) == 0 {
		return
	}

	/*
		srv := &dns.SRV{
			Hdr: dns.RR_Header{
				Name:   fmt.Sprintf("_%s.%s", rq.Proto(), rq.QName()),
				Rrtype: dns.TypeSRV,
				Class:  rq.QClass(),
			},
		}
		port, _ := strconv.Atoi(rq.Port())
		srv.Port = uint16(port)
		srv.Target = "."
	*/

	/*
		soa := &dns.SOA{
			Hdr: dns.RR_Header{
				Name:   fmt.Sprintf("_%s.%s", rq.Proto(), rq.QName()),
				Rrtype: dns.TypeSOA,
				Class:  rq.QClass(),
			},
			Ns:     "ground-control.io",
			Serial: 47,
		}
	*/

	a := &dns.Msg{}
	a.SetReply(r)
	a.Compress = true
	a.Authoritative = true
	//a.Answer = append(rrs, srv)
	a.Answer = rrs
	//a.Answer = append(rrs, soa)
	rq.SizeAndDo(a)
	err := w.WriteMsg(a)
	if err != nil {
		log.Errorf("sending response to %s: %v", rq.QName(), err)
	}

}

func (s *Server) resolve4(name string) net.IP {

	addrs := s.Cache.dns4Lookup(name) //-----------------local entry
	if len(addrs) == 0 {
		addrs = s.Cache.wild4Lookup(name) //------------local wildcard
		if len(addrs) == 0 {
			addrs = System.dns4Lookup(name) //-----------global entry
			if len(addrs) == 0 {
				addrs = System.wild4Lookup(name) //------global wildcard
				if len(addrs) == 0 {
					return nil
				}
			}
		}
	}

	return pickAddr(addrs)

}

func (s *Server) resolve6(name string) net.IP {

	addrs := s.Cache.dns6Lookup(name)
	if len(addrs) == 0 {
		addrs = System.dns6Lookup(name)
		if len(addrs) == 0 {
			return nil
		}
	}

	return pickAddr(addrs)

}

func (s *Server) resolveRdns(ip net.IP) string {
	name := ""
	if ip.To4() != nil {
		name = s.Cache.rdns4Lookup(ip)
		if name == "" {
			name = System.rdns4Lookup(ip)
		}
	} else {
		name = s.Cache.rdns6Lookup(ip)
		if name == "" {
			name = System.rdns6Lookup(ip)
		}
	}
	return name
}

func pickAddr(addrs []net.IP) net.IP {

	if len(addrs) == 0 {
		return nil
	} else if len(addrs) == 1 {
		return addrs[0]
	} else {
		n := int(rand.Int31n(int32(len(addrs))))
		return addrs[n]
	}

}

func (s *Server) forwardDNS() string {

	// don't forward if 0.0.0.0 or the dance server itself
	if s.Cache.network.ForwardDns == 0 || s.Cache.network.ForwardDns == s.Cache.network.DanceAddrV4 {
		return ""
	}

	return Uint32AsIpv4(s.Cache.network.ForwardDns).String() + ":53"
}
