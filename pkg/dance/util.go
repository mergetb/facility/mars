package dance

import (
	"encoding/binary"
	"net"
)

func MacAsUint64(mac net.HardwareAddr) uint64 {

	_mac := []byte{0, 0, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]}

	return binary.BigEndian.Uint64(_mac)

}

func Ipv4AsUint32(ip net.IP) uint32 {

	return binary.BigEndian.Uint32(ip.To4())

}

func Uint64AsMac(x uint64) net.HardwareAddr {

	buf := []byte{0, 0, 0, 0, 0, 0, 0, 0}
	binary.BigEndian.PutUint64(buf, x)
	return net.HardwareAddr(buf[2:])

}

func Uint32AsIpv4(x uint32) net.IP {

	buf := []byte{0, 0, 0, 0}
	binary.BigEndian.PutUint32(buf, x)
	return net.IP(buf)

}
