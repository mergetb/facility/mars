package config

import (
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal"
)

// Return the heartbeat interval from mars config
// Returns 0 if no config present
func HeartbeatIntervalFromConfig() time.Duration {
	config, err := GetConfig()
	if err != nil {
		log.Errorf("failed to read mars config: %+v", err)
		return 0
	}

	return time.Second * time.Duration(config.Reconcilers.HeartbeatIntervalSec)
}

// Return the heartbeat grace period from mars config
// Returns -1 if heartbeat monitoring is disabled
// Returns 0 if no config present
func HeartbeatGracePeriodFromConfig() time.Duration {
	// disable by request
	if !internal.MonitorEtcdServer {
		return -1
	}

	config, err := GetConfig()
	if err != nil {
		log.Errorf("failed to read mars config: %+v", err)
		return 0
	}

	return time.Second * time.Duration(config.Reconcilers.HeartbeatGracePeriodSec)
}
