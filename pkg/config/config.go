package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/service"
)

const (
	DefaultConfigPath              string = "/etc/mars-config.yml"
	DockerIoUsername               string = "isirobot"
	DockerIoRoToken                string = "710e612e-f430-4499-b4ec-c32c9126a6fb"
	DefaultHeartbeatIntervalSec    int = 60
	DefaultHeartbeatGracePeriodSec int = 30
)

type KeyPair struct {
	Private string
	Public  string
}

type Credentials struct {
	Username string
	Password string
	Token    string
}

func (c *Credentials) setDefaults(dflt *Credentials) {
	if c.Username == "" {
		c.Username = dflt.Username
	}

	if c.Password == "" {
		c.Password = dflt.Password
	}

	if c.Token == "" {
		c.Token = dflt.Token
	}
}

type CredentialsWithSsh struct {
	Credentials `yaml:",inline"`
	Ssh         KeyPair `yaml:",omitempty"`
}

type TLSGRPCConfig struct {
	Cacert string `yaml:",omitempty"`
	Cert   string `yaml:",omitempty"`
	Key    string `yaml:",omitempty"`
}

type ServiceConfig struct {
	Address     string        `yaml:",omitempty"`
	Port        int           `yaml:",omitempty"`
	TLS         TLSGRPCConfig `yaml:",omitempty"`
	Credentials Credentials   `yaml:",omitempty"`
}

func (s *ServiceConfig) setDefaults(dflt *ServiceConfig) {
	if s.Address == "" {
		s.Address = dflt.Address
	}

	if s.Port == 0 {
		s.Port = dflt.Port
	}

	if s.TLS.Cacert == "" {
		s.TLS.Cacert = dflt.TLS.Cacert
	}

	if s.TLS.Cert == "" {
		s.TLS.Cert = dflt.TLS.Cert
	}

	if s.TLS.Key == "" {
		s.TLS.Key = dflt.TLS.Key
	}

	s.Credentials.setDefaults(&dflt.Credentials)
}

func (s *ServiceConfig) String() string {
	return fmt.Sprintf("%s:%d", s.Address, s.Port)
}

type InfrapodServices struct {
	Foundry ServiceConfig
	Sled    ServiceConfig

	// third paty
	Etcd  ServiceConfig
	Minio ServiceConfig
}

type InfraserverServices struct {
	Apiserver ServiceConfig
	Canopy    ServiceConfig
	Metal     ServiceConfig

	// third party
	Etcd  ServiceConfig
	Minio ServiceConfig
}

type ServicesConfig struct {
	Infrapod    InfrapodServices
	Infraserver InfraserverServices
	Portal      ServiceConfig
}

type Registry struct {
	Address     string
	NoVerify    bool
	Credentials Credentials `yaml:",omitempty"`
}

type Container struct {
	Registry Registry
	Name     string
	Tag      string
	LogLevel string // will set env. var LOGLEVEL in spawned container
}

func (c *Container) setDefaults(dflt *Container) {
	if c.Registry.Address == "" {
		c.Registry.Address = dflt.Registry.Address
		c.Registry.NoVerify = dflt.Registry.NoVerify
	}

	c.Registry.Credentials.setDefaults(&dflt.Registry.Credentials)

	if c.Name == "" {
		c.Name = dflt.Name
	}

	if c.Tag == "" {
		c.Tag = dflt.Tag
	}

}

func (c *Container) String() string {
	return c.RegUrl()
}

func (c *Container) RegUrl() string {
	return fmt.Sprintf("%s/%s:%s", c.Registry.Address, c.Name, c.Tag)
}

// Per-mtz images creted by infrapod service
type InfrapodImages struct {
	Podinit Container
	Dance   Container
	Foundry Container
	Sled    Container
	Tftp    Container
	Moactl  Container

	// third party
	Etcd  Container
	Minio Container
}

// Images supporting Mars operation on the infraserver
type InfraserverImages struct {
	Apiserver Container
	Canopy    Container
	Cniman    Container
	Infrapod  Container
	Mariner   Container // does not run on the infraserver, but convenient to put here
	Metal     Container
	Wireguard Container

	// third party
	Etcd  Container
	Frr   Container
	Minio Container
}

// GatewayImages run on Gateway machines.
type GatewayImages struct {
	Pathfinder   Container
	ReverseProxy Container
}

// Images used by the Moa subsystem
type MoaImages struct {
	Fastclick Container
	Moa       Container
}

type ImagesConfig struct {
	DefaultRegistry Registry
	DefaultTag      string
	Infrapod        InfrapodImages
	Infraserver     InfraserverImages
	Gateway         GatewayImages
	Moa             MoaImages
}

type NetworkConfig struct {
	ForwardDNS string

	// Expected external open port range for ingresses.
	ExternalPortMin int
	ExternalPortMax int
}

func (n *NetworkConfig) setDefaults(dflt *NetworkConfig) {
	if n.ForwardDNS == "" {
		n.ForwardDNS = dflt.ForwardDNS
	}

	if n.ExternalPortMin == 0 {
		n.ExternalPortMin = 8192
	}

	if n.ExternalPortMax == 0 {
		n.ExternalPortMax = n.ExternalPortMin + 1000
	}
}

type ReconcilersConfig struct {
	HeartbeatIntervalSec    int
	HeartbeatGracePeriodSec int
}

func (r *ReconcilersConfig) setDefaults(dflt *ReconcilersConfig) {
	if r.HeartbeatIntervalSec == 0 {
		r.HeartbeatIntervalSec = dflt.HeartbeatIntervalSec
	}

	if r.HeartbeatGracePeriodSec == 0 {
		r.HeartbeatGracePeriodSec = dflt.HeartbeatGracePeriodSec
	}
}

// TODO for mars
/*
type AlertConfig struct {
	Address  string
	Port     int
	Username string
	Password string
	Type     string
}
*/

// Config is the MARS top level configuration object
type Config struct {
	Services    ServicesConfig
	Images      ImagesConfig
	Network     NetworkConfig
	Ops         CredentialsWithSsh
	IPMI        Credentials
	Reconcilers ReconcilersConfig

	//Alerts   []AlertConfig `yaml:",omitempty" json:",omitempty"`
}

func getConfigFromYaml(filename string) (*Config, error) {

	// dont want a race on the config file, so enter lock to read
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("could not read configuration file %s: %v", filename, err)
	}

	cfg := new(Config)
	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		return nil, fmt.Errorf("could not parse configuration file %v", err)
	}

	return cfg, nil
}

func setDefaultServices(cfg *Config) error {

	// Infrapod services
	{
		c := &cfg.Services.Infrapod

		c.Foundry.setDefaults(&ServiceConfig{
			Address: "foundry",
			Port:    47001, // hardcoded in foundry
		})

		c.Sled.setDefaults(&ServiceConfig{
			Address: "sled",
			Port:    service.SledGRPC,
		})

		c.Etcd.setDefaults(&ServiceConfig{
			Address: "etcd",
			Port:    2379,
		})

		c.Minio.setDefaults(&ServiceConfig{
			Address: "minio",
			Credentials: Credentials{
				Username: "mars",
				Password: "muffins!",
			},
		})
	}

	// Infraserver services
	{
		// Mars services have API handlers on the same IP address, which is aliased as "apiserver"

		c := &cfg.Services.Infraserver

		c.Apiserver.setDefaults(&ServiceConfig{
			Address: "apiserver",
			Port:    service.ApiserverGRPC,
		})

		c.Canopy.setDefaults(&ServiceConfig{
			Address: "apiserver",
			Port:    service.CanopyGRPC,
		})

		c.Metal.setDefaults(&ServiceConfig{
			Address: "apiserver",
			Port:    service.MetalGRPC,
		})

		c.Etcd.setDefaults(&ServiceConfig{
			Address: "etcd",
			Port:    2379,
		})

		c.Minio.setDefaults(&ServiceConfig{
			Address: "minio",
			Credentials: Credentials{
				Username: "mars",
				Password: "muffins!",
			},
		})
	}

	return nil
}

func setDefaultImages(cfg *Config) error {
	if cfg.Images.DefaultRegistry.Address == "" {
		cfg.Images.DefaultRegistry = Registry{
			Address:     "registry.gitlab.com",
			NoVerify:    false,
			Credentials: Credentials{},
		}
	}

	if cfg.Images.DefaultTag == "" {
		cfg.Images.DefaultTag = "latest"
	}

	// Infrapod images
	{
		c := &cfg.Images.Infrapod

		c.Podinit.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/podinit",
		})

		c.Dance.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/dance",
		})

		c.Foundry.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/tech/foundry/foundry",
		})

		c.Sled.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/sled",
		})

		c.Tftp.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/tftp",
		})

		c.Moactl.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/moactld",
		})

		c.Etcd.setDefaults(&Container{
			Registry: Registry{
				Address: "quay.io",
			},
			Tag:  "v3.4.14",
			Name: "coreos/etcd",
		})

		c.Minio.setDefaults(&Container{
			Registry: Registry{
				Address: "docker.io",
				Credentials: Credentials{
					Username: DockerIoUsername,
					Password: DockerIoRoToken,
				},
			},
			Tag:  "RELEASE.2021-01-16T02-19-44Z",
			Name: "minio/minio",
		})
	}

	// Infraserver images
	{
		c := &cfg.Images.Infraserver

		c.Apiserver.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/apiserver",
		})

		c.Canopy.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/canopy",
		})

		c.Cniman.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/cniman",
		})

		c.Infrapod.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/infrapod",
		})

		c.Mariner.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/mariner",
		})

		c.Metal.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/metal",
		})

		c.Wireguard.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/wireguard",
		})

		c.Etcd.setDefaults(&Container{
			Registry: Registry{
				Address: "quay.io",
			},
			Tag:  "v3.4.14",
			Name: "coreos/etcd",
		})

		c.Frr.setDefaults(&Container{
			Registry: Registry{
				Address: "quay.io",
			},
			Tag:  "solovni",
			Name: "mergetb/frr",
		})

		c.Minio.setDefaults(&Container{
			Registry: Registry{
				Address: "docker.io",
				Credentials: Credentials{
					Username: DockerIoUsername,
					Password: DockerIoRoToken,
				},
			},
			Tag:  "RELEASE.2021-01-16T02-19-44Z",
			Name: "minio/minio",
		})
	}

	// Moa Images, for the emu server
	{
		c := &cfg.Images.Moa

		c.Fastclick.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/fastclick",
		})

		c.Moa.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/moa",
		})

		// moactl lives in the infrapod images
	}

	// Gateway Images
	{
		c := &cfg.Images.Gateway

		c.Pathfinder.setDefaults(&Container{
			Registry: cfg.Images.DefaultRegistry,
			Tag:      cfg.Images.DefaultTag,
			Name:     "mergetb/facility/mars/pathfinder",
		})

		c.ReverseProxy.setDefaults(&Container{
			Registry: Registry{
				Address: "docker.io",
				Credentials: Credentials{
					Username: DockerIoUsername,
					Password: DockerIoRoToken,
				},
			},
			Tag:  "v2.10",
			Name: "traefik",
		})
	}

	return nil
}

func setDefaultNetwork(cfg *Config) error {

	// Network config
	{
		c := &cfg.Network

		c.setDefaults(&NetworkConfig{
			ForwardDNS:      "8.8.8.8",
			ExternalPortMin: 8192,
			ExternalPortMax: 9192,
		})
	}

	return nil
}

func setDefaultIPMI(cfg *Config) error {

	// IPMI credentials
	{

		c := &cfg.IPMI

		c.setDefaults(&Credentials{
			Username: "ADMIN",
			Password: "ADMIN",
		})
	}

	return nil
}

func setDefaultReconcilers(cfg *Config) error {

	// Reconciler configs
	{
		c := &cfg.Reconcilers

		c.setDefaults(&ReconcilersConfig{
			HeartbeatIntervalSec:    DefaultHeartbeatIntervalSec,
			HeartbeatGracePeriodSec: DefaultHeartbeatGracePeriodSec,
		})
	}

	return nil
}

func SetDefaults(cfg *Config) error {
	err := setDefaultImages(cfg)
	if err != nil {
		return fmt.Errorf("could not set images defaults: %v", err)
	}

	err = setDefaultServices(cfg)
	if err != nil {
		return fmt.Errorf("could not set services defaults: %v", err)
	}

	err = setDefaultNetwork(cfg)
	if err != nil {
		return fmt.Errorf("could not set network defaults: %v", err)
	}

	err = setDefaultIPMI(cfg)
	if err != nil {
		return fmt.Errorf("could not set ipmi defaults: %v", err)
	}

	err = setDefaultReconcilers(cfg)
	if err != nil {
		return fmt.Errorf("could not set reconcilers defaults: %v", err)
	}

	return nil
}

func InitializeConfig(filename string, force bool) error {
	// parse config from yaml
	ycfg, err := getConfigFromYaml(filename)
	if err != nil {
		return err
	}

	// determine if we have a config yet in storage
	cfg, err := storage.GetConfig()
	if err == nil && cfg != nil {
		if !force {
			return fmt.Errorf("config already exists")
		}
	}

	err = SetDefaults(ycfg)
	if err != nil {
		return err
	}

	return SetConfig(ycfg)
}

func SetConfig(cfg *Config) error {
	buf, err := yaml.Marshal(cfg)
	if err != nil {
		return fmt.Errorf("config encode: %v", err)
	}

	return storage.SetConfig(buf)
}

func GetConfig() (*Config, error) {
	cfg := new(Config)

	dat, err := storage.GetConfig()
	if err != nil {
		return nil, fmt.Errorf("config read: %v", err)
	}
	if dat == nil {
		// return the default config
		err = SetDefaults(cfg)
		if err != nil {
			return nil, err
		}

	} else {
		err = yaml.Unmarshal(dat, cfg)
		if err != nil {
			return nil, fmt.Errorf("config decode: %v", err)
		}
	}

	return cfg, nil
}
