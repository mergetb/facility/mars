package connect

import (
	"crypto/tls"
	"fmt"

	"gitlab.com/mergetb/facility/mars/pkg/config"
	"google.golang.org/grpc/credentials"
)

func PortalEndpoint(cfg *config.Config) (string, error) {

	addr := cfg.Services.Portal.Address
	port := cfg.Services.Portal.Port

	if addr == "" || port == 0 {
		return "", fmt.Errorf("Portal endpoint not configured")
	}

	return fmt.Sprintf("%s:%d", addr, port), nil
}

func PortalCreds() (credentials.TransportCredentials, string, error) {

	cfg, err := config.GetConfig()
	if err != nil {
		return nil, "", err
	}

	ep, err := PortalEndpoint(cfg)
	if err != nil {
		return nil, "", err
	}

	// var pool *x509.CertPool

	// cacert := cfg.Services.Portal.TLS.Cacert

	// if cacert != "" {

	// 	log.Debugf("Using given portal CA cert to authenticate the portal")

	// 	pool = x509.NewCertPool()
	// 	if !pool.AppendCertsFromPEM([]byte(cacert)) {
	// 		return nil, "", fmt.Errorf("failed to append portal cert")
	// 	}
	// }
	// else we are using the system CA certs as pool will be nil

	// svrName, _, err := net.SplitHostPort(ep)
	// if err != nil {
	// 	return nil, "", fmt.Errorf("bad endpoint %s: %s", ep, err)
	// }

	// TODO support for Portal.Cert

	creds := credentials.NewTLS(
		&tls.Config{
			// ServerName:         svrName,
			// RootCAs:            pool,
			InsecureSkipVerify: true,
		},
	)

	return creds, ep, nil
}
