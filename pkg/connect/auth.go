package connect

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// PortalLogin - login to the portal and set teh portal token in the system configuraion
func PortalLogin() error {

	creds, endpoint, err := PortalCreds()
	if err != nil {
		return err
	}

	log.Debugf("Connecting to the portal @ %s", endpoint)

	cfg, err := config.GetConfig()
	if err != nil {
		return err
	}

	user := cfg.Services.Portal.Credentials.Username
	pass := cfg.Services.Portal.Credentials.Password

	if user == "" || pass == "" {
		return fmt.Errorf("Portal Configuration is missing username and/or password.")
	}

	conn, err := grpc.Dial(endpoint,
		grpc.WithTransportCredentials(creds), // no token needed for portal login.
	)

	if err != nil {
		return fmt.Errorf("grpc dial: %v", err)
	}
	defer conn.Close()

	cli := portal.NewIdentityClient(conn)

	log.Debugf("Logging into the portal %s @ %s", user, endpoint)

	resp, err := cli.Login(context.TODO(), &portal.LoginRequest{
		Username: user,
		Password: pass,
	})
	if err != nil {
		return fmt.Errorf("portal login failed: %v", err)
	}

	cfg.Services.Portal.Credentials.Token = resp.Token

	// write out the token
	err = config.SetConfig(cfg)
	if err != nil {
		return err
	}

	return nil
}

func withPortalToken(
	ctx context.Context,
	method string,
	req, reply interface{},
	cc *grpc.ClientConn,
	invoker grpc.UnaryInvoker,
	opts ...grpc.CallOption,
) error {

	cfg, err := config.GetConfig()
	if err == nil {

		tk := cfg.Services.Portal.Credentials.Token

		if tk != "" {
			ctx = metadata.AppendToOutgoingContext(ctx, "Authorization", fmt.Sprintf("Bearer %s", tk))
		}
	}

	// No token, just passthrough.
	return invoker(ctx, method, req, reply, cc, opts...)
}
