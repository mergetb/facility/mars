package connect

import (
	"fmt"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"google.golang.org/grpc"
)

func PortalCommission(f func(portal.CommissionClient) error) error {

	conn, err := portalGrpcConn()
	if err != nil {
		return fmt.Errorf("portal connect error: %s", err)
	}
	defer conn.Close()

	cli := portal.NewCommissionClient(conn)
	return f(cli)
}

func PortalIdentity(f func(portal.IdentityClient) error) error {

	conn, err := portalGrpcConn()
	if err != nil {
		return fmt.Errorf("portal connect error: %s", err)
	}
	defer conn.Close()

	cli := portal.NewIdentityClient(conn)
	return f(cli)
}

func PortalWireguard(f func(portal.WireguardClient) error) error {

	conn, err := portalGrpcConn()
	if err != nil {
		return fmt.Errorf("portal connect error: %s", err)
	}
	defer conn.Close()

	cli := portal.NewWireguardClient(conn)
	return f(cli)
}

func portalGrpcConn() (*grpc.ClientConn, error) {

	err := PortalLogin()
	if err != nil {
		return nil, err
	}

	creds, endpoint, err := PortalCreds()
	if err != nil {
		return nil, err
	}

	conn, err := grpc.Dial(
		endpoint,
		grpc.WithTransportCredentials(creds),
		grpc.WithUnaryInterceptor(
			grpc_middleware.ChainUnaryClient(
				withPortalToken,
			),
		),
	)
	if err != nil {
		return nil, fmt.Errorf("grpc dial: %v", err)
	}

	return conn, nil
}
