package main

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"time"

	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/state"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	rec "gitlab.com/mergetb/tech/reconcile"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	"github.com/golang/protobuf/proto"
	"go.etcd.io/etcd/client/v3"
)

var delay int = 0
var delaymax int = -1
var seed int = -1

func vmKey(hostname, fqvm string) string {
	return fmt.Sprintf("/vm/%s/%s", hostname, fqvm)
}

func vmFqvm(name, mzid string) string {
	return fmt.Sprintf("%s.%s", name, mzid)
}

func readConfFile(filename string) (*TestConf, error) {
	dat, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var conf TestConf
	err = json.Unmarshal(dat, &conf)
	if err != nil {
		return nil, err
	}

	return &conf, nil
}

func applyOption(key, value string) error {
	var err error

	switch key {
	case "delay":
		switch value {
		case "random":
			delay = -1
		default:
			delay, err = strconv.Atoi(value)
			if err != nil {
				return err
			}
		}

		log.Infof("Setting inter-op delay to %d", delay)
	case "delaymax":
		delaymax, err = strconv.Atoi(value)
		if err != nil {
			return err
		}

		log.Infof("Setting inter-op delaymax to %d", delaymax)
	case "seed":
		seed, err = strconv.Atoi(value)
		if err != nil {
			return err
		}

		log.Infof("Setting seed to %d", seed)
	default:
		return fmt.Errorf("Could not apply option %s:%s", key, value)
	}

	return nil
}

func applyDelay() error {
	d := delay
	if delay == -1 {
		d = rand.Intn(delaymax)
	}
	time.Sleep(time.Duration(int32(d)) * time.Second)
	return nil
}

func initRng() {
	s := int64(seed)
	if s < 0 {
		s = time.Now().UTC().UnixNano()
	}

	rand.Seed(s)
	log.Infof("Generated seed %d", s)
}

func deployVm(kv clientv3.KV, fqvm, hostname string, vm *state.Vm) error {
	msg, err := proto.Marshal(vm)
	if err != nil {
		return err
	}

	_, err = kv.Put(context.TODO(), vmKey(hostname, fqvm), string(msg))
	return err
}

func destroyVm(kv clientv3.KV, fqvm, hostname string) error {
	_, err := kv.Delete(context.TODO(), vmKey(hostname, fqvm))
	return err
}

func buildVm(
	mzid string,
	hostname string,
	procs *xir.ProcAllocation,
	memory *xir.MemoryAllocation,
	nics *xir.NICsAllocation,
	disks *xir.DisksAllocation,
	nic_spec *xir.NICSpec,
) *state.Vm {

	return &state.Vm{
		Mzid: mzid,
		Vm: &portal.VirtualMachine{
			VmAlloc: &xir.ResourceAllocation{
				Resource: "resource",
				Facility: "facility",
				Mzid:     mzid,
				Node:     hostname,
				Procs:    procs,
				Memory:   memory,
				NICs:     nics,
				Disks:    disks,
				Model: &xir.Node{
					Image: &xir.StringConstraint{
						Op:    xir.Operator_EQ,
						Value: "fedora-33",
					},
					NIC: nic_spec,
				},
			},
			Users: []*portal.UserInfo{
				{Name: "Test"},
			},
		},
	}
}

func watchVm(rec *reconcile.Reconciler, ch chan error) {
	for {
		comp, err := rec.IsComplete()
		if err != nil {
			ch <- err
			break
		}

		if err != nil {
			ch <- fmt.Errorf("%v", err)
		} else if comp {
			ch <- nil
		}
	}

	close(ch)
}

func testVm(fqvm, hostname string) error {
	key := "mariner"
	subkey := vmKey(hostname, fqvm)
	r := rec.NewReconcilerRaw(storage.EtcdClient, subkey, key, key, "")

	timeout_sec := time.Duration(30 * time.Second)
	for {
		ch := make(chan error)
		go watchVm(r, ch)
		select {
		case result := <-ch:
			return result
		case <-time.After(timeout_sec):
			return fmt.Errorf("vm operation timed out")
		}
	}
}

func main() {
	if len(os.Args) != 2 {
		panic(fmt.Sprintf("Usage: %s <test.json>", os.Args[0]))
	}

	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	conf, err := readConfFile(os.Args[1])
	if err != nil {
		panic(err)
	}

	for _, o := range conf.Options {
		err = applyOption(o.Key, o.Value)
		if err != nil {
			panic(err)
		}
	}

	// inititalize rng
	initRng()

	err = storage.InitMarsEtcdClient()
	if err != nil {
		panic(err)
	}

	kv := clientv3.NewKV(storage.EtcdClient)

	mzid := "r0.e0.p0"
	for _, unit := range conf.Units {
		op := unit.Op
		typ := unit.Type
		fqvm := vmFqvm(unit.Name, mzid)

		log.Infof("Running op:%s vm:%s", op, fqvm)

		switch op {
		case "start":
			vm, err := createVm(typ, mzid, hostname)
			if err != nil {
				panic(err)
			}

			err = deployVm(kv, fqvm, hostname, vm)
			if err != nil {
				panic(fmt.Sprintf("VM failed; err:%v; config:%v",
					err,
					vm,
				))
			}
		case "stop":
			err = destroyVm(kv, fqvm, hostname)
			if err != nil {
				panic(err)
			}
		default:
			panic(fmt.Sprintf("unknown operation type: %s", op))
		}

		err = testVm(fqvm, hostname)
		if err != nil {
			panic(err)
		}

		err = applyDelay()
		if err != nil {
			panic(err)
		}
	}

	log.Info("Test PASSED")
}
