# Mariner Unit Testing

This directory provides unit tests of the Mariner virtual machine manager.

The Mariner testing methodology consists of
- VM models
- Command models
- A deployment model

## VM Models

A VM model defines a VM state supported by the Mariner reconciler. A set of models are defined in
[vm.go](vm.go), including:
* "simple"

## Command Models

A command model specifies a set of VM models to deploy and teardown as part of a test. Command
models are provided under [tests/](tests/), including:
* [simple.json](tests/simple.json)

## Deployment Model

The deployment model consists of the following set of containers:
* A etcd instance supporting the reconciler KVS
* A minio server serving VM images
* A Mariner container
* A driver container

The driver container runs a simple binary that accepts the desired command model, translates the
commands according to the Mariner reconciler specification, and pushes the reconciler operations
into the etcd container's KVS

## HowTo

### Build
```
make
```

#### Run Test
```
sudo ./run-test.sh tests/simple.json
```
