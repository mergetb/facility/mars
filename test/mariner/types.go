package main

type Unit struct {
	Op   string `json:"op"`
	Name string `json:"name"`
	Type string `json:"type,omitempty"`
}

type Option struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type TestConf struct {
	Units   []Unit   `json:"units"`
	Options []Option `json:"options,omitempty"`
}
