package main

import (
	"fmt"
	"math/rand"

	"gitlab.com/mergetb/facility/mars/internal/state"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

// VM Models
var vm_models = map[string]func(string, string) *state.Vm{
	"simple":         VmSimple,
	"simple-e1000":   VmSimpleE1000,
	"simple-e1000e":  VmSimpleE1000E,
	"multiqueue":     VmMultiqueue,
	"simple-fail":    VmSimpleFail,
	"random":         VmRandom,
	"random-2socket": VmRandom2Socket,
}

func MBToBytes(mb uint32) uint64 {
	return uint64(mb * 1024 * 1024)
}

func VmSimple(mzid, hostname string) *state.Vm {
	return buildVm(
		mzid,
		hostname,
		&xir.ProcAllocation{
			Alloc: map[uint32]*xir.SocketAllocation{
				0: {
					Cores: 1,
				},
			},
		},
		&xir.MemoryAllocation{
			Alloc: map[uint32]*xir.DimmAllocation{
				0: {
					Capacity: MBToBytes(512),
				},
			},
		},
		&xir.NICsAllocation{
			Alloc: map[uint32]*xir.NICAllocation{
				0: {
					Alloc: map[uint32]*xir.PortAllocation{
						0: {
							Capacity: 100,
							Vni:      100,
							Vid:      100,
						},
					},
				},
			},
		},
		&xir.DisksAllocation{
			Alloc: map[uint32]*xir.DiskAllocation{
				0: {
					Capacity: MBToBytes(1024),
				},
			},
		},
		nil,
	)
}

func VmSimpleE1000(mzid, hostname string) *state.Vm {
	return buildVm(
		mzid,
		hostname,
		&xir.ProcAllocation{
			Alloc: map[uint32]*xir.SocketAllocation{
				0: {
					Cores: 1,
				},
			},
		},
		&xir.MemoryAllocation{
			Alloc: map[uint32]*xir.DimmAllocation{
				0: {
					Capacity: MBToBytes(512),
				},
			},
		},
		&xir.NICsAllocation{
			Alloc: map[uint32]*xir.NICAllocation{
				0: {
					Alloc: map[uint32]*xir.PortAllocation{
						0: {
							Capacity: 100,
							Vni:      100,
							Vid:      100,
						},
					},
				},
			},
		},
		&xir.DisksAllocation{
			Alloc: map[uint32]*xir.DiskAllocation{
				0: {
					Capacity: MBToBytes(1024),
				},
			},
		},
		&xir.NICSpec{
			Ports: []*xir.PortSpec{
				{
					Model: &xir.NICModelConstraint{
						Op:    xir.Operator_EQ,
						Value: xir.NICModel_E1000,
					},
				},
			},
		},
	)
}

func VmSimpleE1000E(mzid, hostname string) *state.Vm {
	return buildVm(
		mzid,
		hostname,
		&xir.ProcAllocation{
			Alloc: map[uint32]*xir.SocketAllocation{
				0: {
					Cores: 1,
				},
			},
		},
		&xir.MemoryAllocation{
			Alloc: map[uint32]*xir.DimmAllocation{
				0: {
					Capacity: MBToBytes(512),
				},
			},
		},
		&xir.NICsAllocation{
			Alloc: map[uint32]*xir.NICAllocation{
				0: {
					Alloc: map[uint32]*xir.PortAllocation{
						0: {
							Capacity: 100,
							Vni:      100,
							Vid:      100,
						},
					},
				},
			},
		},
		&xir.DisksAllocation{
			Alloc: map[uint32]*xir.DiskAllocation{
				0: {
					Capacity: MBToBytes(1024),
				},
			},
		},
		&xir.NICSpec{
			Ports: []*xir.PortSpec{
				{
					Model: &xir.NICModelConstraint{
						Op:    xir.Operator_EQ,
						Value: xir.NICModel_E1000E,
					},
				},
			},
		},
	)
}

func VmMultiqueue(mzid, hostname string) *state.Vm {
	return buildVm(
		mzid,
		hostname,
		&xir.ProcAllocation{
			Alloc: map[uint32]*xir.SocketAllocation{
				0: {
					Cores: 4,
				},
			},
		},
		&xir.MemoryAllocation{
			Alloc: map[uint32]*xir.DimmAllocation{
				0: {
					Capacity: MBToBytes(1024),
				},
			},
		},
		&xir.NICsAllocation{
			Alloc: map[uint32]*xir.NICAllocation{
				0: {
					Alloc: map[uint32]*xir.PortAllocation{
						0: {
							Capacity: 100,
							Vni:      1,
							Vid:      1,
						},
					},
				},
			},
		},
		&xir.DisksAllocation{
			Alloc: map[uint32]*xir.DiskAllocation{
				0: {
					Capacity: MBToBytes(1024),
				},
			},
		},
		&xir.NICSpec{
			Ports: []*xir.PortSpec{
				{
					Model: &xir.NICModelConstraint{
						Op:    xir.Operator_EQ,
						Value: xir.NICModel_Virtio,
					},
					Queues: &xir.Uint64Constraint{
						Op:    xir.Operator_EQ,
						Value: 4,
					},
				},
			},
		},
	)
}

func VmSimpleFail(mzid, hostname string) *state.Vm {
	return buildVm(
		mzid,
		hostname,
		&xir.ProcAllocation{
			Alloc: map[uint32]*xir.SocketAllocation{
				0: {
					Cores: 1,
				},
				1: {
					Cores: 1,
				},
			},
		},
		&xir.MemoryAllocation{
			Alloc: map[uint32]*xir.DimmAllocation{
				0: {
					Capacity: MBToBytes(512),
				},
			},
		},
		&xir.NICsAllocation{
			Alloc: map[uint32]*xir.NICAllocation{},
		},
		&xir.DisksAllocation{
			Alloc: map[uint32]*xir.DiskAllocation{},
		},
		nil,
	)
}

func doVmRandom(mzid, hostname string, num_sockets uint32) *state.Vm {
	max_disks := 10
	max_nics := 10
	max_ports := 8
	vid := uint32(100)

	Procs := make(map[uint32]*xir.SocketAllocation)
	Memory := make(map[uint32]*xir.DimmAllocation)
	for socket := uint32(0); socket < num_sockets; socket++ {
		num_cores := uint32(1 + rand.Intn(int(32/num_sockets)))
		Procs[socket] = &xir.SocketAllocation{
			Cores: num_cores,
		}

		mb := uint32(64 + rand.Intn(32*1024))
		Memory[socket] = &xir.DimmAllocation{
			Capacity: MBToBytes(mb),
		}
	}

	num_nics := uint32(rand.Intn(max_nics))
	NICs := make(map[uint32]*xir.NICAllocation)
	for nic := uint32(0); nic < num_nics; nic++ {
		NIC := make(map[uint32]*xir.PortAllocation)

		num_ports := uint32(rand.Intn(max_ports))
		for port := uint32(0); port <= num_ports; port++ {
			NIC[port] = &xir.PortAllocation{
				Capacity: uint64(1000),
				Vni:      vid,
				Vid:      vid,
			}
			vid++
		}

		NICs[nic] = &xir.NICAllocation{
			Alloc: NIC,
		}
	}

	Disks := make(map[uint32]*xir.DiskAllocation)
	num_disks := uint32(rand.Intn(max_disks))
	for disk := uint32(0); disk < num_disks; disk++ {
		mb := uint32(rand.Intn(100 * 1024))
		Disks[disk] = &xir.DiskAllocation{
			Capacity: MBToBytes(mb),
		}
	}

	return buildVm(
		mzid,
		hostname,
		&xir.ProcAllocation{
			Alloc: Procs,
		},
		&xir.MemoryAllocation{
			Alloc: Memory,
		},
		&xir.NICsAllocation{
			Alloc: NICs,
		},
		&xir.DisksAllocation{
			Alloc: Disks,
		},
		nil,
	)

}

func VmRandom(mzid, hostname string) *state.Vm {
	return doVmRandom(mzid, hostname, 1)
}

func VmRandom2Socket(mzid, hostname string) *state.Vm {
	return doVmRandom(mzid, hostname, 2)
}

func createVm(typ, mzid, hostname string) (*state.Vm, error) {
	if f, ok := vm_models[typ]; ok {
		return f(mzid, hostname), nil
	} else {
		return nil, fmt.Errorf("no spec defined for VM type: %s", typ)
	}
}
