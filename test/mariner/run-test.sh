#!/bin/bash

REGISTRY=${REGISTRY:-docker.io}
REGISTRY_PATH=${REGISTRY_PATH:-bkocolos}
TAG=${TAG:-latest}

if [ $EUID -ne 0 ] ; then
	echo "Must be run as root"
	exit 1
fi

if [ $# -ne 1 ]; then
	echo "Usage: $0 <test.json>"
	exit 1
fi

if [ ! -f $1 ]; then
	echo "$1 does not exist"
	exit 1
fi

stop_ctrs() {
	podman stop driver
	podman stop mariner
	podman stop etcd
	podman stop mc
	podman stop minio
}

stop_ctrs

podman volume inspect mvol &> /dev/null
if [ $? -ne 0 ]; then
	creat=1
else
	creat=0
fi

set -x
set -e

if [ $creat -eq 1 ]; then
	podman volume create mvol
fi

###
# Setup
podman run --net host --rm -d --name minio minio/minio server /data
podman run --net host --rm -d --name etcd quay.io/coreos/etcd:latest /usr/local/bin/etcd
podman run \
	--net host --rm -d --pid host \
	--name mariner \
	--privileged \
	--restart on-failure \
	--env "MINIO_ROOT_USER=minioadmin" --env "MINIO_ROOT_PASSWORD=minioadmin" \
	--add-host "etcd:127.0.0.1" --add-host "minio:127.0.0.1" \
	-v mvol:/var/lib/mariner \
	-v /run/podman:/run/podman \
	$REGISTRY/$REGISTRY_PATH/mariner:$TAG

podman run --net host --rm -d --name mc -it --entrypoint=/bin/sh minio/mc
podman cp img/fedora-33 mc:/fedora-33
podman exec mc mc config host add minio http://localhost:9000 minioadmin minioadmin
podman exec mc mc mb minio/img
podman exec mc mc cp /fedora-33 minio/img

###
# Run test
podman run \
	--net host --rm -d \
	--name driver \
	-it \
	--add-host "etcd:127.0.0.1" \
	--entrypoint /bin/sh \
	$REGISTRY/$REGISTRY_PATH/mariner:$TAG

podman cp build/driver driver:/driver
podman cp $1 driver:/test.json

set +e

podman exec driver /driver /test.json
rv=$?

###
# Teardown
podman logs mariner &> mariner.log

#if [ $rv -eq 0 ]; then
#	stop_ctrs
#fi

exit $rv
