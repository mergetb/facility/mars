#!/bin/bash

if [ $EUID -ne 0 ] ; then
	echo "Must be run as root"
	exit 1
fi

podman stop driver
podman stop mariner
podman stop etcd
podman stop mc
podman stop minio
