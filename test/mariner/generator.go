package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func getOp() string {
	v := rand.Intn(2)
	if v == 0 {
		return "start"
	} else {
		return "stop"
	}
}

func getName(idx int) string {
	return fmt.Sprintf("vm%d", idx)
}

func getDeleteRandomVm(vms []string) (string, []string) {
	idx := rand.Intn(len(vms))
	lst := len(vms) - 1
	vms[lst], vms[idx] = vms[idx], vms[lst]
	return vms[lst], vms[:lst]
}

func main() {
	if len(os.Args) != 3 {
		panic(fmt.Sprintf("Usage: %s <number of VM creations> <outfile>", os.Args[0]))
	}

	creations, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}

	rand.Seed(time.Now().UTC().UnixNano())

	vmCount := 0
	var vms []string
	var units []Unit

	for {
		var op string
		var vm string

		if vmCount == creations {
			if len(vms) == 0 {
				break
			}

			op = "stop"
		} else if len(vms) == 0 {
			op = "start"
		} else {
			op = getOp()
		}

		switch op {
		case "start":
			vm = getName(vmCount)
			units = append(units,
				Unit{
					Op:   op,
					Name: vm,
					Type: "random",
				},
			)
			vmCount++
			vms = append(vms, vm)
		case "stop":
			vm, vms = getDeleteRandomVm(vms)
			units = append(units,
				Unit{
					Op:   op,
					Name: vm,
				},
			)
		default:
			panic("invalid op")
		}
	}

	res, err := json.MarshalIndent(
		&TestConf{
			Options: []Option{
				{
					Key:   "delay",
					Value: "-1",
				},
				{
					Key:   "delaymax",
					Value: "10",
				},
				{
					Key:   "seed",
					Value: "-1",
				},
			},
			Units: units,
		}, "", "    ",
	)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(os.Args[2], []byte(res), 0644)
	if err != nil {
		panic(err)
	}
}
