package main

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"google.golang.org/protobuf/proto"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	rec "gitlab.com/mergetb/tech/reconcile"
)

type WgPeerTask struct {
	enclaveid   string
	host        string
	pubkey      string
	podnotready bool
}

func (t *WgPeerTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.WgPeerKey {
		return false
	}

	if xk.Host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", xk.Host, hostname)
		return false
	}

	t.enclaveid = xk.Name
	t.host = xk.Host
	t.pubkey = xk.Name2
	t.podnotready = false

	return true
}

// Explicitly set reconciliation status for tasks that are awaiting infrapod creation
func (t *WgPeerTask) setTaskInfrapodPending(td *rec.TaskData) *rec.TaskMessage {
	td.Status.LastStatus = rec.TaskStatus_Pending
	td.Status.Messages = []*rec.TaskMessage{
		rec.TaskMessageWarning("waiting for infrapod creation..."),
	}

	return rec.TaskMessageUndefined()
}

func (t *WgPeerTask) doPut(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": t.pubkey,
	})

	err := t.handlePut(buf, ver)
	if err != nil {
		if t.podnotready {
			// if we couldn't find a pod, leave this as processing
			l.Info("infrapod does not exist: deferring handling of key put")
			return t.setTaskInfrapodPending(td)
		}

		// something else went wrong
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return nil
}

func (t *WgPeerTask) Create(buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": t.pubkey,
	})
	l.Info("wg peer create request")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Update with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return t.doPut(nil, buf, ver, td)
}

func (t *WgPeerTask) Update(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": t.pubkey,
	})
	l.Info("wg peer update")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Update with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return t.doPut(prev, buf, ver, td)
}

func (t *WgPeerTask) Ensure(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": t.pubkey,
	})
	l.Info("wg peer ensure")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Ensure with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return t.doPut(prev, buf, ver, td)
}

func (t *WgPeerTask) Delete(buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": t.pubkey,
	})
	l.Info("wg peer delete")

	// return nil because we don't want random status keys lying around forever

	if buf == nil {
		l.Error("TaskManager passed Delete with nil value; what can we do with this?")
		return nil
	}

	err := t.handleDelete(buf, ver)
	if err != nil {
		l.Error(err)
	}

	return nil
}

func (t *WgPeerTask) handlePut(current []byte, version int64) error {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": t.pubkey,
	})
	l.Debug("wg peer handlePut")

	peer := &portal.WgIfConfig{}
	err := proto.Unmarshal(current, peer)
	if err != nil {
		return fmt.Errorf("proto unmarshal: %v", err)
	}

	err = t.handleProvision(peer, false)
	if err != nil {
		return err
	}

	// add peer to storage (if not already there)
	ifcfg, err := storage.GetWgInterface(t.host, t.enclaveid)
	if err != nil {
		return fmt.Errorf("getting if: %v", err)
	}

	for _, p := range ifcfg.Peers {
		if p.Key == peer.Key {
			l.Debug("pubkey already exists in storage for peer")
			return nil
		}
	}
	ifcfg.Peers = append(ifcfg.Peers, peer)

	err = storage.StoreWgIf(t.host, t.enclaveid, ifcfg)
	if err != nil {
		return fmt.Errorf("storing if: %v", err)
	}

	return nil
}

func (t *WgPeerTask) handleDelete(value []byte, version int64) error {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": t.pubkey,
	})
	l.Debug("wg peer handleDelete")

	peer := &portal.WgIfConfig{}
	err := proto.Unmarshal(value, peer)
	if err != nil {
		return fmt.Errorf("proto unmarshal: %v", err)
	}

	err = t.handleProvision(peer, true)
	if err != nil {
		return err
	}

	// remove peer from storage
	ifcfg, err := storage.GetWgInterface(t.host, t.enclaveid)
	if err != nil {
		// likely there has been a demtz
		l.Debugf("getting if: %v", err)
		return nil
	}

	for i, p := range ifcfg.Peers {
		if p.Key == peer.Key {
			ifcfg.Peers = append(ifcfg.Peers[:i], ifcfg.Peers[i+1:]...)
			break
		}
	}

	err = storage.StoreWgIf(t.host, t.enclaveid, ifcfg)
	if err != nil {
		// likely there has been a demtz
		l.Debugf("storing if: %v", err)
		return nil
	}

	return nil
}

func (t *WgPeerTask) handleProvision(peer *portal.WgIfConfig, remove bool) error {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": t.pubkey,
		"remove":    remove,
	})
	l.Debug("wg peer handleProvision")

	pkey, err := wgtypes.ParseKey(peer.Key)
	if err != nil {
		return fmt.Errorf("wgtypes.ParseKey(): %v", err)
	}

	var ips []net.IPNet
	for _, ip := range peer.Allowedips {
		_, ipnet, err := net.ParseCIDR(ip)
		if err != nil {
			return fmt.Errorf("ParseCidr(): %v", err)
		}
		ips = append(ips, *ipnet)
	}

	// find netns for the enclave
	netns, podfound, err := enclaveid2NetNamepace(t.enclaveid)
	if err != nil {
		// a common reason for error is the pod does not exist; this does not indicate
		// a wg peer error
		if !podfound {
			t.podnotready = true

			if remove {
				// on remove, a missing infrapod means we have nothing to do
				return nil
			}
		}

		return fmt.Errorf("no net namespace found for enclave %s: %s", t.enclaveid, err)
	}

	// change into the netns
	cnt, hostns, err := setNetnsName(netns)
	if err != nil {
		return err
	}
	// make sure we reset to host namespace by calling the provided continuation
	defer cnt(hostns)

	c, err := wgctrl.New()
	if err != nil {
		return err
	}
	defer c.Close()

	// see if the device exists
	devs, err := c.Devices()
	if err != nil {
		return fmt.Errorf("wgctrl devices: %v", err)
	}

	if len(devs) == 0 {
		l.Debug("no wg device exists; cannot add or remove peer")
		return nil
	}

	if len(devs) > 1 {
		l.Warnf("enclave has %d WG devices; expected 1; administering peer on first", len(devs))
	}
	dev := devs[0]

	// On creation, see if the peer is already there
	if !remove {
		for _, p := range dev.Peers {
			// A peer is present if the public key matches
			if p.PublicKey == pkey {
				l.Info("peer already configured on interface")
				return nil
			}
		}
	}

	cfg := wgtypes.Config{
		Peers: []wgtypes.PeerConfig{{
			PublicKey:  pkey,
			AllowedIPs: ips,
			Remove:     remove,
		}},
	}

	err = c.ConfigureDevice(devs[0].Name, cfg)
	if err != nil {
		return fmt.Errorf("wgctrl config dev: %v", err)
	}

	l = l.WithField("allowedips", cfg.Peers[0].AllowedIPs)
	if remove {
		l.Info("removed wg peer from interface")
	} else {
		l.Info("added wg peer to interface")
	}

	return nil
}
