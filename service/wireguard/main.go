package main

import (
	"fmt"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	hostname string
	fqdn     string
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func main() {

	var err error

	log.Infof("starting wireguard reconciler")

	fqdn, err = os.Hostname()
	if err != nil {
		panic(err)
	}
	hostname = strings.Split(fqdn, ".")[0]

	err = storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	// Not clear minio is needed here.
	err = storage.InitMarsMinIOClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	t := &reconcile.ReconcilerManager{
		Manager:                    "wireguard",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*reconcile.Reconciler{
			{
				Name:    fmt.Sprintf("wireguardIF.%s", hostname),
				Prefix:  storage.WgIfReqKey(),
				Desc:    "Manage wireguard interfaces",
				Actions: &WgIfTask{},
			},
			{
				Name:    fmt.Sprintf("wireguardPeer.%s", hostname),
				Prefix:  storage.WgPeerKey(),
				Desc:    "Manage wireguard peers",
				Actions: &WgPeerTask{},
			},
			{
				Name:    fmt.Sprintf("wireguardSync.%s", hostname),
				Prefix:  storage.WgIfKey(),
				Desc:    "Synchronize site wireguard state with portal",
				Actions: &WgPortalTask{},
			},
		},
	}

	t.Run()
}
