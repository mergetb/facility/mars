package main

import (
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
	"golang.zx2c4.com/wireguard/wgctrl"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"google.golang.org/protobuf/proto"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/rtnl"

	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
)

type WgIfTask struct {
	enclaveid   string
	host        string
	netns       string
	priv        string
	pub         string
	port        int
	podnotready bool
}

func (t *WgIfTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.WgIfreqKey {
		return false
	}

	if xk.Host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", xk.Host, hostname)
		return false
	}

	t.enclaveid = xk.Name
	t.host = xk.Host
	t.podnotready = false

	return true
}

func (t *WgIfTask) setTaskInfrapodPending(td *rec.TaskData) *rec.TaskMessage {
	td.Status.LastStatus = rec.TaskStatus_Pending
	td.Status.Messages = []*rec.TaskMessage{
		rec.TaskMessageWarning("waiting for infrapod creation..."),
	}

	return rec.TaskMessageUndefined()
}

func (t *WgIfTask) doPut(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})

	err := t.handlePut(prev, buf, ver)
	if err != nil {
		if t.podnotready {
			// if we couldn't find a pod, leave this as processing
			l.Info("infrapod does not exist: deferring handling of key put")
			return t.setTaskInfrapodPending(td)
		}

		// something else went wrong
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return nil
}

func (t *WgIfTask) Create(buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})
	l.Info("wg interface create")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Create with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return t.doPut(nil, buf, ver, td)
}

func (t *WgIfTask) Update(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})
	l.Info("wg interface update")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Update with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return t.doPut(prev, buf, ver, td)
}

func (t *WgIfTask) Ensure(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})
	l.Info("wg interface ensure")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Ensure with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return t.doPut(prev, buf, ver, td)
}

func (t *WgIfTask) Delete(buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})
	l.Info("wg interface delete")

	// return nil because we don't want random status keys lying around forever

	if buf == nil {
		l.Error("TaskManager passed Delete with nil value; what can we do with this?")
		return nil
	}

	err := t.handleDelete()
	if err != nil {
		l.Error(err)
	}

	return nil
}

func (t *WgIfTask) handleRq(rq *facility.CreateWgInterfaceRequest) error {

	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})

	var htx *rtnl.Context = nil
	var ptx *rtnl.Context = nil
	var lnk *rtnl.Link
	var err error = nil

	// function to make sure a partially configured device is not leaked
	defer func() {
		if err != nil && lnk != nil {
			if htx != nil {
				lnk.Absent(htx)
			}
			if ptx != nil {
				lnk.Absent(ptx)
			}
		}

		if htx != nil {
			htx.Close()
		}

		if ptx != nil {
			ptx.Close()
		}
	}()

	// create a new WG device
	dev := randName()

	htx, err = rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}

	lnk = rtnl.NewLink()
	lnk.Info.Name = dev
	lnk.Info.Mtu = 1420
	lnk.Info.Wireguard = &rtnl.Wireguard{}

	err = lnk.Present(htx)
	if err != nil {
		return fmt.Errorf("create wg dev %s: %v", dev, err)
	}

	// generate keypair and listening port for WG device
	priv, err := wgtypes.GeneratePrivateKey()
	if err != nil {
		return fmt.Errorf("err wg private key: %v", err)
	}

	lport, err := wgPort()
	if err != nil {
		return fmt.Errorf("get wg port: %v", err)
	}

	l.WithField("listenport", lport).Debug("generated wg listen port")

	cfg := wgtypes.Config{
		PrivateKey: &priv,
		ListenPort: &lport,
	}

	c, err := wgctrl.New()
	if err != nil {
		return fmt.Errorf("wgctrl new: %v", err)
	}
	defer c.Close()

	err = c.ConfigureDevice(dev, cfg)
	if err != nil {
		return fmt.Errorf("wgctrl config dev: %v", err)
	}

	// UP the interface which needs to be done in the base netns
	err = lnk.Up(htx)
	if err != nil {
		return err
	}

	// open a context into the infrapod netns
	ptx, err = rtnl.OpenContext(t.netns)
	if err != nil {
		return err
	}

	// move the link
	err = lnk.ChangeContext(htx, ptx)
	if err != nil {
		return err
	}

	htx.Close()
	htx = nil

	// moving into the new netns removes addresses and DOWNs the interface. So bring back up, add
	// address, and add route now
	err = lnk.Up(ptx)
	if err != nil {
		return err
	}

	ip, ipnet, err := net.ParseCIDR(rq.Accessaddr + "/32")
	if err != nil {
		return fmt.Errorf("ParseCidr(): %v", err)
	}

	// add address
	addr := rtnl.NewAddress()
	addr.Info.Address = &net.IPNet{
		IP:   ip,
		Mask: ipnet.Mask,
	}

	err = lnk.AddAddr(ptx, addr)
	if err != nil {
		return fmt.Errorf("add address %s dev %s: %v", rq.Accessaddr, dev, err)
	}

	// add route (TODO: do not hardcode, should come from portal)
	route := &rtnl.Route{
		Hdr: unix.RtMsg{
			Family:   unix.AF_INET,
			Dst_len:  24,
			Protocol: unix.RTPROT_STATIC,
			Scope:    unix.RT_SCOPE_LINK,
		},
		Dest: net.IPv4(192, 168, 254, 0),
		Oif:  uint32(lnk.Msg.Index),
	}
	err = route.Present(ptx)
	if err != nil {
		return fmt.Errorf("add route 192.168.254.0/24 dev %s: %v", dev, err)
	}

	t.priv = priv.String()
	t.pub = priv.PublicKey().String()
	t.port = lport

	l.WithFields(log.Fields{
		"publickey":  t.pub,
		"listenport": t.port,
		"device":     dev,
	}).Info("configured WG ifx for enclave")

	return nil
}

func (t *WgIfTask) handlePut(prev, current []byte, version int64) error {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})

	rq := new(facility.CreateWgInterfaceRequest)
	err := proto.Unmarshal(current, rq)
	if err != nil {
		l.Error(err)
		return fmt.Errorf("proto unmarshal: %v", err)
	}

	// check and see if the key is reconciled already, and do nothing if so
	recd, err := t.isReconciled(rq)
	if err != nil {
		return err
	}

	if recd {
		l.Info("WG ifx already reconciled for enclave")
	} else {

		// blow away any existing WG config and reset what's in storage
		t.handleDelete()

		err = t.handleRq(rq)
		if err != nil {
			l.Error(err)
			return err
		}

		// delete interface if we fail below
		defer func() {
			if err != nil {
				t.handleDelete()
			}
		}()
	}

	// peer requests might exist already; establish them by calling into the peertask
	resp, err := storage.GetWgPeers(t.enclaveid)
	if err != nil {
		return err
	}

	for _, cfg := range resp {
		peertask := &WgPeerTask{
			enclaveid: t.enclaveid,
			host:      t.host,
			pubkey:    cfg.Key,
		}

		l2 := l.WithField("peer", cfg)
		l2.Debug("provisioning existing peer on new wg ifx")

		err = peertask.handleProvision(cfg, false)
		if err != nil {
			l2.Errorf("failed to provision peer on new wg ifx: %v", err)
		}
	}

	// save config
	err = storage.StoreWgIf(
		t.host,
		t.enclaveid,
		&facility.GetWgInterfaceResponse{
			Address:    rq.Accessaddr,
			Privatekey: t.priv,
			Publickey:  t.pub,
			Port:       uint32(t.port),
			Peers:      resp,
		},
	)

	return err
}

func (t *WgIfTask) handleDelete() error {
	l := log.WithFields(log.Fields{
		"enclave": t.enclaveid,
	})

	// Clear if for this enclave
	defer func() {
		err := storage.ClearWgIf(t.enclaveid)
		if err != nil {
			l.Error(err)
		}
	}()

	netns, podfound, err := enclaveid2NetNamepace(t.enclaveid)
	if err != nil {
		// if the pod is missing, we have nothing to do
		if !podfound {
			return nil
		}

		return fmt.Errorf("no net namespace found: %v", err)
	}

	cnt, hostns, err := setNetnsName(netns)
	if err != nil {
		return err
	}
	// make sure we reset to host namespace by calling the provided continuation
	defer cnt(hostns)

	c, err := wgctrl.New()
	if err != nil {
		return err
	}
	defer c.Close()

	// see if a WG device exists
	devs, err := c.Devices()
	if err != nil {
		return fmt.Errorf("wgctrl devices: %v", err)
	}

	if len(devs) == 0 {
		l.Debug("no WG devices in enclave")
		return nil
	} else if len(devs) > 1 {
		l.Warnf("%d WG devices in enclave; expected 1", len(devs))
	}
	dev := devs[0]

	// open a context into the nets
	ptx, err := rtnl.OpenContext(netns)
	if err != nil {
		return err
	}
	defer ptx.Close()

	lnk := rtnl.NewLink()
	lnk.Info.Name = dev.Name
	err = lnk.Absent(ptx)
	if err != nil {
		l.Error(err)
		return err
	}

	return nil
}

// Check if the request is reconciled already
func (t *WgIfTask) isReconciled(rq *facility.CreateWgInterfaceRequest) (bool, error) {
	l := log.WithFields(log.Fields{
		"enclave": t.enclaveid,
	})

	netns, podfound, err := enclaveid2NetNamepace(rq.Enclaveid)
	if err != nil {
		// a common reason for error is the pod does not exist; this not does indicate
		// a wg ifx error
		if !podfound {
			t.podnotready = true
			l.Debug("enclave pod does not exist")
		}

		return false, fmt.Errorf("no net namespace found: %v", err)
	}
	t.netns = netns

	cnt, hostns, err := setNetnsName(t.netns)
	if err != nil {
		l.Error(err)
		return false, err
	}
	// make sure we reset to host namespace by calling the provided continuation
	defer cnt(hostns)

	c, err := wgctrl.New()
	if err != nil {
		return false, err
	}
	defer c.Close()

	// see if a WG device exists
	devs, err := c.Devices()
	if err != nil {
		return false, err
	}

	if len(devs) == 0 {
		return false, nil
	} else if len(devs) > 1 {
		l.Warnf("found %d WG devices in enclave namespace; expected 1", len(devs))
	}
	dev := devs[0]

	// open context into netns
	ptx, err := rtnl.OpenContext(netns)
	if err != nil {
		return false, err
	}

	if rq.Port != 0 {
		l.Warnf("WG ifx request specifies a non-zero port: %d Port will be ignored", rq.Port)
	}

	// To be reconciled, the wg ifx needs the following conditions:
	// 1) if a specific port is requested, the ifx listens on that port
	// 2) the access addr on the ifx matches that requested

	// check the port
	if rq.Port != 0 && dev.ListenPort != int(rq.Port) {
		l.WithFields(log.Fields{
			"requested port": rq.Port,
			"listen port":    dev.ListenPort,
		}).Warn("WG ifx exists, but listen port does not match that requested")
		return false, nil
	}

	lnk, err := rtnl.GetLink(ptx, dev.Name)
	if err != nil {
		return false, err
	}

	// check access addr
	addrs, err := lnk.Addrs(ptx)
	if err != nil {
		return false, err
	}

	for _, a := range addrs {
		l2 := l.WithFields(log.Fields{
			"requested addr":  rq.Accessaddr,
			"configured addr": a.Info.Address.IP.String(),
		})
		l2.Debug("comparing addresses")

		if a.Info.Address.IP.String() == rq.Accessaddr {
			l2.Debug("WG ifx exists with requested access addr and port")

			// set task fields
			t.pub = dev.PublicKey.String()
			t.priv = dev.PrivateKey.String()
			t.port = dev.ListenPort

			return true, nil
		}

		l2.Warn("WG ifx exists, but address does not match that requested")
	}

	return false, nil
}
