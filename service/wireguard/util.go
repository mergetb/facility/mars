package main

import (
	"context"
	"fmt"
	"math/rand"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/vishvananda/netns"
	"golang.org/x/sys/unix"

	"github.com/containers/podman/v4/libpod/define"
	"github.com/containers/podman/v4/pkg/bindings"
	"github.com/containers/podman/v4/pkg/bindings/containers"
	"github.com/containers/podman/v4/pkg/bindings/pods"
)

func closePodmanConn(ctx context.Context) {
	if ctx == nil {
		return
	}

	conn, err := bindings.GetClient(ctx)
	if err != nil {
		log.Errorf("error: trying to close podman connection: %+v", err)
		return
	}

	conn.Client.CloseIdleConnections()
}

func enclaveid2NetNamepace(eid string) (string, bool, error) {
	// Hardcode OK?
	socket := "unix:/run/podman/podman.sock"

	// Connect to Podman socket
	ctx, err := bindings.NewConnection(context.Background(), socket)
	defer closePodmanConn(ctx)
	if err != nil {
		return "", false, err
	}

	pod, err := pods.Inspect(ctx, eid, nil)
	if err != nil {
		return "", false, err
	}

	switch pod.State {
	case define.PodStateRunning:
		break
	default:
		return "", false, fmt.Errorf("cannot get pod netns: state=%+v", pod.State)
	}

	cData, err := containers.Inspect(ctx, pod.InfraContainerID, nil)
	if err != nil {
		return "", true, fmt.Errorf("inspect container %s", pod.InfraContainerID)
	}

	if cData.NetworkSettings == nil {
		return "", true, fmt.Errorf("no network settings for container")
	}

	if cData.NetworkSettings.SandboxKey == "" {
		return "", true, fmt.Errorf("no sandboxkey for container")
	}

	// example ns: /run/netns/cni-f611c6a1-aee5-f112-3a14-e3d65da0cf23
	tkns := strings.Split(cData.NetworkSettings.SandboxKey, "/")
	netns := tkns[len(tkns)-1]

	log.WithFields(log.Fields{
		"enclaveid":  eid,
		"sandboxkey": cData.NetworkSettings.SandboxKey,
		"netns":      netns,
	}).Debug("found net namespace for enclave")

	return netns, true, nil
}

// set the netns to 'name' and return a "continuation" that will reset to the current state when
// called
func setNetnsName(name string) (func(netns.NsHandle), netns.NsHandle, error) {
	var err error

	// Lock the OS thread to prevent namespace switching
	// See: https://pkg.go.dev/github.com/vishvananda/netns#section-readme
	runtime.LockOSThread()

	// if anything errs, unlock the OS thread
	defer func() {
		if err != nil {
			runtime.UnlockOSThread()
		}
	}()

	curns, err := netns.Get()
	if err != nil {
		return nil, 0, fmt.Errorf("netns.Get(): %v", err)
	}

	ns, err := netns.GetFromName(name)
	if err != nil {
		return nil, 0, fmt.Errorf("netns.GetFromName(): %v", err)
	}

	err = netns.Set(ns)
	if err != nil {
		return nil, 0, fmt.Errorf("netns.Set(): %v", err)
	}

	// return a "continuation" that will reset to the current netns
	return func(handle netns.NsHandle) {
		netns.Set(handle)
		handle.Close()
		runtime.UnlockOSThread()
	}, curns, nil
}

// bind a socket to localhost:0 to let the OS choose an open port. Close the socket to free it and
// then return
func wgPort() (int, error) {
	fd, err := unix.Socket(unix.AF_INET, unix.SOCK_DGRAM, unix.IPPROTO_IP)
	if err != nil {
		return 0, fmt.Errorf("unix.Socket: %+v", err)
	}
	defer unix.Close(fd)

	sa4 := &unix.SockaddrInet4{
		Port: 0,
		Addr: [4]byte{127, 0, 0, 1},
	}
	err = unix.Bind(fd, sa4)
	if err != nil {
		return 0, fmt.Errorf("unix.Bind: %+v", err)
	}

	sa, err := unix.Getsockname(fd)
	if err != nil {
		return 0, fmt.Errorf("unix.Getsockname: %+v", err)
	}

	return sa.(*unix.SockaddrInet4).Port, nil
}

func randName() string {
	const size = 4
	const letters = "abcedfghijklmnopqrstuvwxyz"
	s := make([]byte, size)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return "wg" + string(s)
}
