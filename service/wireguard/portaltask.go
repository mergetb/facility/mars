package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	rec "gitlab.com/mergetb/tech/reconcile"

	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	_ "gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/connect"
)

type WgPortalTask struct {
	enclaveid string
	host      string
}

func (t *WgPortalTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.WgIfKey {
		return false
	}

	if xk.Host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", xk.Host, hostname)
		return false
	}

	t.enclaveid = xk.Name
	t.host = xk.Host

	return true
}

func (t *WgPortalTask) Create(buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})
	l.Info("wg portal create")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Create with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return rec.CheckErrorToMessage(t.handlePut(nil, buf, ver))
}

func (t *WgPortalTask) Update(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})
	l.Info("wg portal update")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Update with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return rec.CheckErrorToMessage(t.handlePut(prev, buf, ver))
}

func (t *WgPortalTask) Ensure(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})
	l.Info("wg portal ensure")

	if buf == nil {
		err := fmt.Errorf("TaskManager passed Ensure with nil value; what can we do with this?")
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return rec.CheckErrorToMessage(t.handlePut(prev, buf, ver))
}

func (t *WgPortalTask) Delete(buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
	})
	l.Info("wg portal delete")

	// return nil because we don't want random status keys lying around forever

	if buf == nil {
		l.Error("TaskManager passed Delete with nil value; what can we do with this?")
		return nil
	}

	err := t.handleDelete(buf, ver)
	if err != nil {
		l.Error(err)
	}

	return nil
}

func (t *WgPortalTask) handlePut(prev, current []byte, version int64) error {
	resp := new(facility.GetWgInterfaceResponse)
	old := new(facility.GetWgInterfaceResponse)

	err := proto.Unmarshal(current, resp)
	if err != nil {
		return fmt.Errorf("proto unmarshal: %v", err)
	}

	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": resp.Publickey,
	})

	// The portal needs to be made aware of changes to the wireguard interface configuration.
	// Principally, this means the access address, port, and key cannot change
	//
	// All other changes do not need to be communicated (but really shouldn't ever happen
	// regardless)

	if prev != nil {
		err = proto.Unmarshal(prev, old)
		if err != nil {
			return fmt.Errorf("proto unmarshal: %v", err)
		}

		if resp.Address == old.Address && resp.Port == old.Port && resp.Publickey == old.Publickey {
			// nothing to update
			l.Info("no change to WG ifx device detected; nothing to inform portal about")
			return nil
		}
	}

	if prev == nil {
		l.Info("detected new WG ifx device; informing portal")
	} else {
		l.Warn("detected updated WG ifx device; informing portal")
	}

	// tell portal about it
	return connect.PortalWireguard(
		func(cli portal.WireguardClient) error {
			_, err := cli.AddWgIfConfig(
				context.TODO(),
				&portal.AddWgIfConfigRequest{
					Enclaveid: t.enclaveid,
					Config: &portal.WgIfConfig{
						Endpoint:   fmt.Sprintf("%s:%d", fqdn, resp.Port),
						Key:        resp.Publickey,
						Accessaddr: resp.Address,
					},
					Gateway: true, // this is a gateway interface
				},
			)
			return err
		},
	)
}

func (t *WgPortalTask) handleDelete(current []byte, version int64) error {
	resp := new(facility.GetWgInterfaceResponse)
	err := proto.Unmarshal(current, resp)
	if err != nil {
		return fmt.Errorf("proto unmarshal: %v", err)
	}

	l := log.WithFields(log.Fields{
		"enclaveid": t.enclaveid,
		"publickey": resp.Publickey,
	})
	l.Info("detected deleted WG ifx device; informing portal")

	// tell portal about it
	return connect.PortalWireguard(
		func(cli portal.WireguardClient) error {
			_, err := cli.DelWgIfConfig(
				context.TODO(),
				&portal.DelWgIfConfigRequest{
					Enclaveid: t.enclaveid,
					Key:       resp.Publickey,
				},
			)
			return err
		},
	)
}
