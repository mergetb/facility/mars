/*
 * moactld - proxy for dynamic link configuration handling
 */

package main

import (
	"context"
	"fmt"
	"net"
	"os"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"k8s.io/apimachinery/pkg/util/sets"

	moa "gitlab.com/mergetb/api/moa/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	marspkg "gitlab.com/mergetb/facility/mars/pkg" // for Version
	"gitlab.com/mergetb/facility/mars/pkg/moautil" // for WithMoa
	"gitlab.com/mergetb/facility/mars/service"     // for MoactlPort
)

// Map from emu server to the tags it supports
type TagMux map[string]sets.String

func (t TagMux) Values() []string {
	var r []string
	for k := range t {
		r = append(r, k)
	}
	return r
}

func init() {
	internal.InitLogging()
}

type moaControlServer struct {
	moa.UnimplementedControlServer
	mzid    string // same as hostname
	servers TagMux // list of emu servers for this mz
}

func (s *moaControlServer) Update(ctx context.Context, req *moa.UpdateRequest) (*moa.MoaResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("update request")

	for srv, ss := range s.servers {

		var updates []*moa.LinkUpdate

		// filter the updates to the ones this server can handle
		for _, u := range req.Updates {
			if ss.HasAny(u.Tags...) {
				updates = append(updates, u)
			}
		}

		if updates != nil {
			// TODO: this should probably be kept as an open channel to reduce latency for each request
			err := moautil.WithMoa(srv, func(c moa.ManageClient) error {
				_, err := c.Update(context.TODO(), &moa.UpdateRequest{
					Mzid:    s.mzid,
					Updates: updates,
				})
				if err != nil {
					return fmt.Errorf("update emulation rpc: %w", err)
				}
				return nil
			})

			if err != nil {
				log.WithFields(log.Fields{"err": err, "host": srv}).Error("moa rpc")
				// convert normal error to grpc.status
				return &moa.MoaResponse{}, status.Error(codes.Internal, err.Error())
			}
		}
	}

	return &moa.MoaResponse{}, nil
}

func (s *moaControlServer) Show(ctx context.Context, req *moa.EmulationRequest) (*moa.ShowResponse, error) {
	log.WithFields(log.Fields{"req": req}).Info("show request")

	var links []*portal.LinkEmulation

	for _, srv := range s.servers.Values() {
		err := moautil.WithMoa(srv, func(c moa.ManageClient) error {
			showreq := moa.EmulationRequest{Mzid: s.mzid}
			resp, err := c.Show(context.TODO(), &showreq)
			if err != nil {
				return fmt.Errorf("show emu: %w", err)
			}
			links = append(links, resp.GetLinkEmulation().Links...)
			return nil
		})

		if err != nil {
			log.WithFields(log.Fields{"err": err, "host": srv}).Error("show emu rpc")
			return nil, status.Error(codes.Internal, "rpc failed")
		}
	}

	return &moa.ShowResponse{
		Mzid:          s.mzid,
		LinkEmulation: &portal.LinkEmulationParams{Links: links},
	}, nil
}

func newMoaControlServer(mzid string) *moaControlServer {
	log.Info("fetching mz")
	mz, err := storage.FetchMaterialization(mzid)
	if err != nil {
		log.WithFields(log.Fields{"err": err, "fqdn": mzid}).Fatal("fetch mz")
	}
	if mz.LinkEmulation == nil {
		log.Fatalf("no link emulation found for mz %s", mzid)
	}

	ret := &moaControlServer{mzid: mzid}
	ret.servers = make(TagMux)

	/* Build the mapping of which tags each server supports, so that updates
	can be dispatched appropriately. */

	emu := mz.GetLinkEmulation()
	for _, lnk := range emu.Links {
		// default from mz-specific emulator to support old-style
		srv := emu.Server
		// pick up per-link emu server in new-style
		if lnk.Server != "" {
			srv = lnk.Server
		}
		ss := ret.servers[srv]
		if ss == nil {
			ss = sets.NewString(lnk.Tags...)
			ret.servers[srv] = ss
		} else {
			ss.Insert(lnk.Tags...)
		}
	}

	log.WithFields(log.Fields{"servers": ret.servers}).Info("server created")

	return ret
}

func main() {
	fqdn, err := os.Hostname()
	if err != nil {
		log.Fatalf("failed to get hostname: %v", err)
	}

	log.Infof("moactld %s starting up on port %d on host %s", marspkg.Version, service.MoactlGRPC, fqdn)

	if err = storage.InitMarsEtcdClient(); err != nil {
		log.Fatalf("failed to init mars etcd client: %v", err)
	}

	log.Info("initialiazing minio client")
	err = moautil.InitMarsMinIOClientFromConfig()
	if err != nil {
		log.Fatalf("minio init: %v", err)
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", service.MoactlGRPC))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	moa.RegisterControlServer(grpcServer, newMoaControlServer(fqdn))
	grpcServer.Serve(lis)
}
