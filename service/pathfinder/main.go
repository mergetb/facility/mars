package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	marspkg "gitlab.com/mergetb/facility/mars/pkg"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/tech/reconcile"
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func main() {
	log.Infof("starting pathfinder reconciler version %s", marspkg.Version)

	err := storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	err = storage.InitMarsMinIOClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	fqdn, err := os.Hostname()
	if err != nil {
		log.Fatalf("query hostname: %v", err)
	}
	hostname = strings.Split(fqdn, ".")[0]
	log.Infof("hostname: %s", hostname)
	gatewayFQDN = fqdn
	log.Infof("gateway fqdn: %s", gatewayFQDN)

	it := &IngressTask{}
	err = it.InitTask()
	if err != nil {
		log.Fatalf("init reverse proxy: %v", err)
	}

	t := &reconcile.ReconcilerManager{
		Manager:                    "pathfinder",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*reconcile.Reconciler{{
			Name:            fmt.Sprintf("ingressreq-%s", hostname),
			Prefix:          "/ingressreq/",
			Desc:            fmt.Sprintf("Manage ingress requests on %s", hostname),
			EnsureFrequency: 60 * time.Second,
			Actions:         &IngressReqTask{},
		}, {
			Name:            fmt.Sprintf("ingress-%s", hostname),
			Prefix:          "/ingress/",
			Desc:            fmt.Sprintf("Manage ingresses on %s", hostname),
			EnsureFrequency: 60 * time.Second,
			Actions:         it,
		}},
	}

	t.Run()
}
