package main

import (
	"context"
	"encoding/json"
	"fmt"
	"regexp"
	"time"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	facility "gitlab.com/mergetb/api/facility/v1/go"
	state "gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	rec "gitlab.com/mergetb/tech/reconcile"
	storlib "gitlab.com/mergetb/tech/shared/storage/etcd"
	xir "gitlab.com/mergetb/xir/v0.3/go"
	clientv3 "go.etcd.io/etcd/client/v3"
)

var (

	// ingress key -> /ingress/[gateway]/[mzid]/[proto]:[host]:[port]
	ingressReqKey = regexp.MustCompile(`^/ingressreq/` + namePattern + `/` + namePattern + `/` + ingressId + `$`)

	// max ingresses per mtz allowed.
	ingressCountLimit = int64(10)
)

type IngressReqTask struct {
	key string
}

func (t *IngressReqTask) Parse(key string) bool {
	log.Debugf("parsing new key %s", key)

	tkns := ingressReqKey.FindAllStringSubmatch(key, -1)
	if len(tkns) > 0 && tkns[0][1] == hostname {
		log.Infof("Accepted key for action: %s", key)
		t.key = key
		return true
	}

	return false
}

// Create --
func (t *IngressReqTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	return t.Ensure(nil, value, version, td)
}

// Update --
func (t *IngressReqTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	return t.Ensure(nil, value, version, td)
}

func checkIngressLimit(server, mzid string) error {

	k := fmt.Sprintf("/ingress/%s/%s", server, mzid)
	kv := clientv3.NewKV(storage.EtcdClient)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kv.Get(ctx, k, clientv3.WithPrefix())
	cancel()
	if err != nil {
		return fmt.Errorf("etcd get: '%s': %v", k, err)
	}

	log.Debugf("Found %d existing ingresses for key %s", resp.Count, k)
	if resp.Count > ingressCountLimit {
		return fmt.Errorf(
			"a materialiation is not allowed more than %d ingresses at once",
			ingressCountLimit,
		)
	}

	return nil
}

// Ensure --
func (t *IngressReqTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {

	// Add the runtime bits of the ingress and write the actual Ingress key
	req := new(state.IngressReq)
	err := proto.Unmarshal(value, req)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal: %v", err)
	}

	// do not allow more than ingressCountLimit ingress requests to exist.
	if err = checkIngressLimit(hostname, req.Mzid); err != nil {
		return rec.CheckErrorToMessage(err)
	}

	k := fmt.Sprintf(
		"/ingress/%s/%s/%s:%s:%d", hostname, req.Mzid,
		req.Protocol.String(), req.Hostname, req.Hostport,
	)

	// if the Ingress key exists, we're done. Else create it.
	kv := clientv3.NewKV(storage.EtcdClient)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kv.Get(ctx, k)
	cancel()
	if err != nil {
		return rec.CheckErrorToMessage(fmt.Errorf("etcd get: '%s': %v", k, err))
	}
	if resp.Count != 0 {
		return nil
	}

	g, err := GetPathfinderGoal(req.Mzid)
	if err != nil {
		return rec.TaskMessageErrorf("get pathfinder goal: %v", err)
	}
	g.AddTaskRecord(&rec.TaskRecord{Task: k})

	// Add the runtime ports.
	var extPort uint32

	switch req.Protocol {
	case xir.Protocol_tcp, xir.Protocol_udp:
		extPort, err = GetExternalPort(req.Mzid)
		if err != nil {
			return rec.CheckErrorToMessage(err)
		}
	case xir.Protocol_http:
		extPort = 80
	case xir.Protocol_https:
		extPort = 443
	}

	gwNetPort, err := GetGwNetPort(req.Mzid)
	if err != nil {
		return rec.CheckErrorToMessage(err)
	}

	// may return if the infrapod does not exist.
	gatewayAddr, err := getGwNetAddr(req.Mzid)
	if err != nil {
		return rec.CheckErrorToMessage(err)
	}

	out, err := proto.Marshal(&facility.Ingress{
		Mzid:         req.Mzid,
		Protocol:     req.Protocol,
		Hostname:     req.Hostname,
		Hostport:     req.Hostport,
		Hostaddr:     req.Hostaddr,
		Gwnetport:    gwNetPort,
		Gateway:      gatewayFQDN,
		Gatewayaddr:  gatewayAddr,
		Externalport: extPort,
	})

	txo := new(storage.Txo)
	txo.Put(k, out)
	_, err = txo.Exec()
	if err != nil {
		return rec.CheckErrorToMessage(err)
	}

	g.Update(storage.EtcdClient)

	return nil
}

// Delete --
func (t *IngressReqTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {

	// delete the ingress key.
	req := new(facility.Ingress)
	err := proto.Unmarshal(value, req)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal: %v", err)
	}

	k := fmt.Sprintf(
		"/ingress/%s/%s/%s:%s:%d", hostname, req.Mzid,
		req.Protocol.String(), req.Hostname, req.Hostport,
	)

	g, err := GetPathfinderGoal(req.Mzid)
	if err != nil {
		// not the end of the world. may be demtzing and he goal is already gone.
		log.Warnf("get pathfinder goal: %v", err)
	} else {
		g.RemoveSubtask(&rec.TaskRecord{Task: k})
	}

	log.Debugf("deleting ingress %v", req)

	err = FreeGwNetPort(req.Mzid, req.Gwnetport)
	if err != nil {
		return rec.TaskMessageErrorf("free gw port: %v", err)
	}

	err = FreeExternalPort(req.Externalport)
	if err != nil {
		return rec.TaskMessageErrorf("free external port: %v", err)
	}

	// now delete the actual ingress.
	txo := new(storage.Txo)
	txo.Del(k)
	_, err = txo.Exec()
	if err != nil {
		return rec.CheckErrorToMessage(fmt.Errorf("delete ingress key: %v", err))
	}

	if g != nil {
		g.Update(storage.EtcdClient)
	}

	return nil
}

// Keep track of ports in use. Two ranges supported, external (for ingress) and
// internal gw net ports for routing ingress packets.
var portsKey = "/ports/"
var extPortsKey = portsKey + "external"
var gwNetPortsKey = portsKey + "gwNet"
var extPortsLock = "externalports"
var gwNetPortsLock = "gwnetports"

// PortRange tracks ports in use in the given range.
type PortRange struct {
	InUse map[uint32]string `json:"available"`
}

func GetExternalPort(mzid string) (uint32, error) {

	cfg, err := config.GetConfig()
	if err != nil {
		return 0, fmt.Errorf("get config: %v", err)
	}

	return GetPort(
		mzid,
		extPortsKey,
		extPortsLock,
		uint32(cfg.Network.ExternalPortMin),
		uint32(cfg.Network.ExternalPortMax),
	)
}

func GetGwNetPort(mzid string) (uint32, error) {
	// Just use ephemeral port range
	// This is per mzid so append the mzid to the key
	key := gwNetPortsKey + "/" + mzid
	lock := gwNetPortsLock + "/" + mzid
	return GetPort(mzid, key, lock, 49152, 65535)
}

func FreeExternalPort(port uint32) error {
	return FreePort(extPortsKey, extPortsLock, port)
}

func FreeGwNetPort(mzid string, port uint32) error {
	key := gwNetPortsKey + "/" + mzid
	lock := gwNetPortsLock + "/" + mzid
	return FreePort(key, lock, port)
}

func GetPort(mzid, key, lock string, min, max uint32) (uint32, error) {

	lctx, lcancel := context.WithTimeout(context.Background(), time.Second*10)
	defer lcancel()

	l, err := storlib.NewLock(lctx, storage.EtcdClient, lock)
	if err != nil {
		return 0, fmt.Errorf("etcd lock: '%s': %v", lock, err)
	}
	defer l.Unlock()

	kv := clientv3.NewKV(storage.EtcdClient)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kv.Get(ctx, key)
	cancel()
	if err != nil {
		return 0, fmt.Errorf("etcd get: '%s': %v", key, err)
	}

	ports := &PortRange{}

	if resp.Count == 0 || resp.Kvs[0].Version == 0 {
		ports.InUse = make(map[uint32]string)
	} else {
		err = json.Unmarshal(resp.Kvs[0].Value, ports)
		if err != nil {
			return 0, fmt.Errorf("ports unmarshal: '%s': %v", key, err)
		}
	}

	log.Debugf("Finding port between %d and %d for %s", min, max, key)

	// find an available port. random rather that iterative might be a little better.
	port := uint32(0)
	for i := min; i <= max; i++ {
		id, found := ports.InUse[i]
		if !found || id == "" {
			ports.InUse[i] = mzid
			port = i
			break
		}
	}

	if port == 0 {
		return 0, fmt.Errorf("unable to allocate port for use %s", key)
	}

	buf, err := json.Marshal(ports)
	if err != nil {
		return 0, fmt.Errorf("marshal ports in use %s: %v", key, err)
	}

	ctx, cancel = context.WithTimeout(context.Background(), time.Second*10)
	_, err = kv.Put(ctx, key, string(buf))
	cancel()
	if err != nil {
		return 0, fmt.Errorf("etcd put ports %s: %v", key, err)
	}

	return port, nil
}

// makes me think of Maine for some reason.
func FreePort(key, lock string, port uint32) error {

	lctx, lcancel := context.WithTimeout(context.Background(), time.Second*10)
	defer lcancel()

	l, err := storlib.NewLock(lctx, storage.EtcdClient, lock)
	if err != nil {
		return fmt.Errorf("etcd lock: '%s': %v", lock, err)
	}
	defer l.Unlock()

	kv := clientv3.NewKV(storage.EtcdClient)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	resp, err := kv.Get(ctx, key)
	cancel()
	if err != nil {
		return fmt.Errorf("etcd get: '%s': %v", key, err)
	}

	if resp.Count == 0 {
		log.Errorf("ports entry seems to be missing from etcd for some reason...")
		return nil
	}

	ports := &PortRange{}
	err = json.Unmarshal(resp.Kvs[0].Value, ports)
	if err != nil {
		return fmt.Errorf("ports unmarshal: '%s': %v", key, err)
	}

	if _, found := ports.InUse[port]; found {
		delete(ports.InUse, port)
	}

	// If there are no more reservations, Delete. Else Put.
	if len(ports.InUse) == 0 {
		ctx, cancel = context.WithTimeout(context.Background(), time.Second*10)
		_, err = kv.Delete(ctx, key, clientv3.WithPrefix())
		cancel()
		if err != nil {
			return fmt.Errorf("etcd put ports %s: %v", key, err)
		}
	} else {

		buf, err := json.Marshal(ports)
		if err != nil {
			return fmt.Errorf("marshal ports in use %s: %v", key, err)
		}

		ctx, cancel = context.WithTimeout(context.Background(), time.Second*10)
		_, err = kv.Put(ctx, key, string(buf))
		cancel()
		if err != nil {
			return fmt.Errorf("etcd put ports %s: %v", key, err)
		}
	}

	return nil
}

func GetPathfinderGoal(mzid string) (*rec.TaskGoal, error) {

	g := rec.NewGenericGoal(mzid + "/pathfinderOps")
	err := g.Read(storage.EtcdClient)
	if err != nil {
		log.Errorf("etcd: read %s/pathfinderOps goal, not updating: %v", mzid, err)
		return nil, err
	}

	return g, nil
}
