package main

//
// The Ingress task is responsible for setting up and maintaining
// ingresses into a infrapod and correctly routing packets to
// the infranet address of the experiment node for which they are
// meant.
//
// We assume the reverse proxy traefik is running. This
// task writes appropriate keys into the traefik etcd store and
// trafiuk takes care of the proxying for http/https routes. It
// does not handle tcp/udp packets due to configuration limitiations.
// Traefik would need a static runtime configuration of all Possible
// ports that tcp/udp ingresses could be on and would be listening
// on all of them.
//
// Due to how the infrapod networks are setup, there is a double
// NAT that happens. All infrapods have a control net on 172.30.0.0/16
// and a gateway bridge on 10.254.0.0/16.
//
// So the NATting is:
// infra @ 172.30.0.0 <-> NAT <-> 10.254.0.0/16 <-> NAT <-> external.
//
// The traefik routers listen on the external/gateway bridge port 80/443/8080.
// (8080 is the traefik GUI. We may want to disable this.)
// When they get a matching route (path that maps to a mtz for http)
// it puts it on the 10.254.0.0 address for the destination infrapod.
// This ingress-task then writes the infrapod-internal NAT onto the
// 172.30.0.0 network using the node address that the packet is meant
// to arrive at.
//
// Traefik routes come in two flavors: http(s) and tcp/udp. We do not use
// Traefik for tcp;/usp. The http(s)
// routes are path based and include all the information to get routed
// to the experiment node. The path format is: /[mzid]/[nodename]/[port].
// So to route to a webserver on port 80 on node "foo" in the
// materialization "rlz.exp.project" you'd use
// http://[facility-gateway]/rlz.exp.project/foo/80.
//
// http(s) configuration example:
// * Router: PathPrefix(`/[mzid]/[nodename]/[port]/`)
// * Middleware: path stripper to remove /[mzid]/[nodename]/[port] from the path
//		so the webserver sees the request as "/".
// * Service: to http://10.254.X.X:XX. (where the address puts it in the
//         correct infrapod and the port points to the correct node.
//
//   So the mapping is from gateway port to exp node.
//
//   Example with mtz two.metal.glawler, experiment node foo @ 172.30.0.11, and
//		infrapod gateway address as 10.254.0.7.

//   Router: /two.metal.glawler/foo/80
//   The Service sets up http://10.254.0.7:1234. Then inside the infrapod netns we do
// 		`iptables -A PREROUTING -t nat -i eth2 -p tcp --dport 1234 -j DNAT --to 172.30.0.11:80`
//   which creates the mapping between the "outside" port 1234 to the "inside" experiment node at
//   172.30.0.11 on the infranet. And port 80 is used as that's what's in the url path.
//
// tcp/udp ingresses are port based. No path in tcp. So we just set two firewall rules to route
// these packets. One on the host (gateway) and one in the infrapod. NATted both times. The
// external gateway port is not known at compile or mtz time. The user must check launch
// or mrg to read the port that was chosen by the reconciler. (mrg list ingress mzid)
//
// The life of an inbound http packet to mtz rlz.exp.proj node a on port 80 (vte example):
// internet (http://ifr/rlz.exp.proj:80/rlz.exp.proj/a/80) ->
//   ifr ens2 ->
//     gateway bridge (10.254.0.0/16) ->
//       trafik (route rlz.exp.proj/a/80) ->
//         10.254.0.7:4568 ->  // port 4568 is unique per ingress and tracked by pathfinder
//           infrapod eth2 @ 10.254.0.7/16 ->
//             iptables PREROUTING --dport 4578 ->
//                172.30.0.11:80 ->
//                  node a eth0 port 80.
//
// An alternate system for this could be to run trafik inside the infrapod as well and use traefik routes there as well.
// This would have the benefit of the entire path being in etcd under trafik keys. And no mucking with iptables. The internal
// traefik instance could use the mtz etcd as well. Not sure what kind of load running traefik for each infrapod would
// introduce.
//
// For reference here is a single ingress as seen in etcd for traefik. (NB: no slash at the start of the keys!)
//
//     traefik/http/middlewares/strip-container-prefix/stripprefixregex/regex => /[^/]+/[^/]+/[^/]+
//
//     traefik/http/routers/topod/rule => PathPrefix(`/two.metal.glawler/a/80/`)
//     traefik/http/routers/topod/service two.metal.glawler
//     traefik/http/routers/topod/middlewares => strip-container-prefix
//
//     traefik/http/services/two.metal.glawler/loadbalancer/servers/0/url http://10.254.0.7:8080
//
//  Model integration looks like this?:
//
// # Maps to http://[gateway]/[mzid]/a/80 and https://[gateway]/[mzid]/a/443.
// a = node('nodea')
// a.ingress(http, 80)
// a.ingress(tcp, 5021)
//
import (
	"fmt"
	"os"
	"regexp"
	"runtime"
	"strings"
	"time"

	"github.com/coreos/go-iptables/iptables"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	"github.com/vishvananda/netns"
	facility "gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	rec "gitlab.com/mergetb/tech/reconcile"
	xir "gitlab.com/mergetb/xir/v0.3/go"
	clientv3 "go.etcd.io/etcd/client/v3"
)

var (

	// ingress key -> /ingress/[gateway]/[mzid]/[proto]:[host]:[port]
	namePattern = "([a-zA-Z]+[a-zA-Z0-9_.-]+)"
	ingressId   = "([^:]+):([^:]+):([^:]+)"
	ingressKey  = regexp.MustCompile(`^/ingress/` + namePattern + `/` + namePattern + `/` + ingressId + `$`)

	gatewayInterface string
	hostname         string
	gatewayFQDN      string
)

type IngressTask struct {
	key string
}

func (t *IngressTask) InitTask() error {
	// add the non-ingress rules/data needed by us
	// in the reverse proxy.

	txo := new(storage.Txo)

	// Rule to strip the path from inbound http(s)
	// traefik/http/middlewares/strip-container-prefix/stripprefixregex/regex => /[^/]+/[^/]+/[^/]+
	txo.Put(
		"traefik/http/middlewares/strip-container-prefix/stripprefixregex/regex",
		[]byte("/[^/]+/[^/]+/[^/]+"),
	)

	_, err := txo.Exec()
	if err != nil {
		return err
	}

	// read the gateway interface once during init.
	// GWINTERFACE env var overrides model.
	if value, exists := os.LookupEnv("GWINTERFACE"); exists {
		gatewayInterface = value
	} else {
		gatewayInterface, err = findGatewayLink()
		if err != nil {
			return err
		}
	}

	log.Infof("gateway interface: %s", gatewayInterface)

	return nil
}

func (t *IngressTask) Parse(key string) bool {
	log.Debugf("parsing new key %s", key)

	tkns := ingressKey.FindAllStringSubmatch(key, -1)
	if len(tkns) > 0 && tkns[0][1] == hostname {
		log.Infof("Accepted key for action: %s", key)
		t.key = key
		return true
	}

	return false
}

func (t *IngressTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {

	log.Debugf("[%s] key create observed", t.key)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed us Create with nil value. Nothing to do.")
	}

	return t.handlePut(nil, value, version)
}

func (t *IngressTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {

	log.Debugf("[%s] key update observed", t.key)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed us Update with nil value. Nothing to do.")
	}

	return t.handlePut(prev, value, version)
}

func (t *IngressTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {

	log.Debugf("[%s] key ensure observed", t.key)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed us Ensure with nil value; Nothing to do.")
	}

	return t.handlePut(prev, value, version)
}

func (t *IngressTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {

	log.Debugf("[%s] key delete observed", t.key)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed us Delete with nil value. Nothing to do.")
	}

	return rec.CheckErrorToMessage(t.handleDelete(value, version))
}

func (t *IngressTask) handlePut(prev, current []byte, version int64) *rec.TaskMessage {

	obj := new(facility.Ingress)
	err := proto.Unmarshal(current, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal: %v", err)
	}

	return t.handleIngressEnsure(obj)
}

func (t *IngressTask) handleIngressEnsure(ingress *facility.Ingress) *rec.TaskMessage {
	// We rely on an external reverse proxy for handling the gateway routing
	// on the infraserver machine. This proxy, traefik, looks for etcd
	// keys to setup the proxy routes.

	log.Debugf("handling ingress update: %v", ingress)

	// We need the infrapod to exist. Wait a short time, then give up
	// telling the reconciler package that we are pending. It'll
	// call us back on the next Ensure and we can try again. We don't
	// wait loop forever as we have no idea what's going on outside of
	// ourselves.
	loops := 30
	var err error
	exists := false
	for i := 1; i <= loops; i++ {

		exists, err = infrapodExists(ingress.Mzid)
		if err != nil {
			return rec.CheckErrorToMessage(err)
		}
		if exists {
			// hopefully it does not stop existing before we get to it.
			break
		}

		log.Infof("Waiting for %s infrapod to exist. Attempt %d of %d", ingress.Mzid, i, loops)
		time.Sleep(time.Second * 1)
	}
	if !exists {
		return rec.CheckErrorToMessage(fmt.Errorf("waiting for infrapod to exist"))
	}

	// update the reverse proxy on the gateway (this host)
	if err := t.handleProxyEnsure(ingress); err != nil {
		return rec.CheckErrorToMessage(err)
	}

	// set iptables in the infrapod netns to put packets on infranet.
	if err := t.handleInfrapodEnsure(ingress); err != nil {
		return rec.CheckErrorToMessage(err)
	}

	return nil
}

func runInMtzNetNs(mzid string, f func(string) error) error {

	nns, err := getNetNs(mzid)
	if err != nil {
		return fmt.Errorf("Unable to find network namespace for %s", mzid)
	}

	log.Debugf("Found %s infrapod network ns: %s", mzid, nns)

	runtime.LockOSThread()
	defer runtime.UnlockOSThread()

	hostns, err := netns.Get()
	if err != nil {
		return fmt.Errorf("getns: %v", err)
	}
	defer func() {
		netns.Set(hostns) // set ns back to hostns on exit!
		hostns.Close()
	}()

	podns, err := netns.GetFromName(nns)
	if err != nil {
		return fmt.Errorf("getns(%s): %v", nns, err)
	}
	defer podns.Close()

	err = netns.Set(podns)
	if err != nil {
		return fmt.Errorf("netns set: %v", err)
	}

	return f(podns.String())
}

func iptablesInfrapodCmd(ing *facility.Ingress) []string {

	// ex: iptables -A PREROUTING -t nat -i eth2 -p tcp --dport 64124 -j DNAT --to 172.30.0.14:80
	var proto string
	switch ing.Protocol {
	case xir.Protocol_udp:
		proto = "udp"
	default:
		proto = "tcp"
	}

	iptCmd := []string{
		"-i", "eth2",
		"-p", proto,
		"--dport", fmt.Sprintf("%d", ing.Gwnetport),
		"-j", "DNAT",
		"--to", fmt.Sprintf("%s:%d", ing.Hostaddr, ing.Hostport),
	}

	return iptCmd
}

// handleInfrapodEnsure - make sure the iptables rule is in place in the
// network namespace of the mtz infrapod to move packets from the gateway
// network onto the infranet with appropriate infranet ip.
func (t *IngressTask) handleInfrapodEnsure(ingress *facility.Ingress) error {

	return runInMtzNetNs(ingress.Mzid, func(ns string) error {

		// check for iptables rule and set if it's not there.
		podipt, err := iptables.New()
		if err != nil {
			return fmt.Errorf("iptables new: %v", err)
		}

		iptCmd := iptablesInfrapodCmd(ingress)

		log.Debugf("Adding nat rule to PREROUTING in netns %s: %s", ns, strings.Join(iptCmd, " "))

		// AppendUnique will not add a duplicate rule.
		err = podipt.AppendUnique("nat", "PREROUTING", iptCmd...)
		if err != nil {
			return fmt.Errorf("iptables append unique: %v", err)
		}

		return nil
	})
}

// handleProxyEnsure - set up the reverse proxy rules by setting etcd entries
// that traefik responds to. Or set firewall rules for tcp/udp streams.
func (t *IngressTask) handleProxyEnsure(ingress *facility.Ingress) error {

	name := fmt.Sprintf("%s.%s.%d", ingress.Mzid, ingress.Hostname, ingress.Hostport)
	txo := new(storage.Txo)

	if ingress.Protocol == xir.Protocol_http {
		type kv struct {
			key   string
			value string
		}
		var kvs []kv

		kvs = []kv{
			{
				key:   fmt.Sprintf("traefik/http/routers/%s/rule", name),
				value: fmt.Sprintf("PathPrefix(`/%s/%s/%d`)", ingress.Mzid, ingress.Hostname, ingress.Hostport),
			},
			{
				key:   fmt.Sprintf("traefik/http/routers/%s/service", name),
				value: name,
			},
			{
				key:   fmt.Sprintf("traefik/http/routers/%s/middlewares", name),
				value: "strip-container-prefix",
			},
			{
				key:   fmt.Sprintf("traefik/http/services/%s/loadbalancer/servers/0/url", name),
				value: fmt.Sprintf("http://%s:%d", ingress.Gatewayaddr, ingress.Gwnetport),
			},
		}
		for _, kv := range kvs {
			txo.Put(kv.key, []byte(kv.value))
		}

		_, err := txo.Exec()
		if err != nil {
			return err
		}

	} else if ingress.Protocol == xir.Protocol_tcp || ingress.Protocol == xir.Protocol_udp {

		podipt, err := iptables.New()
		if err != nil {
			return fmt.Errorf("iptables new: %v", err)
		}

		// TODO: read gateway bridge interface name from somewhere. do not hardcode.
		// ex: iptables -A PREROUTING -t nat -i gatewaybr -p tcp --dport 12345 -j DNAT --to 172.30.0.14:80
		proto := ingress.Protocol.String()
		ingName := fmt.Sprintf("%s:%s:%d", ingress.Mzid, ingress.Hostname, ingress.Hostport)
		iptCmd := []string{
			"-i", gatewayInterface,
			"-p", proto,
			"--dport", fmt.Sprintf("%d", ingress.Externalport),
			"-j", "DNAT",
			"--to", fmt.Sprintf("%s:%d", ingress.Gatewayaddr, ingress.Gwnetport),
			"-m", "comment", "--comment", "ingress=" + ingName,
		}

		log.Debugf("Adding nat rule to PREROUTING to gateway: %s", strings.Join(iptCmd, " "))

		// AppendUnique will not add a duplicate rule.
		err = podipt.AppendUnique("nat", "PREROUTING", iptCmd...)
		if err != nil {
			return fmt.Errorf("iptables append unique: %v", err)
		}
	}

	return nil
}

func (t *IngressTask) handleDelete(value []byte, version int64) error {

	ingress := new(facility.Ingress)
	err := proto.Unmarshal(value, ingress)
	if err != nil {
		return fmt.Errorf("unmarshal: %v", err)
	}

	log.Debugf("Deleting ingress %v", ingress)

	if ingress.Protocol == xir.Protocol_http {

		// just delete all routers and services for the host in the mz.
		keys := []string{
			fmt.Sprintf("traefik/http/routers/%s.%s.%d/", ingress.Mzid, ingress.Hostname, ingress.Hostport),
			fmt.Sprintf("traefik/http/services/%s.%s.%d/", ingress.Mzid, ingress.Hostname, ingress.Hostport),
		}

		log.Debugf("Deleting keys: %v", keys)

		txo := new(storage.Txo)
		for _, k := range keys {
			txo.Del(k, clientv3.WithPrefix())
		}

		_, err := txo.Exec()
		if err != nil {
			return err
		}
	} else if ingress.Protocol == xir.Protocol_tcp || ingress.Protocol == xir.Protocol_udp {

		podipt, err := iptables.New()
		if err != nil {
			return fmt.Errorf("iptables new: %v", err)
		}

		// example: iptables -t nat -D PREROUTING -i gatewaybr -p tcp -m tcp --dport 8192 -j DNAT --to-destination 10.254.0.12:49153
		proto := ingress.Protocol.String()
		ingName := fmt.Sprintf("%s:%s:%d", ingress.Mzid, ingress.Hostname, ingress.Hostport)
		iptCmd := []string{
			"-i", gatewayInterface,
			"-p", proto,
			"--dport", fmt.Sprintf("%d", ingress.Externalport),
			"-j", "DNAT",
			"--to-destination", fmt.Sprintf("%s:%d", ingress.Gatewayaddr, ingress.Gwnetport),
			"-m", "comment", "--comment", "ingress=" + ingName,
		}

		log.Debugf("Deleting nat rule from PREROUTING on gateway: %s", strings.Join(iptCmd, " "))

		err = podipt.DeleteIfExists("nat", "PREROUTING", iptCmd...)
		if err != nil {
			return fmt.Errorf("iptables delete if exists: %v", err)
		}
	}

	err = deleteInfrapodIngress(ingress)

	return nil
}

func deleteInfrapodIngress(ingress *facility.Ingress) error {

	return runInMtzNetNs(ingress.Mzid, func(ns string) error {

		// remove the rule if it's there.
		podipt, err := iptables.New()
		if err != nil {
			return fmt.Errorf("iptables new: %v", err)
		}

		iptCmd := iptablesInfrapodCmd(ingress)

		log.Debugf("Deleting nat rule to PREROUTING in netns %s: %s", ns, strings.Join(iptCmd, " "))

		// AppendUnique will not add a duplicate rule.
		err = podipt.DeleteIfExists("nat", "PREROUTING", iptCmd...)
		if err != nil {
			return fmt.Errorf("iptables append unique: %v", err)
		}

		return nil
	})
}

func findGatewayLink() (string, error) {

	fac, err := storage.GetModel()
	if err != nil {
		log.Fatalf("load facility model: %v", err)
	}

	for _, r := range fac.Resources {
		if r.Id == hostname {
			if r.HasRole(xir.Role_BorderGateway) {
				for _, n := range r.NICs {
					for _, p := range n.Ports {
						if p.Role == xir.LinkRole_GatewayLink {
							if p.Name == "" {
								return "", fmt.Errorf("Bad model: no Name in GatewayLink ports")
							}

							return p.Name, nil
						}
					}
				}

				// gateway role without gateway link
				return "", fmt.Errorf("bad model: Role_BorderGateway with no GatewayLink")
			}
		}
	}

	// not an error to not have a gateway
	return "", nil

}
