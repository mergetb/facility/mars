package main

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/containers/podman/v4/pkg/bindings"
	"github.com/containers/podman/v4/pkg/bindings/containers"
	"github.com/containers/podman/v4/pkg/bindings/pods"
	log "github.com/sirupsen/logrus"
)

func initPodmanConn() (context.Context, error) {
	var sock_dir string

	if os.Geteuid() == 0 {
		sock_dir = "/run"
	} else {
		sock_dir = os.Getenv("XDG_RUNTIME_DIR")
	}

	socket := "unix:" + sock_dir + "/podman/podman.sock"

	return bindings.NewConnection(context.Background(), socket)
}

func closePodmanConn(ctx context.Context) {
	if ctx == nil {
		return
	}

	conn, err := bindings.GetClient(ctx)
	if err != nil {
		log.Errorf("error: trying to close podman connection: %+v", err)
		return
	}

	conn.Client.CloseIdleConnections()
}

func getNetNs(mzid string) (string, error) {

	ctx, err := initPodmanConn()
	defer closePodmanConn(ctx)
	if err != nil {
		log.Errorf("init podman connection: %v", err)
		return "", err
	}

	pod, err := pods.Inspect(ctx, mzid, nil)
	if err != nil {
		log.Errorf("podman inspect: %v", err)
		return "", err
	}

	cData, err := containers.Inspect(ctx, pod.InfraContainerID, nil)
	if err != nil {
		return "", fmt.Errorf("inspect container %s", pod.InfraContainerID)
	}

	if cData.NetworkSettings == nil {
		return "", fmt.Errorf("no network settings for container")
	}

	// example ns: /run/netns/cni-f611c6a1-aee5-f112-3a14-e3d65da0cf23
	tkns := strings.Split(cData.NetworkSettings.SandboxKey, "/")
	netns := tkns[len(tkns)-1]

	log.Debugf("found netnamespace for %s: %s", mzid, netns)

	return netns, nil
}

func getGwNetAddr(mzid string) (string, error) {

	ctx, err := initPodmanConn()
	defer closePodmanConn(ctx)
	if err != nil {
		log.Errorf("init podman connection: %v", err)
		return "", err
	}

	cName := fmt.Sprintf("%s-infra", mzid)

	// confirm the infrapod exists. wait some reasonable amount
	// of time then give up. caller should correctly handle if the pod
	// does not exist by getting called again (ensure) or giving up
	// properly.
	loops := 30
	exists := false
	for i := 1; i <= loops; i++ {
		exists, err = infrapodExists(mzid)
		if err != nil {
			return "", err
		}
		if exists {
			// hopefully it does not stop existing before we get to it.
			break
		}

		log.Infof("Waiting for %s infrapod to exist. Attempt %d of %d", mzid, i, loops)
		time.Sleep(time.Second * 1)
	}
	if !exists {
		return "", fmt.Errorf("waiting for infrapod to exist")
	}

	cData, err := containers.Inspect(ctx, cName, nil)
	if err != nil {
		return "", fmt.Errorf("inspect container %s", cName)
	}

	gwnet := "52-mars-gateway"
	n, ok := cData.NetworkSettings.Networks[gwnet]
	if !ok {
		return "", fmt.Errorf("no such container network: %s", gwnet)
	}

	return n.IPAddress, nil
}

func infrapodExists(mzid string) (bool, error) {

	ctx, err := initPodmanConn()
	defer closePodmanConn(ctx)
	if err != nil {
		log.Errorf("init podman connection: %v", err)
		return false, err
	}

	name := []string{fmt.Sprintf("%s-infra", mzid)}

	result, err := containers.List(
		ctx,
		&containers.ListOptions{
			Filters: map[string][]string{
				"name": name,
			},
		},
	)

	if err != nil {
		return false, fmt.Errorf("podman containers list: %v", err)
	}

	return len(result) == 1, nil
}
