package main

import (
	"context"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"
	moa "gitlab.com/mergetb/api/moa/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type moaServer struct {
	moa.UnimplementedManageServer
}

func (s *moaServer) Start(ctx context.Context, req *moa.EmulationRequest) (*moa.MoaResponse, error) {
	return &moa.MoaResponse{}, nil
}

func (s *moaServer) Stop(ctx context.Context, req *moa.EmulationRequest) (*moa.MoaResponse, error) {
	return &moa.MoaResponse{}, nil
}

// Return a list of all links that belong to a given materialzation
func (s *moaServer) Show(ctx context.Context, req *moa.EmulationRequest) (*moa.ShowResponse, error) {

	resp := &moa.ShowResponse{
		Mzid:          req.Mzid,
		LinkEmulation: new(portal.LinkEmulationParams),
	}

	err := showEmu(req.Mzid, resp.LinkEmulation)
	if err != nil {
		err = status.Error(codes.Internal, err.Error())
	}

	return resp, err
}

func (s *moaServer) List(ctx context.Context, req *moa.ListRequest) (*moa.ListResponse, error) {
	e := make([]*moa.ShowResponse, 0)
	for _, mzid := range getMzids() {
		e = append(e, &moa.ShowResponse{
			Mzid: mzid,
		})
	}

	return &moa.ListResponse{Emulations: e}, nil
}

func runGrpc() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", service.MoaGRPC))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	moa.RegisterManageServer(grpcServer, &moaServer{})
	if err := grpcServer.Serve(lis); err != nil {
		log.WithFields(log.Fields{"err": err}).Fatal("grpc service")
	}
}
