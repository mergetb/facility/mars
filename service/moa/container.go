package main

// This file contains the backend implementing link emulation via fastclick in a docker container

import (
	// standard

	"fmt"
	"os"
	"time"

	// merge deps

	"gitlab.com/mergetb/facility/mars/internal/moa/container"
	"gitlab.com/mergetb/facility/mars/internal/moa/metrics"
	"gitlab.com/mergetb/facility/mars/internal/storage/threads"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	// deps
	log "github.com/sirupsen/logrus"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	emulationDir      = "/var/moa/emulation"            // directory where click configuration files are stored
	defaultNumThreads = 8                               // default number of threads to create
	ctrStateTime      = time.Duration(10) * time.Second // how frequently to inspect podman containers for status
)

type containerBackend struct {
	Mzid  string
	Links int // the number of links in this emulation
}

// return the click configuration filename for this emulation
func getConfigPath(mzid string) string {
	return fmt.Sprintf("%s/%s/config", emulationDir, mzid)
}

// Return the path for the click control socket for the mzid
func getControlPath(mzid string) string {
	return fmt.Sprintf("%s/%s/control.sock", emulationDir, mzid)
}

// Return the path for the directory containing files related to this mzid
func getEmulationPath(mzid string) string {
	return fmt.Sprintf("%s/%s", emulationDir, mzid)
}

// return the thread thread with the least number of elements packed into it
func getThread(thrElems []int) int {
	idx := 0
	cnt := 1<<31 - 1

	for k, v := range thrElems {
		if v < cnt {
			cnt = v
			idx = k
		}
	}

	return idx
}

// Generate a click config snippet for the specified network link
func genLinkConfig(f *os.File, ld *LinkDef, thrElems []int) {
	fmt.Fprintf(f, "\n// Link ID %d, tags=%v, interfaces=%v\n\n", ld.Id, ld.Tags, ld.Interfaces)

	for _, iface := range ld.Interfaces {
		fmt.Fprintf(f, "%s_random_sample :: RandomSample(DROP %f);\n", iface, ld.Loss)
		fmt.Fprintf(f, "%s_bwdelay :: LinkUnqueue(LATENCY %dns, BANDWIDTH %dbps);\n", iface, ld.Latency, ld.Capacity)
		fmt.Fprintf(f, "%s_input :: XDPFromDevice(DEV %s, MODE skb);\n", iface, iface)
		fmt.Fprintf(f, "%s_set_ts :: SetTimestamp(FIRST true) -> ThreadSafeQueue(1000000) -> CoDel() -> %s_random_sample -> %s_bwdelay -> XDPToDevice(DEV %s, MODE skb);\n\n", iface, iface, iface, iface)
	}

	if len(ld.Interfaces) == 2 {
		// duplex
		fmt.Fprintf(f, "%s_input -> %s_set_ts;\n", ld.Interfaces[0], ld.Interfaces[1])
		fmt.Fprintf(f, "%s_input -> %s_set_ts;\n", ld.Interfaces[1], ld.Interfaces[0])
	} else {
		// mp
		// connect all interfaces via an ethernet switch
		fmt.Fprintf(f, "mpl_%d_switch :: EtherSwitch();\n", ld.Id)
		for n, iface := range ld.Interfaces {
			fmt.Fprintf(f, "%s_bwdelay_ingress :: LinkUnqueue(LATENCY 0ns, BANDWIDTH %dbps);\n", iface, ld.Capacity)
			fmt.Fprintf(f, "%s_input -> ThreadSafeQueue(1000000) -> %s_bwdelay_ingress -> [%d]mpl_%d_switch;\n", iface, iface, n, ld.Id)
			fmt.Fprintf(f, "mpl_%d_switch[%d] -> %s_set_ts;\n", ld.Id, n, iface)
		}
	}

	// Map this group of interfaces to a click router thread.
	// Keep all interfaces that are part of a MPL in the same thread, but
	// try to distribute equal number of elements among available threads.
	tid := getThread(thrElems)
	thrElems[tid] += len(ld.Interfaces)

	fmt.Fprintf(f, "StaticThreadSched(")
	for n, iface := range ld.Interfaces {
		if n > 0 {
			fmt.Fprintf(f, ", ")
		}
		fmt.Fprintf(f, "%s_input %d, %s_bwdelay %d", iface, tid, iface, tid)
		if len(ld.Interfaces) > 2 {
			fmt.Fprintf(f, ", %s_bwdelay_ingress %d", iface, tid)
		}
	}
	fmt.Fprintf(f, ");\n")
}

func numThreads(mzid string) (int, error) {

	// first look up the mzid, then the "default" key
	for _, s := range []string{mzid, "default"} {
		nthreads, err := threads.Get(s)
		if err != nil {
			return 0, fmt.Errorf("get thread config: %w", err)
		}

		if nthreads > 0 {
			return int(nthreads), nil
		}
	}
	return defaultNumThreads, nil // fall back
}

///////////////////////////////////////////////////////////////////////////////
// Reconciler handlers

func (emu *containerBackend) Create(defs []*LinkDef) error {
	// stop first in case of old version
	container.Stop(emu.Mzid)

	// Consult etcd to determine how many threads to use for this emulation
	nthreads, err := numThreads(emu.Mzid)
	if err != nil {
		return fmt.Errorf("get thread config: %s: %w", emu.Mzid, err)
	}

	// if there are fewer links than requested threads, adjust the number of threads
	if nthreads > emu.Links {
		log.WithFields(log.Fields{"mzid": emu.Mzid, "links": emu.Links, "threads": nthreads}).Info("reducing threads for small emulation")
		nthreads = emu.Links
	}

	log.WithFields(log.Fields{"mzid": emu.Mzid, "threads": nthreads}).Info("create emulation")

	// keep track of how many click elements are used per thread
	thrElems := make([]int, nthreads)

	emudir := getEmulationPath(emu.Mzid)

	// start from a clean slate
	err = os.RemoveAll(emudir)
	if err != nil {
		return fmt.Errorf("rmdir: %s: %v", emudir, err)
	}

	err = os.MkdirAll(emudir, 0755)
	if err != nil {
		return fmt.Errorf("mkdir: %s: %v", emudir, err)
	}

	var fp *os.File
	cfgPath := getConfigPath(emu.Mzid)
	fp, err = os.Create(cfgPath)
	if err != nil {
		return fmt.Errorf("create: %s: %v", cfgPath, err)
	}
	defer fp.Close()
	fmt.Fprintf(fp, "// Emulation Mzid %s, created by moa on %s\n", emu.Mzid, time.Now())

	// create vteps for the links
	err = createVteps(emu.Mzid, defs)
	if err != nil {
		return fmt.Errorf("create vteps: %v", err)
	}

	// generate click configs for the links
	for _, d := range defs {
		genLinkConfig(fp, d, thrElems)
	}

	err = container.Create(emu.Mzid, nthreads)
	if err != nil {
		return fmt.Errorf("create container: %v", err)
	}

	metrics.AddThreads(emu.Mzid, nthreads)

	return nil
}

func (emu *containerBackend) Delete(defs []*LinkDef) error {
	// Handle the deletion as best-effort, and signal if any errors occurred during teardown
	var ret error = nil

	err := container.Stop(emu.Mzid)
	if err != nil {
		log.WithFields(log.Fields{"err": err, "mzid": emu.Mzid}).Error("stop container")
		ret = err
	}

	p := getEmulationPath(emu.Mzid)
	err = os.RemoveAll(p)
	if err != nil {
		log.WithFields(log.Fields{"path": p, "err": err}).Error("remove directory")
		ret = err
	}

	deleteVteps(emu.Mzid, defs)

	metrics.DelThreads(emu.Mzid)

	return ret
}

func (emu *containerBackend) Update(ld *LinkDef, p *LinkDef) error {
	conn, err := Connect(getControlPath(emu.Mzid))
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("failed to connect to click socket")
		return status.Error(codes.Internal, "failed to connect to click control socket")
	}
	defer conn.Close()

	if p.Capacity != ld.Capacity {

		// for LANs, update the ingress element as well
		do_ingress := false
		if len(ld.Interfaces) > 2 {
			do_ingress = true
		}

		for _, iface := range ld.Interfaces {
			v := fmt.Sprintf("%dbps", ld.Capacity)
			if err := conn.WriteHandler(fmt.Sprintf("%s_bwdelay", iface), "bandwidth", v); err != nil {
				log.WithFields(log.Fields{"err": err}).Error("write handler failed")
				return fmt.Errorf("click write handler failed")
			}
			if do_ingress {
				if err := conn.WriteHandler(fmt.Sprintf("%s_bwdelay_ingress", iface), "bandwidth", v); err != nil {
					log.WithFields(log.Fields{"err": err}).Error("write handler failed")
					return fmt.Errorf("click write handler failed")
				}
			}
		}
	}
	if p.Latency != ld.Latency {
		for _, iface := range ld.Interfaces {
			v := fmt.Sprintf("%dns", ld.Latency)
			if err := conn.WriteHandler(fmt.Sprintf("%s_bwdelay", iface), "latency", v); err != nil {
				log.WithFields(log.Fields{"err": err}).Error("write handler failed")
				return fmt.Errorf("click write handler failed")
			}
		}
	}
	if p.Loss != ld.Loss {
		for _, iface := range ld.Interfaces {
			v := fmt.Sprintf("%f", ld.Loss)
			if err := conn.WriteHandler(fmt.Sprintf("%s_random_sample", iface), "drop_prob", v); err != nil {
				log.WithFields(log.Fields{"err": err}).Error("write handler failed")
				return fmt.Errorf("click write handler failed")
			}
		}
	}

	return nil
}

// Thread for getting click container status every 10s
func doContainerStatus() {
	for {
		mzids := getEmulationsByType(xir.Emulation_Click)

		if err := container.CollectMetrics(mzids...); err != nil {
			log.WithFields(log.Fields{"err": err}).Error("collect podman container metrics")
		}
		time.Sleep(ctrStateTime)
	}
}
