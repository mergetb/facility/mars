package main

import (
	// standard
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	// merge deps
	moa "gitlab.com/mergetb/api/moa/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/moa/metrics"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	mars "gitlab.com/mergetb/facility/mars/pkg"
	"gitlab.com/mergetb/facility/mars/pkg/moautil"
	"gitlab.com/mergetb/facility/mars/service"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	// deps
	log "github.com/sirupsen/logrus"
	bolt "go.etcd.io/bbolt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type LinkDef struct {
	Id         int      // unique link ID (internal, no clients use this)
	Interfaces []string // network interfaces for this link
	Capacity   uint64   // bps
	Latency    uint64   // ns
	Loss       float32  // drop prob
	Tags       []string // tags from the XIR
	Nodes      []string // experimental node names connected to this link
}

type EmuBackend interface {
	Create(defs []*LinkDef) error            // create the emulation given a set of links
	Delete([]*LinkDef) error                 // delete the emulation
	Update(def *LinkDef, old *LinkDef) error // update link parameters
}

const (
	dbpath     = "/var/moa/state.db"
	backendKey = "backend/"
)

var (
	db       *bolt.DB
	hostname string
)

///////////////////////////////////////////////////////////////////////////////
// Reconciler handlers

// returns the number of emulated links configured for this emu server
func numLinks(mz *portal.Materialization) int {
	n := 0
	if mz.LinkEmulation != nil {
		for _, l := range mz.LinkEmulation.Links {
			// support older emultions with a single emulation server, or new-style which configures emu server per link
			if mz.LinkEmulation.Server == hostname || l.Server == hostname {
				n = n + 1
			}
		}
	}
	return n
}

func createEmulationReal(mzid string) error {
	mz, err := storage.FetchMaterialization(mzid)
	if err != nil {
		return fmt.Errorf("unable to fetch mz %s: %w", mzid, err)
	}

	nlinks := numLinks(mz)
	if nlinks == 0 {
		log.WithFields(log.Fields{"mzid": mzid}).Info("this server is not needed")
		return nil // no emulation
	}

	var emu EmuBackend

	switch mz.GetLinkEmulation().GetBackend() {
	case xir.Emulation_Click:
		emu = &containerBackend{Mzid: mzid, Links: nlinks}

	//case xir.Emulation_Netem:
	default:
		// default to netem-based backend
		emu = &netemBackend{Mzid: mzid}
	}

	log.WithFields(log.Fields{"mzid": mzid, "backend": mz.GetLinkEmulation().GetBackend().String()}).Info("create emulation")

	// map of tags to link-ID
	tags := make(map[string][]string)

	numEndpoints := 0
	numLinks := 0
	var prefix string

	defs := make([]*LinkDef, 0)

	err = db.Update(func(tx *bolt.Tx) error {
		// ensure no stale state
		tx.DeleteBucket([]byte(mzid))

		// create a root-level bucket for this emulation
		b, err := tx.CreateBucket([]byte(mzid))
		if err != nil {
			return fmt.Errorf("create bucket: %s: %v", mzid, err)
		}

		// loop over all links defined for this emulation
		for _, link := range mz.LinkEmulation.Links {
			if link.Server != hostname && mz.LinkEmulation.Server != hostname {
				continue // link not handled by this emu server
			}

			id, _ := b.NextSequence()

			e := &LinkDef{
				Id:         int(id),
				Interfaces: link.GetInterfaces(),
				Tags:       link.GetTags(),
				Capacity:   link.GetCapacity(),
				Latency:    link.GetLatency(),
				Loss:       link.GetLoss(),
				Nodes:      link.GetNodes(),
			}
			defs = append(defs, e)

			numEndpoints += len(e.Interfaces)

			prefix = fmt.Sprintf("link/%d/", e.Id)
			err = storeLink(b, prefix, e)
			if err != nil {
				return fmt.Errorf("store link: %v", err)
			}

			// update tag map to include this link
			for _, tag := range e.Tags {
				taglist, ok := tags[tag]
				if !ok {
					taglist = make([]string, 0)
				}
				taglist = append(taglist, prefix)
				tags[tag] = taglist
			}

			numLinks += 1
		}

		// write the tag map into the DB
		for tag, v := range tags {
			buf, err := json.Marshal(v)
			if err != nil {
				return fmt.Errorf("marshal: %v", err)
			}
			// append a trailing slash on the tag so we don't match "ab" against "abab"
			prefix = fmt.Sprintf("tag/%s/", tag)
			err = b.Put([]byte(prefix), buf)
			if err != nil {
				return fmt.Errorf("put key: %v", err)
			}
		}

		// store the configured emulation backend
		err = b.Put([]byte(backendKey), []byte(mz.GetLinkEmulation().GetBackend().String()))
		if err != nil {
			return fmt.Errorf("put backend end: %v", err)
		}

		return nil
	})

	if err != nil {
		return fmt.Errorf("db update: %v", err)
	}

	err = emu.Create(defs)
	if err != nil {
		return fmt.Errorf("create emulation backend: %v", err)
	}

	metrics.AddEndpoints(mzid, numEndpoints)
	metrics.AddLinks(mzid, numLinks)

	log.WithFields(log.Fields{"mzid": mzid}).Info("emulation created")

	return nil
}

// Wrapper around emulation creation to provide a single point for logging and metric generation
func createEmulation(mzid string) (err error) {
	if err = createEmulationReal(mzid); err != nil {
		log.WithFields(log.Fields{"mzid": mzid, "err": err}).Error("create emulation")
		metrics.EmuError(mzid, "create")
	}
	return
}

func fetchEmulation(tx *bolt.Tx, mzid string) (b *bolt.Bucket, err error) {
	b = tx.Bucket([]byte(mzid))
	if b == nil {
		log.WithFields(log.Fields{"mzid": mzid}).Error("emulation bucket does not exist")
		err = status.Error(codes.InvalidArgument, "emulation does not exist")
	}
	return
}

// Fetch the network emulation backend configured for this Mzid
func fetchBackend(b *bolt.Bucket) (xir.Emulation, error) {
	k := b.Get([]byte(backendKey))
	if k != nil {
		v, ok := xir.Emulation_value[string(k)]
		if ok {
			return xir.Emulation(v), nil
		}
		log.WithFields(log.Fields{"backend": string(k)}).Error("unknown moa backend")
		return -1, fmt.Errorf("unknown backend: %v", string(k))
	}
	// else use netem default

	return 0, nil
}

func newBackend(btype xir.Emulation, mzid string) EmuBackend {
	switch btype {
	case xir.Emulation_Click:
		return &containerBackend{Mzid: mzid}

	default:
		return &netemBackend{Mzid: mzid}
	}
}

func getBackend(b *bolt.Bucket, mzid string) (EmuBackend, error) {
	btype, err := fetchBackend(b)
	if err != nil {
		return nil, err
	}

	return newBackend(btype, mzid), nil
}

// return a list of mzids where the emulation backend is of the given type
func getEmulationsByType(t xir.Emulation) []string {
	mzids := getMzids()
	ret := make([]string, 0)

	_ = db.View(func(tx *bolt.Tx) error {
		for _, k := range mzids {
			b, err := fetchEmulation(tx, k)
			if err != nil {
				continue
			}
			emu, err := fetchBackend(b)
			if err != nil {
				continue
			}
			if emu == t {
				ret = append(ret, k)
			}
		}

		return nil
	})

	return ret
}

func deleteEmulation(mzid string) {

	var emu EmuBackend = nil

	// Handle the deletion as best-effort, and signal if any errors occurred during teardown
	has_error := false

	defs := make([]*LinkDef, 0)

	_ = db.Update(func(tx *bolt.Tx) error {
		b, err := fetchEmulation(tx, mzid)
		if err != nil {
			has_error = true
			return nil
		}

		emu, err = getBackend(b, mzid)
		if err != nil {
			has_error = true
		}

		b.ForEach(func(k, v []byte) error {
			if strings.HasPrefix(string(k), "link/") {
				ld := parseLink(v)
				if ld != nil {
					defs = append(defs, ld)
				}
			}
			return nil
		})

		err = tx.DeleteBucket([]byte(mzid))
		if err != nil {
			log.WithFields(log.Fields{"err": err, "mzid": mzid}).Error("delete bucket")
			has_error = true
		}

		has_error = false
		return nil
	})

	if emu != nil {
		err := emu.Delete(defs)
		if err != nil {
			log.WithFields(log.Fields{"err": err, "mzid": mzid}).Error("delete emulation backend")
			has_error = true
		}
	}

	metrics.DelEndpoints(mzid)
	metrics.DelLinks(mzid)
	if has_error {
		metrics.EmuError(mzid, "delete")
	}

	log.WithFields(log.Fields{"mzid": mzid, "error": has_error}).Info("emulation deleted")
}

func parseLink(buf []byte) *LinkDef {
	var ld LinkDef
	err := json.Unmarshal(buf, &ld)
	if err != nil {
		log.WithFields(log.Fields{"buf": buf}).Error("unable to parse linkdef json")
		return nil
	}
	return &ld
}

// fetch the LinkDef from the db and deserialize the json
func fetchLink(b *bolt.Bucket, s string) *LinkDef {
	v := b.Get([]byte(s))
	if v == nil {
		log.WithFields(log.Fields{"link": s}).Error("unable to fetch linkdef")
		return nil
	}
	return parseLink(v)
}

func storeLink(b *bolt.Bucket, key string, ld *LinkDef) error {
	buf, err := json.Marshal(ld)
	if err != nil {
		log.Error("failed to marshal linkdef")
		return status.Error(codes.Internal, "failed to put marshal link def")
	}

	err = b.Put([]byte(key), buf)
	if err != nil {
		log.WithFields(log.Fields{"link": key, "json": string(buf)}).Error("failed to put link update")
		return status.Error(codes.Internal, "failed to put link update")
	}

	return nil
}

func updateLink(mzid string, b *bolt.Bucket, link string, p *moa.LinkUpdate) error {
	ld := fetchLink(b, link)
	if ld == nil {
		return status.Error(codes.Internal, "failed to fetch link def")
	}

	changed := false
	old := &LinkDef{
		Capacity: ld.Capacity,
		Latency:  ld.Latency,
		Loss:     ld.Loss,
	}
	if p.Capacity != 0 && p.Capacity != ld.Capacity {
		log.WithFields(log.Fields{"new": p.Capacity, "old": ld.Capacity, "id": ld.Id}).Info("link capacity changed")
		changed = true
		ld.Capacity = p.Capacity
	}
	if p.Latency != 0 && p.Latency != ld.Latency {
		log.WithFields(log.Fields{"new": p.Latency, "old": ld.Latency, "id": ld.Id}).Info("link latency changed")
		changed = true
		ld.Latency = p.Latency
	}
	if p.LossEnforce && p.Loss != ld.Loss {
		log.WithFields(log.Fields{"new": p.Loss, "old": ld.Loss, "id": ld.Id}).Info("link loss changed")
		changed = true
		ld.Loss = p.Loss
	}
	if changed {
		emu, err := getBackend(b, mzid)
		if err != nil {
			return status.Error(codes.Internal, "unknown moa backend type")
		}

		err = emu.Update(ld, old)
		if err != nil {
			return err
		}

		err = storeLink(b, link, ld)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *moaServer) Update(ctx context.Context, req *moa.UpdateRequest) (*moa.MoaResponse, error) {
	metrics.LinkUpdate(req.Mzid)

	err := db.Update(func(tx *bolt.Tx) error {
		b, err := fetchEmulation(tx, req.Mzid)
		if err != nil {
			return err
		}

		// for each link update
		for _, u := range req.GetUpdates() {
			// for each tag that belongs to this link update
			for _, tag := range u.GetTags() {
				// retrieve the click elements that match this tag
				key := fmt.Sprintf("tag/%s/", tag)
				v := b.Get([]byte(key))
				if v == nil {
					continue // tag not handled by this emu server
				}

				// the value is a json-encoded list of link keys
				var e []string
				err := json.Unmarshal(v, &e)
				if err != nil {
					log.WithFields(log.Fields{"json": v}).Error("unable to unmarshal json")
					return status.Error(codes.Internal, "unable to unmarshal json tag entry")
				}

				// list of link keys
				for _, link := range e {
					err = updateLink(req.Mzid, b, link, u)
					if err != nil {
						return err
					}
				}
			}
		}

		return nil
	})

	if err != nil {
		metrics.EmuError(req.Mzid, "update")
	}

	return &moa.MoaResponse{}, err
}

func showEmu(mzid string, params *portal.LinkEmulationParams) error {

	err := db.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(mzid))

		// all links in this emulation have a "link/" prefix for the key
		prefix := []byte("link/")
		linkCursor := b.Cursor()
		for k, v := linkCursor.Seek(prefix); k != nil && bytes.HasPrefix(k, prefix); k, v = linkCursor.Next() {
			link := parseLink(v)
			if link == nil {
				return errors.New("parse link def")
			}
			params.Links = append(params.Links, &portal.LinkEmulation{
				Interfaces: link.Interfaces,
				Capacity:   link.Capacity,
				Latency:    link.Latency,
				Loss:       link.Loss,
				Tags:       link.Tags,
				Nodes:      link.Nodes,
			})
		}

		return nil
	})

	return err
}

// Returns a slice containing all known materialization IDs from the local db
func getMzids() []string {
	mzs := make([]string, 0)

	_ = db.View(func(tx *bolt.Tx) error {
		// all the toplevel keys are materialization names
		c := tx.Cursor()
		for k, _ := c.First(); k != nil; k, _ = c.Next() {
			mzs = append(mzs, string(k))
		}
		return nil
	})

	return mzs
}

func init() {
	var err error

	internal.InitLogging()
	internal.InitReconciler()

	hostname, err = os.Hostname()
	if err != nil {
		log.Fatalf("unable to get hostname: %v", err)
	}
	hostname = strings.Split(hostname, ".")[0]
	log.Infof("moa %s starting up on %s:%d", mars.Version, hostname, service.MoaGRPC)

	if db, err = bolt.Open(dbpath, 0666, nil); err != nil {
		log.WithFields(log.Fields{"err": err.Error(), "path": dbpath}).Fatal("open database")
	}

	if err = storage.InitMarsEtcdClient(); err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	if err = moautil.InitMarsMinIOClientFromConfig(); err != nil {
		log.Fatalf("minio init: %v", err)
	}
}

func main() {
	go runGrpc()
	go metrics.Serve()
	go doContainerStatus()
	runReconciler()
}
