package main

// This file contains the backend for implementing link emulation use the netem kernel module

import (
	// standard

	"fmt"
	"os/exec"

	// merge deps

	"gitlab.com/mergetb/tech/rtnl"

	// deps
	log "github.com/sirupsen/logrus"
)

type netemBackend struct {
	Mzid string
}

func (emu *netemBackend) Create(defs []*LinkDef) error {
	if err := createVteps(emu.Mzid, defs); err != nil {
		return fmt.Errorf("createVteps: %v", err)
	}

	for _, d := range defs {
		err := configNetemLink(d)
		if err != nil {
			return fmt.Errorf("configNetemLink: %v", err)
		}
	}
	return nil
}

func ensureNoBridge(rtx *rtnl.Context, name string) error {
	bridge := rtnl.NewLink()

	bridge.Info.Name = name
	bridge.Info.Bridge = &rtnl.Bridge{}

	return bridge.Absent(rtx)
}

func ensureBridge(rtx *rtnl.Context, name string) (*rtnl.Link, error) {
	bridge := rtnl.NewLink()

	bridge.Info.Name = name
	bridge.Info.Bridge = &rtnl.Bridge{}

	err := bridge.Present(rtx)
	if err != nil {
		return nil, err
	}

	err = bridge.Up(rtx)
	if err != nil {
		return nil, err
	}

	return bridge, nil
}

// Delete netem config for the specified network link
func delLinkConfig(ld *LinkDef) error {
	log.WithFields(log.Fields{"link ID": ld.Id, "tags": ld.Tags, "interfaces": ld.Interfaces}).Info("deleting netem bridge")

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// ensure no bridge based on the first interface
	br := fmt.Sprintf("br-%s", ld.Interfaces[0])
	return ensureNoBridge(rtx, br)
}

// Generate a netem config for the specified network link
func configNetemLink(ld *LinkDef) error {
	l := log.WithFields(log.Fields{
		"link ID":    ld.Id,
		"tags":       ld.Tags,
		"interfaces": ld.Interfaces,
	})

	l.Info("generating netem config")

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// ensure bridge based on the first interface
	br := fmt.Sprintf("br-%s", ld.Interfaces[0])
	bridge, err := ensureBridge(rtx, br)
	if err != nil {
		log.WithFields(log.Fields{"bridge": br, "err": err, "caller": "configNetemLink", "fn": "ensureBridge"}).Error("failed to create bridge")
		return fmt.Errorf("ensureBridge: %s: %v", br, err)
	}

	for _, ifx := range ld.Interfaces {
		lnk, err := rtnl.GetLink(rtx, ifx)
		if err != nil {
			return fmt.Errorf("GetLink: %s: %v", ifx, err)
		}

		err = lnk.SetMaster(rtx, int(bridge.Msg.Index))
		if err != nil {
			return fmt.Errorf("SetMaster: %s: %v", ifx, err)
		}

		l.Debugf("added ifx %s to bridge %s", lnk.Info.Name, br)

		// delete existing qdisc (ok if error, likely enodev)
		cmd := exec.Command("tc", "qdisc", "del", "dev", ifx, "root")
		out, err := cmd.CombinedOutput()
		if err != nil {
			log.Debugf("delete qdisc: %s", out)
		}

		// create netem
		args := []string{"qdisc", "add", "dev", ifx, "root", "netem"}

		// capacity
		if ld.Capacity > 0 {
			v := fmt.Sprintf("%dkbit", ld.Capacity/1000)
			args = append(args, []string{"rate", v}...)
		}

		// loss/latency
		if ld.Loss > 0 {
			v := fmt.Sprintf("%d", int(ld.Loss*100))
			args = append(args, []string{"loss", "random", v}...)
		}

		// latency
		if ld.Latency > 0 {
			v := fmt.Sprintf("%dms", ld.Latency/1000000)
			args = append(args, []string{"delay", v}...)
		}

		cmd = exec.Command("tc", args...)
		l.Debug(cmd)
		out, err = cmd.CombinedOutput()
		if err != nil {
			return fmt.Errorf("create qdisc: %s", out)
		}
	}

	return nil
}

///////////////////////////////////////////////////////////////////////////////
// Reconciler handlers

func (emu *netemBackend) Delete(defs []*LinkDef) error {
	for _, ld := range defs {
		err := delLinkConfig(ld)
		if err != nil {
			log.WithFields(log.Fields{"ld": ld, "err": err}).Error("unable to delete link config")
		}
	}

	deleteVteps(emu.Mzid, defs)
	return nil // best effort
}

func (emu *netemBackend) Update(ld *LinkDef, old *LinkDef) error {
	// update config
	err := configNetemLink(ld)
	if err != nil {
		return err
	}
	return nil
}
