package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	rec "gitlab.com/mergetb/tech/reconcile"
)

type MoaTask struct {
	Mzid string
}

func (t *MoaTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.EmuKey {
		return false
	}

	t.Mzid = xk.Name

	return true
}

func (t *MoaTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	return rec.CheckErrorToMessage(createEmulation(t.Mzid))
}

func (t *MoaTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.WithFields(log.Fields{"mzid": t.Mzid}).Error("emulation update not supported")
	return rec.TaskMessageWarningf("[%s] ensure not supported", t.Mzid)
}

func (t *MoaTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	// createEmulation is idempotent so calling it if the emulation already exists is safe
	return rec.CheckErrorToMessage(createEmulation(t.Mzid))
}

func (t *MoaTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	deleteEmulation(t.Mzid)
	return nil
}

func runReconciler() {
	t := rec.ReconcilerManager{
		Manager:                    "moa",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  "/mzparam/",
			Name:    hostname,
			Desc:    fmt.Sprintf("emulation manager service on hostname: %s", hostname),
			Actions: &MoaTask{},
		}},
	}

	log.Info("starting moa reconciler loop")

	t.Run()
}
