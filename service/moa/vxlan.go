package main

// This file contains code for creating and deleting vxlan tunnel endpoint devices (vteps)
// for emulations

import (
	// merge deps
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/canopy"

	// deps
	log "github.com/sirupsen/logrus"
)

func createVteps(mzid string, defs []*LinkDef) error {
	for _, ld := range defs {
		for _, ifx := range ld.Interfaces {
			l := log.WithFields(log.Fields{
				"mzid":      mzid,
				"interface": ifx,
			})

			l.Debug("creating vtep")

			// get vtep config from storage
			vtep, err := storage.GetVtep(hostname, ifx)
			if err != nil {
				l.Errorf("could not get vtep state from storage: %+v", err)
				return err
			}

			// setup the vtep
			err = canopy.CreateVtep(vtep)
			if err != nil {
				l.Errorf("could not create vtep: %+v", err)
				return err
			}

			l.Info("created vtep for emulation")
		}
	}

	return nil
}

func deleteVteps(mzid string, defs []*LinkDef) error {
	for _, ld := range defs {
		for _, ifx := range ld.Interfaces {
			l := log.WithFields(log.Fields{
				"mzid":      mzid,
				"interface": ifx,
			})

			// setup the vtep
			err := canopy.DeleteVtep(ifx)
			if err != nil {
				l.Errorf("delete vtep: %+v", err)
			}

			l.Info("deleted vtep for emulation")
		}
	}

	// best effort
	return nil
}
