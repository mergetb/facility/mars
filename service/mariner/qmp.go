// This file is derived with modifications from: https://github.com/quadrifoglio/go-qmp,

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"strings"
)

const (
	MessageTypeSuccess = 1
	MessageTypeError   = 2
	MessageTypeEvent   = 3
)

type JsonValue interface{}
type JsonObject map[string]interface{}
type JsonArray []interface{}

type MessageType byte

// QMPSession represents a connection to a QMP instance
type QMPSession struct {
	c io.ReadWriteCloser

	Greeting      GreetingMessage
	AsyncMessages []AsyncMessage
}

// GreetingMessage is the first message sent by QMP
// It contains information about the QEMU instance
type GreetingMessage struct {
	QMP struct {
		Version struct {
			Qemu struct {
				Micro int `json:"micro"`
				Minor int `json:"minor"`
				Major int `json:"major"`
			} `json:"qemu"`

			Package string `json:"package"`
		} `json:"version"`

		Capabilities []interface{} `json:"capabilities"`
	}
}

// Timestamp is the timestamp representation in QMP messages
type Timestamp struct {
	Seconds      uint64 `json:"seconds"`
	Microseconds uint64 `json:"microseconds"`
}

// SuccessMessage is a successfull response from QMP
type SuccessMessage struct {
	Return JsonValue `json:"return"`
}

// ErrorMessage represents an error sent by QMP
type ErrorMessage struct {
	Error struct {
		Class string `json:"class"`
		Desc  string `json:"desc"`
	} `json:"error"`
}

// AsyncMessage is an asynchronous event sent by QMP
type AsyncMessage struct {
	Event     string     `json:"event"`
	Data      JsonObject `json:"data"`
	Timestamp Timestamp  `json:"timestamp"`
}

func QMPOpen(proto string, path string) (QMPSession, error) {
	var q QMPSession

	c, err := net.Dial(proto, path)
	if err != nil {
		return q, err
	}
	q.c = c

	err = json.NewDecoder(c).Decode(&q.Greeting)
	if err != nil {
		c.Close()
		return q, fmt.Errorf("cannot decode qmp JSON greeting: %v", err)
	}

	_, err = q.Command("qmp_capabilities", nil)
	if err != nil {
		return q, err
	}

	return q, nil
}

func (q QMPSession) Close() error {
	return q.c.Close()
}

func (q QMPSession) Command(command string, arguments map[string]interface{}) (JsonValue, error) {
	cmd := make(JsonObject)
	cmd["execute"] = command

	if arguments != nil {
		cmd["arguments"] = arguments
	}

	err := json.NewEncoder(q.c).Encode(cmd)
	if err != nil {
		return nil, fmt.Errorf("qmp: encode json: %s", err)
	}

	t, data, err := q.read()
	if err != nil {
		return nil, err
	}

	if t == MessageTypeSuccess {
		var m SuccessMessage

		err := json.Unmarshal(data, &m)
		if err != nil {
			return nil, fmt.Errorf("qmp: decode json error: %s", err)
		}

		return m.Return, nil
	} else if t == MessageTypeError {
		var e ErrorMessage

		err := json.Unmarshal(data, &e)
		if err != nil {
			return nil, fmt.Errorf("qmp: decode json error: %s", err)
		}

		return nil, fmt.Errorf("qmp error %s: %s", e.Error.Class, e.Error.Desc)
	}

	return nil, fmt.Errorf("qmp: unknown message")
}

func (q QMPSession) CommandEncodeJSON(command string, arguments JsonValue) (JsonValue, error) {
	var m map[string]interface{}

	bytes, err := json.Marshal(arguments)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(bytes, &m)
	if err != nil {
		return nil, err
	}

	return q.Command(command, m)
}

// HumanMonitorCommand sends a HMP command to QEMU via the QMP protocol
func (q QMPSession) HumanMonitorCommand(command string) (JsonValue, error) {
	return q.Command("human-monitor-command", map[string]interface{}{"command-line": command})
}

func (q *QMPSession) read() (MessageType, []byte, error) {
	scanner := bufio.NewScanner(q.c)
	for scanner.Scan() {
		str := scanner.Text()

		if strings.Contains(str, "\"return\"") {
			return MessageTypeSuccess, []byte(str), nil
		} else if strings.Contains(str, "\"error\"") {
			return MessageTypeError, []byte(str), nil
		} else if strings.Contains(str, "\"event\"") {
			var e AsyncMessage

			err := json.Unmarshal([]byte(str), &e)
			if err != nil {
				return 0, nil, fmt.Errorf("qmp: json: failed to decode event: %s", err)
			}

			q.AsyncMessages = append(q.AsyncMessages, e)
			continue
		}
	}

	if err := scanner.Err(); err != nil {
		return 0, nil, fmt.Errorf("qmp: failed to read line: %s", err)
	}

	return 0, nil, fmt.Errorf("qmp: invalid response")
}

func (q *QMPSession) readAsync() (AsyncMessage, error) {
	var e AsyncMessage

	if len(q.AsyncMessages) > 0 {
		e = q.AsyncMessages[0]
		q.AsyncMessages = q.AsyncMessages[1:]
		return e, nil
	}

	scanner := bufio.NewScanner(q.c)
	for scanner.Scan() {
		str := scanner.Text()

		if strings.Contains(str, "\"return\"") {
			return e, fmt.Errorf("received unexpected MessageTypeSuccess")
		} else if strings.Contains(str, "\"error\"") {
			return e, fmt.Errorf("received unexpected MessageTypeError")
		} else if strings.Contains(str, "\"event\"") {
			err := json.Unmarshal([]byte(str), &e)
			if err != nil {
				return e, fmt.Errorf("qmp: json: failed to decode event: %s", err)
			}

			return e, nil
		}
	}

	if err := scanner.Err(); err != nil {
		return e, fmt.Errorf("qmp: failed to read line: %s", err)
	}

	return e, fmt.Errorf("qmp: invalid response")
}
