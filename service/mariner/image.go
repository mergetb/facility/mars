package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"

	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/tech/shared/storage/decompression"
)

const (
	images_addr   = "images:9000"
	images_bucket = "images"
)

var (
	cache_dir string
	meta_file string
	image_cli *minio.Client
)

type ImageRevision struct {
	Name      string
	Md5sum    string
	Timestamp int64
	Fqvms     map[string]bool
	IsBios    bool
}

// The cache is a collection of metadata indexed by the well known image "name" (e.g., "bullseye-rootfs")
type CacheDat struct {
	// For each well-known name, a slice of all of the revisions of that image that we've downloaded,
	// sorted by newest to oldest
	Images map[string][]*ImageRevision
}

func (i *ImageRevision) String() string {
	return fmt.Sprintf("%s-%d", i.Name, i.Timestamp)
}

func (i *ImageRevision) Path() string {
	return fmt.Sprintf("%s/%s", cache_dir, i.String())
}

func initImagesClient() error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	username := cfg.Services.Infrapod.Minio.Credentials.Username
	password := cfg.Services.Infrapod.Minio.Credentials.Password

	image_cli, err = minio.New(
		images_addr,
		&minio.Options{
			Creds: credentials.NewStaticV4(
				username,
				password,
				"",
			),
		},
	)
	if err != nil {
		return fmt.Errorf("init images client: %v", err)
	}

	return nil
}

// initializes an image cache based on the current time
func initImageCache() error {
	err := initImagesClient()
	if err != nil {
		return err
	}

	cache_dir = fmt.Sprintf("%s/img.d", mariner_dir)
	err = os.MkdirAll(cache_dir, 0755)
	if err != nil {
		return err
	}

	meta_file = fmt.Sprintf("%s/img.d/meta.json", mariner_dir)
	_, err = readCacheDat()
	if err == nil {
		// if we have a cache, reap any stale revisions
		reapStaleRevisions()
		return nil
	}

	// no cache yes; initialize metadata
	cache := &CacheDat{
		Images: make(map[string][]*ImageRevision),
	}

	err = writeCacheDat(cache)
	if err != nil {
		return err
	}

	log.Debugf("initialized image cache metadata")
	return nil
}

func readCacheDat() (*CacheDat, error) {
	cache := new(CacheDat)

	dat, err := ioutil.ReadFile(meta_file)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(dat, &cache)
	if err != nil {
		return nil, err
	}

	return cache, nil
}

func writeCacheDat(cache *CacheDat) error {
	res, err := json.Marshal(cache)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(meta_file, []byte(res), 0644)
	if err != nil {
		return fmt.Errorf("failed to write '%s': %v", meta_file, err)
	}

	return nil
}

func readImageRevisionNewest(image string) (*CacheDat, *ImageRevision, error) {
	cache, err := readCacheDat()
	if err != nil {
		return nil, nil, err
	}

	if revs, ok := cache.Images[image]; ok {
		// return most recent image
		return cache, revs[0], nil
	}

	return nil, nil, fmt.Errorf("no metadata for image '%s' in cache", image)
}

func writeNewImageRevision(dat *ImageRevision) error {
	cache, err := readCacheDat()
	if err != nil {
		return err
	}

	// prepend and save
	if _, ok := cache.Images[dat.Name]; !ok {
		cache.Images[dat.Name] = []*ImageRevision{dat}
	} else {
		cache.Images[dat.Name] = append(
			[]*ImageRevision{dat},
			cache.Images[dat.Name]...,
		)
	}

	return writeCacheDat(cache)
}

func writeLocalImage(dat *ImageRevision, object *minio.Object, decompress bool) error {
	fname := dat.Path()

	if decompress {
		log.Infof("fetching and decompressing image '%s' from minio server", dat.Name)

		err := decompression.MagicDecompressorSparseBySeekable(fname, object)

		if err != nil {
			return fmt.Errorf("failed to copy compressed minio image to local file '%s': %v", fname, err)
		}
	} else {
		log.Infof("fetching image '%s' from minio server", dat.Name)

		file, err := os.Create(fname)
		if err != nil {
			return fmt.Errorf("failed to create '%s': %v", fname, err)
		}
		defer file.Close()

		_, err = io.Copy(file, object)
		if err != nil {
			return fmt.Errorf("failed to copy minio image to local file '%s': %v", fname, err)
		}
	}

	return nil
}

func prepareRootfs(fname, dst_fname string) error {
	var image_info ImageInfo

	cmd := exec.Command("qemu-img", "info", fname, "--output", "json")
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("failed to query '%s' info: %v", fname, string(out))
	}

	err = json.Unmarshal(out, &image_info)
	if err != nil {
		return err
	}

	if image_info.Format != "raw" {
		log.Warnf("image '%s' is not a raw image", fname)
	}

	cmd = exec.Command("qemu-img",
		"create",
		"-f", "qcow2",
		"-F", image_info.Format,
		"-b", fname,
		dst_fname,
		"32G", // TODO: from ResourceAllocation? How large can these reasonably be?
	)
	out, err = cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("qemu-img failed: %v", string(out))
	}

	log.Debugf("prepared rootfs %s", dst_fname)

	return nil
}

func now() int64 {
	return time.Now().Unix()
}

func fetchAndCacheImage(fqvm, image string, decompress bool) (*ImageRevision, error) {
	var cache *CacheDat
	var dat *ImageRevision
	var err error
	var isbios bool = false

	// get md5sum from minio object
	info, err := image_cli.StatObject(
		context.Background(),
		images_bucket,
		fmt.Sprintf("efi/%s", image),
		minio.StatObjectOptions{},
	)

	// efi doesn't exist, try bios
	if err != nil {
		info, err = image_cli.StatObject(
			context.Background(),
			images_bucket,
			fmt.Sprintf("bios/%s", image),
			minio.StatObjectOptions{},
		)

		if err != nil {
			return nil, fmt.Errorf("failed to query minio image '%s': %v", image, err)
		}

		isbios = true
	}
	minio_md5sum := info.ETag

	// determine if we have this image cached yet
	cache, dat, err = readImageRevisionNewest(image)
	if err == nil {
		log.Debugf("newest revision of image '%s' is '%s' with md5sum: %s", image, dat, dat.Md5sum)
		if minio_md5sum == dat.Md5sum {
			log.Debugf("found image '%s' in cache (md5sum:%s)", image, dat.Md5sum)

			// add fqvm and write metadata
			dat.Fqvms[fqvm] = true

			// update existing revision
			err = writeCacheDat(cache)
			if err != nil {
				return nil, err
			}

			return dat, nil
		}
	}

	// need to pull new revision
	image_path := fmt.Sprintf("efi/%s", image)
	if isbios {
		image_path = fmt.Sprintf("bios/%s", image)
	}

	obj, err := image_cli.GetObject(
		context.Background(),
		images_bucket,
		image_path,
		minio.GetObjectOptions{},
	)

	if err != nil {
		return nil, fmt.Errorf("failed to get image '%s': %v", image, err)
	}
	defer obj.Close()

	// generate new ImageRevision
	dat = &ImageRevision{
		Name:      image,
		Md5sum:    info.ETag,
		Timestamp: now(),
		Fqvms: map[string]bool{
			fqvm: true,
		},
		IsBios: isbios,
	}

	// copy to local filesystem
	err = writeLocalImage(dat, obj, decompress)
	if err != nil {
		return nil, fmt.Errorf("failed to copy image '%s' to filesystem: %v", image, err)
	}

	log.Debugf("added image '%s' to cache at %s", image, dat.Path())

	// update metadata
	err = writeNewImageRevision(dat)
	if err != nil {
		return nil, err
	}

	log.Debugf("added fqvm '%s' to image '%s' metadata", fqvm, dat)
	return dat, nil
}

func checkReapRevision(cache *CacheDat, revision *ImageRevision, rev_idx int) {
	// if there are no FQVMs on the revision, and there is at least one newer
	// revision, then delete
	if len(revision.Fqvms) == 0 && rev_idx > 0 {
		// remove image
		err := os.Remove(revision.Path())
		if err != nil {
			log.Errorf("failed to remove image revision %s (path: %s): %v", revision, revision.Path(), err)
			return
		}

		// remove metadata
		cache.Images[revision.Name] = append(
			cache.Images[revision.Name][:rev_idx],
			cache.Images[revision.Name][rev_idx+1:]...,
		)

		log.Debugf("reaped revision %s from image %s", revision, revision.Name)
	}
}

// Reap any stale revisions. A stale revision is one with no FQVMs attached, and for
// which we have a newer version in the cache
func reapStaleRevisions() {
	cache, err := readCacheDat()
	if err != nil {
		log.Error(err)
		return
	}

	for image := range cache.Images {
		// iterate from back to front, so that removing revisions doesn't affect the loop control
		// variable (rev_idx)
		for rev_idx := len(cache.Images[image]) - 1; rev_idx >= 0; rev_idx -= 1 {
			rev := cache.Images[image][rev_idx]
			checkReapRevision(cache, rev, rev_idx)
		}
	}

	err = writeCacheDat(cache)
	if err != nil {
		log.Error(err)
	}
}

// search through the image cache, find the revision used by this fqvm, and remove the fqvm
func releaseImage(fqvm, image string) error {
	cache, err := readCacheDat()
	if err != nil {
		return err
	}

	var revs []*ImageRevision
	var ok bool
	if revs, ok = cache.Images[image]; !ok {
		return fmt.Errorf("no revisions for image '%s'", image)
	}

	for rev_idx, rev := range revs {
		if _, ok = rev.Fqvms[fqvm]; ok {
			delete(cache.Images[image][rev_idx].Fqvms, fqvm)

			// If this was the last fqvm using the revision, and there is a newer
			// revision in the cache, then delete the old revision now
			checkReapRevision(cache, rev, rev_idx)

			err = writeCacheDat(cache)
			if err != nil {
				return err
			}

			log.Debugf("removed fqvm '%s' from image revision '%s'", fqvm, rev)
			return nil
		}
	}

	return fmt.Errorf("could not find revision of image '%s' with fqvm '%s'", image, fqvm)
}

// read the cache to find the image used by this fqvm
func findImageRevision(fqvm, image string) (*ImageRevision, error) {
	cache, err := readCacheDat()
	if err != nil {
		return nil, err
	}

	var revs []*ImageRevision
	var ok bool
	if revs, ok = cache.Images[image]; !ok {
		return nil, fmt.Errorf("no revisions for image '%s'", image)
	}

	for rev_idx, rev := range revs {
		if _, ok = rev.Fqvms[fqvm]; ok {
			return cache.Images[image][rev_idx], nil
		}
	}

	return nil, fmt.Errorf("could not find revision of image '%s' with fqvm '%s'", image, fqvm)
}

/*
 * Fetch images from the facility images minio store, and cache them locally on
 * the hypervisor for future use
 *
 * Each "image" as specified by a VM resource model is actually three images in minio:
 *  - the kernel image
 *  - the initramfs image
 *  - the rootfs image
 *
 * For each image, we first check whether we have an image with the same name
 * locally and, if so, compare its md5sum against that of the image in minio.
 * If the two match, there is no need to go to minio
 *
 * Parameters:
 *  fqvm: fqvm of the vm
 *  osImage: the name of the OS (e.g., "bullseye")
 */
func fetchAndCacheOS(fqvm, osImage string) (bool, error) {
	var err error
	var kdat, idat, rdat *ImageRevision

	kernel := fmt.Sprintf("%s-kernel", osImage)
	initrd := fmt.Sprintf("%s-initramfs", osImage)
	rootfs := fmt.Sprintf("%s-rootfs", osImage)

	// release FQVM from any images it grabs a reference to
	defer func() {
		if err != nil {
			releaseOS(fqvm, osImage)
		}
	}()

	kdat, err = fetchAndCacheImage(fqvm, kernel, false)
	if err != nil {
		return false, err
	}

	idat, err = fetchAndCacheImage(fqvm, initrd, false)
	if err != nil {
		return false, err
	}

	rdat, err = fetchAndCacheImage(fqvm, rootfs, true)
	if err != nil {
		return false, err
	}
	isBios := rdat.IsBios

	// setup symlinks to kernel/initrd
	dst := getKernelFile(fqvm)
	err = os.Symlink(kdat.Path(), dst)
	if err != nil {
		return false, fmt.Errorf("failed to create symlink to kernel: %v", err)
	}

	dst = getInitrdFile(fqvm)
	err = os.Symlink(idat.Path(), dst)
	if err != nil {
		return false, fmt.Errorf("failed to create symlink to initrd: %v", err)
	}

	// make a VM specific qcow2 using the rootfs as the backing file
	dst = getRootfsFile(fqvm)
	err = prepareRootfs(rdat.Path(), dst)
	if err != nil {
		return false, fmt.Errorf("failed to prepare rootfs: %v", err)
	}

	return isBios, nil
}

func releaseOS(fqvm, osImage string) {
	kernel := fmt.Sprintf("%s-kernel", osImage)
	initrd := fmt.Sprintf("%s-initramfs", osImage)
	rootfs := fmt.Sprintf("%s-rootfs", osImage)

	err := releaseImage(fqvm, kernel)
	if err != nil {
		log.Error(err)
	}

	err = releaseImage(fqvm, initrd)
	if err != nil {
		log.Error(err)
	}

	err = releaseImage(fqvm, rootfs)
	if err != nil {
		log.Error(err)
	}
}

func wipeRootfs(fqvm, osImage string) error {
	rootfs := fmt.Sprintf("%s-rootfs", osImage)
	dst := getRootfsFile(fqvm)

	// remove the current, if it exists
	err := os.Remove(dst)
	if err != nil {
		return err
	}

	/// find the revision for this image
	rdat, err := findImageRevision(fqvm, rootfs)
	if err != nil {
		return err
	}

	// make a VM specific qcow2 using the rootfs as the backing file
	err = prepareRootfs(rdat.Path(), dst)
	if err != nil {
		return fmt.Errorf("failed to prepare rootfs: %v", err)
	}

	return nil
}
