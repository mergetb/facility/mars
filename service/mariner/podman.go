package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"

	"github.com/containers/podman/v4/pkg/bindings"
	"github.com/containers/podman/v4/pkg/bindings/containers"
	_ "github.com/containers/podman/v4/pkg/bindings/images"
	_ "github.com/containers/podman/v4/pkg/domain/entities"
	"github.com/containers/podman/v4/pkg/specgen"
)

var (
	marinerContainer string
	marinerImage     string
)

func initPodmanConn() (context.Context, error) {
	var sock_dir string

	if os.Geteuid() == 0 {
		sock_dir = "/run"
	} else {
		sock_dir = os.Getenv("XDG_RUNTIME_DIR")
	}

	socket := "unix:" + sock_dir + "/podman/podman.sock"
	return bindings.NewConnection(context.Background(), socket)
}

func closePodmanConn(ctx context.Context) {
	if ctx == nil {
		return
	}

	conn, err := bindings.GetClient(ctx)
	if err != nil {
		log.Errorf("error: trying to close podman connection: %+v", err)
		return
	}

	conn.Client.CloseIdleConnections()
}

func findMarinerImage(ctx context.Context) (string, error) {
	dat, err := containers.Inspect(ctx, marinerContainer, nil)
	if err != nil {
		log.Error(err)
		return "", err
	}

	return dat.ImageName, nil
}

func findMarinerContainer(ctx context.Context) (string, error) {
	dat, err := containers.List(ctx, &containers.ListOptions{})
	if err != nil {
		log.Error(err)
		return "", err
	}

	for _, ctr := range dat {
		for _, name := range ctr.Names {
			if name == "mars-mariner" || name == "mariner" {
				return name, nil
			}
		}
	}

	return "", fmt.Errorf("could not find Mariner container: neither 'mars-mariner' nor 'mariner' is present")
}

func podmanEnsureNoVM(ctx context.Context, fqvm string) error {
	exists, err := containers.Exists(ctx, fqvm, &containers.ExistsOptions{})
	if err != nil {
		return fmt.Errorf("failed to determine if container exists: %v", err)
	} else if !exists {
		return nil
	}

	force := true
	_, err = containers.Remove(ctx, fqvm, &containers.RemoveOptions{
		Force: &force,
	})
	if err != nil {
		return fmt.Errorf("failed to remove container: %v", err)
	}

	return nil
}

func podmanStartVM(fqvm string, args []string) error {
	ctx, err := initPodmanConn()
	defer closePodmanConn(ctx)
	if err != nil {
		return nil
	}

	err = podmanEnsureNoVM(ctx, fqvm)
	if err != nil {
		return nil
	}

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name: fqvm,
			Entrypoint: []string{
				"qemu-system-x86_64",
			},
			Command: args,
			PidNS: specgen.Namespace{
				NSMode: specgen.Host,
			},
			Remove:        false,
			RestartPolicy: "no",
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: marinerImage,
			VolumesFrom: []string{
				marinerContainer,
			},
		},
		ContainerSecurityConfig: specgen.ContainerSecurityConfig{
			Privileged: true,
		},
		ContainerNetworkConfig: specgen.ContainerNetworkConfig{
			NetNS: specgen.Namespace{
				NSMode: specgen.Host,
			},
		},
	}

	c, err := containers.CreateWithSpec(ctx, spec, nil)
	if err != nil {
		return fmt.Errorf("failed to create container: %v", err)
	}

	err = containers.Start(ctx, c.ID, nil)
	if err != nil {
		return fmt.Errorf("failed to start container: %v", err)
	}

	return nil
}

func podmanStopVM(fqvm string) error {
	ctx, err := initPodmanConn()
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	exists, err := containers.Exists(ctx, fqvm, &containers.ExistsOptions{})
	if err != nil {
		return fmt.Errorf("failed to determine if container exists: %v", err)
	} else if !exists {
		log.Debugf("cannot stop container '%s' which does not exist", fqvm)
		return nil
	}

	timeout := uint(10)
	err = containers.Stop(ctx, fqvm, &containers.StopOptions{
		Timeout: &timeout,
	})
	if err != nil {
		return fmt.Errorf("failed to stop container: %v", err)
	}

	force := true
	_, err = containers.Remove(ctx, fqvm, &containers.RemoveOptions{
		Force: &force,
	})
	if err != nil {
		return fmt.Errorf("failed to remove container: %v", err)
	}

	return nil
}

func podmanInit() error {
	ctx, err := initPodmanConn()
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	marinerContainer, err = findMarinerContainer(ctx)
	if err != nil {
		return err
	}

	marinerImage, err = findMarinerImage(ctx)
	if err != nil {
		return err
	}

	log.Debugf("mariner container imageName: %s", marinerImage)
	return nil
}
