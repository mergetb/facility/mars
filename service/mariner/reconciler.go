package main

import (
	"fmt"
	"time"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/api/portal/v1/go"
	rec "gitlab.com/mergetb/tech/reconcile"

	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
)

func marinerKey() string {
	return fmt.Sprintf("/vm/%s/", hostname)
}

func runReconciler() {
	t := rec.ReconcilerManager{
		Manager:                    "mariner",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{{
			Prefix:          marinerKey(),
			Name:            hostname,
			Desc:            fmt.Sprintf("Mariner service on hostname: %s", hostname),
			EnsureFrequency: 60 * time.Second,
			Actions:         &VMTask{},
		}},
	}

	log.Infof("starting Mariner reconciler loop")
	t.Run()
}

type VMTask struct {
	fqvm    string
	message string
	key     string
	vm      *state.Vm
}

func (t *VMTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.VMKey {
		return false
	}

	if xk.Host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", xk.Host, hostname)
		return false
	}

	t.fqvm = xk.Name
	t.key = key
	return true
}

func (t *VMTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"fqvm": t.fqvm,
		"key":  t.key,
	})
	l.Info("handling VM create")

	t.vm = new(state.Vm)
	err := proto.Unmarshal(value, t.vm)
	if err != nil {
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	err = marinerCreateVM(t.fqvm, t.vm)
	if err != nil {
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	return rec.TaskMessageInfo("VM created")
}

func (t *VMTask) doUpdate() error {
	l := log.WithFields(log.Fields{
		"fqvm":  t.fqvm,
		"state": t.vm.State.String(),
	})

	switch t.vm.State {
	case state.MachineState_On:
		t.message = "VM up and running"
		return nil

	case state.MachineState_Restarted, state.MachineState_RestartedWarm:
		l.Info("handling VM update")

		var mode portal.RebootMaterializationMode
		var s string

		if t.vm.State == state.MachineState_Restarted {
			mode = portal.RebootMaterializationMode_Cycle
			s = "VM power cycled"
		} else {
			mode = portal.RebootMaterializationMode_Reboot
			s = "VM warm rebooted"
		}

		err := marinerRebootVM(t.fqvm, mode)
		if err != nil {
			return fmt.Errorf("%s reboot failed: %+v", s, err)
		}

		t.message = s
	case state.MachineState_Reimaged:
		l.Info("handling VM update")

		err := marinerReimageVM(t.fqvm)
		if err != nil {
			return err
		}

		t.message = fmt.Sprintf("VM reimaged with: %s",
			t.vm.GetVm().GetVmAlloc().GetModel().GetImage().GetValue(),
		)
	default:
		return fmt.Errorf("cannot handle MachineState: %+v", t.vm.State)
	}

	return nil
}

func (t *VMTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"fqvm": t.fqvm,
		"key":  t.key,
	})

	t.vm = new(state.Vm)
	err := proto.Unmarshal(value, t.vm)
	if err != nil {
		return rec.TaskMessageError(err)
	}

	err = t.doUpdate()
	if err != nil {
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	if t.message == "" {
		return nil
	}

	return rec.TaskMessageInfo(t.message)
}

// We explicitly ignore MachineState on Ensure; just set VMs up
func (t *VMTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"fqvm": t.fqvm,
		"key":  t.key,
	})
	l.Debug("handling VM ensure")

	t.vm = new(state.Vm)
	err := proto.Unmarshal(value, t.vm)
	if err != nil {
		l.Error(err)
		return rec.TaskMessageError(err)
	}

	// check if running and found
	running, err := checkVMStatus(t.fqvm)
	present := (err == nil)

	if present && running {
		l.Debug("VM up and running")
		return rec.TaskMessageInfo("VM up and running")
	}

	// if present, try to just boot it up
	if present {
		l.Warn("VM present but not running; attempting power cycle")

		err = marinerRebootVM(t.fqvm, portal.RebootMaterializationMode_Cycle)
		if err == nil {
			return rec.TaskMessageWarning("VM power cycled")
		}

		l.Error(err)
	}

	//  try to reload
	l.Info("Attempting to reload VM")
	err = marinerReloadVM(t.fqvm, false)
	if err == nil {
		return rec.TaskMessageWarning("VM reloaded")
	}
	l.Warnf("Failed to reload VM: %+v", err)

	// could not reload; try to kill and recreate
	l.Info("Attempting to recreate VM")
	err = marinerKillVM(t.fqvm)
	if err != nil {
		l.Error(err)
		return rec.TaskMessageErrorf("Could not kill VM: %+v", err)
	}

	err = marinerCreateVM(t.fqvm, t.vm)
	if err != nil {
		l.Error(err)
		return rec.TaskMessageErrorf("Could not create VM: %+v", err)
	}

	return rec.TaskMessageWarning("VM recreated")
}

func (t *VMTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	l := log.WithFields(log.Fields{
		"fqvm": t.fqvm,
		"key":  t.key,
	})
	l.Info("handling VM delete")

	err := marinerKillVM(t.fqvm)
	if err != nil {
		return rec.TaskMessageErrorf("failed to delete VM: %v", err)
	}

	return nil
}
