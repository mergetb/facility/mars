package main

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/canopy"
	"gitlab.com/mergetb/tech/rtnl"
)

type CpuInfoCpu struct {
	Cpu    int `json:"cpu"`
	Core   int `json:"core"`
	Socket int `json:"socket"`
	Node   int `json:"node"`
}

type CpuInfo struct {
	Cpus []CpuInfoCpu `json:"cpus"`
}

const (
	max_tap_idx    = 1023
	mariner_bridge = "mbr"
	jumbo_mtu      = 9216
)

var (
	cpuTopology = make(map[int][]int)
	taps        = make(map[int]bool)
	used_taps   map[int]string
	free_taps   []int
)

func getVMsDir() string {
	return fmt.Sprintf("%s/vms", mariner_dir)
}

func getVMDir(fqvm string) string {
	return fmt.Sprintf("%s/%s", getVMsDir(), fqvm)
}

func getQmpSockFile(fqvm string) string {
	return fmt.Sprintf("%s/qmp.sock", getVMDir(fqvm))
}

func getQemuPidFile(fqvm string) string {
	return fmt.Sprintf("%s/qemu.pid", getVMDir(fqvm))
}

func getVncSockFile(fqvm string) string {
	return fmt.Sprintf("%s/vnc.sock", getVMDir(fqvm))
}

func getSerialSockFile(fqvm string) string {
	return fmt.Sprintf("%s/serial.sock", getVMDir(fqvm))
}

func getDiskFile(fqvm, suffix string, idx uint32) string {
	return fmt.Sprintf("%s/disk%d.%s", getVMDir(fqvm), idx, suffix)
}

func getKernelFile(fqvm string) string {
	return fmt.Sprintf("%s/kernel", getVMDir(fqvm))
}

func getInitrdFile(fqvm string) string {
	return fmt.Sprintf("%s/initrd", getVMDir(fqvm))
}

func getRootfsFile(fqvm string) string {
	return fmt.Sprintf("%s/rootfs", getVMDir(fqvm))
}

func getTapName(idx int) string {
	return fmt.Sprintf("mtap%d", idx)
}

func getVMConfigFile(fqvm string) string {
	return fmt.Sprintf("%s/config.json", getVMDir(fqvm))
}

func getVMPid(fqvm string) (int, error) {
	pid_file := getQemuPidFile(fqvm)
	dat, err := ioutil.ReadFile(pid_file)
	if err != nil {
		return 0, fmt.Errorf("could not read pid_file '%s': %s", pid_file, string(dat))
	}

	pid, err := strconv.Atoi(strings.TrimSuffix(string(dat), "\n"))
	if err != nil {
		return 0, fmt.Errorf("could not read pid from pid_file '%s': %v", pid_file, err)
	}

	return pid, nil
}

func saveVMConfig(fqvm string, config *VMConfig) error {
	json_dat, err := json.Marshal(config)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(getVMConfigFile(fqvm), json_dat, 0644)
	if err != nil {
		return err
	}

	return nil
}

func restoreVMConfig(fqvm string) (*VMConfig, error) {
	dat, err := ioutil.ReadFile(getVMConfigFile(fqvm))
	if err != nil {
		return nil, err
	}

	config := new(VMConfig)
	err = json.Unmarshal(dat, config)
	if err != nil {
		return nil, err
	}

	return config, nil
}

func setupVMHost(fqvm string) error {
	dir := getVMDir(fqvm)
	err := os.MkdirAll(dir, 0755)
	if err != nil {
		return err
	}
	return nil
}

func cleanupVMHost(fqvm string) error {
	dir := getVMDir(fqvm)
	err := os.RemoveAll(dir)
	if err != nil {
		return err
	}

	log.Debugf("cleared directory: '%s' directory", dir)
	return nil
}

func createQcow2Image(fname string, size uint64) error {
	// see if it exists first
	_, err := os.Stat(fname)
	if err == nil {
		return fmt.Errorf("cannot create %s: file already exists", fname)
	} else if !os.IsNotExist(err) {
		return fmt.Errorf("cannot create %s: %v", fname, err)
	}

	cmd := exec.Command("qemu-img", "create", "-f", "qcow2", fname, strconv.FormatUint(size, 10))
	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf(string(out))
	}

	return nil
}

func initTaps() error {
	used_taps = make(map[int]string)

	// First, determine taps used by currently existing VMs
	vms, err := getAllVMs()
	if err != nil {
		return err
	}

	for _, fqvm := range vms {
		log.Debugf("found fqvm %s in storage", fqvm)

		config, err := restoreVMConfig(fqvm)
		if err != nil {
			log.Debugf("failed to restore config for fqvm:%s; assuming inconsistent VM state; deleting", fqvm)
			marinerKillVM(fqvm)
			continue
		}

		nics, err := getVMNicsConfig(config)
		if err != nil {
			return err
		}

		for _, idx := range nics {
			log.Debugf("found Mariner tap device %s in use by fqvm:%s", getTapName(idx), fqvm)
			used_taps[idx] = fqvm
		}
	}

	// also check against groudn truth links in existence
	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}

	links, err := rtnl.ReadLinks(rtx, nil)
	if err != nil {
		return err
	}

	reg := regexp.MustCompile(`mtap([0-9]+)$`)

	for _, link := range links {
		match := reg.FindStringSubmatch(link.Info.Name)
		if match != nil {
			index, err := strconv.Atoi(match[1])
			if err != nil {
				return err
			}

			if _, ok := used_taps[index]; !ok {
				log.Debugf("found Mariner tap device %s but not in use by any VM; reaping", link.Info.Name)
				err = link.Absent(rtx)
				if err != nil {
					log.Errorf("failed to reap device %s: %v: marking as unusable", link.Info.Name, err)
					used_taps[index] = "system"
				}
			}
		}
	}

	free_taps = nil
	for i := 0; i <= max_tap_idx; i++ {
		if _, ok := used_taps[i]; !ok {
			free_taps = append(free_taps, i)
		}
	}

	return nil
}

func allocTapIdx(fqvm string) (int, error) {
	if len(free_taps) == 0 {
		return -1, fmt.Errorf("exhausted all taps")
	}

	var idx int

	for {
		idx = free_taps[0]
		free_taps = free_taps[1:]

		if fqvm, ok := used_taps[idx]; ok {
			log.Errorf("found TAP device %s both free and in use (by fqvm:%s); this is a BUG", getTapName(idx), fqvm)
			// try again
			continue
		}
		used_taps[idx] = fqvm
		break
	}

	return idx, nil
}

func freeTapIdx(idx int) {
	if _, ok := used_taps[idx]; !ok {
		log.Errorf("freeing TAP device %s that we did not think was in use; aborting", getTapName(idx))
		return
	}

	free_taps = append(free_taps, idx)
	delete(used_taps, idx)
}

func allocTap(device_id, brname string, vid uint32, multiqueue, vlan_restricted bool) error {
	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// find bridge
	bridge, err := canopy.EnsureBridge(rtx, brname, vlan_restricted)
	if err != nil {
		return err
	}

	lnk := rtnl.NewLink()
	lnk.Info.Name = device_id
	lnk.Info.Tap = &rtnl.Tap{}

	// Delete any existing link with this name
	err = lnk.Absent(rtx)
	if err != nil {
		return err
	}

	// Create it
	cmd := exec.Command("ip", "tuntap", "add", "name", device_id, "mode", "tap")
	if multiqueue {
		cmd.Args = append(cmd.Args, "multi_queue")
	}
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("failed to create tap device: %v", err)
	}

	lnk, err = rtnl.GetLink(rtx, device_id)
	if err != nil {
		return err
	}

	err = lnk.Set(rtx)
	if err != nil {
		return err
	}

	// Set Mtu
	err = lnk.SetMtu(rtx, jumbo_mtu)
	if err != nil {
		return err
	}

	// Set master
	err = lnk.SetMaster(rtx, int(bridge.Msg.Index))
	if err != nil {
		return err
	}

	// older VLAN-based hypervisor realization
	if vid > 0 {
		// Create access port with this VID
		err = lnk.SetUntagged(rtx, uint16(vid), false, true, false)
		if err != nil {
			return err
		}
	}

	// Bring device up
	err = lnk.Up(rtx)
	if err != nil {
		return err
	}

	return nil
}

func freeTap(device_id string) error {
	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	lnk := rtnl.NewLink()
	lnk.Info.Name = device_id
	lnk.Info.Tap = &rtnl.Tap{}

	// Delete any existing link with this name
	err = lnk.Absent(rtx)
	if err != nil {
		return err
	}

	return nil
}

func generateUnicastMacAddr() (string, error) {
	buf := make([]byte, 6)
	_, err := rand.Read(buf)
	if err != nil {
		return "", fmt.Errorf("failed to generate random macaddr bytes: %v", err)
	}

	// ensure local+unicast address
	buf[0] = (buf[0] | 2) & 0xfe

	ar := fmt.Sprintf("%02x:%02x:%02x:%02x:%02x:%02x", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5])
	return ar, nil
}

func parseCpuTopology() error {
	cmd := exec.Command("lscpu", "-a", "-e=CPU,CORE,SOCKET,NODE", "-J")

	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("lscpu failed: %s", string(out))
	}

	var info CpuInfo
	err = json.Unmarshal(out, &info)
	if err != nil {
		return fmt.Errorf("failed to unmarshal lscpu JSON: %v", err)
	}

	for _, cpu := range info.Cpus {
		if cpu.Socket != cpu.Node {
			log.Infof("Found CPU with socket (%s) != node (%s)", cpu.Socket, cpu.Node)
		}

		cpuTopology[cpu.Socket] = append(cpuTopology[cpu.Socket], cpu.Cpu)
	}

	return nil
}

func socketToCpuList(socket int) string {
	var listStr []string

	for _, cpu := range cpuTopology[socket] {
		listStr = append(listStr, fmt.Sprintf("%d", cpu))
	}

	return strings.Join(listStr, ",")
}

func setThreadAffinity(thread int, socket int) error {
	cpuList := socketToCpuList(socket)
	cmd := exec.Command("taskset", "-c", "-p", cpuList, fmt.Sprintf("%d", thread))

	out, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("failed to taskset thread %d: %v", thread, string(out))
	}

	return nil
}

func mapVMToHost(fqvm string, qemu_pid int, mapping map[int]int) error {
	for thread, socket := range mapping {
		log.Debugf("mapping thread:%d to host socket:%d", thread, socket)
		err := setThreadAffinity(thread, socket)
		if err != nil {
			return err
		}
	}

	return nil
}

func getAllVMs() ([]string, error) {
	var vms []string

	dir := getVMsDir()
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return vms, err
	}

	for _, file := range files {
		fqvm := file.Name()

		// os.Stat follows symlinks
		info, err := os.Stat(fmt.Sprintf("%s/%s", dir, fqvm))
		if err != nil {
			return vms, err
		}

		if info.IsDir() {
			vms = append(vms, fqvm)
		}
	}

	return vms, nil
}

func marinerSetupHost() error {
	err := os.MkdirAll(getVMsDir(), 0755)
	if err != nil {
		return err
	}

	err = initTaps()
	if err != nil {
		return err
	}

	err = initImageCache()
	if err != nil {
		return err
	}

	err = parseCpuTopology()
	if err != nil {
		return err
	}

	err = podmanInit()
	if err != nil {
		return err
	}

	return nil
}

// Query the tap waypoint in storage to determine the VID for this interface
func findPortVid(fqvm, name string) (uint32, error) {
	tapname := fmt.Sprintf("%s.%s", fqvm, name)
	tap, err := storage.GetTap(hostname, tapname)
	if err != nil {
		return 0, err
	}

	return tap.Tap.Vid, nil
}

func removeBridge(name string) error {
	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	return canopy.RemoveBridge(rtx, name)
}
