package main

import (
	"encoding/json"
	"fmt"
)

const (
	Aes          = "aes"
	Auto         = "auto"
	BlkDebug     = "blkdebug"
	BlklogWrites = "blklogwrites"
	BlkReplay    = "blkreplay"
	BlkVerify    = "blkverify"
	Bochs        = "bochs"
	Cloop        = "cloop"
	Compress     = "compress"
	CopyOnRead   = "copy-on-read"
	Definition   = "definition"
	Dmg          = "dmg"
	Falloc       = "falloc"
	File         = "file"
	Flags        = "flags"
	Ftp          = "ftp"
	Ftps         = "ftps"
	Full         = "full"
	Gluster      = "gluster"
	HostCdrom    = "host_cdrom"
	HostDevice   = "host_device"
	Http         = "http"
	Https        = "https"
	Ignore       = "ignore"
	IoUring      = "io_uring"
	Iscsi        = "iscsi"
	Luks         = "luks"
	Metadata     = "metadata"
	Mode         = "mode"
	Native       = "native"
	Nbd          = "nbd"
	Nfs          = "nfs"
	Null         = "null"
	NullAio      = "null-aio"
	NullCo       = "null-co"
	Nvme         = "nvme"
	On           = "on"
	Off          = "off"
	Parallels    = "parallels"
	Preallocate  = "preallocate"
	Qcow         = "qcow"
	Qcow2        = "qcow2"
	Qed          = "qed"
	Quorom       = "quorom"
	Raw          = "raw"
	Rbd          = "rbd"
	Reference    = "reference"
	Replication  = "replication"
	Sheepdog     = "sheepdog"
	Ssh          = "ssh"
	Threads      = "threads"
	Throttle     = "throttle"
	Unmap        = "unmap"
	Vdi          = "vdi"
	Vhdx         = "vhdx"
	Vmdk         = "vmdk"
	Vpc          = "vpc"
	Vvfat        = "vvfat"
)

type VersionTriple struct {
	Major int `json:"major"`
	Minor int `json:"minor"`
	Micro int `json:"micro"`
}

type VersionInfo struct {
	Qemu    VersionTriple `json:"qemu"`
	Package string        `json:"package"`
}

type RunState string
type StatusInfo struct {
	Running    bool     `json:"running"`
	Singlestop bool     `json:"singlestep"`
	Status     RunState `json:"status"`
}

type MachineInfo struct {
	Name             string `json:"name"`
	Alias            string `json:"alias"`
	IsDefault        bool   `json:"is-default"`
	CpuMax           int    `json:cpu-max"`
	HotPluggableCpus bool   `json:hotpluggable-cpus"`
}

type CpuInstanceProperties struct {
	NodeId   int `json:"node-id"`
	SocketId int `json:"socket-id"`
	CoreId   int `json:"core-id"`
	ThreadId int `json:"thread-id"`
}

type HotpluggableCPU struct {
	Type       string                `json:"type"`
	VcpusCount int                   `json:"vcpus-count"`
	Props      CpuInstanceProperties `json:"props"`
	QomPath    string                `json:"qom-path"`
}

type CpuInfoArch string
type SysEmuTarget string
type CpuInfoFastBase struct {
	CpuIndex int                   `json:"cpu-index"`
	QomPath  string                `json:"qom-path"`
	ThreadId int                   `json:"thread-id"`
	Props    CpuInstanceProperties `json:"props,omitempty"`
	Arch     CpuInfoArch           `json:"arch"`
	Target   SysEmuTarget          `json:"target"`
}

type CpuS390State string
type CpuInfoS390 struct {
	CpuState CpuS390State `json:"cpu-state"`
}

type CpuInfoFast struct {
	*CpuInfoFastBase
	// union below
	S390X *CpuInfoS390 `json:"s390x,omitempty"`
}

type PciMemoryRange struct {
	Base  int `json:"base"`
	Limit int `json:"limit"`
}

type PciMemoryRegion struct {
	Bar       int    `json:"bar"`
	Type      string `json:"type"`
	Address   int    `json:"address"`
	Size      int    `json:"size"`
	Prefetch  bool   `json:"prefetch"`
	MemType64 bool   `json:"mem_type_64"`
}

type PciBusInfo struct {
	Number            int            `json:"number"`
	Secondary         int            `json:"secondary"`
	Subordinate       int            `json:"subordinate"`
	IoRange           PciMemoryRange `json:"io_range"`
	MemoryRange       PciMemoryRange `json:"memory_range"`
	PrefetchableRange PciMemoryRange `json:"prefetchable_range"`
}

type PciBridgeInfo struct {
	Bus     PciBusInfo      `json:"bus"`
	Devices []PciDeviceInfo `json:"devices"`
}

type PciDeviceClass struct {
	Desc  string `json:"desc"`
	Class int    `json:"class"`
}

type PciDeviceId struct {
	Device int `json:"device"`
	Vendor int `json:"vendor"`
}

type PciDeviceInfo struct {
	Bus       int               `json:"bus"`
	Slot      int               `json:"slot"`
	Function  int               `json:"function"`
	ClassInfo PciDeviceClass    `json:"class_info"`
	Id        PciDeviceId       `json:"id"`
	Irq       int               `json:"irq"`
	QdevId    string            `json:"qdev_id"`
	PciBridge PciBridgeInfo     `json:"pci_bridge"`
	Regions   []PciMemoryRegion `json:"regions"`
}

type PciInfo struct {
	Bus     int             `json:"bus"`
	Devices []PciDeviceInfo `json:"devices"`
}

type NetdevBase struct {
	Id   string `json:"id"`
	Type string `json:"type"`
}

type NetdevTapOptions struct {
	Ifname     string `json:"ifname"`
	Fd         string `json:"fd,omitempty"`
	Fds        string `json:"fds,omitempty"`
	Script     string `json:"script,omitempty"`
	Downscript string `json:"downscript,omitempty"`
	Br         string `json:"br,omitempty"`
	Helper     string `json:"helper,omitempty"`
	Sndbuf     uint64 `json:"sndbuf,omitempty"`
	VnetHdr    bool   `json:"vnet_hdr,omitempty"`
	Vhost      bool   `json:"vhost,omitempty"`
	Vhostfd    string `json:"vhostfd,omitempty"`
	Vhostfds   string `json:"vhostfds,omitempty"`
	Vhostforce bool   `json:"vhostforce,omitempty"`
	Queues     uint32 `json:"queues,omitempty"`
	PollUs     uint32 `json:"poll-us,omitempty"`
}

type Netdev struct {
	*NetdevBase
	// union of everything below
	*NetdevTapOptions
}

func (b *Netdev) MarshalJSON() ([]byte, error) {
	switch b.Type {
	case "tap":
		return json.Marshal(&struct {
			NetdevBase
			NetdevTapOptions
		}{
			*b.NetdevBase,
			*b.NetdevTapOptions,
		})
	default:
		return nil, fmt.Errorf("cannot marshal Netdev: unknown type: %v", b.Type)
	}
}

func (b *Netdev) UnmarshalJSON(data []byte) error {
	var base NetdevBase

	err := json.Unmarshal(data, &base)
	if err != nil {
		return err
	}

	switch base.Type {
	case "tap":
		type NetdevTap Netdev
		return json.Unmarshal(data, &NetdevTap{
			NetdevBase:       b.NetdevBase,
			NetdevTapOptions: b.NetdevTapOptions,
		})
	default:
		return fmt.Errorf("cannot unmarshal Netdev: unknown type: %v", base.Type)
	}
}

type DeviceBase struct {
	Driver string `json:"driver"`
	Bus    string `json:"bus,omitempty"`
	Id     string `json:"id"`
}

/*
 * e1000 has not been QAPIfied yet. This structure is inferred based on
 * the output of: `qemu-system-x86_64 -device e1000,help`
 */
type E1000 struct {
	DeviceBase
	Addr              string `json:"addr,omitempty"`
	AutoNegotiation   bool   `json:"autonegotiation,omitempty"`
	BootIndex         int32  `json:"bootindex,omitempty"`
	ExtraMacRegisters bool   `json:"extra_mac_registers,omitempty"`
	FailoverPairId    string `json:"failover_pair_id,omitempty"`
	Mac               string `json:"mac,omitempty"`
	MigrateTsoProps   bool   `json:"migrate_tso_props,omitempty"`
	Mitigation        bool   `json:"mitigation,omitempty"`
	Multifunction     bool   `json:"multifunction,omitempty"`
	Netdev            string `json:"netdev,omitempty"`
	Rombar            uint32 `json:"rombar,omitempty"`
	Romfile           string `json:"romfile,omitempty"`
	XPcieExtcapInit   bool   `json:"x-pcie-extcap-init,omitempty"`
	XPcieLnkstaDllla  bool   `json:"x-pcie-lnksta-dllla,omitempty"`
}

/*
 * e1000e has not been QAPIfied yet. This structure is inferred based on
 * the output of: `qemu-system-x86_64 -device e1000e,help`
 */
type E1000E struct {
	DeviceBase
	Addr             string `json:"addr,omitempty"`
	BootIndex        int32  `json:"bootindex,omitempty"`
	DisableVnetHdr   uint8  `json:"disable_vnet_hdr,omitempty"`
	FailoverPairId   string `json:"failover_pair_id,omitempty"`
	Mac              string `json:"mac,omitempty"`
	Multifunction    bool   `json:"multifunction,omitempty"`
	Netdev           string `json:"netdev,omitempty"`
	Rombar           uint32 `json:"rombar,omitempty"`
	Romfile          string `json:"romfile,omitempty"`
	Subsys           uint16 `json:"subsys,omitempty"`
	SubsysVen        uint16 `json:"subsys_ven,omitempty"`
	XPcieExtcapInit  bool   `json:"x-pcie-extcap-init,omitempty"`
	XPcieLnkstaDllla bool   `json:"x-pcie-lnksta-dllla,omitempty"`
}

/*
 * virtio-net-pci has not been QAPIfied yet. This structure is inferred based on
 * the output of: `qemu-system-x86_64 -device virtio-net-pci,help`
 */
type VirtioNetPci struct {
	DeviceBase
	Addr                           string `json:"addr,omitempty"`
	AnyLayout                      bool   `json:"any_layout,omitempty""`
	Ats                            bool   `json:"ats,omitempty""`
	BootIndex                      int32  `json:"bootindex,omitempty"`
	Csum                           bool   `json:"csum,omitempty"`
	CtrlGuestOffloads              bool   `json:"ctrl_guest_offloads,omitempty"`
	CtrlMacAddr                    bool   `json:"ctrl_mac_addr,omitempty"`
	CtrlRx                         bool   `json:"ctrl_rx,omitempty"`
	CtrlVlan                       bool   `json:"ctrl_vlan,omitempty"`
	CtrlVq                         bool   `json:"ctrl_vq,omitempty"`
	DisableLegacy                  string `json:"disable-legacy,omitempty"`
	DisableModern                  bool   `json:"disable-modern,omitempty"`
	Duplex                         string `json:"duplex,omitempty"`
	EventIdx                       bool   `json:"event_idx,omitempty"`
	Failover                       bool   `json:"failover,omitempty"`
	Gso                            bool   `json:"gso,omitempty"`
	GuestAnnounce                  bool   `json:"guest_announce,omitempty"`
	GuestCsum                      bool   `json:"guest_csum,omitempty"`
	GuestEcn                       bool   `json:"guest_ecn,omitempty"`
	GuestRscExt                    bool   `json:"guest_rsc_ext,omitempty"`
	GuestTso4                      bool   `json:"guest_tso4,omitempty"`
	GuestTso6                      bool   `json:"guest_tso6,omitempty"`
	Hash                           bool   `json:"hash,omitempty"`
	HostEcn                        bool   `json:"host_ecn,omitempty"`
	HostMtu                        uint16 `json:"host_mtu,omitempty"`
	HostTso4                       bool   `json:"host_tso4,omitempty"`
	HostTso6                       bool   `json:"host_tso6,omitempty"`
	HostUfo                        bool   `json:"host_ufo,omitempty"`
	IndirectDesc                   bool   `json:"indirect_desc,omitempty"`
	Ioeventfd                      bool   `json:"ioeventfd,omitempty"`
	IommuPlatform                  bool   `json:"iommu_platform,omitempty"`
	Mac                            string `json:"mac,omitempty"`
	MigrateExtra                   bool   `json:"migrate-extra,omitempty"`
	ModernPioNotify                bool   `json:"modern-pio-notify,omitempty"`
	Mq                             bool   `json:"mq,omitempty"`
	MrgRxbuf                       bool   `json:"mrg_rxbuf,omitempty"`
	Multifunction                  bool   `json:"multifunction,omitempty"`
	Netdev                         string `json:"netdev,omitempty"`
	NotifyOnEmpty                  bool   `json:"notify_on_empty,omitempty"`
	Packed                         bool   `json:"packed,omitempty"`
	PagePerVq                      bool   `json:"page-per-vq,omitempty"`
	Rombar                         uint32 `json:"rombar,omitempty"`
	Romfile                        string `json:"romfile,omitempty"`
	RscInterval                    uint32 `json:"rsc_interval,omitempty"`
	Rss                            bool   `json:"rss,omitempty"`
	RxQueueSize                    uint16 `json:"rx_queue_size,omitempty"`
	Speed                          int32  `json:"speed,omitempty"`
	Status                         bool   `json:"status,omitempty"`
	Tx                             string `json:"tx,omitempty"`
	TxQueueSize                    uint16 `json:"tx_queue_size,omitempty"`
	UseDisabledFlag                bool   `json:"use-disabled-flag,omitempty"`
	UseStarted                     bool   `json:"use-started,omitempty"`
	Vectors                        uint32 `json:"vectors,omitempty"`
	VirtioBackend                  string `json:"virtio-backend,omitempty"`
	VirtioPciBusMasterBugMigration bool   `json:"virtio-pci-bus-master-bug-migration,omitempty"`
	XDisableLegacyCheck            bool   `json:"x-disable-legacy-check,omitempty"`
	XDisablePcie                   bool   `json:"x-disable-pcie,omitempty"`
	XIgnoreBackendFeatures         bool   `json:"x-ignore-backend-features,omitempty"`
	XMtuBypassBackend              bool   `json:"x-mtu-bypass-backend,omitempty"`
	XPcieDeverrInit                bool   `json:"x-pcie-deverr-init,omitempty"`
	XPcieExtcapInit                bool   `json:"x-pcie-extcap-init,omitempty"`
	XPcieFlrInit                   bool   `json:"x-pcie-flr-init,omitempty"`
	XPcieLnkctlInit                bool   `json:"x-pcie-lnkctl-init,omitempty"`
	XPcieLnkstaDllla               bool   `json:"x-pcie-lnksta-dllla,omitempty"`
	XPciePmInit                    bool   `json:"x-pcie-pm-init,omitempty"`
	XTxburst                       int32  `json:"x-txburst,omitempty"`
	XTxtimer                       uint32 `json:"x-txtimer,omitempty"`
}

/*
 * virtio-blk-pci has not been QAPIfied yet. This structure is inferred based on
 * the output of: `qemu-system-x86_64 -device virtio-blk-pci,help`
 */
type VirtioBlkPci struct {
	DeviceBase
	Addr                           string `json:"addr,omitempty"`
	AnyLayout                      bool   `json:"any_layout,omitempty""`
	Ats                            bool   `json:"ats,omitempty""`
	BootIndex                      int32  `json:"bootindex,omitempty"`
	Class                          uint32 `json:"class,omitempty"`
	ConfigWce                      bool   `json:"config-wce,omitempty"`
	Cyls                           uint32 `json:"cyls,omitempty"`
	Csum                           bool   `json:"csum,omitempty"`
	DisableLegacy                  string `json:"disable-legacy,omitempty"`
	DisableModern                  bool   `json:"disable-modern,omitempty"`
	Discard                        bool   `json:"discard,omitempty"`
	DiscardGranularity             uint64 `json:"discard_granularity,omitempty"`
	Drive                          string `json:"drive,omitempty"`
	EventIdx                       bool   `json:"event_idx,omitempty"`
	FailoverPairId                 string `json:"failover_pair_id,omitempty"`
	Heads                          uint32 `json:"heads,omitempty"`
	IndirectDesc                   bool   `json:"indirect_desc,omitempty"`
	Ioeventfd                      bool   `json:"ioeventfd,omitempty"`
	IommuPlatform                  bool   `json:"iommu_platform,omitempty"`
	IoThread                       string `json:"iothread,omitempty"`
	Lcyls                          uint32 `json:"lcyls,omitempty"`
	Lheads                         uint32 `json:"lheads,omitempty"`
	MaxDiscardSectors              uint32 `json:"max-discard-sectors,omitempty"`
	MaxWriteZeroesSectors          uint32 `json:"max-write-zeroes-sectors,omitempty"`
	MigrateExtra                   bool   `json:"migrate-extra,omitempty"`
	MinIoSize                      uint64 `json:"min_io_size,omitempty"`
	ModernPioNotify                bool   `json:"modern-pio-notify,omitempty"`
	Multifunction                  bool   `json:"multifunction,omitempty"`
	NotifyOnEmpty                  bool   `json:"notify_no_empty,omitempty"`
	NumQueues                      uint16 `json:"num-queues,omitempty"`
	OptIoSize                      uint64 `json:"opt_io_size,omitempty"`
	Packed                         bool   `json:"packed,omitempty"`
	PagePerVq                      bool   `json:"page-per-vq,omitempty"`
	PhysicalBlockSize              uint16 `json:"physical_block_size,omitempty"`
	QueueSize                      uint16 `json:"queue-size,omitempty"`
	RequestMerging                 bool   `json:"request-merging,omitempty"`
	Rerror                         string `json:"rerror,omitempty"`
	Rombar                         uint32 `json:"rombar,omitempty"`
	Romfile                        string `json:"romfile,omitempty"`
	Scsi                           bool   `json:"scsi,omitempty"`
	Secs                           uint32 `json:"secs,omitempty"`
	SegMaxAdjust                   bool   `json:"seg-max-adjust,omitempty"`
	Serial                         string `json:"serial,omitempty"`
	ShareRw                        bool   `json:"share-rw,omitempty"`
	UseDisabledFlag                bool   `json:"use-disabled-flag,omitempty"`
	UseStarted                     bool   `json:"use-started,omitempty"`
	Vectors                        uint32 `json:"vectors,omitempty"`
	VirtioBackend                  string `json:"virtio-backend,omitempty"`
	VirtioPciBusMasterBugMigration bool   `json:"virtio-pci-bus-master-bug-migration,omitempty"`
	XDisableLegacyCheck            bool   `json:"x-disable-legacy-check,omitempty"`
	XDisablePcie                   bool   `json:"x-disable-pcie,omitempty"`
	XIgnoreBackendFeatures         bool   `json:"x-ignore-backend-features,omitempty"`
	XMtuBypassBackend              bool   `json:"x-mtu-bypass-backend,omitempty"`
	XPcieDeverrInit                bool   `json:"x-pcie-deverr-init,omitempty"`
	XPcieExtcapInit                bool   `json:"x-pcie-extcap-init,omitempty"`
	XPcieFlrInit                   bool   `json:"x-pcie-flr-init,omitempty"`
	XPcieLnkctlInit                bool   `json:"x-pcie-lnkctl-init,omitempty"`
	XPcieLnkstaDllla               bool   `json:"x-pcie-lnksta-dllla,omitempty"`
	XPciePmInit                    bool   `json:"x-pcie-pm-init,omitempty"`
}

type QCryptoBlockFormat string
type QCryptoBlockOptionsBase struct {
	Format QCryptoBlockFormat `json:"format"`
}

type QCryptoBlockOptionsQCow struct {
	KeySecret string `json:"key-secret,omitempty"`
}

type QCryptoBlockCreateOptionsLUKS struct {
	KeySecret string `json:"key-secret,omitempty"`
}

type QCryptoBlockCreateOptions struct {
	*QCryptoBlockOptionsBase
	// union below
	*QCryptoBlockOptionsQCow
	*QCryptoBlockCreateOptionsLUKS
}

func (q *QCryptoBlockCreateOptions) MarshalJSON() ([]byte, error) {
	switch q.Format {
	case "qcow":
		return json.Marshal(&struct {
			QCryptoBlockOptionsBase
			QCryptoBlockOptionsQCow
		}{
			*q.QCryptoBlockOptionsBase,
			*q.QCryptoBlockOptionsQCow,
		})
	case "luks":
		return json.Marshal(&struct {
			QCryptoBlockOptionsBase
			QCryptoBlockCreateOptionsLUKS
		}{
			*q.QCryptoBlockOptionsBase,
			*q.QCryptoBlockCreateOptionsLUKS,
		})
	default:
		return nil, fmt.Errorf("cannot marshal QCryptoBlockCreateOptions: unknown format: %v", q.Format)
	}
}

func (q *QCryptoBlockCreateOptions) UnmarshalJSON(data []byte) error {
	var base QCryptoBlockOptionsBase

	err := json.Unmarshal(data, &base)
	if err != nil {
		return err
	}

	switch base.Format {
	case "qcow":
		type QCryptoQCow QCryptoBlockCreateOptions
		return json.Unmarshal(data, &QCryptoQCow{
			QCryptoBlockOptionsBase: q.QCryptoBlockOptionsBase,
			QCryptoBlockOptionsQCow: q.QCryptoBlockOptionsQCow,
		})
	case "luks":
		type QCryptoLUKS QCryptoBlockCreateOptions
		return json.Unmarshal(data, &QCryptoLUKS{
			QCryptoBlockOptionsBase:       q.QCryptoBlockOptionsBase,
			QCryptoBlockCreateOptionsLUKS: q.QCryptoBlockCreateOptionsLUKS,
		})
	default:
		return fmt.Errorf("cannot unmarshal QCryptoBlockCreateOptions: unknown format: %v", base.Format)
	}
}

type IndicatorType string

// Alternate
type BlockdevRef struct {
	Indicator  IndicatorType
	Definition *BlockdevOptions
	Reference  string
}

func (b *BlockdevRef) MarshalJSON() ([]byte, error) {
	switch b.Indicator {
	case Definition:
		return json.Marshal(&b.Definition)
	case Reference:
		return json.Marshal(&b.Reference)
	default:
		return nil, fmt.Errorf("cannot marshal BlockdevRef: unknown indicator: %v", b.Indicator)
	}
}

func (b *BlockdevRef) UnmarshalJSON(data []byte) error {
	var obj interface{}

	err := json.Unmarshal(data, &obj)
	if err != nil {
		return err
	}

	switch v := obj.(type) {
	case BlockdevOptions:
		b.Definition = obj.(*BlockdevOptions)
		b.Indicator = Definition
	case string:
		b.Reference = obj.(string)
		b.Indicator = Reference
	default:
		return fmt.Errorf("cannot unmarshal BlockdevRef: unknown type: %v", v)
	}

	return nil
}

// Alternate
type BlockdevRefOrNull struct {
	Indicator  IndicatorType
	Definition *BlockdevOptions
	Reference  string
	Null       *string
}

func (b *BlockdevRefOrNull) MarshalJSON() ([]byte, error) {
	switch b.Indicator {
	case Definition:
		return json.Marshal(&b.Definition)
	case Reference:
		return json.Marshal(&b.Reference)
	case Null:
		return json.Marshal(nil)
	default:
		return nil, fmt.Errorf("cannot marshal BlockdevRef: unknown indicator: %v", b.Indicator)
	}
}

func (b *BlockdevRefOrNull) UnmarshalJSON(data []byte) error {
	var obj interface{}

	err := json.Unmarshal(data, &obj)
	if err != nil {
		return err
	}

	switch obj.(type) {
	case BlockdevOptions:
		b.Definition = obj.(*BlockdevOptions)
		b.Indicator = Definition
	case string:
		b.Reference = obj.(string)
		b.Indicator = Reference
	default:
		b.Indicator = Null
	}

	return nil
}

type BlockdevOptionsGenericFormat struct {
	File *BlockdevRef `json:"file"`
}

type BlockdevOptionsGenericCOWFormat struct {
	*BlockdevOptionsGenericFormat
	Backing *BlockdevRefOrNull `json:"backing,omitempty"`
}

type Qcow2OverlapCheckMode string
type Qcow2OverlapCheckFlags struct {
	Template        Qcow2OverlapCheckMode `json:"mode,omitempty"`
	MainHeader      bool                  `json:"main-header,omitempty"`
	ActiveL1        bool                  `json:"active-l1,omitempty"`
	ActiveL2        bool                  `json:"active-l2,omitempty"`
	RefcountTable   bool                  `json:"refcount-table,omitempty"`
	RefcountBlock   bool                  `json:"refcount-block,omitempty"`
	SnapshotTable   bool                  `json:"snapshot-table,omitempty"`
	InactiveL1      bool                  `json:"inactive-l1,omitempty"`
	InactiveL2      bool                  `json:"inactive-l2,omitempty"`
	BitmapDirectory bool                  `json:"bitmap-directory,omitempty"`
}

type Qcow2OverlapChecks struct {
	Indicator IndicatorType
	Flags     Qcow2OverlapCheckFlags `json:"flags"`
	Mode      Qcow2OverlapCheckMode  `json:"mode"`
}

func (q *Qcow2OverlapChecks) MarshalJSON() ([]byte, error) {
	switch q.Indicator {
	case Flags:
		return json.Marshal(&q.Flags)
	case Mode:
		return json.Marshal(&q.Mode)
	default:
		return nil, fmt.Errorf("cannot marshal Qcow2OverlapChecks: unknown indicator: %v", q.Indicator)
	}
}

func (q *Qcow2OverlapChecks) UnmarshalJSON(data []byte) error {
	var obj interface{}

	err := json.Unmarshal(data, &obj)
	if err != nil {
		return err
	}

	switch v := obj.(type) {
	case Qcow2OverlapCheckFlags:
		q.Flags = obj.(Qcow2OverlapCheckFlags)
		q.Indicator = Flags
	case string:
		q.Mode = obj.(Qcow2OverlapCheckMode)
		q.Indicator = Mode
	default:
		return fmt.Errorf("cannot unmarshal Qcow2OverlapChecks: unknown type %v", v)
	}

	return nil
}

type BlockdevQcow2Encryption IndicatorType
type BlockdevOptionsQcow2 struct {
	*BlockdevOptionsGenericCOWFormat
	LazyRefcounts       bool                    `json:"lazy-refcounts,omitempty"`
	PassDiscardRequest  bool                    `json:"pass-discard-request,omitempty"`
	PassDiscardSnapshot bool                    `json:"pass-discard-snapshot,omitempty"`
	PassDiscardOther    bool                    `json:"pass-discard-other,omitempty"`
	OverlapCheck        *Qcow2OverlapChecks     `json:"overlap-check,omitempty"`
	CacheSize           int                     `json:"cache-size,omitempty""`
	L2CacheSize         int                     `json:"l2-cache-size,omitempty""`
	L2CacheEntrySize    int                     `json:"l2-cache-entry-size,omitempty""`
	RefcountCacheSize   int                     `json:"refcount-cache-size,omitempty""`
	CacheCleanInterval  int                     `json:"cache-clean-interval,omitempty""`
	Encrypt             BlockdevQcow2Encryption `json:"encrypt,omitempty"`
	DataFile            *BlockdevRef            `json:"data-file,omitempty"`
}

type BlockdevAioOptions IndicatorType
type BlockdevOptionsFile struct {
	Filename           string             `json:"filename"`
	PrManager          string             `json:"pr-manager,omitempty"`
	Locking            IndicatorType      `json:"locking,omitempty"`
	Aio                BlockdevAioOptions `json:"aio,omitempty"`
	DropCache          bool               `json:"drop-cache,omitempty"`
	XCheckCacheDropped bool               `json:"x-check-cache-dropped,omitempty"`
}

type BlockdevDiscardOptions string
type BlockdevCacheOptions struct {
	Direct  bool `json:"direct"`
	NoFlush bool `json:"no-flush"`
}
type BlockdevDetectZeroesOption string

type BlockdevOptionsBase struct {
	Driver       string                     `json:"driver"`
	NodeName     string                     `json:"node-name,omitempty"`
	Discard      BlockdevDiscardOptions     `json:"discard,omitempty"`
	Cache        *BlockdevCacheOptions      `json:"cache,omitempty"`
	ReadOnly     bool                       `json:"read-only,omitempty"`
	AutoReadOnly bool                       `json:"auto-read-only,omitempty"`
	ForceShare   bool                       `json:"force-share,omitempty"`
	DetectZeroes BlockdevDetectZeroesOption `json:"detect-zeroes,omitempty"`
}

type BlockdevOptions struct {
	Base *BlockdevOptionsBase
	// union of everything below
	Qcow2 *BlockdevOptionsQcow2
	File  *BlockdevOptionsFile
}

func (b *BlockdevOptions) MarshalJSON() ([]byte, error) {
	switch b.Base.Driver {
	case "qcow2":
		return json.Marshal(&struct {
			BlockdevOptionsBase
			BlockdevOptionsQcow2
		}{
			*b.Base,
			*b.Qcow2,
		})
	case "file":
		return json.Marshal(&struct {
			BlockdevOptionsBase
			BlockdevOptionsFile
		}{
			*b.Base,
			*b.File,
		})
	default:
		return nil, fmt.Errorf("cannot marshal BlockdevOptions: unknown driver: %v", b.Base.Driver)
	}
}

func (b *BlockdevOptions) UnmarshalJSON(data []byte) error {
	var base BlockdevOptionsBase

	err := json.Unmarshal(data, &base)
	if err != nil {
		return err
	}

	switch base.Driver {
	case "qcow2":
		return json.Unmarshal(data, &struct {
			*BlockdevOptionsBase
			*BlockdevOptionsQcow2
		}{
			b.Base,
			b.Qcow2,
		})
	case "file":
		return json.Unmarshal(data, &struct {
			*BlockdevOptionsBase
			*BlockdevOptionsFile
		}{
			b.Base,
			b.File,
		})
	default:
		return fmt.Errorf("cannot unmarshal BlockdevOptions: unknown driver: %v", base.Driver)
	}
}

type QCryptoBlockInfoLUKSSlot struct {
	Active    bool `json:"active"`
	Iters     int  `json:"iters,omitempty"`
	Stripes   int  `json:"stripes,omitempty"`
	KeyOffset int  `json:"key-offset"`
}

type QCryptoCipherAlgorithm string
type QCryptoCipherMode string
type QCryptoIVGenAlgorithm string
type QCryptoHashAlgorithm string
type QCryptoBlockInfoLUKS struct {
	CipherAlg     QCryptoCipherAlgorithm     `json:"cipher-alg"`
	CipherMode    QCryptoCipherMode          `json:"cipher-mode"`
	IvgenAlg      QCryptoIVGenAlgorithm      `json:"ivgen-alg"`
	IvgenHashAlg  QCryptoHashAlgorithm       `json:"ivgen-hash-alg,omitempty"`
	PayloadOffset int                        `json:"payload-offset"`
	MaterKeyIters int                        `json:"master-key-iters"`
	Uuid          string                     `json:"uuid"`
	Slots         []QCryptoBlockInfoLUKSSlot `json:"slots"`
}

type BlockdevQcow2EncryptionFormat string
type ImageInfoSpecificQCow2EncryptionBase struct {
	Format BlockdevQcow2EncryptionFormat `json:"format"`
}

type ImageInfoSpecificQcow2Encryption struct {
	Base *ImageInfoSpecificQCow2EncryptionBase
	// union below
	Luks *QCryptoBlockInfoLUKS
}

type Qcow2BitmapInfoFlags string
type Qcow2BitmapInfo struct {
	Name        string                 `json:"name"`
	Granularity uint32                 `json:"granularity"`
	Flags       []Qcow2BitmapInfoFlags `json:"flags"`
}

type Qcow2CompressionType string
type ImageInfoSpecificQcow2 struct {
	Compat          string                           `json:"compat"`
	DataFile        string                           `json:"data-file,omitempty"`
	DataFileRaw     bool                             `json:"data-file-raw,omitempty"`
	ExtendedL2      bool                             `json:"extended-l2,omitempty"`
	LazyRefcounts   bool                             `json:"lazy-refcounts,omitempty"`
	Corrupt         bool                             `json:"corrupt,omitempty"`
	RefcountBits    int                              `json:"refcount-bits"`
	Encrypt         ImageInfoSpecificQcow2Encryption `json:"encrypt,omitempty"`
	Bitmaps         []Qcow2BitmapInfo                `json:"bitmaps,omitempty"`
	CompressionType Qcow2CompressionType             `json:"compression-type"`
}

// "simple" union
type ImageInfoSpecific struct {
	Type string
	// union below
	Qcow2 ImageInfoSpecificQcow2
	//Vmdk  *ImageInfoSpecificVmdk
	//Luks  *QCryptoBlockInfoLUKS
}

type SnapshotInfo struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	VmStateSize int    `json:"vm-state-size"`
	DateSec     int    `json:"date-sec"`
	DateNsec    int    `json:"date-nsec"`
	VmClockSec  int    `json:"vm-clock-sec"`
	VmClockNsec int    `json:"vm-clock-nsec"`
	ICount      int    `json:"icount,omitempty"`
}

type ImageInfo struct {
	Filename              string            `json:"filename:`
	Format                string            `json:"format"`
	DirtyFlag             bool              `json:"dirty-flag,omitempty"`
	ActualSize            int               `json:"actual-size,omitempty"`
	VirtualSize           int               `json:"virtual-size"`
	ClusterSize           int               `json:"cluster-size,omitempty"`
	Encrypted             bool              `json:"encrypted,omitempty"`
	Compressed            bool              `json:"compressed,omitempty"`
	BackingFilename       string            `json:"backing-filename,omitempty"`
	FullBackingFilename   string            `json:"full-backing-filename,omitempty"`
	BackingFilenameFormat string            `json:"backing-file-format,omitempty"`
	Snapshots             []SnapshotInfo    `json:"snapshots,omitempty"`
	BackingImage          *ImageInfo        `json:"backing-image,omitempty"`
	FormatSpecific        ImageInfoSpecific `json:"format-specific,omitempty"`
}

func (i ImageInfoSpecific) MarhshalJSON() ([]byte, error) {
	switch i.Type {
	case "qcow2":
		return json.Marshal(&struct {
			Type string                 `json:"type"`
			Data ImageInfoSpecificQcow2 `json:"data"`
		}{
			i.Type,
			i.Qcow2,
		})
	default:
		return nil, fmt.Errorf("cannot marshal ImageInfoSpecific: unknown type: %v", i.Type)
	}
}

func (i ImageInfoSpecific) UnmarshalJSON(data []byte) error {
	var obj map[string]interface{}

	err := json.Unmarshal(data, &obj)
	if err != nil {
		return err
	}

	switch obj["type"] {
	case "qcow2":
		return json.Unmarshal(data, &struct {
			Type string                 `json:"type"`
			Data ImageInfoSpecificQcow2 `json:"data"`
		}{
			i.Type,
			i.Qcow2,
		})
	default:
		return fmt.Errorf("cannot unmarshal ImageInfoSpecific: unknown type: %v", obj["type"])
	}
}

//The structures below are needed for creating qcow2 backed block devices. We currently just use
// qemu-img, but support via QMP could be added
/*
type BlockdevDriver string
type PreallocMode string
type BlockdevCreateOptionsFile struct {
	Filename       string       `json:"filename"`
	Size           uint64       `json:"size"`
	Preallocation  PreallocMode `json:"preallocation,omitempty"`
	Nocow          bool         `json:"nocow,omitempty"`
	ExtentSizeHint uint64       `json:"extent-size-hint,omitempty"`
}

type BlockdevQcow2Version string
type Qcow2CompressionType string
type BlockdevCreateOptionsQcow2 struct {
	File            *BlockdevRef               `json:"file"`
	DataFile        *BlockdevRef               `json:"data-file,omitempty"`
	DataFileRaw     bool                       `json:"data-file-raw,omitempty"`
	ExtendedL2      bool                       `json:"extended-l2,omitempty"`
	Size            uint64                     `json:"size"`
	Version         *BlockdevQcow2Version      `json:"version,omitempty"`
	BackingFile     string                     `json:"backing-file,omitempty"`
	BackingFmt      *BlockdevDriver            `json:"backing-fmt,omitempty"`
	Encrypt         *QCryptoBlockCreateOptions `json:"encrypt,omitempty"`
	ClusterSize     uint64                     `json:"cluster-size,omitempty"`
	Preallocation   PreallocMode               `json:"preallocation,omitempty"`
	LazyRefcounts   bool                       `json:"lazy-refcounts,omitempty"`
	RefcountBits    int                        `json:"refcount-bits,omitempty"`
	CompressionType *Qcow2CompressionType      `json:"compression_type,omitempty"`
}

type BlockdevCreateOptionsBase struct {
	Driver BlockdevDriver `json:"driver"`
}

type BlockdevCreateOptions struct {
	Base *BlockdevCreateOptionsBase
	// union of everything below
	File  *BlockdevCreateOptionsFile
	Qcow2 *BlockdevCreateOptionsQcow2
}

func (b *BlockdevCreateOptions) MarshalJSON() ([]byte, error) {
	switch b.Base.Driver {
	case "qcow2":
		return json.Marshal(&struct{
			BlockdevCreateOptionsBase
			BlockdevCreateOptionsQcow2
		}{
			*b.Base,
			*b.Qcow2,
		})
	case "file":
		return json.Marshal(&struct{
			BlockdevCreateOptionsBase
			BlockdevCreateOptionsFile
		}{
			*b.Base,
			*b.File,
		})
	default:
		return nil, fmt.Errorf("cannot marshal BlockdevCreateOptions: unknown driver: %v", b.Base.Driver)
	}
}

func (b *BlockdevCreateOptions) UnmarshalJSON(data []byte) error {
	var base BlockdevCreateOptionsBase

	err := json.Unmarshal(data, &base)
	if err != nil {
		return err
	}

	switch base.Driver {
	case "qcow2":
		return json.Unmarshal(data, &struct{
			*BlockdevCreateOptionsBase
			*BlockdevCreateOptionsQcow2
		}{
			b.Base,
			b.Qcow2,
		})
	case "file":
		return json.Unmarshal(data, &struct{
			*BlockdevCreateOptionsBase
			*BlockdevCreateOptionsFile
		}{
			b.Base,
			b.File,
		})
	default:
		return fmt.Errorf("cannot unmarshal BlockdevCreateOptions: unknown driver: %v", base.Driver)
	}
}
*/
