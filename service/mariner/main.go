package main

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
)

const (
	dflt_mariner_dir string = "/var/lib/mariner"
)

var (
	hostname    string
	mariner_dir string = dflt_mariner_dir
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func parseEnv() {
	x, ok := os.LookupEnv("MARINER_DIR")
	if !ok {
		log.Warnf("MARINER_DIR not set; using default=%s", mariner_dir)
	} else {
		mariner_dir = x
	}
}

func main() {
	var err error

	parseEnv()

	fqdn, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	hostname = strings.Split(fqdn, ".")[0]

	err = storage.InitMarsEtcdClient()
	if err != nil {
		panic(err)
	}

	cfg, err := config.GetConfig()
	if err != nil {
		panic(err)
	}

	username := cfg.Services.Infraserver.Minio.Credentials.Username
	password := cfg.Services.Infraserver.Minio.Credentials.Password

	err = storage.InitMarsMinIOClientCreds(username, password)
	if err != nil {
		panic(err)
	}

	err = marinerSetupHost()
	if err != nil {
		panic(err)
	}

	runReconciler()
}
