package service

const (
	SporeGRPC     = 6000
	ApiserverGRPC = 6001
	CanopyGRPC    = 6002
	NexGRPC       = 6003
	SledGRPC      = 6004
	MetalGRPC     = 6005
	MoactlGRPC    = 6006
	MoaGRPC       = 6007
)

const (
	ApiserverHTTP = 8081
	CanopyHTTP    = 8082
	SledHTTP      = 8084
	MetalHTTP     = 8085

	CnimanHTTP = 9029
)
