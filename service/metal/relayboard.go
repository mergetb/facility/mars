package main

import (
	"gitlab.com/mergetb/xir/v0.3/go"
)

func turnOnRelayboard(rb *xir.RelayBoard) error {
	return nil
}

func turnOffRelayboard(rb *xir.RelayBoard) error {
	return nil
}

func cycleRelayboard(rb *xir.RelayBoard) error {
	return nil
}

func rebootRelayboard(rb *xir.RelayBoard) error {
	return nil
}
