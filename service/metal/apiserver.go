package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/service"
)

type M struct{}

func runApiserver() {

	go runGrpc()
	runGrpcGw()

}

func runGrpc() {

	log.Info("Running Metal API server")

	creds, err := credentials.NewServerTLSFromFile(
		"/certs/apiserver.pem",
		"/certs/apiserver-key.pem",
	)
	if err != nil {
		log.Fatalf("failed to read TLS cert: %v", err)
	}

	grpcServer := grpc.NewServer(grpc.Creds(creds))
	facility.RegisterMetalServer(grpcServer, &M{})

	l, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", service.MetalGRPC))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	log.Infof("listening on tcp://0.0.0.0:%d", service.MetalGRPC)
	grpcServer.Serve(l)

}

func runGrpcGw() {

	log.Infof("Running Metal gateway on port %d", service.MetalHTTP)
	creds := credentials.NewTLS(&tls.Config{
		InsecureSkipVerify: true,
	})

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(creds)}
	err := facility.RegisterMetalHandlerFromEndpoint(
		ctx,
		mux,
		fmt.Sprintf("localhost:%d", service.MetalGRPC),
		opts)
	if err != nil {
		log.Fatal(err)
	}

	//TODO
	domain := fmt.Sprintf("%s.facility.example.net", hostname)

	handler := cors.New(cors.Options{
		AllowedOrigins: []string{fmt.Sprintf("https://*.%s", domain)},
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
		},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	}).Handler(mux)

	log.Fatal(http.ListenAndServe(
		fmt.Sprintf(":%d", service.MetalHTTP),
		handler,
	))

}
