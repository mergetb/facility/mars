package main

import (
	"gitlab.com/mergetb/xir/v0.3/go"
)

func turnOnPdu(rb *xir.PowerDistributionUnit) error {
	return nil
}

func turnOffPdu(pdu *xir.PowerDistributionUnit) error {
	return nil
}

func cyclePdu(pdu *xir.PowerDistributionUnit) error {
	return nil
}

func rebootPdu(pdu *xir.PowerDistributionUnit) error {
	return nil
}
