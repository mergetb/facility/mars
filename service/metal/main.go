package main

import (
	"os"
	"strings"

	"gitlab.com/mergetb/facility/mars/internal"
)

var hostname string

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func main() {
	fqdn, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	hostname = strings.Split(fqdn, ".")[0]

	go runReconciler()
	runApiserver()
}
