package main

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	ipmi "github.com/vmware/goipmi"

	xir "gitlab.com/mergetb/xir/v0.3/go"

	"gitlab.com/mergetb/facility/mars/pkg/config"
)

func ipmiClient(host string) (*ipmi.Client, error) {
	cfg, err := config.GetConfig()
	if err != nil {
		return nil, fmt.Errorf("read config: %w", err)
	}

	username := cfg.IPMI.Username
	password := cfg.IPMI.Password

	conn := &ipmi.Connection{
		Hostname:  host,
		Username:  username,
		Password:  password,
		Interface: "lanplus",
	}

	client, err := ipmi.NewClient(conn)
	if err != nil {
		return nil, fmt.Errorf("failed to create ipmi client: %w", err)
	}

	return client, nil
}

func turnOnIpmi(bmc *xir.BMC) error {
	return ipmiAction(bmc.Host, ipmi.ControlPowerUp)
}

func turnOffIpmi(bmc *xir.BMC) error {
	return ipmiAction(bmc.Host, ipmi.ControlPowerDown)
}

func cycleIpmi(bmc *xir.BMC) error {
	// using ipmi.ControlPowerCycle doesn't guarantee that the node
	// is going to be powered on at the end if it's in off state
	ipmiAction(bmc.Host, ipmi.ControlPowerDown)
	// ipmitool says it waits "for at leat 1s"
	time.Sleep(2 * time.Second)
	return ipmiAction(bmc.Host, ipmi.ControlPowerUp)
}

func rebootIpmi(bmc *xir.BMC) error {
	return ipmiAction(bmc.Host, ipmi.ControlPowerAcpiSoft)
}

func ipmiAction(host string, action ipmi.ChassisControl) error {
	client, err := ipmiClient(host)
	if err != nil {
		log.WithError(err)
		return fmt.Errorf("failed to create ipmi client on %s", host)
	}
	defer client.Close()

	err = client.Control(action)
	if err != nil {
		log.WithError(err)
		return fmt.Errorf("ipmi control failed to %v %s", action, host)
	}

	return nil
}
