package main

import (
	"fmt"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/xir/v0.3/go"
)

func runReconciler() {
	err := storage.InitMarsEtcdClient()
	if err != nil {
		panic(err)
	}

	// runPollCheck()

	t := rec.ReconcilerManager{
		Manager:                    "metal",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{
			{
				Prefix:  "/metal/",
				Name:    fmt.Sprintf("power.%s", hostname),
				Desc:    fmt.Sprintf("Metal service watching /metal/ on hostname: %s", hostname),
				Actions: &MetalTask{},
			},
			{
				Prefix:  "/reboot/",
				Name:    fmt.Sprintf("reboot.%s", hostname),
				Desc:    fmt.Sprintf("Metal service watching /reboot/ on hostname: %s", hostname),
				Actions: &RebootTask{},
			},
		},
	}

	log.Infof("starting Metal reconciler loop")
	t.Run()
}

type MetalTask struct {
	Host string
}

func (t *MetalTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.MetalKey {
		return false
	}

	t.Host = xk.Host
	return true
}

func (t *MetalTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] metal create observed", t.Host)

	target := new(state.Metal)
	err := proto.Unmarshal(value, target)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal target metal %s: %v", t.Host, err)
	}

	current := &state.Metal{
		State: state.MachineState_Unknown,
	}

	return t.handlePut(target, current)
}

func (t *MetalTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] metal update observed", t.Host)

	target := new(state.Metal)
	err := proto.Unmarshal(value, target)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal target metal %s: %v", t.Host, err)
	}

	current := new(state.Metal)
	err = proto.Unmarshal(prev, current)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal current metal %s: %v", t.Host, err)
	}

	return t.handlePut(target, current)
}

func (t *MetalTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] metal ensure observed; nothing to do", t.Host)
	return rec.TaskMessageUndefined()
}

func (t *MetalTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] metal delete observed", t.Host)

	current := new(state.Metal)
	err := proto.Unmarshal(value, current)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal target metal %s: %v", t.Host, err)
	}

	return t.handleDelete(current)
}

func (t *MetalTask) handlePut(target, current *state.Metal) *rec.TaskMessage {
	log.Infof("[%s] handling PUT; target state %+v, current state %+v",
		t.Host,
		target.State,
		current.State,
	)

	switch target.State {
	case state.MachineState_Unknown:
		// nothing to do
		log.Warnf("reconciling machine state unknown?")
		return nil

	case state.MachineState_On:
		switch current.State {

		case state.MachineState_Off:
			return rec.CheckErrorToMessage(turnOn(target.Metal.Resource))

		default:
			// nothing to do
			return nil
		}

	case state.MachineState_Off:
		switch current.State {

		case state.MachineState_Off:
			// nothing to do
			return nil

		default:
			return rec.CheckErrorToMessage(turnOff(target.Metal.Resource))
		}

	case state.MachineState_Restarted, state.MachineState_RestartedWarm, state.MachineState_Reimaged:
		switch current.State {

		// turn on if currently off
		case state.MachineState_Off:
			return rec.CheckErrorToMessage(turnOn(target.Metal.Resource))

		// otherwise restart
		default:
			if target.State == state.MachineState_RestartedWarm {
				return rec.CheckErrorToMessage(reboot(target.Metal.Resource))
			} else {
				return rec.CheckErrorToMessage(cycle(target.Metal.Resource))
			}
		}

	default:
		return rec.TaskMessageErrorf("unknown target state: %+v", target.State)
	}
}

func (t *MetalTask) handleDelete(current *state.Metal) *rec.TaskMessage {
	// XXX is this what we always want to do?
	return rec.CheckErrorToMessage(cycle(t.Host))
}

type RebootTask struct {
	Host string
}

func (t *RebootTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.RebootKey {
		return false
	}

	t.Host = xk.Host
	return true
}

func (t *RebootTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] reboot create observed", t.Host)
	return t.handlePut()
}

func (t *RebootTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] reboot update observed", t.Host)
	return t.handlePut()
}

func (t *RebootTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] reboot ensure observed; nothing to do", t.Host)
	return rec.TaskMessageUndefined()
}

func (t *RebootTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] reboot delete observed", t.Host)
	return t.handleDelete()
}

func (t *RebootTask) handlePut() *rec.TaskMessage {
	log.Debugf("[%s] handling PUT", t.Host)
	return rec.CheckErrorToMessage(cycle(t.Host))
}

func (t *RebootTask) handleDelete() *rec.TaskMessage {
	return nil
}

func turnOn(resource string) error {

	log.Infof("turning on %s", resource)

	facility, err := storage.GetModel()
	if err != nil {
		return err
	}

	rs := facility.Resource(resource)
	if rs == nil {
		return fmt.Errorf("resource %s not found", resource)
	}

	if rs.PowerControl == nil {
		return fmt.Errorf("resource %s has no power control spec", resource)
	}

	switch p := rs.PowerControl.(type) {
	case *xir.Resource_Ipmi:
		return turnOnIpmi(p.Ipmi)
	case *xir.Resource_Relayboard:
		return turnOnRelayboard(p.Relayboard)
	case *xir.Resource_Pdu:
		return turnOnPdu(p.Pdu)
	case *xir.Resource_Raven:
		return turnOnRaven(resource, p.Raven)
	}

	return nil

}

func turnOff(resource string) error {

	log.Infof("turning off %s", resource)

	facility, err := storage.GetModel()
	if err != nil {
		return err
	}

	rs := facility.Resource(resource)
	if rs == nil {
		return fmt.Errorf("resource %s not found", resource)
	}

	if rs.PowerControl == nil {
		return fmt.Errorf("resource %s has no power control spec", resource)
	}

	switch p := rs.PowerControl.(type) {
	case *xir.Resource_Ipmi:
		return turnOffIpmi(p.Ipmi)
	case *xir.Resource_Relayboard:
		return turnOffRelayboard(p.Relayboard)
	case *xir.Resource_Pdu:
		return turnOffPdu(p.Pdu)
	case *xir.Resource_Raven:
		return turnOffRaven(resource, p.Raven)
	}

	return nil

}

func reboot(resource string) error {

	log.Infof("rebooting %s", resource)

	facility, err := storage.GetModel()
	if err != nil {
		return err
	}

	rs := facility.Resource(resource)
	if rs == nil {
		return fmt.Errorf("resource %s not found", resource)
	}

	if rs.PowerControl == nil {
		return fmt.Errorf("resource %s has no power control spec", resource)
	}

	log.Debugf("power control info %v", rs.PowerControl)

	switch p := rs.PowerControl.(type) {
	case *xir.Resource_Ipmi:
		return rebootIpmi(p.Ipmi)
	case *xir.Resource_Relayboard:
		return rebootRelayboard(p.Relayboard)
	case *xir.Resource_Pdu:
		return rebootPdu(p.Pdu)
	case *xir.Resource_Raven:
		return rebootRaven(resource, p.Raven)
	}

	return nil

}

func cycle(resource string) error {

	log.Infof("cycling %s", resource)

	facility, err := storage.GetModel()
	if err != nil {
		return err
	}

	rs := facility.Resource(resource)
	if rs == nil {
		return fmt.Errorf("resource %s not found", resource)
	}

	if rs.PowerControl == nil {
		return fmt.Errorf("resource %s has no power control spec", resource)
	}

	log.Debugf("power control info %v", rs.PowerControl)

	switch p := rs.PowerControl.(type) {
	case *xir.Resource_Ipmi:
		return cycleIpmi(p.Ipmi)
	case *xir.Resource_Relayboard:
		return cycleRelayboard(p.Relayboard)
	case *xir.Resource_Pdu:
		return cyclePdu(p.Pdu)
	case *xir.Resource_Raven:
		return cycleRaven(resource, p.Raven)
	}

	return nil

}

func runPollCheck() {
	// This is a somewhat atypical poll check, because the keys we watch fight over the desired target
	// state of the resource in question
	//
	// For example, if `/reboot/<x>` and `/metal/<x>` are both present, then blindly following the reboot
	// process for all keys will wipe out a metal resource that may already be in its desired metal state

	// Furthermore, determining if a target state of `state.MachineState_Restarted/Reimaged` has been reconciled is
	// impossible because it is by definition an ephemeral target state
	//
	// So for now this is just a no-op; the tasker will call out to us with any keys that we missed if we were offline,
	// which actually seems like the right behavior for this particular reconciler implementation
}
