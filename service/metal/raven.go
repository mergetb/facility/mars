package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/raven/api"
	"gitlab.com/mergetb/xir/v0.3/go"
)

func turnOnRaven(node string, rv *xir.Raven) error {
	return rvnPowerPost(node, rv, rvnapi.PowerOnAction)
}

func turnOffRaven(node string, rv *xir.Raven) error {
	return rvnPowerPost(node, rv, rvnapi.PowerOffAction)
}

func cycleRaven(node string, rv *xir.Raven) error {
	return rvnPowerPost(node, rv, rvnapi.PowerCycleAction)
}

func rebootRaven(node string, rv *xir.Raven) error {
	return rvnRebootPost(node, rv, rvnapi.RebootAction)
}

func rvnPowerPost(node string, rv *xir.Raven, action string) error {

	log.Infof("using raven power control interface at %s", rv.Host)

	msg := rvnapi.PowerMessage{
		Action: action,
		Nodes:  []string{node},
	}

	buf, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("marshal raven power message: %v", err)
	}

	rdr := bytes.NewReader(buf)

	resp, err := http.Post(
		fmt.Sprintf("http://%s:9316/power", rv.Host),
		"application/json",
		rdr,
	)
	if err != nil {
		return fmt.Errorf("raven post: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		msg := ""
		buf, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			msg = string(buf)
		}
		return fmt.Errorf("raven response: %s: %v", msg, err)
	}

	return nil

}

func rvnRebootPost(node string, rv *xir.Raven, action string) error {

	log.Infof("using raven reboot control interface at %s", rv.Host)

	msg := rvnapi.RebootMessage{
		Action: action,
		Nodes:  []string{node},
		Reset:  false,
	}

	buf, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("marshal raven reboot message: %v", err)
	}

	rdr := bytes.NewReader(buf)

	resp, err := http.Post(
		fmt.Sprintf("http://%s:9316/reboot", rv.Host),
		"application/json",
		rdr,
	)
	if err != nil {
		return fmt.Errorf("raven post: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		msg := ""
		buf, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			msg = string(buf)
		}
		return fmt.Errorf("raven response: %s: %v", msg, err)
	}

	return nil

}
