package main

/* TODO:
   the nex package uses the old clientv3 import location,
   which needs to be updated to the new location, so it doesn't quite compile.

import (
	"context"
	"fmt"
	"io/ioutil"
	"regexp"

	"github.com/minio/minio-go/v7"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	nameExp = "([a-zA-Z_]+[a-zA-Z0-9_]+)"
	mzpkey  = regexp.MustCompile(`^/mzparam/` + nameExp + `\.` + nameExp + `\.` + nameExp)
)

type task struct {
	realization string
	experiment  string
	project     string
}

// Actions interface implementation ===========================================

func (t *task) Parse(key string) bool {

	tkns := mzpkey.FindAllStringSubmatch(key, -1)

	if len(tkns) == 0 || len(tkns[0]) != 4 {
		return false
	}

	t.realization = tkns[0][1]
	t.experiment = tkns[0][2]
	t.project = tkns[0][3]

	return true

}

func (t *task) Create(buf []byte, ver int64, r *reconcile.Reconciler) error {

	log.Infof("[%s] mzparam create", t.mzid())

	mzid := t.mzid()

	mzn, err := loadMaterialization(mzid)
	if err != nil {
		return fmt.Errorf("[%s] load mzn: %v", mzid, err)
	}

	cli, conn, err := nexClient(mzid)
	if err != nil {
		return fmt.Errorf("[%s] nex client: %v", mzid, err)
	}
	defer conn.Close()

	err = createNexNetwork(cli, mzid, mzn)
	if err != nil {
		return fmt.Errorf("[%s] create nex network: %v", mzid, err)
	}

	err = addNexMembers(cli, mzid, mzn)
	if err != nil {
		return fmt.Errorf("[%s] add nex members: %v", mzid, err)
	}

	return nil

}

func (t *task) Update(prev, buf []byte, ver int64, r *reconcile.Reconciler) error {

	log.Infof("[%s] mzparam update", t.mzid())

	log.Warnf("[%s] nex update handler not implemented", t.mzid())

	return nil

}

func (t *task) Delete(buf []byte, ver int64, r *reconcile.Reconciler) error {

	log.Infof("[%s] mzparam delete", t.mzid())

	// nothing to really do here, infrapod reconciler will kill the whole pod
	// nex lives in, so no point in clearing anything out

	return nil

}

func (t *task) Expired(x *reconcile.Task) {

	log.Infof("[%s] task expiration", t.mzid())

	log.Warnf("[%s] nex expiration handler not implemented", t.mzid())

}

// Helpers ====================================================================

func (t *task) mzid() string {

	return fmt.Sprintf("%s.%s.%s", t.realization, t.experiment, t.project)

}

func loadMaterialization(mzid string) (*portal.Materialization, error) {

	obj, err := storage.MinIOClient.GetObject(
		context.TODO(),
		"materializations",
		mzid,
		minio.GetObjectOptions{},
	)
	if err != nil {
		return nil, fmt.Errorf(
			"[%s] failed to get mzn from minio: %v", mzid, err)
	}
	defer obj.Close()

	buf, err := ioutil.ReadAll(obj)
	if err != nil {
		return nil, fmt.Errorf(
			"[%s] failed to read mzn from minio: %v", mzid, err)
	}

	mzn := new(portal.Materialization)
	err = proto.Unmarshal(buf, mzn)
	if err != nil {
		return nil, fmt.Errorf("[%s] failed to parse mzn: %v", mzid, err)
	}

	return mzn, nil

}

func createNexNetwork(
	cli nex.NexClient, mzid string, mzn *portal.Materialization) error {

	//TODO we need a network per leaf here

	_, err := cli.AddNetwork(
		context.TODO(),
		&nex.AddNetworkRequest{
			Network: &nex.Network{
				Name: mzid,
			},
		},
	)

	return err

}

func addNexMembers(
	cli nex.NexClient, mzid string, mzn *portal.Materialization) error {

	// TODO members need to be per leaf-network

	var members []*nex.Member

	for _, x := range mzn.Metal {
		members = append(members, &nex.Member{
			Mac:   x.Inframac,
			Names: []string{x.Model.Id},
		})
	}

	_, err := cli.AddMembers(
		context.TODO(),
		&nex.MemberList{
			Net:  mzid,
			List: members,
		},
	)

	return err

}

func nexClient(mzid string) (nex.NexClient, *grpc.ClientConn, error) {

	conn, err := grpc.Dial(
		fmt.Sprintf("infrapod.%s:6000", mzid),
		grpc.WithInsecure(), //TODO creds
	)
	if err != nil {
		return nil, nil, fmt.Errorf("dial nex: %v", err)
	}

	cli := nex.NewNexClient(conn)

	return cli, conn, nil

}
*/
