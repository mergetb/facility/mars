package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net"
	"os/exec"

	// "reflect"
	"strings"
	"time"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"
	"golang.org/x/sys/unix"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	rec "gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/rtnl"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/canopy"
	"gitlab.com/mergetb/facility/mars/pkg/config"
)

func runReconciler() {
	//runPollCheck()

	t := rec.ReconcilerManager{
		Manager:                    "canopy",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{
			{
				Prefix: fmt.Sprintf("/net/%s", hostname),
				Name:   hostname,
				Desc:   fmt.Sprintf("Canopy service watching /net/%s", hostname),
				// Although canopy is fully idempotent, ensuring links by re-writing the status
				// causes periodic spikes in latency. Disabling it until we have a proper check.
				// EnsureFrequency: 60 * time.Second,
				Actions: &CanopyTask{},
			},
		},
	}

	log.Infof("canopy reconciling on keys in /net/%s", hostname)
	t.Run()
}

type CanopyTask struct {
	key  string
	typ  irec.KeyType
	host string
	name string
}

func (t *CanopyTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	switch xk.Type {
	case irec.NetPhyKey, irec.NetVtepKey, irec.NetVtepwKey, irec.NetVlanKey, irec.NetAccessKey,
		irec.NetTrunkKey, irec.NetPeerKey:
		break
	default:
		return false
	}

	if xk.Host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", xk.Host, hostname)
		return false
	}

	t.key = key
	t.typ = xk.Type
	t.host = xk.Host
	t.name = xk.Name

	return true
}

func (t *CanopyTask) handlePut(prev, current []byte, version int64) *rec.TaskMessage {
	switch t.typ {
	case irec.NetPhyKey:
		obj := new(state.Phy)
		err := proto.Unmarshal(current, obj)
		if err != nil {
			return rec.TaskMessageErrorf("unmarshal: %v", err)
		}
		return rec.CheckErrorToMessage(t.handlePhyUpdate(obj))

	case irec.NetVtepKey, irec.NetVtepwKey:
		obj := new(state.Vtep)
		err := proto.Unmarshal(current, obj)
		if err != nil {
			return rec.TaskMessageErrorf("unmarshal: %v", err)
		}

		prevObj := new(state.Vtep)
		if prev != nil {
			err := proto.Unmarshal(prev, prevObj)
			if err != nil {
				return rec.TaskMessageErrorf("unmarshal: %v", err)
			}
		}

		return rec.CheckErrorToMessage(t.handleVtepUpdate(obj, prevObj))

	case irec.NetVlanKey:
		obj := new(state.Vlan)
		err := proto.Unmarshal(current, obj)
		if err != nil {
			return rec.TaskMessageErrorf("unmarshal: %v", err)
		}
		return rec.CheckErrorToMessage(t.handleVlanUpdate(obj))

	case irec.NetAccessKey:
		obj := new(state.Access)
		err := proto.Unmarshal(current, obj)
		if err != nil {
			return rec.TaskMessageErrorf("unmarshal: %v", err)
		}
		return rec.CheckErrorToMessage(t.handleAccessUpdate(obj))

	case irec.NetTrunkKey:
		obj := new(state.Trunk)
		err := proto.Unmarshal(current, obj)
		if err != nil {
			return rec.TaskMessageErrorf("unmarshal: %v", err)
		}
		return rec.CheckErrorToMessage(t.handleTrunkUpdate(obj))

	case irec.NetPeerKey:
		obj := new(state.Peer)
		err := proto.Unmarshal(current, obj)
		if err != nil {
			return rec.TaskMessageErrorf("unmarshal: %v", err)
		}

		prevObj := new(state.Peer)
		if prev != nil {
			err := proto.Unmarshal(prev, prevObj)
			if err != nil {
				return rec.TaskMessageErrorf("unmarshal: %v", err)
			}
		}

		return rec.CheckErrorToMessage(t.handlePeerUpdate(obj, prevObj))

	default:
		break
	}

	return rec.TaskMessageErrorf("bad key type: %d", t.typ)
}

func (t *CanopyTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] key create observed", t.key)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed us Create with nil value; what can we do with this?")
	}

	return t.handlePut(nil, value, version)
}

func (t *CanopyTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] key update observed", t.key)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed us Update with nil value; what can we do with this?")
	}

	return t.handlePut(prev, value, version)
}

// Need to actually check the state of links before changing any state.
// If you just blindly apply the current state, it will work as canopy is fully idempotent,
// but it will cause periodic spikes in latency.
func (t *CanopyTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] key ensure observed", t.key)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed us Ensure with nil value; what can we do with this?")
	}

	return t.handlePut(prev, value, version)
}

func (t *CanopyTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] key delete observed", t.key)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed us Delete with nil value; what can we do with this?")
	}

	return rec.CheckErrorToMessage(t.handleDelete(value, version))
}

func (t *CanopyTask) handleDelete(value []byte, version int64) error {
	switch t.typ {
	case irec.NetPhyKey:
		obj := new(state.Phy)
		err := proto.Unmarshal(value, obj)
		if err != nil {
			return fmt.Errorf("unmarshal: %v", err)
		}
		return t.handlePhyDelete(obj)

	case irec.NetVtepKey, irec.NetVtepwKey:
		obj := new(state.Vtep)
		err := proto.Unmarshal(value, obj)
		if err != nil {
			return fmt.Errorf("unmarshal: %v", err)
		}
		return t.handleVtepDelete(obj)

	case irec.NetVlanKey:
		obj := new(state.Vlan)
		err := proto.Unmarshal(value, obj)
		if err != nil {
			return fmt.Errorf("unmarshal: %v", err)
		}
		return t.handleVlanDelete(obj)

	case irec.NetAccessKey:
		obj := new(state.Access)
		err := proto.Unmarshal(value, obj)
		if err != nil {
			return fmt.Errorf("unmarshal: %v", err)
		}
		return t.handleAccessDelete(obj)

	case irec.NetTrunkKey:
		obj := new(state.Trunk)
		err := proto.Unmarshal(value, obj)
		if err != nil {
			return fmt.Errorf("unmarshal: %v", err)
		}
		return t.handleTrunkDelete(obj)

	case irec.NetPeerKey:
		obj := new(state.Peer)
		err := proto.Unmarshal(value, obj)
		if err != nil {
			return fmt.Errorf("unmarshal: %v", err)
		}
		return t.handlePeerDelete(obj)

	default:
		break
	}

	return fmt.Errorf("bad key type: %d", t.typ)
}

func (t *CanopyTask) handleVtepUpdate(vtep, prev *state.Vtep) error {
	if is_infraserver || is_network_emulator {
		log.Infof("Skipping infraserver/emulator vtep update %s/%s for %s", vtep.Host, vtep.Vtep.Name, vtep.Mzid)
		return nil
	}

	log.Infof("Handling vtep update %s/%s for %s", vtep.Host, vtep.Vtep.Name, vtep.Mzid)

	return canopy.CreateVtep(vtep)
}

func (t *CanopyTask) handleVtepDelete(vtep *state.Vtep) error {
	if is_infraserver || is_network_emulator {
		log.Infof("Skipping infraserver/emulator vtep delete %s/%s for %s", vtep.Host, vtep.Vtep.Name, vtep.Mzid)
		return nil
	}

	log.Infof("Handling vtep delete %s/%s for %s", vtep.Host, vtep.Vtep.Name, vtep.Mzid)

	err := canopy.DeleteVtep(vtep.Vtep.Name)
	if err != nil {
		log.WithFields(log.Fields{"err": err, "vtep": vtep}).Error("delete vtep")
	}

	if is_hypervisor && // don't delete bridge devices except on the hypervisor
		vtep.Vtep.Vni > 0 && // new-style with per-VNI bridge device
		vtep.Bridge != nil &&
		vtep.Bridge.Bridge == fmt.Sprintf("mbr%d", vtep.Vtep.Vni) { // must match what the portal assigns

		rtx, err := rtnl.OpenDefaultContext()
		if err != nil {
			return err
		}
		defer rtx.Close()

		err = canopy.RemoveBridge(rtx, vtep.Bridge.Bridge)
		if err != nil {
			log.WithFields(log.Fields{"err": err, "bridge": vtep.Bridge, "vtep": vtep.Vtep}).Error("remove bridge")
		}
	}

	return err
}

func (t *CanopyTask) handleAccessUpdate(access *state.Access) error {
	log.Infof("Handling access update %s/%s for %s", access.Host, access.Access.Port.Name, access.Mzid)

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// get the port
	port, err := phyLink(rtx, access.Access.Port)
	if err != nil {
		return err
	}

	// ensure the port is on the bridge
	bridge, err := canopy.EnsureBridgeMember(rtx, access.Bridge)
	if err != nil {
		return err
	}

	if port.Info.Master == 0 {
		err = port.SetMaster(rtx, int(bridge.Msg.Index))
		if err != nil {
			return err
		}
	}

	// get currently bridged interface info and remove any existing vlan tags
	// from the specified port (access mode is exclusive)
	brmap, err := ifFamily(rtx, unix.AF_BRIDGE)
	if err != nil {
		return err
	}

	brinfo, ok := brmap[port.Msg.Index]
	if ok {
		//remove existing untagged
		for _, v := range brinfo.Info.Untagged {
			log.Infof("removing untagged vid %d from %s", v, access.Access.Port.Name)
			err = port.SetUntagged(rtx, v, true, true, false)
			if err != nil {
				return fmt.Errorf("clear untagged failed: %v", err)
			}
		}

	} else {
		log.Warnf("bridge info not found for %s", access.Access.Port.Name)
	}

	err = port.SetUntagged(rtx, uint16(access.Access.Vid), false, true, false)
	if err != nil {
		return err
	}

	if access.Mtu != 0 {
		err = port.SetMtu(rtx, int(access.Mtu))
		if err != nil {
			return err
		}
	}

	err = port.Up(rtx)
	if err != nil {
		return err
	}

	return nil
}

func (t *CanopyTask) handleAccessDelete(access *state.Access) error {
	log.Infof("Handling access delete %s/%s for %s", access.Host, access.Access.Port.Name, access.Mzid)

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// get the port
	port, err := phyLink(rtx, access.Access.Port)
	if err != nil {
		return err
	}

	err = port.SetUntagged(rtx, uint16(access.Access.Vid), true, true, false)
	if err != nil {
		return fmt.Errorf("set access failed: %v", err)
	}

	return nil
}

func (t *CanopyTask) handleTrunkUpdate(trunk *state.Trunk) error {
	log.Infof("Handling trunk update %s/%s for %s", trunk.Host, trunk.Trunk.Port, trunk.Mzid)
	return t.handleTrunkMod(trunk, false)
}

func (t *CanopyTask) handleTrunkDelete(trunk *state.Trunk) error {
	log.Infof("Handling trunk delete %s/%s for %s", trunk.Host, trunk.Trunk.Port, trunk.Mzid)
	return t.handleTrunkMod(trunk, true)
}

func (t *CanopyTask) handleTrunkMod(trunk *state.Trunk, remove bool) error {
	if !remove {
		log.Infof("Handling trunk update %s/%s for %s", trunk.Host, trunk.Trunk.Port.Name, trunk.Mzid)
	} else {
		log.Infof("Handling trunk delete %s/%s for %s", trunk.Host, trunk.Trunk.Port.Name, trunk.Mzid)
	}

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// get the port
	port, err := phyLink(rtx, trunk.Trunk.Port)
	if err != nil {
		return err
	}

	// ensure port is on the bridge
	bridge, err := canopy.EnsureBridgeMember(rtx, trunk.Bridge)
	if err != nil {
		return err
	}
	if port.Info.Master == 0 {

		err = port.SetMaster(rtx, int(bridge.Msg.Index))
		if err != nil {
			return nil
		}

	}

	if !remove && trunk.Mtu != 0 {
		err = port.SetMtu(rtx, int(trunk.Mtu))
		if err != nil {
			return err
		}
	}

	// ensure port is up
	err = port.Up(rtx)
	if err != nil {
		return nil
	}

	for _, vid := range trunk.Trunk.Vids {
		err = port.SetTagged(rtx, uint16(vid), remove, false, false)
		if err != nil {
			return fmt.Errorf("set tagged failed: %v", err)
		}
	}

	return nil
}

func (t *CanopyTask) handlePeerUpdate(peer, prev *state.Peer) error {
	// determine if anything has changed
	// if prev != nil && reflect.DeepEqual(peer, prev) {
	// 	log.Debug("prev and current peers are equal")
	// 	return nil
	// }

	log.Infof("Handling peer update %s/%s for %s", peer.Host, peer.Peer.Interface, peer.Mzid)
	return t.handlePeerMod(peer, false)
}

func (t *CanopyTask) handlePeerDelete(peer *state.Peer) error {
	log.Infof("Handling peer delete %s/%s for %s", peer.Host, peer.Peer.Interface, peer.Mzid)
	return t.handlePeerMod(peer, true)
}

func (t *CanopyTask) handlePeerMod(peer *state.Peer, remove bool) error {
	log.Info("ENTER PEER MOD")

	cmd := ""
	if remove {
		cmd = "no"
	}

	if peer.Peer.LocalAsn == 0 {
		return fmt.Errorf("Local ASN cannot be zero")
	}
	if peer.Peer.RemoteAsn == 0 {
		return fmt.Errorf("Remote ASN cannot be zero")
	}

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	lnk, err := phyLink(rtx, peer.Peer.Interface)
	if err != nil {
		return err
	}

	// on the cumulus switches, if this port was previously used as an access port, it needs to be removed from the "bridge" bridge
	// otherwise bgp will fail to come up (issue #116). The device doesn't get removed from the bridge when tearing down an experiment
	// where the port was in access mode (only the vlan tags are cleared)
	if lnk.Info.Master != 0 {
		log.Infof("removing %s from bridge id %d", peer.Peer.Interface.Name, lnk.Info.Master)
		err = lnk.SetNoMaster(rtx)
		if err != nil {
			return err
		}
	}

	if !remove {
		log.Info("ENSURE UNDERLAY")
		err := ensureRouterUnderlayAddr(peer)
		if err != nil {
			return err
		}
		log.Info("ENSURE ROUTER CONFIG")
		err = ensurePeerRouterConfig(peer)
		if err != nil {
			return err
		}
	}

	log.Info("SETUP ROUTER")
	err = vtyshExec(
		fmt.Sprintf("router bgp %d", peer.Peer.LocalAsn),
		fmt.Sprintf("%s neighbor %s interface peer-group mergepg",
			cmd, lnk.Info.Name),
	)
	if err != nil {
		return err
	}

	err = vtyshWrite()
	if err != nil {
		return nil
	}

	if !remove {
		// remove any master
		err = lnk.SetNoMaster(rtx)
		if err != nil {
			return err
		}

		if peer.Mtu != 0 {
			err = lnk.SetMtu(rtx, int(peer.Mtu))
			if err != nil {
				return err
			}
		}

		err = lnk.Up(rtx)
		if err != nil {
			return err
		}

		// ensure ipv6 link local address is present
		err = EnsureIpv6LinkLocal(rtx, lnk.Info.Name)
		if err != nil {
			return fmt.Errorf("failed to ensure ipv6 link local: %v", err)
		}
	}
	return nil
}

func (t *CanopyTask) handlePhyUpdate(phy *state.Phy) error {
	log.Infof("Phy update %s/%s for %s", phy.Host, phy.Phy.Name, phy.Mzid)

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// get the port
	lnk, err := linkByMacOrName(rtx, phy.Phy.Name, "")
	if err != nil {
		return fmt.Errorf("port get name %s: %v", phy.Phy.Name, err)
	}

	err = lnk.Up(rtx)
	if err != nil {
		return fmt.Errorf("link up %s: %v", phy.Phy.Name, err)
	}

	return nil

}

func (t *CanopyTask) handlePhyDelete(phy *state.Phy) error {
	log.Infof("Phy delete %s/%s for %s", phy.Host, phy.Phy.Name, phy.Mzid)

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	// get the port
	lnk, err := linkByMacOrName(rtx, phy.Phy.Name, "")
	if err != nil {
		return fmt.Errorf("port get name %s: %v", phy.Phy.Name, err)
	}

	err = lnk.Down(rtx)
	if err != nil {
		return fmt.Errorf("link down %s: %v", phy.Phy.Name, err)
	}

	return nil

}

func (t *CanopyTask) handleVlanUpdate(vlan *state.Vlan) error {
	log.Infof("Handling vlan update %s/%s for %s", vlan.Host, vlan.Vlan.Name, vlan.Mzid)

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	parent, err := phyLink(rtx, vlan.Vlan.Parent)
	if err != nil {
		return fmt.Errorf("vlan get parent %s: %v", vlan.Vlan.Parent.Name, err)
	}

	lnk := rtnl.NewLink()
	lnk.Info.Name = vlan.Vlan.Name
	lnk.Info.Vlan = &rtnl.Vlan{
		Id:   uint16(vlan.Vlan.Vid),
		Link: uint32(parent.Msg.Index),
	}

	err = lnk.Present(rtx)
	if err != nil {
		return fmt.Errorf("vlan present: %v", err)
	}

	if vlan.Bridge != nil {

		log.Infof("adding vlan to bridge %s", vlan.Bridge.Bridge)

		// ensure the vlan is on the bridge
		bridge, err := canopy.EnsureBridgeMember(rtx, vlan.Bridge)
		if err != nil {
			return fmt.Errorf("vlan sure bridge: %v", err)
		}

		if lnk.Info.Master == 0 {

			err = lnk.SetMaster(rtx, int(bridge.Msg.Index))
			if err != nil {
				return fmt.Errorf("vlan set master: %v", err)
			}

		}

		for _, vid := range vlan.Bridge.Untagged {
			pvid := vid == vlan.Bridge.Pvid
			lnk.SetUntagged(rtx, uint16(vid), false, pvid, false)
		}

		for _, vid := range vlan.Bridge.Tagged {
			pvid := vid == vlan.Bridge.Pvid
			lnk.SetTagged(rtx, uint16(vid), false, pvid, false)
		}

	}

	for _, x := range vlan.Vlan.Addrs {

		ip, ipnet, err := net.ParseCIDR(x)
		if err != nil {
			return fmt.Errorf("parse vlan addr %s: %v", x, err)
		}

		addr := rtnl.NewAddress()
		addr.Info.Address = &net.IPNet{
			IP:   ip,
			Mask: ipnet.Mask,
		}

		err = lnk.AddAddr(rtx, addr)
		if err != nil {
			if !strings.Contains(err.Error(), "exists") {
				return fmt.Errorf("add vlan addr %s: %v", x, err)
			}
		}
	}

	if vlan.Mtu != 0 {
		err = lnk.SetMtu(rtx, int(vlan.Mtu))
		if err != nil {
			return err
		}
	}

	err = parent.Up(rtx)
	if err != nil {
		return fmt.Errorf("vlan parent up: %v", err)
	}

	err = lnk.Up(rtx)
	if err != nil {
		return fmt.Errorf("vlan up: %v", err)
	}

	return nil
}

func (t *CanopyTask) handleVlanDelete(vlan *state.Vlan) error {
	log.Infof("Handling vlan delete %s/%s for %s", vlan.Host, vlan.Vlan.Name, vlan.Mzid)

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	lnk := rtnl.NewLink()
	lnk.Info.Name = vlan.Vlan.Name

	err = lnk.Absent(rtx)
	if err != nil {
		return err
	}

	return nil
}

func linkByMac(rtx *rtnl.Context, mac string) (*rtnl.Link, error) {
	addr, err := net.ParseMAC(mac)
	if err != nil {
		return nil, fmt.Errorf("parse MAC %s: %v", mac, err)
	}

	links, err := rtnl.ReadLinks(rtx, nil)
	if err != nil {
		return nil, fmt.Errorf("read links: %v", err)
	}

	for _, link := range links {
		// look for physical links
		if link.Info.Type() == rtnl.PhysicalType && bytes.Equal(addr, link.Info.Address) {
			return link, nil
		}
	}

	return nil, fmt.Errorf("cannot find link with MAC %s", mac)
}

// Find the rtnetlink associated with this port using the MAC if provided,
// and falling back to the name if not
func linkByMacOrName(rtx *rtnl.Context, name, mac string) (*rtnl.Link, error) {
	if mac != "" {
		return linkByMac(rtx, mac)
	}

	if name != "" {
		return rtnl.GetLink(rtx, name)
	}

	return nil, fmt.Errorf("no mac or name for port")
}

func phyLink(rtx *rtnl.Context, iface *portal.PhysicalInterface) (*rtnl.Link, error) {
	return canopy.LinkByMacOrName(rtx, iface.Name, iface.Mac)
}

func ensurePeerRouterConfig(peer *state.Peer) error {
	vtyshMtx.Lock()

	var err error
	var out []byte
	var msg string

	// If Canopy and FRR start up at the same time, it's possible FRR is not
	// ready for bgpd connections yet, so use a retry loop.
	retries := 3
	for i := 0; i < retries; i++ {

		out, err = exec.Command(
			"vtysh",
			"-c", "show bgp detail json",
		).CombinedOutput()
		if err == nil {
			break
		}
		msg = strings.TrimSuffix(string(out), "\n")
		log.WithError(fmt.Errorf(msg)).Warn(
			"could not connect to frr, retrying in 10 seconds")
		time.Sleep(10 * time.Second)

	}
	if err != nil {
		vtyshMtx.Unlock()
		return fmt.Errorf("vtysh show bgp: %v: %s", err, msg)
	}

	var bgpinfo FrrBgpDetail
	err = json.Unmarshal(out, &bgpinfo)
	if err != nil {
		vtyshMtx.Unlock()
		return fmt.Errorf("parse vtysh `show bgp detail` response: %v", err)
	}

	vtyshMtx.Unlock()

	// TODO checking for just the eistence of the router is not very robust,
	// there are lots of properties on the mergepg peer group that get setup
	// that should also be checked to make sure they are correct.
	if strings.Contains(bgpinfo.Warning, "not found") {
		err = configureBgpInstance(peer)
		if err != nil {
			return err
		}
	}

	return nil
}

func configureBgpInstance(peer *state.Peer) error {
	err := vtyshExec(
		fmt.Sprintf("router bgp %d", peer.Peer.LocalAsn),
		fmt.Sprintf("bgp router-id %s", peer.Peer.Network),
		fmt.Sprintf("neighbor mergepg peer-group"),
		fmt.Sprintf("neighbor mergepg remote-as external"),
		fmt.Sprintf("neighbor mergepg capability extended-nexthop"),
		"address-family ipv4 unicast",
		fmt.Sprintf("network %s/32", peer.Peer.Network),
		"exit-address-family",

		"address-family ipv6 unicast",
		fmt.Sprintf("neighbor mergepg activate"),
		"exit-address-family",

		"address-family l2vpn evpn",
		fmt.Sprintf("neighbor mergepg activate"),
		"advertise-all-vni",
		"advertise-default-gw",
		"exit-address-family",
	)
	if err != nil {
		return err
	}

	return vtyshWrite()
}

func ensureRouterUnderlayAddr(peer *state.Peer) error {
	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer rtx.Close()

	if peer.Peer.Network == "" {
		return fmt.Errorf("peer network missing")
	}

	ip := net.ParseIP(peer.Peer.Network)
	if ip == nil {
		return fmt.Errorf("could not parse network %s", peer.Peer.Network)
	}

	lo, err := rtnl.GetLink(rtx, "lo")
	if err != nil {
		return fmt.Errorf("get lo handle: %v", err)
	}

	addrs, err := lo.Addrs(rtx)
	if err != nil {
		return fmt.Errorf("read addrs: %v", err)
	}

	for _, a := range addrs {
		if a.Info.Address.Contains(ip) {
			log.Debugf("lo already has %s", ip.String())
			return nil
		}
	}

	log.Infof("adding IP %s to lo", ip.String())

	addr := rtnl.NewAddress()
	addr.Info.Address = &net.IPNet{
		IP:   ip,
		Mask: net.IPv4Mask(255, 255, 255, 255),
	}

	err = lo.AddAddr(rtx, addr)
	if err != nil {
		return fmt.Errorf("add network to lo")
	}

	return nil
}

func runPollCheck() {
	log.Infof("running poll check")

	resp, err := storage.EtcdClient.Get(
		context.TODO(),
		fmt.Sprintf("/net/%s/", hostname),
		clientv3.WithPrefix(),
	)
	if err != nil {
		log.Errorf("failed to get data for poll check: %v", err)
	}

	// the intent is that canopy is fully idempotent

	for _, kv := range resp.Kvs {
		t := &CanopyTask{}

		if !t.Parse(string(kv.Key)) {
			log.Warnf("cannot parse key: %s", string(kv.Key))
			continue
		}

		err := t.handlePut(nil, kv.Value, kv.Version)
		if err != nil {
			log.Error(err)
		}
	}
}
