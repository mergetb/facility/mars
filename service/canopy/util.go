package main

import (
	"fmt"
	"strings"

	"github.com/lorenzosaino/go-sysctl"
	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"

	"gitlab.com/mergetb/tech/rtnl"
)

func ifFamily(ctx *rtnl.Context, family uint8) (map[int32]*rtnl.Link, error) {

	spec := rtnl.NewLink()
	spec.Msg.Family = family
	ifInfo, err := rtnl.ReadLinks(ctx, spec)
	if err != nil {
		log.WithError(err).Error("failed to read interface info")
		return nil, err
	}

	ifMap := make(map[int32]*rtnl.Link)
	for _, bi := range ifInfo {
		ifMap[bi.Msg.Index] = bi
	}

	return ifMap, nil

}

func EnsureIpv6LinkLocal(rtx *rtnl.Context, name string) error {

	err := sysctl.Set(
		fmt.Sprintf("net.ipv6.conf.%s.disable_ipv6", name),
		"0",
	)
	if err != nil {
		log.
			WithError(err).
			Error("failed to ensure ipv6 for interface")
		return err
	}

	lnk, err := rtnl.GetLink(rtx, name)
	if err != nil {
		return err
	}

	saddr := fmt.Sprintf(
		"fe80::%02x%02x:%02xff:fe%02x:%02x%02x/64",
		lnk.Info.Address[0]^0b00000010,
		lnk.Info.Address[1],
		lnk.Info.Address[2],
		lnk.Info.Address[3],
		lnk.Info.Address[4],
		lnk.Info.Address[5],
	)
	log.Infof("ipv6 addr: %s", saddr)

	addr, err := rtnl.ParseAddr(saddr)
	if err != nil {
		return err
	}
	fam := "ipv4"
	if addr.Family() == unix.AF_INET6 {
		fam = "ipv6"
	}

	log.Infof("parsed: %s/%d %s", addr.Info.Address.IP.String(), addr.Prefix(), fam)
	err = lnk.AddAddr(rtx, addr)
	if err != nil {
		if strings.Contains(err.Error(), "exists") {
			return nil
		}
		return err
	}

	return nil
}
