package main

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	mars "gitlab.com/mergetb/facility/mars/pkg"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

var (
	hostname            string
	is_infraserver      bool
	is_network_emulator bool
	is_hypervisor	bool
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func main() {
	err := storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	// get minio creds from etcd
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatalf("read config: %v", err)
	}

	iservices := &cfg.Services.Infraserver
	muser := iservices.Minio.Credentials.Username
	mpass := iservices.Minio.Credentials.Password

	err = storage.InitMarsMinIOClientCreds(muser, mpass)
	if err != nil {
		log.Fatalf("minio init: %v", err)
	}

	fqdn, err := os.Hostname()
	if err != nil {
		log.Fatalf("os.Hostname: %v", err)
	}
	hostname = strings.Split(fqdn, ".")[0]

	// find our role based on our hostname
	model, err := storage.GetModel()
	if err != nil {
		log.Fatalf("read model: %v", err)
	}

	for _, r := range model.Resources {
		if r.Id == hostname {
			is_infraserver = r.HasRole(xir.Role_InfrapodServer)
			is_network_emulator = r.HasRole(xir.Role_NetworkEmulator)
			is_hypervisor = r.HasRole(xir.Role_Hypervisor)
			break
		}
	}

	log.Infof("starting canopy")
	log.Infof("version %s", mars.Version)
	log.Infof("hostname: %s", hostname)
	log.Infof("is_infraserver: %v", is_infraserver)
	log.Infof("is_network_emulator: %v", is_network_emulator)
	log.Infof("is_hypervisor: %v", is_hypervisor)

	go runReconciler()
	runApiserver()
}
