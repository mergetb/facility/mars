package main

import (
	"fmt"
	"os/exec"
	"sync"

	log "github.com/sirupsen/logrus"
)

var vtyshMtx sync.Mutex

func vtyshExec(commands ...string) error {

	vtyshMtx.Lock()
	defer vtyshMtx.Unlock()

	cmd := []string{
		"-c", "configure",
	}
	for _, x := range commands {
		cmd = append(cmd, "-c", x)
	}

	out, err := exec.Command("vtysh", cmd...).CombinedOutput()
	if err != nil {
		return fmt.Errorf("vtysh: %v: %s", err, string(out))
	}

	log.Infof("vtysh %+v", cmd)

	return nil

}

func vtyshWrite() error {

	vtyshMtx.Lock()
	defer vtyshMtx.Unlock()

	out, err := exec.Command("vtysh", "-c", "write").CombinedOutput()
	if err != nil {
		return fmt.Errorf("vtysh write: %v: %s", err, string(out))
	}

	return nil

}
