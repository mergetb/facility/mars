package main

type FrrBgpDetail struct {
	VrfId         int
	VrfName       string
	TableVersion  int
	RouterId      string
	DefaultLocPrf int
	Routes        map[string][]FrrBgpRoute

	Warning string
}

type FrrBgpRoute struct {
	Valid     bool
	Bestpath  bool
	PathFrom  string
	Prefix    string
	PrefixLen int
	Network   string
	Med       int
	Metric    int
	Weight    int
	PeerId    string
	Aspath    string
	Path      string
	Origin    string
	NextHops  []FrrNextHop
}

type FrrNextHop struct {
	Ip    string
	Afi   string
	Scope string
	Used  bool
}

// There are MANY more atttributes that can be collected here, not time to tap
// them all out now.
type FrrBgpNeigh struct {
	BgpNeighborAddr                string
	RemoteAs                       uint32
	LocalAs                        uint32
	NbrExternalLink                bool
	Hostname                       string
	PeerGroup                      string
	BgpVersion                     int
	RemoteRouterId                 string
	LocalRouterId                  string
	BgpState                       string
	BgpTimerUpMsec                 int
	BgpTimerUpString               string
	BgpTimerUpEstablishedEpoch     int
	BgpTimerLastRead               int
	BgpTimerLastWrite              int
	BgpInUpdateElapsedTimeMsecs    int
	BgpTimerHoldTimeMsecs          int
	BgpTimerKeepAliveIntervalMsecs int
	NeighborCapabilities           FrrNeighborCapabilities
	AddressFamilyInfo              map[string]FrrAddressFamilyInfo
}

type FrrNeighborCapabilities struct {
	FourByteAs      string `json:"4byteAs"`
	AddPath         FrrAddPath
	HostName        FrrHostnameCap
	GracefulRestart string
}

type FrrAddPath struct {
	Ipv4Unicast FrrAFCap
	Ipv6Unicast FrrAFCap
	L2VpnEvpn   FrrAFCap
}

type FrrAFCap struct {
	RxAdvertisedAndReceived bool
	TxAdvertised            bool
	TxReceived              bool
}

type FrrHostnameCap struct {
	AdvHostname   string
	AdvDomainName string
	RcvHostName   string
	RcvDomainName string
}

type FrrAddressFamilyInfo struct {
	PeerGroupMember       string
	UpdateGroupId         int
	SubGroupId            int
	PacketQueueLength     int
	CommAttriSentToNbr    string
	AcceptedPrefixCounter int
	SentPrefixCounter     int
}
