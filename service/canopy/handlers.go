package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os/exec"

	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/tech/rtnl"
)

func (c *C) ListPhys(
	ctx context.Context, rq *facility.ListPhyRequest,
) (*facility.ListPhyResponse, error) {

	phys, err := listPhys()
	return &facility.ListPhyResponse{Phys: phys}, err

}

func listPhys() (map[string]*facility.Phy, error) {
	phys, rtx, err := listNetdevs(rtnl.PhysicalType)
	if err != nil {
		return nil, err
	}
	defer rtx.Close()

	result := make(map[string]*facility.Phy)
	for _, x := range phys {
		result[x.Info.Name] = &facility.Phy{
			Info: &portal.PhysicalInterface{
				Name: x.Info.Name,
				Mac:  x.Info.Address.String(),
			},
			Link: linkState(x),
		}
	}

	return result, nil
}

func (c *C) ListVteps(
	ctx context.Context, rq *facility.ListVtepRequest,
) (*facility.ListVtepResponse, error) {

	vteps, err := listVteps()
	return &facility.ListVtepResponse{Vteps: vteps}, err
}

func listVteps() (map[string]*facility.Vtep, error) {

	vteps, rtx, err := listNetdevs(rtnl.VxlanType)
	if err != nil {
		return nil, fmt.Errorf("list netdevs: %v", err)
	}
	defer rtx.Close()

	result := make(map[string]*facility.Vtep)
	for _, x := range vteps {

		var parent *portal.PhysicalInterface

		if x.Info.Vxlan.Link == 0 {
			log.Debugf("no interface index for vtep %s", x.Info.Name)
			parent = nil
		} else {
			plink, err := rtnl.GetLinkByIndex(rtx, int32(x.Info.Vxlan.Link))
			if err != nil {
				return nil, fmt.Errorf("get link: %v", err)
			}

			parent = &portal.PhysicalInterface{
				Name: plink.Info.Name,
				Mac:  plink.Info.Address.String(),
			}
		}

		result[x.Info.Name] = &facility.Vtep{
			Info: &portal.Vtep{
				Name:     x.Info.Name,
				Vni:      x.Info.Vxlan.Vni,
				TunnelIp: x.Info.Vxlan.Local.String(),
				Parent:   parent,
			},
			Link: linkState(x),
		}

	}

	return result, nil

}

func (c *C) ListVlans(
	ctx context.Context, rq *facility.ListVlanRequest,
) (*facility.ListVlanResponse, error) {

	vlans, err := listVlans()
	return &facility.ListVlanResponse{Vlans: vlans}, err

}

func listVlans() (map[string]*facility.Vlan, error) {

	vlans, rtx, err := listNetdevs(rtnl.VlanType)
	if err != nil {
		return nil, err
	}
	defer rtx.Close()

	result := make(map[string]*facility.Vlan)
	for _, x := range vlans {

		parent, err := rtnl.GetLinkByIndex(rtx, int32(x.Info.Vlan.Link))
		if err != nil {
			return nil, err
		}

		switch parent.Info.Type() {
		case rtnl.PhysicalType:
			result[x.Info.Name] = &facility.Vlan{
				Info: &portal.VLANInterface{
					Name: x.Info.Name,
					Vid:  uint32(x.Info.Vlan.Id),
					Parent: &portal.PhysicalInterface{
						Name: parent.Info.Name,
						Mac:  parent.Info.Address.String(),
					},
				},
				Link: linkState(x),
			}
		default:
			return nil, fmt.Errorf("unsupported VLAN parent interface type: %v", parent.Info.Type())
		}
	}

	return result, nil

}

func (c *C) ListAccess(
	ctx context.Context, rq *facility.ListAccessRequest,
) (*facility.ListAccessResponse, error) {

	access, err := listAccess()
	return &facility.ListAccessResponse{Access: access}, err

}

func listAccess() (map[string]*facility.Access, error) {

	phys, rtx, err := listNetdevs(rtnl.PhysicalType)
	if err != nil {
		return nil, err
	}
	defer rtx.Close()

	result := make(map[string]*facility.Access)
	for _, x := range phys {

		if x.Info.Pvid != 0 {
			for _, u := range x.Info.Untagged {
				if x.Info.Pvid == u {
					result[x.Info.Name] = &facility.Access{
						Info: &portal.VLANAccessPort{
							Port: &portal.PhysicalInterface{
								Name: x.Info.Name,
								Mac:  x.Info.Address.String(),
							},
							Vid: uint32(x.Info.Pvid),
						},
						Link: linkState(x),
					}
				}
			}
		}

	}

	return result, nil

}

func (c *C) ListTrunks(
	ctx context.Context, rq *facility.ListTrunkRequest,
) (*facility.ListTrunkResponse, error) {

	trunks, err := listTrunks()
	return &facility.ListTrunkResponse{Trunks: trunks}, err

}

func listTrunks() (map[string]*facility.Trunk, error) {

	phys, rtx, err := listNetdevs(rtnl.PhysicalType)
	if err != nil {
		return nil, err
	}
	defer rtx.Close()

	result := make(map[string]*facility.Trunk)
	for _, x := range phys {
		if len(x.Info.Tagged) > 0 {
			var tags []uint32
			for _, t := range x.Info.Tagged {
				tags = append(tags, uint32(t))
			}

			result[x.Info.Name] = &facility.Trunk{
				Info: &portal.VLANTrunkPort{
					Port: &portal.PhysicalInterface{
						Name: x.Info.Name,
						Mac:  x.Info.Address.String(),
					},
					Vids: tags,
				},
				Link: linkState(x),
			}
		}
	}

	return result, nil

}

func (c *C) ListPeers(
	ctx context.Context, rq *facility.ListPeerRequest,
) (*facility.ListPeerResponse, error) {

	peers, err := listPeers()
	return &facility.ListPeerResponse{Peers: peers}, err

}

func listPeers() (map[string]*portal.BGPPeer, error) {
	phys, rtx, err := listNetdevs(rtnl.PhysicalType)
	if err != nil {
		return nil, err
	}
	defer rtx.Close()

	macMap := make(map[string]string)
	for _, x := range phys {
		macMap[x.Info.Name] = x.Info.Address.String()
	}

	out, err := exec.Command(
		"vtysh",
		"-c", "show bgp neigh json",
	).CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("vtysh show bgp neigh: %v: %s", err, string(out))
	}

	nbrs := make(map[string]FrrBgpNeigh)
	err = json.Unmarshal(out, &nbrs)
	if err != nil {
		return nil, fmt.Errorf("parse bgp neighbors: %v", err)
	}

	result := make(map[string]*portal.BGPPeer)
	for ifx, nbr := range nbrs {
		for family := range nbr.AddressFamilyInfo {
			result[ifx] = &portal.BGPPeer{
				Interface: &portal.PhysicalInterface{
					Name: ifx,
					Mac:  macMap[ifx],
				},
				RemoteAsn: nbr.RemoteAs,
				Network:   family, //XXX HACK not really for this ....
			}
		}
	}

	return result, nil

}

func listNetdevs(typ rtnl.LinkType) ([]*rtnl.Link, *rtnl.Context, error) {

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return nil, nil, err
	}

	alllinks, err := rtnl.ReadLinks(rtx, nil)
	if err != nil {
		return nil, nil, err
	}

	brmap, err := ifFamily(rtx, unix.AF_BRIDGE)
	if err != nil {
		return nil, nil, fmt.Errorf("ifFamily: %v", err)
	}

	var links []*rtnl.Link
	for _, x := range alllinks {
		if x.Info.Type() == typ && x.IsUp() {
			brinfo, ok := brmap[x.Msg.Index]
			if ok {
				x.Info.Pvid = brinfo.Info.Pvid
				x.Info.Untagged = brinfo.Info.Untagged
				x.Info.Tagged = brinfo.Info.Tagged
			}
			links = append(links, x)
		}
	}

	return links, rtx, nil

}

func linkState(x *rtnl.Link) facility.LinkState {
	if x.IsUp() {
		return facility.LinkState_Up
	}
	return facility.LinkState_Down
}

func (c *C) ListAll(
	ctx context.Context, rq *facility.CanopyListAllRequest,
) (*facility.CanopyListAllResponse, error) {

	phys, err := listPhys()
	if err != nil {
		return nil, err
	}

	vteps, err := listVteps()
	if err != nil {
		return nil, err
	}

	vlans, err := listVlans()
	if err != nil {
		return nil, err
	}

	access, err := listAccess()
	if err != nil {
		return nil, err
	}

	trunks, err := listTrunks()
	if err != nil {
		return nil, err
	}

	peers, err := listPeers()
	if err != nil {
		return nil, err
	}

	return &facility.CanopyListAllResponse{
		Config: &facility.CanopyConfig{
			Phys:   phys,
			Vteps:  vteps,
			Vlans:  vlans,
			Access: access,
			Trunks: trunks,
			Peers:  peers,
		},
	}, nil

}
