package main

import (
	"os"

	"gitlab.com/mergetb/facility/mars/internal"
)

var hostname string

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func main() {
	fqdn, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	// NOTE: unlike reconcilers that run in the host namespace, reconcilers in infrapods need to use
	// the fqdn as the hostname
	hostname = fqdn

	initMinioClient()
	go runApiserver()
	go runIPXE()
	runReconciler()
}
