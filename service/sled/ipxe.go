package main

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"
	"text/template"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func runIPXE() {

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.GET("ipxe", ipxeHandler)
	e.Static("/", "/srv/sled")

	e.Logger.Fatal(e.Start(":80"))

}

func findClientMac(c echo.Context) (string, error) {

	// get MAC address from IP address
	ip := strings.Split(c.RealIP(), "/")[0]
	ipmtx.Lock()
	mac, ok := ipToMac[ip]
	ipmtx.Unlock()

	if !ok {
		return "", fmt.Errorf("no record of infranet IP %s", ip)
	}

	return mac, nil

}

func ipxeHandler(c echo.Context) error {

	mac, err := findClientMac(c)
	if err != nil {
		return fmt.Errorf("find client MAC: %v", err)
	}

	tmpl, err := template.New("ipxe").Parse(ipxeTemplate)
	if err != nil {
		return fmt.Errorf("template create error: %v", err)
	}

	cmdline := []string{
		"console=tty0",
		"console=ttyS0,115200n8",
		"root=PARTUUID=a0000000-0000-0000-0000-00000000000a",
		"rootfstype=ext4",
		"rw",
	}

	cfg := IpxeParams{
		Kernel:      "kernel",
		Initramfs:   "initramfs",
		Server:      "sled",
		InfranetMac: mac,
		Cmdline:     strings.Join(cmdline, " "),
	}

	var out bytes.Buffer
	err = tmpl.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("template exec error: %v", err)
	}

	return c.String(http.StatusOK, out.String())

}

type IpxeParams struct {
	Kernel      string
	Initramfs   string
	Server      string
	InfranetMac string
	Cmdline     string
}

// once we reach this, the client has already dhcp'd successfully
var ipxeTemplate = `#!ipxe
kernel http://{{.Server}}/{{.Kernel}} initrd=initramfs inframac={{.InfranetMac}} {{.Cmdline}}
initrd http://{{.Server}}/{{.Initramfs}}
boot`
