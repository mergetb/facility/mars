package main

import (
	"fmt"
	"strings"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/facility/mars/internal/materialize"
	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	rec "gitlab.com/mergetb/tech/reconcile"
)

var (
	defaultImage   = "bullseye"
	defaultRootDev = "/dev/sda"
	defaultServer  = "images:9000"
	defaultVariant = "efi"
)

// runReconciler is the main function for updating sled state into the keyvalue store.  It will also
// modify/update the shared memory variables (macToClient, ipToMac) with the updated key
// information.

func runReconciler() {
	err := storage.InitMarsEtcdClient()
	if err != nil {
		panic(err)
	}

	t := rec.ReconcilerManager{
		Manager:                    "sled",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  "/metal/",
			Name:    hostname,
			Desc:    fmt.Sprintf("Sled service watching /metal/ on hostname: %s", hostname),
			Actions: &SledTask{},
		}},
	}

	log.Infof("starting Sled reconciler loop")
	t.Run()
}

type SledTask struct {
	Host string
	Key  string
}

func (t *SledTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.MetalKey {
		return false
	}

	t.Host = xk.Host
	t.Key = key
	return true
}

func (t *SledTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] metal create observed", t.Host)
	return t.handlePut(nil, value, version, td)
}

func (t *SledTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] metal update observed", t.Host)
	return t.handlePut(prev, value, version, td)
}

func (t *SledTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] metal ensure observed", t.Host)

	orig_ts := td.Status.Clone()

	m := t.handlePut(prev, value, version, td)

	// if it wrote an update, respect it
	if m.Level == rec.TaskMessage_Undefined {
		return m
	}

	// otherwise, restore the raw status
	td.Status = orig_ts
	return rec.TaskMessageUndefined()
}

func (t *SledTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] metal delete observed", t.Host)

	if value == nil {
		return rec.TaskMessageErrorf("TaskManager passed Delete with nil value; what can we do with this?")
	}

	current := new(state.Metal)
	err := proto.Unmarshal(value, current)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal current metal %s: %v", t.Host, err)
	}

	return t.handleDelete(current)
}

func (t *SledTask) handlePut(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {

	target := new(state.Metal)
	err := proto.Unmarshal(value, target)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal target metal %s: %v", t.Host, err)
	}

	log.Debugf("[%s] handling PUT; target state %+v", t.Host, target)

	mac := target.Metal.Inframac

	// update macToClient map
	log.Debugf("taking MAC lock for: %s", mac)
	cbmtx.Lock()
	c, ok := macToClient[mac]

	if ok {
		log.Debug("adding object to existing spec")

		c.mtx.Lock()
		c.mac = mac
		c.spec = target
		c.metalkey = t.Host
		c.td = td
		c.version = version
		c.mtx.Unlock()

		macToClient[mac] = c
	} else {
		log.Debug("adding new client spec")
		macToClient[mac] = &Client{
			mac:      mac,
			spec:     target,
			metalkey: t.Host,
			td:       td,
			version:  version,
		}
		c = macToClient[mac]
	}

	cbmtx.Unlock()
	log.Debugf("released MAC lock for: %s", mac)

	boot, err := getBootAddress(c.spec)
	if err != nil {
		return rec.TaskMessageErrorf("no boot address for %s", mac)
	}

	// remove bits if given, just key on the IP
	ip := strings.Split(boot, "/")[0]

	// update mac map
	log.Debugf("taking IP lock for: %s", ip)
	ipmtx.Lock()
	_, ok = ipToMac[ip]

	if ok {
		log.Warnf("overwriting MAC of existing IP %s", boot)
	} else {
		log.Debugf("adding new MAC spec %s: %s", ip, mac)
	}

	ipToMac[ip] = mac
	ipmtx.Unlock()
	log.Debugf("released IP lock for: %s", ip)

	switch target.State {
	case state.MachineState_On:
		// node is on; determine what state it came from to set an appropriate message

		if prev == nil {
			// reasonable generic message if we don't know prev state
			return rec.TaskMessageInfo("node in state: On")
		}

		current := new(state.Metal)
		err := proto.Unmarshal(prev, current)
		if err != nil {
			return rec.TaskMessageErrorf("unmarshal prev metal %s: %v", t.Host, err)
		}

		switch current.State {
		case state.MachineState_Restarted, state.MachineState_RestartedWarm:
			return rec.TaskMessageInfo("node rebooted")
		case state.MachineState_Reimaged:
			return rec.TaskMessageInfof("node reimaged with: %s",
				target.GetMetal().GetModel().GetImage().GetValue(),
			)
		case state.MachineState_Off:
			return rec.TaskMessageInfo("node powered on")
		default:
			// reasonable generic message
			return rec.TaskMessageInfo("node in state: On")
		}
	case state.MachineState_Off:
		return rec.TaskMessageInfo("node powered off")
	case state.MachineState_Restarted, state.MachineState_RestartedWarm, state.MachineState_Reimaged:
		// transient states; breakout
		break
	case state.MachineState_Unknown:
		fallthrough
	default:
		return rec.TaskMessageErrorf("unknown state: %s", target.State.String())
	}

	// the node hasn't connected yet, so manually set the status
	td.Status.LastStatus = rec.TaskStatus_Pending
	td.Status.Messages = []*rec.TaskMessage{
		rec.TaskMessageWarning("waiting for node to connect..."),
	}

	return rec.TaskMessageUndefined()
}

func (t *SledTask) handleDelete(target *state.Metal) *rec.TaskMessage {

	mac := target.Metal.Inframac

	// read macToClient map; do not remove, as sled statemachine may still rely on this structure
	// existing even after a delete been issued
	cbmtx.Lock()
	c, ok := macToClient[mac]
	cbmtx.Unlock()

	if !ok {
		return rec.TaskMessageErrorf("client %s not present in lookup table")
	}

	c.mtx.Lock()
	defer c.mtx.Unlock()

	boot, err := getBootAddress(c.spec)
	if err != nil {
		return rec.TaskMessageErrorf("no boot address for %s", mac)
	}

	// remove bits if given, just key on the IP
	ip := strings.Split(boot, "/")[0]

	// update mac map
	log.Debugf("taking IP lock for: %s", ip)
	ipmtx.Lock()

	if _, ok = ipToMac[ip]; !ok {
		log.Errorf("unable to remove IP %s from mac lookup table: not present", ip)
	} else {
		delete(ipToMac, ip)
	}
	ipmtx.Unlock()
	log.Debugf("released IP lock for: %s", ip)

	return nil
}

// Get the boot address for a metal resource
//
// In Merge v1, all resources boot on the harbor infranet, even if they are being booted
// into a new bare metal materialization for a user experiment. When nodes are in a bare metal
// materialization, the InfranetAddr in the target spec is the infranet address for that
// materialization, _not_ the harbor network address
//
// As of v1.0.1 (hash 31fae2e26fa33809739c1ec063e2c82d75139765) of the Merge API the boot address is
// stored along with the infranet address in the target spec. However, we need to implement some
// logic to support materializations that predate this change, which we do by directly querying the
// harbor metal key from storage
func getBootAddress(target *state.Metal) (string, error) {
	if target.InfranetBootAddr != "" {
		return target.InfranetBootAddr, nil
	}

	log.Warnf("Metal state with no InfranetBootAddr found; querying against harbor metal key")
	harbor, err := materialize.RetrieveHarborMetal(target.Metal.Resource)
	if err != nil {
		return "", fmt.Errorf("could not fetch harbor metal for %s: %v", target.Metal.Resource, err)
	}

	return harbor.Metal.InfranetAddr, nil
}
