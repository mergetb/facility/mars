package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

func initMinioClient() {

	err := storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	// get minio creds from etcd
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatalf("read config: %v", err)
	}

	iservices := &cfg.Services.Infraserver
	muser := iservices.Minio.Credentials.Username
	mpass := iservices.Minio.Credentials.Password

	err = storage.InitMarsMinIOClientCreds(muser, mpass)
	if err != nil {
		log.Fatalf("minio init: %v", err)
	}

}

// using a resource name, find the resource in the facility model and return the XIR firmware
func getResourceFirmware(id string) (*xir.Firmware, error) {

	fac, err := storage.GetModel()
	if err != nil {
		return nil, fmt.Errorf("failed to load model: %v", err)
	}

	rsrc := fac.Resource(id)
	if rsrc == nil {
		return nil, fmt.Errorf("no resource '%s' in facility model", id)
	}

	fw := rsrc.GetFirmware()
	if fw == nil {
		return nil, fmt.Errorf("resource '%s' has no firmware", id)
	}

	return fw, nil

}

func getResourceImage(id string) (string, error) {

	fac, err := storage.GetModel()
	if err != nil {
		return "", fmt.Errorf("failed to load model: %v", err)
	}

	rsrc := fac.Resource(id)
	if rsrc == nil {
		return "", fmt.Errorf("no resource '%s' in facility model", id)
	}

	os := rsrc.GetOS()
	if os == nil {
		return "", fmt.Errorf("resource '%s' has no OS Config", id)
	}

	return os.DefaultImage, nil

}
