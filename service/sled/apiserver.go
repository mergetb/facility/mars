package main

import (
	"context"
	"fmt"
	"io"
	"net"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/api/facility/v1/go/state"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/service"
	rec "gitlab.com/mergetb/tech/reconcile"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

// macToClient is keyed by MAC address
var cbmtx sync.Mutex
var macToClient = make(map[string]*Client)

// ipToMac is keyed by IP address
var ipmtx sync.Mutex
var ipToMac = make(map[string]string)

type S struct{}

// Client struct wraps all the sled client information into a data structure.
type Client struct {
	// data for each client
	mac        string                   // MAC address of this client
	spec       *state.Metal             // read from etcd/metalkey
	srv        facility.Sled_WaitServer // gets added when client connects
	image      string                   // stored based on stamp/standby ack to prevent retransmits
	variant    string                   // image variant based on the firmware of the underlying resource
	skip_stamp bool                     // skip stamping if it's already been written

	// etcd key this client is associated with
	metalkey string // etcdkey for this client

	// reconciler data
	td      *rec.TaskData // need this because kexec ack needs to update
	version int64         // same as above
	err     error         // client error; communicated through reconciler status

	// synchronize access to this client between apiserver.reconciler
	mtx sync.Mutex
}

func (c *Client) updateMachineState(ms state.MachineState) error {
	c.spec.State = ms
	out, err := proto.Marshal(c.spec)
	if err != nil {
		return fmt.Errorf("failed to marshal spec: %+v", err)
	}

	_, err = storage.EtcdClient.Put(
		context.TODO(),
		fmt.Sprintf("/metal/%s", c.metalkey),
		string(out),
	)
	if err != nil {
		return fmt.Errorf("failed to write spec: %+v", err)
	}

	return nil
}

func (c *Client) setImage(image string) {
	if image == "" || image == "none" {
		image = c.spec.GetMetal().GetModel().GetImage().GetValue()

		if image == "" {
			image = fmt.Sprintf("%s/%s-rootfs", defaultVariant, defaultImage)
		}
	}

	c.image = image
}

func (c *Client) getState() (facility.SledState_State, error) {
	// if we have a spec, we want to reach kexec
	if c.spec != nil {
		return facility.SledState_Kexec, nil
	}

	// else we are stamping default
	return facility.SledState_Stamp, nil
}

// updateState is the main function responsible for transitioning state on the receipt of
// an acknowledgement.
func (c *Client) updateState(ackedState facility.SledState_State) error {

	l := log.WithFields(log.Fields{
		"client":     c.mac,
		"ackedState": ackedState,
	})
	l.Infof("acking state")

	err := c.handleTransition(ackedState)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) sendDefaultImage(variant, image, rootdev, server string) error {
	return c.srv.Send(&facility.SledWaitResponse{
		WaitResponse: &facility.SledWaitResponse_Stamp{&facility.Stamp{
			Device: rootdev,
			Image:  fmt.Sprintf("%s/%s-rootfs", variant, image),
			Server: server,
		}},
	})
}

func (c *Client) handleTransition(ackState facility.SledState_State) error {
	l := log.WithFields(log.Fields{
		"client": c.mac,
	})
	l.Debug("Begin transition")

	// get the current states as stored in etcd to see how we need to transition
	desiredState, err := c.getState()
	if err != nil {
		return err
	}

	l.Infof("FROM ackedState: %v TO desiredState: %v", ackState, desiredState)

	// if the current and desired states are the same
	if ackState == desiredState || desiredState == facility.SledState_None {
		if ackState == facility.SledState_Stamp {
			if c.spec.Metal.Model.Image.Value == c.image {
				l.Infof("desiredState %v: stamped image: %s", desiredState, c.image)
			}
		} else {
			l.Infof("hit desiredState: %v", desiredState)
			return nil
		}
	}

	// after all our initial checks, lets begin to walk through the state machine
	nextState := ackState.Number() + 1
	if nextState >= desiredState.Number() {
		return c.helperState(desiredState)
	} else {
		nextState := ackState + 1

		// skip stamping if specified
		if nextState == facility.SledState_Stamp && c.skip_stamp {
			nextState++
		}

		err := c.helperState(nextState)
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Client) helperState(desiredState facility.SledState_State) error {
	l := log.WithFields(log.Fields{
		"client":       c.mac,
		"desiredState": desiredState,
	})
	l.WithField("data", c).Debug("helperState")

	toSend, desc := c.actionSpec(desiredState)
	if toSend != nil {
		l.Infof("Sending: %+v", toSend)
		c.updateStatusInProgress(desc)
		err := c.srv.Send(toSend)

		// TODO: Retry
		if err != nil {
			return err
		}
	}

	return nil
}

func (c *Client) actionSpec(action facility.SledState_State) (*facility.SledWaitResponse, string) {

	var img, kernel, initramfs, rootdev string
	var cmdline []string

	l := log.WithFields(log.Fields{
		"client": c.mac,
		"action": action,
	})
	l.Debug("actionSpec")

	// get boot variant based on resource firmware
	fw, err := getResourceFirmware(c.spec.Metal.Resource)
	if err != nil {
		l.Warnf("could not get firmware for client: %v. Assuming EFI", err)
		fw = &xir.Firmware{Kind: xir.Firmware_UEFI}
	}
	variant := firmwareToBootVariant(fw)

	// look for image in spec
	if c.spec.Metal != nil &&
		c.spec.Metal.Model != nil &&
		c.spec.Metal.Model.Image != nil {

		img = fmt.Sprintf("%s/%s-rootfs", variant, c.spec.Metal.Model.Image.Value)
		kernel = fmt.Sprintf("%s/%s-kernel", variant, c.spec.Metal.Model.Image.Value)
		initramfs = fmt.Sprintf("%s/%s-initramfs", variant, c.spec.Metal.Model.Image.Value)

	} else {
		// start with global defaults
		img = fmt.Sprintf("%s/%s-rootfs", variant, defaultImage)
		kernel = fmt.Sprintf("%s/%s-kernel", variant, defaultImage)
		initramfs = fmt.Sprintf("%s/%s-initramfs", variant, defaultImage)

		// use resource-specific default if present
		rimg, err := getResourceImage(c.spec.Metal.Resource)
		if err != nil {
			l.Warnf("could not get resource specific image: %v", err)
		} else if rimg == "" {
			l.Debugf("no default image for '%s': using global default %s", c.spec.Metal.Resource, defaultImage)
		} else {
			img = fmt.Sprintf("%s/%s-rootfs", variant, rimg)
			kernel = fmt.Sprintf("%s/%s-kernel", variant, rimg)
			initramfs = fmt.Sprintf("%s/%s-initramfs", variant, rimg)
		}
	}

	// look for rootdev in spec
	if c.spec.Metal != nil && c.spec.Metal.Rootdev != "" {
		rootdev = c.spec.Metal.Rootdev
	} else {
		rootdev = defaultRootDev
	}

	cmdline, err = c.buildCmdline()
	if err != nil {
		l.Error(err)
		return nil, err.Error()
	}

	server := defaultServer //XXX hardcoded

	switch action {
	case facility.SledState_None:
		fallthrough
	case facility.SledState_Waiting:
		return &facility.SledWaitResponse{
				WaitResponse: &facility.SledWaitResponse_Standby{&facility.Standby{}},
			},
			"waiting..."

	case facility.SledState_Stamp:
		return &facility.SledWaitResponse{
				WaitResponse: &facility.SledWaitResponse_Stamp{&facility.Stamp{
					Device: rootdev,
					Image:  img,
					Server: server,
				}},
			},
			fmt.Sprintf("stamping image %s...", img)

	case facility.SledState_Kexec:
		return &facility.SledWaitResponse{
				WaitResponse: &facility.SledWaitResponse_Kexec{&facility.Kexec{
					Kernel:    kernel,
					Initramfs: initramfs,
					Cmdline:   strings.Join(cmdline, " "),
					Server:    server,
				}},
			},
			fmt.Sprintf("kexec'ing...")
	default:
		return nil, ""
	}
}

func (c *Client) checkAndUpdateImage(image string, state facility.SledState_State) error {
	c.setImage(image)
	return c.updateState(state)
}

func (c *Client) updateStatusComplete() error {
	if c.td == nil {
		log.Errorf("%s is missing task data", c.mac)
		return fmt.Errorf("%s is missing task data", c.mac)
	}
	c.err = nil

	var message string
	if c.skip_stamp {
		message = fmt.Sprintf("kexec'd (skipped stamp) with: %s", c.image)
	} else {
		message = fmt.Sprintf("stamped and kexec'd with: %s", c.image)
	}

	c.td.SetStatusClearMessages(
		rec.TaskStatus_Success,
		rec.TaskMessageInfo(message),
	)
	_, err := c.td.Status.Update(c.td.EtcdClient())
	if err != nil {
		log.Warnf("error updating status: %+v", err)
	}

	return err
}

func (c *Client) updateStatusInProgress(format string, a ...interface{}) error {
	if c.td == nil {
		log.Errorf("%s is missing task data", c.mac)
		return fmt.Errorf("%s is missing task data", c.mac)
	}
	c.err = nil

	c.td.SetStatusClearMessages(
		rec.TaskStatus_Processing,
		rec.TaskMessageInfof(format, a...),
	)
	_, err := c.td.Status.Update(c.td.EtcdClient())

	if err != nil {
		log.Warnf("error updating status: %+v", err)
	}

	return err
}

func (c *Client) updateStatusError(errMsg error) error {
	if c.td == nil {
		log.Errorf("%s is missing task data", c.mac)
		return fmt.Errorf("%s is missing task data", c.mac)
	}

	// if the err message already in memory, dont update
	if c.err != nil && errMsg != nil && c.err.Error() == errMsg.Error() {
		return nil
	}

	c.err = errMsg
	c.td.SetStatusAppendMessages(
		rec.TaskStatus_Error,
		rec.TaskMessageError(errMsg),
	)
	_, err := c.td.Status.Update(c.td.EtcdClient())

	if err != nil {
		log.Warnf("error updating status: %+v", err)
	}

	return err
}

func (c *Client) buildCmdline() ([]string, error) {
	if c.spec.Metal == nil {
		return nil, fmt.Errorf("model error: no metal specification")
	}

	if c.spec.Metal.Infravid == 0 {
		return nil, fmt.Errorf("model error: infravid cannot be 0")
	}

	inframac := c.spec.Metal.Inframac
	if inframac == "" {
		return nil, fmt.Errorf("model error: no inframac provided")
	}

	cmdline := []string{
		"console=tty0",
		"console=ttyS0,115200n8",
		"root=PARTUUID=a0000000-0000-0000-0000-00000000000a",
		"rootfstype=ext4",
		"rw",
		fmt.Sprintf("inframac=%s", inframac),
	}

	// append VLAN subinterface
	if c.spec.Metal.Infravid != 1 {
		cmdline = append(cmdline, fmt.Sprintf("infravid=%d", c.spec.Metal.Infravid))
	}

	return cmdline, nil
}

func (s *S) processClientRq(c *Client, rq *facility.SledWaitRequest) error {
	var err error

	l := log.WithFields(log.Fields{
		"client": c.mac,
		"rq":     rq,
	})

	l.Debug("processClientRq")

	if rq.Ack != nil {
		err = nil
		found := false
		clientErr := rq.Ack.Error
		if clientErr != "" {
			l.Errorf("client encountered error: %s", clientErr)
			return c.updateStatusError(fmt.Errorf("%s", clientErr))
		}
		if rq.Ack.GetStamp() {
			l.Info("Got Stamp ack")
			err = c.checkAndUpdateImage("", facility.SledState_Stamp)
			found = true
		}
		if rq.Ack.GetKexec() {
			l.Infof("Got Kexec ack")

			// on successful boot, make sure metal key reflects the current status
			err = c.updateMachineState(state.MachineState_On)
			if err != nil {
				l.Errorf("error updating machine state: %+v", err)
			}

			return c.updateStatusComplete()
		}
		if rq.Ack.GetBackoff() {
			l.Infof("Got Backoff")
			err = c.updateState(facility.SledState_Waiting)
			found = true
		}
		if rq.Ack.GetStandby() != "" {
			l.Infof("Got Standby")
			err = c.checkAndUpdateImage(rq.Ack.GetStandby(), facility.SledState_Waiting)
			found = true
		}
		if err != nil {
			log.Errorf("updateState failed: %v", err)
			return c.updateStatusError(err)
		}
		if found {
			return err
		}

		err := c.srv.Send(&facility.SledWaitResponse{
			WaitResponse: &facility.SledWaitResponse_Standby{&facility.Standby{}},
		})
		if err != nil {
			log.Errorf("standby sent failed: %v", err)
			return err
		}
	} else {
		l.Infof("initial request, sending standby")
		err := c.srv.Send(&facility.SledWaitResponse{
			WaitResponse: &facility.SledWaitResponse_Standby{&facility.Standby{}},
		})
		if err != nil {
			log.Errorf("standby send failed: %v", err)
			return err
		}
	}

	return err
}

// Wait function is the only function that implements the Server.
func (s *S) Wait(srv facility.Sled_WaitServer) error {
	for {
		rq, err := srv.Recv()
		if err == io.EOF {
			log.Errorf("recv close")
			return nil
		}
		if err != nil {
			log.Errorf("recv err: %v", err)
			return nil
		}

		log.Infof("[Wait request] %+v", rq)

		_, err = net.ParseMAC(rq.Mac)
		if err != nil {
			log.Errorf("invalid mac: %s:  %v", rq.Mac, err)
			continue
		}

		mac := rq.Mac
		client, err := getClient(mac)
		if err != nil {
			log.Errorf("could not get client from mac %s: %+v", mac, err)
			continue
		}
		log.Debugf("found client: %+v", client)

		client.mtx.Lock()
		client.srv = srv
		err = s.processClientRq(client, rq)
		if err != nil {
			log.Errorf("encountered error processing client %s: %+v", client.mac, err)
		}
		client.mtx.Unlock()

		cont := !rq.Ack.GetKexec() // if the client acked a kexec, we're done
		if !cont {
			log.Infof("[Done processing client] %s", mac)
			break
		}
	}

	return nil
}

func firmwareToBootVariant(fw *xir.Firmware) string {
	switch fw.Kind {
	case xir.Firmware_UEFI:
		return "efi"
	case xir.Firmware_BIOS:
		return "bios"
	}

	log.Warnf("unknown xir firmware type: %v; defaulting to EFI", fw.Kind)
	return "efi"
}

func getClient(mac string) (*Client, error) {
	cbmtx.Lock()
	c, ok := macToClient[mac]
	cbmtx.Unlock()

	if !ok {
		return nil, fmt.Errorf("no client with MAC %s exists", mac)
	}

	// determine from the spec whether to stamp for this client
	ms := c.spec.GetState()

	switch ms {
	case state.MachineState_On, state.MachineState_Restarted, state.MachineState_RestartedWarm:
		c.skip_stamp = true
	case state.MachineState_Reimaged:
		c.skip_stamp = false
	case state.MachineState_Off, state.MachineState_Unknown:
		fallthrough
	default:
		return nil, fmt.Errorf("encountered bad MachineState: %s", ms.String())
	}

	return c, nil
}

// runApiserver starts the api server
func runApiserver() {
	log.Info("Running Sled API server")

	grpcServer := grpc.NewServer()
	facility.RegisterSledServer(grpcServer, &S{})

	l, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", service.SledGRPC))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	log.Infof("listening on tcp://0.0.0.0:%d", service.SledGRPC)

	grpcServer.Serve(l)
}
