package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/internal"
	//irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	marspkg "gitlab.com/mergetb/facility/mars/pkg"
	"gitlab.com/mergetb/facility/mars/service"
)

type ms struct{}

func init() {
	internal.InitLogging()
}

func main() {
	err := storage.InitMarsMinIOClient()
	if err != nil {
		log.Fatalf("init minio: %v", err)
	}

	err = storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("init etcd: %v", err)
	}

	go runHeartbeater()
	go runGrpc()
	runGrpcGw()
}

func runGrpc() {

	log.Info("running Mars API server")
	log.Infof("version %s", marspkg.Version)

	creds, err := credentials.NewServerTLSFromFile(
		"/certs/apiserver.pem",
		"/certs/apiserver-key.pem",
	)
	if err != nil {
		log.Fatalf("failed to read TLS cert: %v", err)
	}

	opts := []grpc.ServerOption{
		grpc.Creds(creds),
	}
	opts = append(opts, storage.GRPCMaxServer...)

	grpcServer := grpc.NewServer(opts...)
	facility.RegisterFacilityServer(grpcServer, &ms{})
	facility.RegisterWireguardServer(grpcServer, &ms{})
	facility.RegisterMoaServer(grpcServer, &ms{})

	l, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", service.ApiserverGRPC))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	log.Infof("listening on tcp://0.0.0.0:%d", service.ApiserverGRPC)
	grpcServer.Serve(l)

}

func runGrpcGw() {

	log.Infof("Running Mars gateway on port %d", service.ApiserverHTTP)
	creds := credentials.NewTLS(&tls.Config{
		InsecureSkipVerify: true,
	})

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(creds),
		storage.GRPCMaxMessage,
	}
	err := facility.RegisterFacilityHandlerFromEndpoint(
		ctx,
		mux,
		fmt.Sprintf("localhost:%d", service.ApiserverGRPC),
		opts,
	)
	if err != nil {
		log.Fatal(err)
	}

	err = facility.RegisterWireguardHandlerFromEndpoint(
		ctx,
		mux,
		fmt.Sprintf("localhost:%d", service.ApiserverGRPC),
		opts,
	)
	if err != nil {
		log.Fatal(err)
	}

	//TODO
	domain := "facility.example.net"

	handler := cors.New(cors.Options{
		AllowedOrigins: []string{fmt.Sprintf("https://*.%s", domain)},
		AllowedMethods: []string{
			http.MethodHead,
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
		},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	}).Handler(mux)

	log.Fatal(http.ListenAndServe(
		fmt.Sprintf(":%d", service.ApiserverHTTP),
		handler,
	))

}
