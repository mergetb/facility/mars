package main

import (
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/reconcile/pkg/heartbeat"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
)

// Send heartbeats to reconcilers by periodically writing a key to etcd
// Use a periodic interval from config
func runHeartbeater() {

	interval := config.HeartbeatIntervalFromConfig()
	if interval == 0 {
		log.Warn("disabling reconciler heartbeat service. Interval 0 (or not set) in mars config")
		return
	}

	interval_sec := uint64(interval.Seconds())

	log.Infof("starting reconciler heartbeat service with interval of %d seconds", interval_sec)

	for {
		err := heartbeat.SendEtcdHeartbeat(storage.EtcdClient, interval_sec)
		if err != nil {
			log.Errorf("failed to sent reconciler heartbeat: %+v", err)
		} else {
			log.Tracef("sent etcd heartbeat with interval of %d seconds", interval_sec)
		}

		time.Sleep(interval)
	}
}
