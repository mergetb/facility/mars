package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/portal/v1/go"
	xir "gitlab.com/mergetb/xir/v0.3/go"

	mtz "gitlab.com/mergetb/facility/mars/internal/materialize"
	"gitlab.com/mergetb/facility/mars/internal/storage"
)

/*=============================================================================
Generic
-------
	minio:/materialization/:mzid             	portal.Materialization
	etcd:/mzparam/:mzid							portal.MzParameters

Compute
-------
	etcd:/metal/:resource						portal.BareMetal
	etcd:/vm/:hypervisor/:fqvm					portal.VirtualMachine

Networking
----------
	etcd:/net/:host/phy/:port					portal.Endpoint
	etcd:/net/:host/vtep/:vid					portal.Endpoint
	etcd:/net/:host/vlan/:vni					portal.Endpoint
	etcd:/net/:host/access/:port				portal.Waypoint
	etcd:/net/:host/trunk/:port					portal.Waypoint
	etcd:/net/:host/vtepw/:port					portal.Waypoint
	etcd:/net/:host/peer/:peer					portal.Waypoint

Physics
-------
	minio:/sim/:simulator/:mzid					Simulation
*=============================================================================*/

func materialize(mz *portal.Materialization) error {

	// put materialization data in MinIO

	err := storage.SaveMaterialization(mz)
	if err != nil {
		return err
	}

	err = mtz.SetMaterializationState(mz)
	if err != nil {
		return err
	}

	return nil

}

func dematerialize(rid, eid, pid string) error {

	mzid := mtz.NewMzidFromIds(rid, eid, pid)
	enclaveid := mzid.Mid()

	// remove materialization data from MinIO

	exists, err := storage.MaterializationExists(rid, eid, pid)
	if err != nil {
		return err
	}
	if !exists {
		log.Warnf("Demat for non-existent mtz: %s", enclaveid)
		return nil
	}

	err = mtz.ClearMaterializationState(rid, eid, pid)
	if err != nil {
		return err
	}

	err = storage.DeleteMaterialization(rid, eid, pid)
	if err != nil {
		return err
	}

	err = storage.ClearWgState(enclaveid)
	if err != nil {
		return err
	}

	return nil

}

func reboot(rid, eid, pid string, hostnames []string, mode portal.RebootMaterializationMode) error {

	exists, err := storage.MaterializationExists(rid, eid, pid)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("materialization does not exist")
	}

	err = mtz.RebootMaterialization(rid, eid, pid, hostnames, mode)
	if err != nil {
		return err
	}

	return nil
}

func doNewIngressRequest(mzid, host string, port uint32, protocol xir.Protocol) error {
	return mtz.NewIngressRequest(mzid, host, port, protocol)
}

func doDeleteIngressRequest(mzid, host string, port uint32, protocol xir.Protocol) error {
	return mtz.DeleteIngressRequest(mzid, host, port, protocol)
}
