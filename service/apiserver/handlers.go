package main

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	portal "gitlab.com/mergetb/api/portal/v1/go"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/internal/storage/threads"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (m *ms) Materialize(
	ctx context.Context, rq *facility.MaterializeRequest,
) (*facility.MaterializeResponse, error) {

	mzid := fmt.Sprintf("%s.%s.%s", rq.Materialization.Rid, rq.Materialization.Eid, rq.Materialization.Pid)

	log.Infof("Materialize request: %s", mzid)

	// prevent null pointer dereference
	if rq.Materialization.Params == nil {
		log.Errorf("missing mzparams")
		return nil, status.Error(codes.InvalidArgument, "missing mzparams")
	}

	err := materialize(rq.Materialization)
	if err != nil {
		log.Errorf("materialization error: %v", err)
		return nil, err
	}

	return &facility.MaterializeResponse{}, nil

}

func (m *ms) Dematerialize(
	ctx context.Context, rq *facility.DematerializeRequest,
) (*facility.DematerializeResponse, error) {

	mzid := fmt.Sprintf("%s.%s.%s", rq.Rid, rq.Eid, rq.Pid)

	log.Infof("Dematerialize request: %s", mzid)

	err := dematerialize(rq.Rid, rq.Eid, rq.Pid)
	if err != nil {
		log.Errorf("dematerialization error: %v", err)
		return nil, err
	}

	return &facility.DematerializeResponse{}, nil

}

func (m *ms) RebootMaterialization(
	ctx context.Context, rq *facility.RebootMaterializationRequest,
) (*facility.RebootMaterializationResponse, error) {

	mzid := fmt.Sprintf("%s.%s.%s", rq.Rid, rq.Eid, rq.Pid)

	l := log.WithFields(log.Fields{
		"mzid":      mzid,
		"mode":      rq.Mode.String(),
		"hostnames": rq.Hostnames,
	})
	l.Info("RebootMaterialization request")

	err := reboot(rq.Rid, rq.Eid, rq.Pid, rq.Hostnames, rq.Mode)
	if err != nil {
		l.Error("reboot error: %v", err)
		return nil, err
	}

	return &facility.RebootMaterializationResponse{}, nil
}

/*
func (m *ms) Status(
	ctx context.Context, rq *facility.StatusRequest,
) (*facility.StatusResponse, error) {

	log.Debug("Status request")

	return nil, nil

}
*/

func (m *ms) Info(
	ctx context.Context, rq *facility.InfoRequest,
) (*facility.InfoResponse, error) {

	log.Debug("Info request")

	return &facility.InfoResponse{
		APIServerVersion: "0.1.0",
	}, nil

}

func (m *ms) ListMetal(
	ctx context.Context, rq *facility.ListMetalRequest,
) (*facility.ListMetalResponse, error) {

	log.Debug("List metal request")

	metal, err := storage.ListMetal(rq.All)
	if err != nil {
		return nil, err
	}

	return &facility.ListMetalResponse{Machines: metal}, nil

}

func (m *ms) ListVM(
	ctx context.Context, rq *facility.ListVMRequest,
) (*facility.ListVMResponse, error) {

	log.Debug("List vm request")

	metal, err := storage.ListVM(rq.All)
	if err != nil {
		return nil, err
	}

	return &facility.ListVMResponse{Machines: metal}, nil

}

func (m *ms) ListInfrapods(
	ctx context.Context, rq *facility.ListInfrapodsRequest,
) (*facility.ListInfrapodsResponse, error) {

	log.Debug("List infrapod request")

	pods, err := storage.ListInfrapods()
	if err != nil {
		return nil, err
	}

	return &facility.ListInfrapodsResponse{Infrapods: pods}, nil

}

func (m *ms) ListNet(
	ctx context.Context, rq *facility.ListNetRequest,
) (*facility.ListNetResponse, error) {

	log.Debug("List net request")

	nes, err := storage.ListNet()
	if err != nil {
		return nil, err
	}

	return &facility.ListNetResponse{
		Phys:   nes.Phys,
		Vlans:  nes.Vlans,
		Vteps:  nes.Vteps,
		Access: nes.Access,
		Trunks: nes.Trunks,
		Peers:  nes.Peers,
		/* XXX Routes:      nes.Routes, */
		/* XXX Vrfs:        nes.Vrfs, */
		/* XXX PrefixLists: nes.PrefixLists, */
		/* XXX  RouteMaps: nes.RouteMaps, */
	}, nil

}

func (m *ms) ListMz(
	ctx context.Context, rq *facility.ListMzRequest,
) (*facility.ListMzResponse, error) {

	result, err := storage.ListMz()
	if err != nil {
		return nil, err
	}

	return &facility.ListMzResponse{
		Result: result,
	}, nil

}

func (m *ms) ListIngresses(
	ctx context.Context, rq *facility.ListIngressesRequest,
) (*facility.ListIngressesResponse, error) {

	result, err := storage.ListIngresses()
	if err != nil {
		return nil, err
	}

	return &facility.ListIngressesResponse{
		Ingresses: result,
	}, nil
}

func (m *ms) GetMaterializationIngresses(
	ctx context.Context, rq *facility.GetMaterializationIngressesRequest,
) (*facility.GetMaterializationIngressesResponse, error) {

	// lazy lazy
	ings, err := storage.ListIngresses()
	if err != nil {
		return nil, err
	}

	result := &facility.GetMaterializationIngressesResponse{}

	for _, ing := range ings {
		if ing.Mzid == rq.Mzid {
			result.Ingresses = append(result.Ingresses, ing)
		}
	}

	return result, nil
}

func (m *ms) GetMaterialization(ctx context.Context, rq *facility.GetMaterializationRequest) (*facility.GetMaterializationResponse, error) {
	log.Debug("Get Materialization Request")

	if rq.Summary || rq.StatusMS != 0 {
		tf, err := storage.ReadTaskForest(fmt.Sprintf("%s.%s.%s", rq.Rid, rq.Eid, rq.Pid), time.Duration(rq.StatusMS)*time.Millisecond)
		if err != nil {
			return nil, err
		}

		tt := portal.NewTaskTreeFromReconcileForest(tf)

		ans := &facility.GetMaterializationResponse{}

		if rq.Summary {
			ans.Summary = tt.ToTaskSummary()
		}

		if rq.StatusMS != 0 {
			ans.Status = tt
		}

		return ans, nil
	}

	mz, err := storage.FetchMaterialization(fmt.Sprintf("%s.%s.%s", rq.Rid, rq.Eid, rq.Pid))
	if err != nil {
		return nil, err
	}

	return &facility.GetMaterializationResponse{
		Materialization: mz,
	}, nil
}

func (m *ms) GetMaterializationStatus(ctx context.Context, rq *facility.GetMaterializationStatusRequest) (*facility.GetMaterializationStatusResponse, error) {
	log.Debug("Get Materialization Status Request")

	tf, err := storage.ReadTaskForest(fmt.Sprintf("%s.%s.%s", rq.Rid, rq.Eid, rq.Pid), -1)
	if err != nil {
		return nil, err
	}

	return &facility.GetMaterializationStatusResponse{
		Status: tf,
	}, nil
}

func (m *ms) GetMaterializationShortStatus(ctx context.Context, rq *facility.GetMaterializationShortStatusRequest) (*facility.GetMaterializationShortStatusResponse, error) {
	log.Debug("Get Materialization Short Status Request")

	tf, err := storage.ReadTaskForest(fmt.Sprintf("%s.%s.%s", rq.Rid, rq.Eid, rq.Pid), 0)
	if err != nil {
		return nil, err
	}

	return &facility.GetMaterializationShortStatusResponse{
		Status: tf.ToTaskSummary(),
	}, nil
}

func (m *ms) NewIngress(
	ctx context.Context, rq *facility.NewIngressRequest,
) (*facility.NewIngressResponse, error) {

	mzid := fmt.Sprintf("%s.%s.%s", rq.Realization, rq.Experiment, rq.Project)

	err := doNewIngressRequest(mzid, rq.Host, rq.Port, rq.Protocol)
	if err != nil {
		return nil, err
	}

	return &facility.NewIngressResponse{}, nil
}

func (m *ms) DeleteIngress(
	ctx context.Context, rq *facility.DeleteIngressRequest,
) (*facility.DeleteIngressResponse, error) {

	mzid := fmt.Sprintf("%s.%s.%s", rq.Realization, rq.Experiment, rq.Project)

	err := doDeleteIngressRequest(mzid, rq.Host, rq.Port, rq.Protocol)
	if err != nil {
		return nil, err
	}

	return &facility.DeleteIngressResponse{}, nil
}

func (m *ms) SetMachinePowerState(
	ctx context.Context, rq *facility.SetMachineStateRequest,
) (*facility.SetMachineStateResponse, error) {

	l := log.WithFields(log.Fields{
		"states": rq.States,
	})
	l.Info("SetMachinePowerState request")

	err := storage.SetMachinePowerState(rq.States)
	if err != nil {
		return nil, err
	}

	return &facility.SetMachineStateResponse{}, nil

}

func (m *ms) SetMachineImage(
	ctx context.Context, rq *facility.SetMachineImageRequest,
) (*facility.SetMachineImageResponse, error) {

	l := log.WithFields(log.Fields{
		"images": rq.Images,
	})
	l.Info("SetMachineImage request")

	err := storage.SetMachineImage(rq.Images)
	if err != nil {
		return nil, err
	}

	return &facility.SetMachineImageResponse{}, nil
}

func (m *ms) InitInfranet(
	ctx context.Context, rq *facility.InitInfranetRequest,
) (*facility.InitInfranetResponse, error) {

	return &facility.InitInfranetResponse{}, nil

}

func (m *ms) DeinitInfranet(
	ctx context.Context, rq *facility.DeinitInfranetRequest,
) (*facility.DeinitInfranetResponse, error) {

	return &facility.DeinitInfranetResponse{}, nil

}

func (m *ms) ClearReconcilerStatus(
	ctx context.Context, rq *facility.ClearReconcilerStatusRequest,
) (*facility.ClearReconcilerStatusResponse, error) {

	log.Info("Clear reconciler status request")

	err := storage.ClearReconcilerState(rq.Keys)
	if err != nil {
		log.Errorf("clear reconciler state failed: %v", err)
		return nil, fmt.Errorf("db error")
	}

	return &facility.ClearReconcilerStatusResponse{}, err

}

func (m *ms) AddWgPeers(
	ctx context.Context, rq *facility.AddWgPeersRequest,
) (*facility.AddWgPeersResponse, error) {

	l := log.WithField("request", rq)
	l.Info("Add Wireguard peers request")

	mtz, err := storage.FetchMaterialization(rq.Enclaveid)
	if err != nil {
		l.Errorf("FetchMaterialization: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	err = storage.AddWgPeers(mtz.Params.InfrapodServer, rq)
	if err != nil {
		l.Errorf("add wireguard peers failed: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &facility.AddWgPeersResponse{}, nil
}

func (m *ms) DelWgPeer(
	ctx context.Context, rq *facility.DelWgPeerRequest,
) (*facility.DelWgPeerResponse, error) {

	l := log.WithField("request", rq)
	l.Info("Delete Wireguard peer request")

	mtz, err := storage.FetchMaterialization(rq.Enclaveid)
	if err != nil {
		l.Debugf("FetchMaterialization: %v", err)
		return nil, err
	}

	err = storage.DelWgPeer(mtz.Params.InfrapodServer, rq)
	if err != nil {
		l.Errorf("del wireguard peer failed: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &facility.DelWgPeerResponse{}, nil
}

func (m *ms) CreateWgInterface(
	ctx context.Context, rq *facility.CreateWgInterfaceRequest,
) (*facility.CreateWgInterfaceResponse, error) {

	l := log.WithField("request", rq)
	l.Info("Create Wireguard interface request")

	mtz, err := storage.FetchMaterialization(rq.Enclaveid)
	if err != nil {
		l.Errorf("FetchMaterialization: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	err = storage.CreateWgInterface(mtz.Params.InfrapodServer, rq)
	if err != nil {
		l.Errorf("create wg wireguard failed: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &facility.CreateWgInterfaceResponse{}, nil
}

func (m *ms) DeleteWgInterface(
	ctx context.Context, rq *facility.DeleteWgInterfaceRequest,
) (*facility.DeleteWgInterfaceResponse, error) {

	l := log.WithField("request", rq)
	l.Info("Delete Wireguard interface request")

	err := storage.ClearWgState(rq.Enclaveid)
	if err != nil {
		l.Debugf("delete wg wireguard failed: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &facility.DeleteWgInterfaceResponse{}, nil
}

func (m *ms) GetWgInterface(
	ctx context.Context, rq *facility.GetWgInterfaceRequest,
) (*facility.GetWgInterfaceResponse, error) {

	l := log.WithField("request", rq)
	l.Info("Get Wireguard interface request")

	mtz, err := storage.FetchMaterialization(rq.Enclaveid)
	if err != nil {
		l.Errorf("FetchMaterialization: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	resp, err := storage.GetWgInterface(mtz.Params.InfrapodServer, rq.Enclaveid)
	if err != nil {
		l.Errorf("GetWgInterface: %v", err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	return resp, nil
}

func (m *ms) GetThreads(
	ctx context.Context, rq *facility.GetThreadsRequest,
) (*facility.GetThreadsResponse, error) {

	l := log.WithField("request", rq)
	l.Info("Get Threads request")

	mz := rq.GetMzid()
	if mz != "" {
		n, err := threads.Get(mz)
		if err != nil {
			return nil, status.Error(codes.Internal, fmt.Sprintf("get threads: %v", err))
		}
		return &facility.GetThreadsResponse{Req: []*facility.SetThreadsRequest{{Mzid: mz, Threads: n}}}, nil
	}

	n, err := threads.List()
	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("list threads: %v", err))
	}
	req := make([]*facility.SetThreadsRequest, 0)
	for _, v := range n {
		req = append(req, &facility.SetThreadsRequest{Mzid: v.Mzid, Threads: uint32(v.Threads)})
	}
	return &facility.GetThreadsResponse{Req: req}, nil
}

func (m *ms) SetThreads(
	ctx context.Context, rq *facility.SetThreadsRequest,
) (*facility.SetThreadsResponse, error) {

	l := log.WithField("request", rq)
	l.Info("Set Threads request")

	n := rq.GetThreads()
	mzid := rq.GetMzid()

	var err error
	if n == 0 {
		err = threads.Unset(mzid)
	} else {
		err = threads.Set(mzid, n)
	}

	if err != nil {
		return nil, status.Error(codes.Internal, fmt.Sprintf("set threads: %v", err))
	}
	return &facility.SetThreadsResponse{}, nil
}
