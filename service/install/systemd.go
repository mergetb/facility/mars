package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/coreos/go-systemd/v22/dbus"
)

func initSystemdService(filename, name, contents string, write, enable bool) error {
	if write {
		err := ioutil.WriteFile(filename, []byte(contents), 0644)
		if err != nil {
			return fmt.Errorf("write file %s: %v", filename, err)
		}
	}

	sysd, err := dbus.New()
	if err != nil {
		return fmt.Errorf("failed to get dbus connection: %v", err)
	}
	defer sysd.Close()

	err = sysd.Reload()
	if err != nil {
		return fmt.Errorf("systemd reload failed: %v", err)
	}

	if enable {
		_, _, err = sysd.EnableUnitFiles([]string{name}, false, true)
		if err != nil {
			return fmt.Errorf("failed to enable %s: %v", name, err)
		}
	}

	_, err = sysd.RestartUnit(name, "replace", nil)
	if err != nil {
		return fmt.Errorf("failed to restart %s: %v", name, err)
	}

	return nil
}

func deinitSystemdService(name string, remove bool) error {
	sysd, err := dbus.New()
	if err != nil {
		return fmt.Errorf("failed to get dbus connection: %v", err)
	}
	defer sysd.Close()

	_, err = sysd.StopUnit(name, "replace", nil)
	if err != nil {
		return fmt.Errorf("failed to stop %s: %v", name, err)
	}

	_, err = sysd.DisableUnitFiles([]string{name}, false)
	if err != nil {
		return fmt.Errorf("failed to disable %s: %v", name, err)
	}

	if remove {
		err = os.Remove(name)
		if err != nil {
			return fmt.Errorf("failed to remove %s: %v", name, err)
		}
	}

	err = sysd.Reload()
	if err != nil {
		return fmt.Errorf("systemd reload failed: %v", err)
	}

	return nil
}

func updateSystemdService(filename, name, contents string) error {
	return initSystemdService(filename, name, contents, true, false)
}

func enableSystemdService(name string) error {
	return initSystemdService("", name, "", false, true)
}
