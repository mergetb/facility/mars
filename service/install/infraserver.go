package main

import (
	"fmt"
	"io/ioutil"
	"reflect"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"gitlab.com/mergetb/facility/install/pkg/install"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	rec "gitlab.com/mergetb/tech/reconcile"
)

/*
 * The install service monitors the "/config" etcd key. When configuration
 * changes are made that affect the operation of the native infraserver
 * services, it updates systemd definitions and restarts services as needed to
 * reconcile the installation with the configuration
 */

func runInfraReconciler(infraserver bool) {

	name := fmt.Sprintf("%s-install", hostname)

	var act *IfrTask = nil
	if infraserver {
		act = NewInfraserverTask()
	} else {
		act = NewInfrapodServerTask()
	}

	t := rec.ReconcilerManager{
		Manager:                    name,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  installKey,
			Name:    name,
			Desc:    "Mars dynamic installation service",
			Actions: act,
		}},
	}

	log.Info("Starting the Mars install reconciler")

	t.Run()
}

type IfrTask struct {
	services []string
}

func (t *IfrTask) HasService(s string) bool {
	for _, v := range t.services {
		if v == s {
			return true
		}
	}
	return false
}

// services running on an Infrapod server
func NewInfrapodServerTask() *IfrTask {
	return &IfrTask{services: []string{
		"mars-frr",
		"mars-canopy",
		"mars-infrapod",
		"mars-wireguard",
	}}
}

// services running on an Infraserver
func NewInfraserverTask() *IfrTask {
	return &IfrTask{services: []string{
		"mars-etcd",
		"mars-minio",
		"mars-apiserver",
		"mars-frr",
		"mars-canopy",
		"mars-metal",
		"mars-foundry",
		"mars-infrapod",
		"mars-metal",
		"mars-wireguard",
	}}
}

func (t *IfrTask) Parse(key string) bool {
	return key == installKey
}

func (t *IfrTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	newconfig := new(config.Config)

	log.Infof("received /config create")

	err := yaml.Unmarshal(value, newconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal value: %v", err)
	}

	return rec.CheckErrorToMessage(t.handleConfigCreate(newconfig))

}

func (t *IfrTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	oldconfig := new(config.Config)
	newconfig := new(config.Config)

	log.Infof("received /config update")

	err := yaml.Unmarshal(prev, oldconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal prev: %v", err)
	}

	err = yaml.Unmarshal(value, newconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal value: %v", err)
	}

	return rec.CheckErrorToMessage(t.handleConfigUpdate(oldconfig, newconfig))
}

func (t *IfrTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("/config ensure; ignoring")
	return rec.TaskMessageUndefined()
}

func (t *IfrTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("/config deleted; ignoring")
	return nil
}

func reconcileServices(name string, configNew *config.Config, svcOld, svcNew *config.ServiceConfig) error {
	tlsOld := &svcOld.TLS
	tlsNew := &svcNew.TLS

	if tlsOld.Cacert != tlsNew.Cacert {
		log.Warnf("Cacert configuration not handled")
	}

	if tlsOld.Cert != tlsNew.Cert {
		filename := install.TLSCertPath(name)
		if filename == "" {
			return fmt.Errorf("cannot update cert for %s: no cert path", name)
		}

		err := ioutil.WriteFile(filename, []byte(tlsNew.Cert), 0600)
		if err != nil {
			return fmt.Errorf("write cert %s: %v", filename, err)
		}

		log.Infof("updated TLS cert for %s", name)
	}

	if tlsOld.Key != tlsNew.Key {
		filename := install.TLSKeyPath(name)
		if filename == "" {
			return fmt.Errorf("cannot update key for %s: no key path", name)
		}

		err := ioutil.WriteFile(filename, []byte(tlsNew.Key), 0600)
		if err != nil {
			return fmt.Errorf("write key %s: %v", filename, err)
		}

		log.Infof("updated TLS key for %s", name)
	}

	return nil
}

func (t *IfrTask) handleConfigCreate(cfg *config.Config) error {
	images := []string{
		"mars-apiserver",
		"mars-frr",
		"mars-canopy",
		"mars-infrapod",
		"mars-metal",
		"mars-wireguard",
		//"mars-etcd",
		//"mars-minio",
	}

	for _, name := range images {

		if !t.HasService(name) {
			continue
		}

		contents, err := install.BuildContainerSpec(name, cfg)
		if err != nil {
			return fmt.Errorf("build container spec %s: %v", name, err)
		}

		service := fmt.Sprintf("%s.service", name)
		err = updateSystemdService(fmt.Sprintf("%s/%s", install.SystemdInstallPath(), service), service, contents)
		if err != nil {
			return fmt.Errorf("failed to init systemd service: %v", err)
		}

		log.Infof("restarted container for %s", name)
	}

	return nil
}

func (t *IfrTask) handleConfigUpdate(oldconfig, newconfig *config.Config) error {

	serviceUpdated := make(map[string]bool)

	// 1) Update any state that needs to change due to a service configuration update
	iSvcOld := &oldconfig.Services.Infraserver
	iSvcNew := &newconfig.Services.Infraserver
	iSvcCmps := map[string][]*config.ServiceConfig{
		"mars-apiserver": {&iSvcOld.Apiserver, &iSvcNew.Apiserver},
		"mars-canopy":    {&iSvcOld.Canopy, &iSvcNew.Canopy},
		"mars-metal":     {&iSvcOld.Metal, &iSvcNew.Metal},
		"mars-etcd":      {&iSvcOld.Etcd, &iSvcNew.Etcd},
		"mars-minio":     {&iSvcOld.Minio, &iSvcNew.Minio},
	}

	for name, cmp := range iSvcCmps {
		changed := !reflect.DeepEqual(cmp[0], cmp[1])

		if changed {
			err := reconcileServices(name, newconfig, cmp[0], cmp[1])
			if err != nil {
				return fmt.Errorf("failed to update service for %s: %v", name, err)
			}

			log.Infof("detected service change for %s", name)
		}

		serviceUpdated[name] = changed
	}

	// 2) Do the same for infrapod services
	ipSvcOld := &oldconfig.Services.Infrapod
	ipSvcNew := &newconfig.Services.Infrapod
	ipSvcCmps := map[string][]*config.ServiceConfig{
		"mars-foundry": {&ipSvcOld.Foundry, &ipSvcNew.Foundry},
	}

	for name, cmp := range ipSvcCmps {
		changed := !reflect.DeepEqual(cmp[0], cmp[1])

		if changed {
			err := reconcileServices(name, newconfig, cmp[0], cmp[1])
			if err != nil {
				return fmt.Errorf("failed to update service for %s: %v", name, err)
			}

			log.Infof("detected service change for %s", name)
		}

		serviceUpdated[name] = changed
	}

	// Regenerate systemd specs and restart containers when either of the following conditions are met
	// 1) something in the service configuration changed (as reflected in serviceUpdated map)
	// 2) something in the container systemd unit changed (as detected below)

	images := []string{
		"mars-apiserver",
		"mars-frr",
		"mars-canopy",
		"mars-infrapod",
		"mars-metal",
		"mars-wireguard",
		//"mars-etcd",
		//"mars-minio",
	}

	for _, name := range images {

		if !t.HasService(name) {
			continue
		}

		svcUpdated, ok := serviceUpdated[name]
		if !ok {
			svcUpdated = false
		}

		contentsNew, err := install.BuildContainerSpec(name, newconfig)
		if err != nil {
			return fmt.Errorf("build container spec %s: %v", name, err)
		}

		contentsOld, err := install.BuildContainerSpec(name, oldconfig)
		if err != nil {
			return fmt.Errorf("build container spec %s: %v", name, err)
		}

		if contentsNew != contentsOld || svcUpdated {
			service := fmt.Sprintf("%s.service", name)
			err = updateSystemdService(fmt.Sprintf("%s/%s", install.SystemdInstallPath(), service), service, contentsNew)
			if err != nil {
				return fmt.Errorf("failed to init systemd service: %v", err)
			}

			log.Infof("restarted container for %s", name)
		}
	}

	return nil
}
