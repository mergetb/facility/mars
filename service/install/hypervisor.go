package main

import (
	"context"
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"syscall"

	"github.com/containers/podman/v4/pkg/bindings"
	"github.com/containers/podman/v4/pkg/bindings/volumes"
	"github.com/containers/podman/v4/pkg/domain/entities"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"gitlab.com/mergetb/facility/install/pkg/install"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/facility/mars/pkg/volume"
	rec "gitlab.com/mergetb/tech/reconcile"

	"gitlab.com/mergetb/xir/v0.3/go"
)

const (
	volume_mountpoint       string = "/var/mnt/mariner"
	dflt_mariner_mountpoint        = "/var/lib/mariner"
)

var (
	mariner_mountpoint string = dflt_mariner_mountpoint
)

/*
 * The install service monitors the "/config" etcd key. When configuration
 * changes are made that affect the operation of the hypervisor services, it
 * updates systemd definitions and restarts services as needed to reconcile the
 * installation with the configuration
 */

func runHypervisorReconciler() {
	name := fmt.Sprintf("%s-install", hostname)

	t := rec.ReconcilerManager{
		Manager:                    name,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  installKey,
			Name:    name,
			Desc:    "Mars dynamic installation service",
			Actions: &HypTask{},
		}},
	}

	log.Infof("Performing initial Mars hypervisor install")
	err := initialInstall()
	if err != nil {
		log.Error(err)
	}

	log.Info("Starting the Mars hypervisor install reconciler")
	t.Run()
}

type HypTask struct{}

func (t *HypTask) Parse(key string) bool {
	return key == installKey
}

func (t *HypTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	newconfig := new(config.Config)

	log.Infof("received /config create")

	err := yaml.Unmarshal(value, newconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal value: %v", err)
	}

	return rec.CheckErrorToMessage(handleHypInstall(newconfig))
}

func (t *HypTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	oldconfig := new(config.Config)
	newconfig := new(config.Config)

	log.Infof("received /config update")

	err := yaml.Unmarshal(prev, oldconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal prev: %v", err)
	}

	err = yaml.Unmarshal(value, newconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal value: %v", err)
	}

	return rec.CheckErrorToMessage(handleHypConfigUpdate(oldconfig, newconfig))
}

func (t *HypTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("/config ensure; treating as update")
	return t.Update(prev, value, version, td)
}

func (t *HypTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("/config deleted; ignoring")
	return nil
}

func closePodmanConn(ctx context.Context) {
	if ctx == nil {
		return
	}

	conn, err := bindings.GetClient(ctx)
	if err != nil {
		log.Errorf("error: trying to close podman connection: %+v", err)
		return
	}

	conn.Client.CloseIdleConnections()
}

// Setup systemd units for initial install
func handleHypInstall(newconfig *config.Config) error {
	images := []string{
		"mars-frr",
		"mars-canopy",
		"mars-mariner",
	}

	for _, name := range images {
		spec, err := buildSpecTemplate(name, newconfig)
		if err != nil {
			return fmt.Errorf("build container spec %s: %v", name, err)
		}

		contents, err := install.SpecToContents(spec)
		if err != nil {
			return fmt.Errorf("build container contents %s: %v", name, err)
		}

		service := fmt.Sprintf("%s.service", name)
		err = updateSystemdService(fmt.Sprintf("%s/%s", install.SystemdInstallPath(), service), service, contents)
		if err != nil {
			return fmt.Errorf("failed to init systemd service: %v", err)
		}

		log.Infof("restarted container for %s", name)
	}

	return nil

}

// Regenerate systemd specs and restart containers if anything in the container
// systemd unit changes
func handleHypConfigUpdate(oldconfig, newconfig *config.Config) error {
	images := []string{
		"mars-frr",
		"mars-canopy",
		"mars-mariner",
	}

	for _, name := range images {
		specNew, err := buildSpecTemplate(name, newconfig)
		if err != nil {
			return fmt.Errorf("build container spec %s: %v", name, err)
		}

		specOld, err := buildSpecTemplate(name, oldconfig)
		if err != nil {
			return fmt.Errorf("build container spec %s: %v", name, err)
		}

		contentsNew, err := install.SpecToContents(specNew)
		if err != nil {
			return fmt.Errorf("build container contents %s: %v", name, err)
		}

		contentsOld, err := install.SpecToContents(specOld)
		if err != nil {
			return fmt.Errorf("build container contents %s: %v", name, err)
		}

		if contentsNew != contentsOld {
			service := fmt.Sprintf("%s.service", name)
			err = updateSystemdService(fmt.Sprintf("%s/%s", install.SystemdInstallPath(), service), service, contentsNew)
			if err != nil {
				return fmt.Errorf("failed to init systemd service: %v", err)
			}

			log.Infof("restarted container for %s", name)
		}
	}

	return nil
}

func setupRemotePodman() error {
	err := enableSystemdService("podman.socket")
	if err != nil {
		return err
	}

	return enableSystemdService("podman.service")
}

func writeFile(filename string, contents []byte, mode fs.FileMode) error {
	dir := filepath.Dir(filename)
	err := os.MkdirAll(dir, 0755)
	if err != nil {
		return fmt.Errorf("mkdirall %s: %v", dir, err)
	}

	err = ioutil.WriteFile(filename, contents, mode)
	if err != nil {
		return fmt.Errorf("write file %s: %v", filename, err)
	}

	return nil
}

func installCerts(cfg *config.Config) error {
	iServices := &cfg.Services.Infraserver
	ipServices := &cfg.Services.Infrapod
	svcMap := map[string]*config.ServiceConfig{
		"mars-apiserver": &iServices.Apiserver,
		"mars-canopy":    &iServices.Canopy,
		"mars-metal":     &iServices.Metal,
		"mars-etcd":      &iServices.Etcd,
		"mars-minio":     &iServices.Minio,
		"mars-foundry":   &ipServices.Foundry,
	}

	for name, svc := range svcMap {
		tls := svc.TLS

		if tls.Cacert != "" {
			log.Warnf("Cacert configuration not handled")
		}

		if tls.Cert != "" {
			filename := install.TLSCertPath(name)
			if filename == "" {
				return fmt.Errorf("cannot install cert for %s: no cert path", name)
			}

			err := writeFile(filename, []byte(tls.Cert), 0600)
			if err != nil {
				return fmt.Errorf("write cert %s: %v", filename, err)
			}

			log.Infof("installed TLS cert for %s", name)
		}

		if tls.Key != "" {
			filename := install.TLSKeyPath(name)
			if filename == "" {
				return fmt.Errorf("cannot install key for %s: no key path", name)
			}

			err := writeFile(filename, []byte(tls.Key), 0600)
			if err != nil {
				return fmt.Errorf("write cert %s: %v", filename, err)
			}

			log.Infof("installed TLS key for %s", name)
		}
	}

	return nil
}

func createMvol() error {
	var sock_dir string

	if os.Geteuid() == 0 {
		sock_dir = "/run"
	} else {
		sock_dir = os.Getenv("XDG_RUNTIME_DIR")
	}

	socket := "unix:" + sock_dir + "/podman/podman.sock"
	ctx, err := bindings.NewConnection(context.Background(), socket)
	defer closePodmanConn(ctx)
	if err != nil {
		return fmt.Errorf("podman connect: %v", err)
	}

	// create mariner podman volume
	vols, err := volumes.List(ctx, &volumes.ListOptions{})
	if err != nil {
		return fmt.Errorf("list volumes: %v", err)
	}

	for _, v := range vols {
		if v.Name == "mvol" {
			log.Infof("found existing volume mvol")
			return nil
		}
	}

	_, err = volumes.Create(
		ctx,
		entities.VolumeCreateOptions{
			Name:   "mvol",
			Driver: "local",
		},
		nil,
	)
	if err != nil {
		return fmt.Errorf("create volume: %v", err)
	}

	return nil

}

func createMdArray() error {
	fac, err := storage.GetModel()
	if err != nil {
		return fmt.Errorf("failed to get model: %v", err)
	}

	for _, r := range fac.Resources {
		if r.Id != hostname {
			continue
		}

		disks := []*xir.Disk{}
		for _, d := range r.Disks {
			if d.HasRole(xir.DiskRole_Mariner) {
				log.Debugf("found Mariner disk: %+v", d)

				// make sure it's not also a SysDisk
				if d.HasRole(xir.DiskRole_System) {
					log.Warnf("disk %+v has Mariner and System roles; ignoring", d)
					continue
				}

				disks = append(disks, d)
			}
		}

		log.Infof("found %d disks with role:DiskRole_Mariner: %s", len(disks), disks)

		if len(disks) == 0 {
			return nil
		}

		err = volume.CreateDiskPool(r, disks)
		if err != nil {
			return err
		}

		err = volume.MountDiskPool(volume_mountpoint)
		if err != nil {
			return err
		}

		// set the mariner mountpoint
		mariner_mountpoint = volume_mountpoint
		return nil
	}

	return fmt.Errorf("no resource found for hostname %s", hostname)
}

func installVolumes(config *config.Config) error {
	// create mvol which provides storage on the rootfs
	err := createMvol()
	if err != nil {
		return err
	}

	// create MD array to pool dedicated drives for VM storage
	err = createMdArray()
	if err != nil {
		return err
	}

	return nil
}

func installFiles(cfg *config.Config) error {
	filename := install.FrrDaemonsPath()
	contents := []byte(install.FrrDaemons)

	err := writeFile(filename, contents, 0644)
	if err != nil {
		return fmt.Errorf("write file %s: %v", filename, err)
	}

	subdirs := []string{"run", "etc"}
	for _, d := range subdirs {
		dir := fmt.Sprintf("/var/vol/frr/%s", d)
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return fmt.Errorf("mkdir %s: %v", dir, err)
		}

		// owned by frr uid/gid
		frrid := 92
		err = syscall.Chown(dir, frrid, frrid)
		if err != nil {
			return fmt.Errorf("chown %s: %v", dir, err)
		}
	}

	return nil
}

func buildSpecTemplate(name string, cfg *config.Config) (*install.PodmanServiceTemplateArgs, error) {
	// build template based on the config
	spec, err := install.BuildContainerSpecTemplate(name, cfg)
	if err != nil {
		return nil, fmt.Errorf("build container spec %s: %v", name, err)
	}

	// indicate mountpoint to Mariner container, if not default
	if name == "mars-mariner" {
		if mariner_mountpoint != dflt_mariner_mountpoint {
			spec.RunOptions = append(spec.RunOptions,
				"-v", fmt.Sprintf("%s:/var/mnt/mariner:z", mariner_mountpoint),
				"-e", "MARINER_DIR=/var/mnt/mariner",
			)
		}
	}

	return spec, nil
}

// The initial installation entails setting pulling containers and setting up
// volumes, certs, and other configuration state for the Mars hypervisor
// services. This is anaolgous to the ignition configuration process for the
// infraserver fcos nodes
func initialInstall() error {
	// read facility configuration from etcd
	config, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("read config: %v", err)
	}

	// setup podman service
	err = setupRemotePodman()
	if err != nil {
		return fmt.Errorf("podman enable: %v", err)
	}

	// setup certs
	err = installCerts(config)
	if err != nil {
		return fmt.Errorf("install certs: %v", err)
	}

	// setup volumes
	err = installVolumes(config)
	if err != nil {
		return fmt.Errorf("install volumes: %v", err)
	}

	// setup files
	err = installFiles(config)
	if err != nil {
		return fmt.Errorf("install files: %v", err)
	}

	images := []string{
		"mars-frr",
		"mars-canopy",
		"mars-mariner",
	}

	for _, name := range images {
		// build template based on the config
		spec, err := buildSpecTemplate(name, config)

		// generate systemd contents
		contents, err := install.SpecToContents(spec)
		if err != nil {
			return fmt.Errorf("build container contents %s: %v", name, err)
		}

		service := fmt.Sprintf("%s.service", name)
		err = initSystemdService(fmt.Sprintf("%s/%s", install.SystemdInstallPath(), service), service, contents, true, true)
		if err != nil {
			return fmt.Errorf("failed to init systemd service: %v", err)
		}

		log.Infof("initialized %s", service)
	}

	return nil
}
