package main

import (
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	mars "gitlab.com/mergetb/facility/mars/pkg"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	xir "gitlab.com/mergetb/xir/v0.3/go"
)

var (
	hostname string
)

const (
	installKey string = "/config"
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func main() {
	fqdn, err := os.Hostname()
	if err != nil {
		log.Fatalf("os.Hostname: %v", err)
	}
	hostname = strings.Split(fqdn, ".")[0]

	log.WithFields(log.Fields{"version": mars.Version, "hostname": hostname}).Info("starting up")

	err = storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	// get minio creds from etcd
	config, err := config.GetConfig()
	if err != nil {
		log.Fatalf("read config: %v", err)
	}

	iservices := &config.Services.Infraserver
	muser := iservices.Minio.Credentials.Username
	mpass := iservices.Minio.Credentials.Password

	err = storage.InitMarsMinIOClientCreds(muser, mpass)
	if err != nil {
		log.Fatalf("minio init: %v", err)
	}

	// find our role based on our hostname
	model, err := storage.GetModel()
	if err != nil {
		log.Fatalf("read model: %v", err)
	}

	for _, r := range model.Resources {
		if r.Id == hostname {
			if r.HasRole(xir.Role_InfraServer) {
				// Role_InfraServer nodes usually have both Role_InfrapodServer and Role_Gateway
				// Role_InfrapodServer servers are not gateways
				if r.HasRole(xir.Role_Gateway) {
					go runPathfinderReconciler()
				}
				runInfraReconciler(true)
			} else if r.HasRole(xir.Role_InfrapodServer) {
				runInfraReconciler(false)
			} else if r.HasRole(xir.Role_Hypervisor) {
				runHypervisorReconciler()
			} else if r.HasRole(xir.Role_NetworkEmulator) {
				runEmulatorReconciler()
			} else if r.HasRole(xir.Role_Gateway) {
				runPathfinderReconciler()
			}
		}
	}

	log.Fatalf("no reconciler to run")

}
