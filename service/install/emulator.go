package main

import (
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"gitlab.com/mergetb/facility/install/pkg/install"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	rec "gitlab.com/mergetb/tech/reconcile"
)

/*
 * The install service monitors the "/config" etcd key. When configuration
 * changes are made that affect the operation of the native infraserver
 * services, it updates systemd definitions and restarts services as needed to
 * reconcile the installation with the configuration
 */

func runEmulatorReconciler() {

	name := fmt.Sprintf("%s-install", hostname)

	t := rec.ReconcilerManager{
		Manager:                    name,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  installKey,
			Name:    name,
			Desc:    "Mars dynamic installation service for MOA",
			Actions: &EmuTask{},
		}},
	}

	log.Info("Starting the Mars emulator install reconciler")

	t.Run()
}

type EmuTask struct{}

func (t *EmuTask) Parse(key string) bool {
	return key == installKey
}

func (t *EmuTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("received /config create")
	return rec.CheckErrorToMessage(enableSystemdService("mars-moa.service"))
}

func (t *EmuTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	oldconfig := new(config.Config)
	newconfig := new(config.Config)

	log.Infof("received /config update")

	err := yaml.Unmarshal(prev, oldconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal prev: %v", err)
	}

	err = yaml.Unmarshal(value, newconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal value: %v", err)
	}

	return rec.CheckErrorToMessage(handleEmuConfigUpdate(oldconfig, newconfig))
}

func (t *EmuTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("/config ensure; ignoring")
	return rec.TaskMessageUndefined()
}

func (t *EmuTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("/config deleted; ignoring")
	return nil
}

func handleEmuConfigUpdate(oldconfig, newconfig *config.Config) error {
	iImgOld := &oldconfig.Images.Moa.Moa
	iImgNew := &newconfig.Images.Moa.Moa

	// check if the Moa image has changed
	if !reflect.DeepEqual(iImgOld, iImgNew) {
		log.WithFields(log.Fields{"image": iImgNew.RegUrl()}).Info("moa image change")

		contents, err := install.BuildContainerSpec("mars-moa", newconfig)
		if err != nil {
			return fmt.Errorf("build container spect: %w", err)
		}

		return updateSystemdService(fmt.Sprintf("%s/mars-moa.service", install.SystemdInstallPath()), "mars-moa.service", contents)
	}

	iSvcOld := &oldconfig.Services.Infraserver
	iSvcNew := &newconfig.Services.Infraserver

	// currently we only care about getting the update minio credentials for moa
	// moad starts before `mrs config init` is run on the ops node, so it starts up with the default value
	if iSvcOld.Minio.Credentials.Username != iSvcNew.Minio.Credentials.Username ||
		iSvcOld.Minio.Credentials.Password != iSvcNew.Minio.Credentials.Password {
		log.Info("detected minio credential change, restarting moa")
		return enableSystemdService("mars-moa.service") // just restart without re-writing the file
	}
	// etcd doesn't have any credentials

	return nil
}
