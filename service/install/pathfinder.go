package main

import (
	"fmt"
	"reflect"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"

	"gitlab.com/mergetb/facility/install/pkg/install"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	rec "gitlab.com/mergetb/tech/reconcile"
)

/*
 * The install service monitors the "/config" etcd key. When configuration
 * changes are made that affect the operation of the native infraserver
 * services, it updates systemd definitions and restarts services as needed to
 * reconcile the installation with the configuration.
 *
 * Note that pathfinder is two services: a reverse proxy service (traefik) and
 * the pathfinder service which configures facilty ingress.
 */

func runPathfinderReconciler() {

	name := fmt.Sprintf("%s-install", hostname)

	t := rec.ReconcilerManager{
		Manager:                    name,
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  installKey,
			Name:    name,
			Desc:    "Mars dynamic installation service for Pathfinder",
			Actions: &PathfinderTask{},
		}},
	}

	log.Info("Starting the Mars emulator install reconciler")

	t.Run()
}

type PathfinderTask struct{}

func (t *PathfinderTask) Parse(key string) bool {
	return key == installKey
}

func (t *PathfinderTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {

	log.Infof("received /config create")

	err := enableSystemdService("mars-reverseproxy.service")
	if err != nil {
		return rec.CheckErrorToMessage(err)
	}

	return rec.CheckErrorToMessage(enableSystemdService("mars-pathfinder.service"))
}

func (t *PathfinderTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	oldconfig := new(config.Config)
	newconfig := new(config.Config)

	log.Infof("received /config update")

	err := yaml.Unmarshal(prev, oldconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal prev: %v", err)
	}

	err = yaml.Unmarshal(value, newconfig)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal value: %v", err)
	}

	return rec.CheckErrorToMessage(handlePathfinderConfigUpdate(oldconfig, newconfig))
}

func (t *PathfinderTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("/config ensure; ignoring")
	return rec.TaskMessageUndefined()
}

func (t *PathfinderTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("/config deleted; ignoring")
	return nil
}

func handlePathfinderConfigUpdate(oc, nc *config.Config) error {

	type iCfg struct {
		new  config.Container
		old  config.Container
		spec string
	}

	imagesCfg := []iCfg{
		{
			old:  oc.Images.Gateway.Pathfinder,
			new:  nc.Images.Gateway.Pathfinder,
			spec: "mars-pathfinder",
		},
		{
			old:  oc.Images.Gateway.ReverseProxy,
			new:  nc.Images.Gateway.ReverseProxy,
			spec: "mars-reverse-proxy",
		},
	}

	// check if the images have changed
	for _, ic := range imagesCfg {
		if !reflect.DeepEqual(ic.old, ic.new) {
			log.WithFields(log.Fields{"image": ic.new.RegUrl()}).Info("image change")

			contents, err := install.BuildContainerSpec(ic.spec, nc)
			if err != nil {
				return fmt.Errorf("build container spect: %w", err)
			}

			return updateSystemdService(
				fmt.Sprintf("%s/%s.service", ic.spec, install.SystemdInstallPath()),
				fmt.Sprintf("%s.service", ic.spec),
				contents,
			)
		}
	}

	// Handle other updates? Might be neat to update on reverse proxy config updates.
	// These are in a file, so we'd need a config key that stores the file path...

	return nil
}
