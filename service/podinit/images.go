package main

import (
	"context"
	"fmt"

	"github.com/minio/minio-go/v7"
	miniocredentials "github.com/minio/minio-go/v7/pkg/credentials"
	iampolicy "github.com/minio/minio/pkg/iam/policy"
	"github.com/minio/minio/pkg/madmin"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/pkg/config"
)

func configureMinioImages() error {
	log.Info("configuring images MinIO bucket")

	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	username := cfg.Services.Infrapod.Minio.Credentials.Username
	password := cfg.Services.Infrapod.Minio.Credentials.Password

	mc, err := minio.New(
		fmt.Sprintf("localhost:9000"),
		&minio.Options{
			Creds: miniocredentials.NewStaticV4(
				username,
				password,
				"",
			),
			//TODO Secure: true, // implies ssl
		},
	)
	if err != nil {
		return fmt.Errorf("new minio client: %v", err)
	}

	err = initializeImageBucket(mc, username, password)
	if err != nil {
		return err
	}

	err = createBucket("sys", mc)
	if err != nil {
		return err
	}

	return nil
}

func initializeImageBucket(mc *minio.Client, username, password string) error {
	err := createBucket("images", mc)
	if err != nil {
		return err
	}

	//TODO use ssl
	ssl := false

	ma, err := madmin.New(
		fmt.Sprintf("localhost:9000"),
		username,
		password,
		ssl,
	)
	if err != nil {
		return fmt.Errorf("minio admin client: %v", err)
	}

	err = createImageBucketPolicy(ma)
	if err != nil {
		return err
	}

	err = createImageBucketReaderUser(ma)
	if err != nil {
		return err
	}

	return nil
}

func createBucket(bucket string, mc *minio.Client) error {
	found, err := mc.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("%s bucket exists check: %v", bucket, err)
	}

	if found {
		// nothing to do
		return nil
	}

	err = mc.MakeBucket(context.TODO(), bucket, minio.MakeBucketOptions{})
	if err != nil {
		return fmt.Errorf("make image bucket: %v", err)
	}

	return nil
}

func createImageBucketPolicy(ma *madmin.AdminClient) error {
	p := &iampolicy.Policy{
		Version: "2012-10-17",
		Statements: []iampolicy.Statement{
			{
				Actions: iampolicy.ActionSet{"s3:GetObject": {}},
				Effect:  "Allow",
				Resources: iampolicy.ResourceSet{iampolicy.Resource{
					Pattern: "images/*",
				}: {}},
			},
		},
	}

	err := ma.AddCannedPolicy(context.TODO(), "image-read", p)
	if err != nil {
		return fmt.Errorf("add image read policy: %v", err)
	}

	return nil
}

func createImageBucketReaderUser(ma *madmin.AdminClient) error {
	// not intended to be an access control at this time, just a well known
	// credential.
	err := ma.AddUser(context.TODO(), "image", "imageread")
	if err != nil {
		return fmt.Errorf("create minio image user: %v", err)
	}

	err = ma.SetPolicy(context.TODO(), "image-read", "image", false)
	if err != nil {
		return fmt.Errorf("set image-read policy to image user: %v", err)
	}

	return nil
}
