package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"

	log "github.com/sirupsen/logrus"

	portal "gitlab.com/mergetb/api/portal/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	foundry "gitlab.com/mergetb/tech/foundry/api"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func configureFoundry(mzid string, hypervisor bool, mz *portal.Materialization) error {
	tbx, err := storage.GetModel()
	if err != nil {
		return fmt.Errorf("fetch testbed model: %v", err)
	}

	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	var configs []*foundry.MachineConfig

	hfi_map, vfi_map := findEndpoints(mz)

	for _, x := range mz.Metal {
		log.Infof("configuring metal resource %v", x.Resource)

		r := tbx.Resource(x.Resource)
		if r == nil {
			return fmt.Errorf("resource %s not found", x.Resource)
		}

		ifx := r.Infranet()
		if ifx == nil {
			return fmt.Errorf("resource %s has no infranet interface", x.Resource)
		}

		if ifx.Mac == "" {
			return fmt.Errorf("resource %s has no infranet mac", x.Resource)
		}

		mc := &foundry.MachineConfig{
			Id:     ifx.Mac,
			Config: new(foundry.Config),
		}

		// Use infra FQDN as hostname
		fqdn := fmt.Sprintf("%s.infra.%s", x.Model.Id, mzid)
		mc.Config.Hostname = fqdn
		/*
			mc.Config.Hosts = []*foundry.Host{{
				Name: fqdn,
				Ipv4: "127.0.0.1",
				Ipv6: "::1",
			}}
		*/

		mc.Config.ExpandRootfs = true

		for _, u := range x.Users {
			mc.Config.Users = append(mc.Config.Users, &foundry.User{
				Name:              u.Name,
				Groups:            u.Groups,
				SshAuthorizedKeys: u.Pubkeys,
				SshPrivateKeys:    u.Privkeys,
			})
		}

		// add ops account to hypervisors
		if hypervisor {
			addOpsAccount(cfg, mc.Config)
		}

		// Build interface configs
		for _, socket := range x.GetModel().GetSockets() {
			for _, fi := range hfi_map[socket.GetEndpoint().GetElement()][x.Resource] {
				fi.Addrs = socket.GetAddrs()
				mc.Config.Interfaces = append(mc.Config.Interfaces, fi)
			}
		}

		// Add routes if they exist.
		nodeConf := x.GetModel().GetConf()
		if nodeConf != nil {
			for _, r := range nodeConf.Routes {
				mc.Config.Routes = append(mc.Config.Routes, &foundry.Route{
					Src:     r.Src,
					Dst:     r.Dst,
					Gateway: r.Gw,
				})
			}
		}

		// Configure DHCP on management net for hypervisors
		if hypervisor {
			mgmt := r.Mgmt()
			mc.Config.Interfaces = append(mc.Config.Interfaces,
				&foundry.Interface{
					Name: r.PortName(mgmt),
					Mac:  mgmt.GetMac(),
					Dhcp: &foundry.Dhcp{
						Dhcp:       "ipv4",
						UseDns:     false,
						UseDomains: false,
					},
				},
			)
		}

		configs = append(configs, mc)
	}

	// Configure VMs

	for _, x := range mz.Vms {
		log.Infof("configuring VM %s on resource %v", x.VmAlloc.Node, x.VmAlloc.Resource)

		mc := &foundry.MachineConfig{
			Id:     x.Inframac,
			Config: new(foundry.Config),
		}

		// Use FQDN as hostname
		fqdn := fmt.Sprintf("%s.infra.%s", x.VmAlloc.Node, mzid)
		mc.Config.Hostname = fqdn
		/*
			mc.Config.Hosts = []*foundry.Host{{
				Name: fqdn,
				Ipv4: "127.0.0.1",
				Ipv6: "::1",
			}}
		*/

		mc.Config.ExpandRootfs = true

		for _, u := range x.Users {
			mc.Config.Users = append(mc.Config.Users, &foundry.User{
				Name:              u.Name,
				Groups:            u.Groups,
				SshAuthorizedKeys: u.Pubkeys,
				SshPrivateKeys:    u.Privkeys,
			})
		}

		// Build interface configs
		for _, socket := range x.VmAlloc.GetModel().GetSockets() {
			for _, fi := range vfi_map[socket.GetEndpoint().GetElement()][x.VmAlloc.Node] {
				fi.Addrs = socket.GetAddrs()
				mc.Config.Interfaces = append(mc.Config.Interfaces, fi)
			}
		}

		// Add routes if they exist.
		nodeConf := x.VmAlloc.GetModel().GetConf()
		if nodeConf != nil {
			for _, r := range nodeConf.Routes {

				mc.Config.Routes = append(mc.Config.Routes, &foundry.Route{
					Src:     r.Src,
					Dst:     r.Dst,
					Gateway: r.Gw,
				})
			}
		}

		configs = append(configs, mc)
	}

	endpoint := fmt.Sprintf("localhost:27000")
	creds, err := foundryTlsConfig(endpoint)
	if err != nil {
		return fmt.Errorf("foundry tls config: %v", err)
	}

	conn, err := grpc.Dial(
		endpoint,
		grpc.WithTransportCredentials(creds),
		storage.GRPCMaxMessage,
	)
	if err != nil {
		return fmt.Errorf("dial foundry @ %s: %v", endpoint, err)
	}

	cli := foundry.NewManageClient(conn)
	_, err = cli.Set(
		context.TODO(),
		&foundry.SetRequest{
			Configs: configs,
		},
	)
	if err != nil {
		return fmt.Errorf("foundry set error: %v", err)
	}

	log.Infof("configured %d foundry nodes", len(configs))
	return nil
}

func findEndpoints(mz *portal.Materialization) (
	map[string]map[string][]*foundry.Interface,
	map[string]map[string][]*foundry.Interface,
) {
	// Build interfaces from the endpoint lists (found in the links)
	// and save it for later

	// We have to iterate over the links to get the endpoints, as that's
	// the only place where complete information about the endpoints
	// (like ports and vids) are stored

	// Save the endpoints/interfaces in the form of hfi_map[link_id][host], vfi_map[link_id][guest]
	// for bare metal and virtual endpoints, respectively
	hfi_map := make(map[string]map[string][]*foundry.Interface)
	vfi_map := make(map[string]map[string][]*foundry.Interface)

	for _, links := range mz.GetLinks() {
		link_id := links.GetRealization().GetLink().GetId()

		// No need to construct infranet endpoints
		if link_id == "infranet" || link_id == "harbor" {
			continue
		}

		for _, seg := range links.GetRealization().GetSegments() {
			for _, ep := range seg.GetEndpoints() {
				fi := &foundry.Interface{
					Mac: ep.GetMac(),
					Mtu: ep.GetMtu(),
				}
				virtual := false

				switch ep.Interface.(type) {
				case *portal.Endpoint_Phy:
					fi.Name = ep.GetPhy().GetName()
					virtual = ep.Virtual

				case *portal.Endpoint_Vlan:
					fi.Name = ep.GetVlan().GetParent().Name
					fi.Vlan = &foundry.Vlan{
						Parent: fi.Name,
						Vid:    int32(ep.GetVlan().GetVid()),
					}

				case *portal.Endpoint_Vtep:
					fi.Name = ep.GetVtep().GetName()

					fi.Vxlan = &foundry.Vxlan{
						Vni: int32(ep.GetVtep().GetVni()),
					}
				}

				if virtual {
					if _, ok := vfi_map[link_id]; !ok {
						vfi_map[link_id] = make(map[string][]*foundry.Interface)
					}

					vfi_map[link_id][ep.GetNode()] = append(vfi_map[link_id][ep.GetNode()], fi)
				} else {
					if _, ok := hfi_map[link_id]; !ok {
						hfi_map[link_id] = make(map[string][]*foundry.Interface)
					}

					hfi_map[link_id][ep.GetHost()] = append(hfi_map[link_id][ep.GetHost()], fi)
				}
			}
		}
	}

	return hfi_map, vfi_map
}

func foundryTlsConfig(endpoint string) (credentials.TransportCredentials, error) {
	b, err := ioutil.ReadFile("/certs/foundry.pem")
	if err != nil {
		return nil, fmt.Errorf("read foundry management cert: %v", err)
	}

	cp := x509.NewCertPool()
	if !cp.AppendCertsFromPEM(b) {
		return nil, fmt.Errorf("credentials: failed to append certificates")
	}

	return credentials.NewTLS(&tls.Config{
		ServerName:         endpoint,
		RootCAs:            cp,
		InsecureSkipVerify: true,
	}), nil
}

func addOpsAccount(cfg *config.Config, fc *foundry.Config) {
	if cfg.Ops.Username == "" || cfg.Ops.Password == "" {
		log.Warnf("Not installing ops user: username or password is nil")
		return
	}

	pubkeys := []string{}

	if cfg.Ops.Ssh.Private == "" || cfg.Ops.Ssh.Public == "" {
		log.Warnf("Installing ops user without authorized key")
	} else {
		pubkeys = append(pubkeys, cfg.Ops.Ssh.Public)
	}

	fc.Users = append(fc.Users, &foundry.User{
		Name:              cfg.Ops.Username,
		Groups:            []string{"wheel"}, // fedora hardcode
		Password:          cfg.Ops.Password,
		SshAuthorizedKeys: pubkeys,
	})
}
