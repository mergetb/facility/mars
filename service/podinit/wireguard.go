package main

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/facility/mars/internal/storage"
)

// The purpose of this function is to ensure that a WG ifx and peers are setup when the infrapod
// starts up.  The typical process for creating WG ifx in a pod is to first create it in the
// *host netns* and then *move it* into the pod's netns:
//   https://www.wireguard.com/netns/
//
// Therefore all this function does is ping the wireguard reconciler. It does
// this by simply re-writing the WG interface request and peer config requests
// for this infrapod into the etcd keys being watched by the reconciler.

// Note that if no interface has been requested for this infrapod (i.e., there
// are no active XDC attachments), then we have nothing to do

func pingWgIfx(host, enclaveid string) error {
	// 1) query storage for ifx requested
	req, err := storage.GetCreateWgInterfaceRequest(host, enclaveid)
	if err != nil {
		// not an error to not have an interface in storage; assume no active attachments
		log.Infof("no ifx request in storage for enclave %s", enclaveid)
		return nil
	}

	log.Infof("requesting access addr %s for enclave %s", req.Accessaddr, enclaveid)

	// 2) write ifx back into etcd to kick the iftask reconciler
	err = storage.CreateWgInterface(host, &facility.CreateWgInterfaceRequest{
		Enclaveid:  enclaveid,
		Accessaddr: req.Accessaddr,
	})
	if err != nil {
		return err
	}

	// 3) query storage for ifx actually created
	resp, err := storage.GetWgInterface(host, enclaveid)
	if err != nil {
		// not an error to not have an interface in storage; assume no active attachments
		log.Infof("no ifx in storage for enclave %s", enclaveid)
		return nil
	}

	log.Infof("requesting %d peers for enclave %s", len(resp.Peers), enclaveid)

	// 4) write peers back into etcd to kick the ifpeer reconciler
	return storage.AddWgPeers(host, &facility.AddWgPeersRequest{
		Enclaveid: enclaveid,
		Configs:   resp.Peers,
	})
}
