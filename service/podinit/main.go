package main

import (
	"fmt"
	"net"
	"os"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/coreos/go-iptables/iptables"

	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
)

func init() {
	internal.InitLogging()
}

// ENV vars
// PODINIT_GATEWAY_SUBNET
// PODINIT_MZID
// PODINIT_IS_HARBOR
// PODINIT_HOST

func getEnv(key string) string {
	val, ok := os.LookupEnv(key)
	if !ok {
		log.Fatalf("%s ENV var is not set", key)
	}

	log.Infof("%s: %v", key, val)
	return val
}

func waitForPorts(ports []int, timeout int) error {
	var ch = make(chan struct{})
	var wg sync.WaitGroup

	wg.Add(len(ports))

	go func() {
		for _, port := range ports {
			go func(host string, port int) {
				defer wg.Done()
				for {
					conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
					if err == nil {
						conn.Close()
						return
					}

					log.Debugf("couldn't dial %s:%d; sleeping 1 second", host, port)
					time.Sleep(1 * time.Second)
				}
			}("localhost", port)
		}
		wg.Wait()
		close(ch)
	}()

	select {
	case <-ch: // ports are ready
		return nil
	case <-time.After(time.Duration(timeout) * time.Second):
		return fmt.Errorf("ports weren't ready in %d seconds", timeout)
	}
}

func main() {
	gatewaySubnet := getEnv("PODINIT_GATEWAY_SUBNET")
	mzid := getEnv("PODINIT_MZID")
	isHarborStr := getEnv("PODINIT_IS_HARBOR")
	host := getEnv("PODINIT_HOST")
	isHarbor := isHarborStr == "1"

	// ports we need to access: etcd, foundry, and minio if we are harbor
	mars_ports := []int{
		2379, 27000,
	}
	if isHarbor {
		mars_ports = append(mars_ports, 9000)
	}

	// give the network 1 minute to come up
	err := waitForPorts(mars_ports, 60)
	if err != nil {
		log.Fatal(err)
	}

	err = storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatal(err)
	}

	username := cfg.Services.Infraserver.Minio.Credentials.Username
	password := cfg.Services.Infraserver.Minio.Credentials.Password

	err = storage.InitMarsMinIOClientCreds(username, password)
	if err != nil {
		log.Fatal(err)
	}

	mz, err := storage.FetchMaterialization(mzid)
	if err != nil {
		log.Fatal(err)
	}

	err = configureFoundry(mzid, isHarbor, mz)
	if err != nil {
		log.Fatal(err)
	}

	if isHarbor {
		err = configureMinioImages()
		if err != nil {
			log.Fatal(err)
		}
	}

	ifx, err := findGatewayIfx(gatewaySubnet)
	if err != nil {
		log.Fatal(err)
	}

	err = enableMasquerade(ifx)
	if err != nil {
		log.Fatal(err)
	}

	err = pingWgIfx(host, mzid)
	if err != nil {
		log.Fatal(err)
	}

	log.Infof("podinit succeeded; sleeping forever")

	// sleep forever
	for {
		select {}
	}
}

func findGatewayIfx(gatewaySubnet string) (string, error) {
	_, gwIpnet, err := net.ParseCIDR(gatewaySubnet)
	if err != nil {
		return "", err
	}

	ifxx, err := net.Interfaces()
	if err != nil {
		return "", err
	}

	for _, ifx := range ifxx {
		addrs, err := ifx.Addrs()
		if err != nil {
			return "", err
		}

		for _, addr := range addrs {
			ip, _, err := net.ParseCIDR(addr.String())
			if err == nil && gwIpnet.Contains(ip) {
				return ifx.Name, nil
			}
		}
	}

	return "", fmt.Errorf("no interface with ipv4 address on subnet %s", gatewaySubnet)
}

func enableMasquerade(ifx string) error {
	ipt, err := iptables.New()
	if err != nil {
		return fmt.Errorf("new iptables: %v", err)
	}

	masq := []string{"-o", ifx, "-j", "MASQUERADE"}
	return ipt.AppendUnique("nat", "POSTROUTING", masq...)
}
