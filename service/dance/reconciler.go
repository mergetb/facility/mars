package main

import (
	"context"
	"fmt"
	"net"
	"time"

	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
	clientv3 "go.etcd.io/etcd/client/v3"

	facility "gitlab.com/mergetb/api/facility/v1/go"
	irec "gitlab.com/mergetb/facility/mars/internal/reconcile"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/facility/mars/pkg/dance"
	rec "gitlab.com/mergetb/tech/reconcile"
)

func runReconciler() {
	// runPollCheck()

	// we are by definition only watching for our own mzid
	networkKey := fmt.Sprintf("/dance/%s/network/%s", hostname, hostname)
	entryKey := fmt.Sprintf("/dance/%s/entry/%s", hostname, hostname)
	dns4Key := fmt.Sprintf("/dance/%s/dns4/%s", hostname, hostname)

	// Adding a set tasks here effectively serializes updates, as the TaskManager will call out to
	// them sequentially. For tasks with no cross-task dependencies this is fine; however, in this
	// case the NetworkTask{} for domain 'x' needs to run before any EntryTask{} or Dns4Task{}
	// instances try to process updates to the domain 'x' network
	//
	// Therefore what we do is process NetworkTask{} in a separate thread and EntryTask{}/Dns4Task{}
	// instances will wait a certain amount of time when processing updates to give the network time
	// to appear

	tNet := rec.ReconcilerManager{
		Manager:                    "dance.network",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{
			{
				Prefix: networkKey,
				Name:   "dance.network",
				Desc:   fmt.Sprintf("Dance service watching %s", networkKey),
				// EnsureFrequency: 60 * time.Second,
				Actions: &NetworkTask{},
			},
		},
	}

	tTask := rec.ReconcilerManager{
		Manager:                    "dance.entries",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*rec.Reconciler{
			{
				Prefix: entryKey,
				Name:   "dance.entry",
				Desc:   fmt.Sprintf("Dance service watching %s", entryKey),
				// EnsureFrequency: 60 * time.Second,
				Actions: &EntryTask{},
			},
			{
				Prefix: dns4Key,
				Name:   "dance.dns4",
				Desc:   fmt.Sprintf("Dance service watching %s", dns4Key),
				// EnsureFrequency: 60 * time.Second,
				Actions: &Dns4Task{},
			},
		},
	}

	log.Infof("starting dance network reconcilers")
	rec.RunReconcilerManagers(&tNet, &tTask)
}

type NetworkTask struct {
	host   string
	domain string
}

func (t *NetworkTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.DanceNetKey {
		return false
	}

	if xk.Host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", xk.Host, hostname)
		return false
	}

	t.host = xk.Host
	t.domain = xk.Name

	return true
}

func (t *NetworkTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] network create observed", t.host)

	obj := new(facility.DanceNetwork)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal network %s: %v", t.domain, err)
	}

	return t.handlePut(obj)
}

func (t *NetworkTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] network update observed", t.host)

	obj := new(facility.DanceNetwork)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal network %s: %v", t.domain, err)
	}

	return t.handlePut(obj)
}

func (t *NetworkTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] network ensure observed", t.host)

	obj := new(facility.DanceNetwork)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal network %s: %v", t.domain, err)
	}

	return t.handlePut(obj)

}

func (t *NetworkTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] network delete observed", t.host)

	obj := new(facility.DanceNetwork)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal network %s: %v", t.domain, err)
	}

	return rec.CheckErrorToMessage(t.handleDelete(obj))
}

func (t *NetworkTask) handlePut(network *facility.DanceNetwork) *rec.TaskMessage {
	srvmtx.Lock()
	s, ok := servers[t.domain]
	if !ok {
		s = dance.NewServer(network)
		servers[t.domain] = s
	}
	srvmtx.Unlock()

	if !ok {
		log.Infof("created dance network %+v", network)
	} else {
		// don't support updates because the dance package does not have a way to stop
		// the underlying dhcp/dns servers at the moment
		return rec.TaskMessageWarningf("dance network %s already exists and does not support reconfiguration", t.domain)
	}

	s.Cache.UpdateNetwork(network)
	go func() { s.Run() }()

	return nil
}

func (t *NetworkTask) handleDelete(network *facility.DanceNetwork) error {
	srvmtx.Lock()
	s, ok := servers[t.domain]
	srvmtx.Unlock()

	if !ok {
		return fmt.Errorf("delete for unknown domain: %s", t.domain)
	}

	// don't delete the network from the map. This allows the concurrent dance.task services to
	// remove things without the network disappearing

	s.Stop()
	return nil
}

func (t *NetworkTask) ensureNetwork(timeout int) (*dance.Server, error) {
	for i := 1; i <= timeout; i++ {
		srvmtx.Lock()
		s, ok := servers[t.domain]
		srvmtx.Unlock()

		if ok {
			return s, nil
		}

		time.Sleep(time.Second * 1)
		log.Debugf("waiting %d seconds for dance domain %s to be created", timeout-i, t.domain)
	}

	return nil, fmt.Errorf("dance network domain %s does not exist", t.domain)
}

type EntryTask struct {
	host   string
	domain string
	mac    string
}

func (t *EntryTask) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.DanceEntryKey {
		return false
	}

	if xk.Host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", xk.Host, hostname)
		return false
	}

	t.host = xk.Host
	t.domain = xk.Name
	t.mac = xk.Name2

	return true
}

func (t *EntryTask) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] entry create observed", t.host)

	obj := new(facility.DanceEntry)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal entry %s/%s: %v", t.domain, t.mac, err)
	}

	return t.handlePut(obj)
}

func (t *EntryTask) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] entry update observed", t.host)

	obj := new(facility.DanceEntry)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal entry %s/%s: %v", t.domain, t.mac, err)
	}

	return t.handlePut(obj)
}

func (t *EntryTask) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] entry ensure observed", t.host)

	obj := new(facility.DanceEntry)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal entry %s/%s: %v", t.domain, t.mac, err)
	}

	return t.handlePut(obj)

}

func (t *EntryTask) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] entry delete observed", t.host)

	obj := new(facility.DanceEntry)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal entry %s/%s: %v", t.domain, t.mac, err)
	}

	return t.handleDelete(obj)
}

func (t *EntryTask) handlePut(entry *facility.DanceEntry) *rec.TaskMessage {
	s, err := t.ensureNetwork(10)
	if err != nil {
		return rec.CheckErrorToMessage(err)
	}

	mac, err := net.ParseMAC(t.mac)
	if err != nil {
		return rec.TaskMessageErrorf("parse mac %s: %v", t.mac, err)
	}

	log.Infof("adding dance entry for mac:%s, ipv4:%s, names:%+v",
		mac.String(),
		dance.Uint32AsIpv4(entry.Ipv4),
		entry.Names,
	)

	m := map[uint64]*facility.DanceEntry{
		dance.MacAsUint64(mac): entry,
	}
	s.Cache.UpdateEntries(m)

	return nil
}

func (t *EntryTask) handleDelete(entry *facility.DanceEntry) *rec.TaskMessage {
	s, err := t.ensureNetwork(10)
	if err != nil {
		return rec.CheckErrorToMessage(err)
	}
	if entry == nil {
		return rec.TaskMessageErrorf("delete for nil dance entry")
	}

	mac, err := net.ParseMAC(t.mac)
	if err != nil {
		return rec.TaskMessageErrorf("parse mac %s: %v", t.mac, err)
	}

	log.Infof("deleting dance entry for mac:%s, ipv4:%s, names:%+v",
		mac.String(),
		dance.Uint32AsIpv4(entry.Ipv4),
		entry.Names,
	)

	m := map[uint64]*facility.DanceEntry{
		dance.MacAsUint64(mac): entry,
	}
	s.Cache.RemoveEntries(m)

	return nil
}

func (t *EntryTask) ensureNetwork(timeout int) (*dance.Server, error) {
	tNet := &NetworkTask{
		host:   t.host,
		domain: t.domain,
	}

	return tNet.ensureNetwork(timeout)
}

type Dns4Task struct {
	host   string
	domain string
	name   string
}

func (t *Dns4Task) Parse(key string) bool {
	xk := irec.ParseKey(key)
	if xk == nil {
		return false
	}

	if xk.Type != irec.DanceDns4Key {
		return false
	}

	if xk.Host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", xk.Host, hostname)
		return false
	}

	t.host = xk.Host
	t.domain = xk.Name
	t.name = xk.Name2

	return true
}

func (t *Dns4Task) Create(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] dns4 create observed", t.host)

	obj := new(facility.DanceDns4Entry)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal dns4 %s/%s: %v", t.domain, t.name, err)
	}

	return t.handlePut(obj)
}

func (t *Dns4Task) Update(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] dns4 update observed", t.host)

	obj := new(facility.DanceDns4Entry)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal dns4 %s/%s: %v", t.domain, t.name, err)
	}

	return t.handlePut(obj)
}

func (t *Dns4Task) Ensure(prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] dns4 ensure observed", t.host)

	obj := new(facility.DanceDns4Entry)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal dns4 %s/%s: %v", t.domain, t.name, err)
	}

	return t.handlePut(obj)

}

func (t *Dns4Task) Delete(value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] dns4 delete observed", t.host)

	obj := new(facility.DanceDns4Entry)
	err := proto.Unmarshal(value, obj)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal dns4 %s/%s: %v", t.domain, t.name, err)
	}

	return t.handleDelete(obj)
}

func (t *Dns4Task) handlePut(entry *facility.DanceDns4Entry) *rec.TaskMessage {
	s, err := t.ensureNetwork(10)
	if err != nil {
		return rec.CheckErrorToMessage(err)
	}

	for _, ipv4 := range entry.Ipv4 {
		log.Infof("adding dance dns4 entry for %s:%s", t.name, dance.Uint32AsIpv4(ipv4))
	}

	m := map[string][]uint32{
		t.name: entry.Ipv4,
	}
	s.Cache.SetName4Raw(m)

	return nil
}

func (t *Dns4Task) handleDelete(entry *facility.DanceDns4Entry) *rec.TaskMessage {
	s, err := t.ensureNetwork(10)
	if err != nil {
		return rec.TaskMessageErrorf("update for unknown domain: %s", t.domain)
	}
	if entry == nil {
		return rec.TaskMessageErrorf("delete for nil dance dns4 entry")
	}

	log.Infof("deleting dance dns4 entry for %s:%+v", t.name, entry.Ipv4)

	m := map[string][]uint32{
		t.name: entry.Ipv4,
	}
	s.Cache.RemoveName4Raw(m)

	return nil
}

func (t *Dns4Task) ensureNetwork(timeout int) (*dance.Server, error) {
	tNet := &NetworkTask{
		host:   t.host,
		domain: t.domain,
	}

	return tNet.ensureNetwork(timeout)
}

func runPollCheck() {
	log.Infof("running poll check")

	resp, err := storage.EtcdClient.Get(
		context.TODO(),
		fmt.Sprintf("/dance/%s/", hostname),
		clientv3.WithPrefix(),
	)
	if err != nil {
		log.Errorf("failed to get data for poll check: %v", err)
	}

	entries := make(map[string][]*facility.DanceEntry)
	dns4 := make(map[string][]*facility.DanceDns4Entry)

	for _, kv := range resp.Kvs {
		key := string(kv.Key)

		n := &NetworkTask{}
		e := &EntryTask{}
		d := &Dns4Task{}

		if n.Parse(key) {
			obj := new(facility.DanceNetwork)
			err := proto.Unmarshal(kv.Value, obj)
			if err != nil {
				log.Errorf("unmarshal network %s: %v", n.domain, err)
				continue
			}

			m := n.handlePut(obj)
			if m != nil && m.Level == rec.TaskMessage_Error {
				log.Error(m.Message)
			}
		} else if e.Parse(key) {
			obj := new(facility.DanceEntry)
			err := proto.Unmarshal(kv.Value, obj)
			if err != nil {
				log.Errorf("unmarshal entry %s/%s: %v", e.domain, e.mac, err)
				continue
			}

			entries[key] = append(entries[key], obj)
		} else if d.Parse(key) {
			obj := new(facility.DanceDns4Entry)
			err := proto.Unmarshal(kv.Value, obj)
			if err != nil {
				log.Errorf("unmarshal dns4 %s/%s: %v", d.domain, d.name, err)
				continue
			}

			dns4[key] = append(dns4[key], obj)
		}
	}

	for key, entry := range entries {
		for _, ent := range entry {
			e := &EntryTask{}
			e.Parse(key)

			err := e.handlePut(ent)
			if err != nil {
				log.Error(err)
			}
		}
	}

	for key, entry := range dns4 {
		for _, ent := range entry {
			d := &Dns4Task{}
			d.Parse(key)

			err := d.handlePut(ent)
			if err != nil {
				log.Error(err)
			}
		}
	}
}
