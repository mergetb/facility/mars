package main

import (
	"sync"

	"gitlab.com/mergetb/facility/mars/pkg/dance"
)

var srvmtx sync.RWMutex
var servers = make(map[string]*dance.Server)
