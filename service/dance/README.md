Dance: Distributed Address & Name Cache Endpoint
================================================

Dance is a name and address server that provides DHCP and DNS services backed by
an etcd data store. On startup Dance loads it's complete configuration into
memory from etcd and then keeps that in-memory data in sync with etcd through
watch events. All requests are served from data in memory making Dance a) 
_eventually consistent_ and b) _fast_.

### Data layout

A dance data configuration is made up of a network configuration and a set of
entries.

#### Common configuration

The following is the network configuration

```proto
message DanceNetwork {
    uint32 dance_addr_v4            = 1; // 4
    bytes  dance_addr_v6            = 2; // 16
    uint32 subnet_mask              = 3; // 4
    string domain                   = 4; // N
    uint32 gateway                  = 5; // 4
    repeated string search          = 6; // N
    map<uint32,bytes> dhcp_options  = 7; // N
    string service_interface        = 8; // N
}                                        // 28+N B
```

#### Entries

Entries are stored in a map with the mac-address as a key.

```
mac-address -> entry
```

Entries have the following protobuf structure.

```proto
message DanceEntry {
    uint32          ipv4    = 1; // 4
    bytes           ipv6    = 2; // 16
    repeated string names   = 3; // N
}                                // 20+N B
``` 

#### Etcd storage

Dance objects are stored in etcd as serialized protobufs.

##### Network

```
/dance/<host>/network/<id> -> Network 
```

- `<host>`: name of the host the network is to be served from
- `<id>`: name of the network

##### Entries

```
/dance/<host>/entry/<network-id>/<mac> -> Entry
```

- `<host>`: name of the host the network is to be served from
- `<network-id>`: name of the network
- `<mac>`: mac address of a host in the network
