package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	mars "gitlab.com/mergetb/facility/mars/pkg"
)

var hostname string

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func main() {
	log.Infof("starting dance endpoint")
	log.Infof("version %s", mars.Version)
	log.Infof("Log Level: %s", log.GetLevel())

	fqdn, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	// NOTE: unlike reconcilers that run in the host namespace, reconcilers in infrapods need to use
	// the fqdn as the hostname
	hostname = fqdn
	log.Infof("hostname: %s", hostname)

	err = storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("failed to init mars etcd client: %v", err)
	}

	root := cobra.Command{
		Use:   "dance",
		Short: "Distributed address & name caching endpoint",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			run()
		},
	}

	root.Execute()
}

func run() {
	// coredns will flip out if it sees args it does not recognize
	os.Args = os.Args[0:1]
	runReconciler()
}
