package coredance

import (
	"context"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"strings"

	"github.com/coredns/coredns/plugin"
	clog "github.com/coredns/coredns/plugin/pkg/log"
	"github.com/coredns/coredns/request"
	"github.com/miekg/dns"
)

var Dns4Lookup func(name string) []net.IP
var Dns6Lookup func(name string) []net.IP

//TODO
//  - reverse DNS

const (
	ipv4 = 1
	ipv6 = 2
)

var log = clog.NewWithPlugin("dance")

var Version = "undefined"

type Dance struct {
	Next plugin.Handler
}

func (x Dance) ServeDNS(
	ctx context.Context, w dns.ResponseWriter, r *dns.Msg,
) (int, error) {

	rq := request.Request{W: w, Req: r}
	name := strings.Trim(rq.QName(), ".")

	var (
		addr net.IP
		rr   dns.RR
	)

	switch rq.Family() {

	case ipv4:
		addr = resolve4(name)
		if addr == nil {
			return -1, fmt.Errorf("dns4 %s: not found", name)
		}
		rr = &dns.A{
			Hdr: dns.RR_Header{
				Name:   rq.QName(),
				Rrtype: dns.TypeA,
				Class:  rq.QClass(),
			},
			A: addr,
		}

	case ipv6:
		if addr == nil {
			return -1, fmt.Errorf("dns6 %s: not found", name)
		}
		rr = &dns.AAAA{
			Hdr: dns.RR_Header{
				Name:   rq.QName(),
				Rrtype: dns.TypeAAAA,
				Class:  rq.QClass(),
			},
			AAAA: addr,
		}
		addr = resolve6(name)

	}

	srv := &dns.SRV{
		Hdr: dns.RR_Header{
			Name:   fmt.Sprintf("_%s.%s", rq.Proto(), rq.QName()),
			Rrtype: dns.TypeSRV,
			Class:  rq.QClass(),
		},
	}
	port, _ := strconv.Atoi(rq.Port())
	srv.Port = uint16(port)
	srv.Target = "."

	a := &dns.Msg{}
	a.SetReply(r)
	a.Compress = true
	a.Authoritative = true
	a.Answer = []dns.RR{rr, srv}
	rq.SizeAndDo(a)
	w.WriteMsg(a)

	pw := NewResponsePrinter(w)

	return plugin.NextOrFailure(x.Name(), x.Next, ctx, pw, r)

}

func resolve4(name string) net.IP {

	addrs := Dns4Lookup(name)
	if len(addrs) == 0 {
		return nil
	}

	return pickAddr(addrs)

}

func resolve6(name string) net.IP {

	addrs := Dns6Lookup(name)
	if len(addrs) == 0 {
		return nil
	}

	return pickAddr(addrs)

}

func pickAddr(addrs []net.IP) net.IP {

	if len(addrs) == 0 {
		return nil
	} else if len(addrs) == 1 {
		return addrs[0]
	} else {
		n := int(rand.Int31n(int32(len(addrs))))
		return addrs[n]
	}

}

func (x Dance) Name() string {
	return "dance"
}

type ResponsePrinter struct {
	dns.ResponseWriter
}

func NewResponsePrinter(w dns.ResponseWriter) *ResponsePrinter {
	return &ResponsePrinter{ResponseWriter: w}
}

func (r *ResponsePrinter) WriteMsg(res *dns.Msg) error {
	return r.ResponseWriter.WriteMsg(res)
}
