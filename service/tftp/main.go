package main

import (
	"fmt"
	"io"
	"os"

	"github.com/pin/tftp"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/facility/mars/internal"
)

func init() {
	internal.InitLogging()
}

func main() {

	readHandler := func(path string, rf io.ReaderFrom) error {

		filename := fmt.Sprintf("/srv/tftp/%s", path)

		log.Infof("OPEN: %s", filename)

		f, err := os.Open(filename)
		if err != nil {
			return err
		}

		_, err = rf.ReadFrom(f)
		if err != nil {
			return err
		}

		return nil

	}

	writeHandler := func(path string, wt io.WriterTo) error { return nil }

	server := tftp.NewServer(readHandler, writeHandler)

	log.Fatal(server.ListenAndServe(":69"))

}
