package main

import (
	"bytes"
	"fmt"
	"net"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/coreos/go-iptables/iptables"

	"golang.org/x/sys/unix"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/tech/rtnl"
	"gitlab.com/mergetb/xir/v0.3/go"
)

func findDevice(macstr string) (string, error) {
	mac, err := net.ParseMAC(macstr)
	if err != nil {
		return "", fmt.Errorf("parse MAC %s: %v", macstr, err)
	}

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return "", fmt.Errorf("rtnl open: %v", err)
	}
	defer rtx.Close()

	links, err := rtnl.ReadLinks(rtx, nil)
	if err != nil {
		return "", fmt.Errorf("rtnl readlinks: %v", err)
	}

	for _, link := range links {
		if bytes.Equal(mac, link.Info.Address) {
			// ensure device is up
			if !link.IsUp() {
				return "", fmt.Errorf("found gateway device %s, but not up", link.Info.Name)
			}

			// ensure device has an IPV4 or IPV6 address
			addrs, err := link.Addrs(rtx)
			if err != nil {
				return "", fmt.Errorf("failed to get device %s addresses: %v", link.Info.Name, err)
			}

			for _, addr := range addrs {
				if addr.Family() == unix.AF_INET || addr.Family() == unix.AF_INET6 {
					return link.Info.Name, nil
				}
			}

			return "", fmt.Errorf("found gateway device %s, but no IPv4 or IPv6 address assigned", link.Info.Name)
		}
	}

	return "", fmt.Errorf("could not find link with MAC address %s", macstr)
}

func findGatewayLink() (string, error) {
	fac, err := storage.GetModel()
	if err != nil {
		log.Fatalf("load facility model: %v", err)
	}

	for _, r := range fac.Resources {
		if r.Id == hostname {
			if r.HasRole(xir.Role_BorderGateway) {
				for _, n := range r.NICs {
					for _, p := range n.Ports {
						if p.Role == xir.LinkRole_GatewayLink {
							return findDevice(p.Mac)
						}
					}
				}

				// gateway role without gateway link
				return "", fmt.Errorf("bad model: Role_BorderGateway with no GatewayLink")
			}
		}
	}

	// not an error to not have a gateway
	return "", nil
}

func enableIpv4Forwarding() error {

	// firewall
	ipt, err := iptables.New()
	if err != nil {
		return fmt.Errorf("new iptables: %v", err)
	}

	err = ipt.ChangePolicy("filter", "FORWARD", "ACCEPT")
	if err != nil {
		return fmt.Errorf("iptables change policy: %v", err)
	}

	// kernel config
	return os.WriteFile(
		"/proc/sys/net/ipv4/ip_forward",
		[]byte("1"),
		0644,
	)

}
