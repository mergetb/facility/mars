package main

import (
	"context"
	"fmt"

	"github.com/containers/podman/v4/libpod/define"
	"github.com/containers/podman/v4/pkg/bindings"
	"github.com/containers/podman/v4/pkg/bindings/containers"
	"github.com/containers/podman/v4/pkg/bindings/images"
	"github.com/containers/podman/v4/pkg/bindings/network"
	"github.com/containers/podman/v4/pkg/bindings/pods"
	"github.com/containers/podman/v4/pkg/domain/entities"
	"github.com/containers/podman/v4/pkg/specgen"

	log "github.com/sirupsen/logrus"
)

func initPodmanConn(server string) (context.Context, error) {
	uri := fmt.Sprintf("tcp://%s:7474", server)
	return bindings.NewConnection(context.Background(), uri)
}

// Unless you close the connection yourself, it hangs around,
// causing a TCP connection/goroutine leak.
//
// At time of writing, as far as I can tell from the documentation,
// the only way to do so is to grab the client from the context
// and close it yourself.
//
// TODO: This is copy-pasted around in a few places -- move it to a common utils package?
func closePodmanConn(ctx context.Context) {
	if ctx == nil {
		return
	}

	conn, err := bindings.GetClient(ctx)
	if err != nil {
		log.Errorf("error: trying to close podman connection: %+v", err)
		return
	}

	conn.Client.CloseIdleConnections()
}

func podmanPodCreate(server string, spec *entities.PodSpec) error {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	_, err = pods.CreatePodFromSpec(ctx, spec)
	if err != nil {
		return fmt.Errorf("pods.CreatePodFromSpec(): %v", err)
	}

	log.Infof("created pod %s", spec.PodSpecGen.Name)
	return nil
}

func podmanPodRemove(server, name string, force bool) error {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	rep, err := pods.Remove(ctx, name, &pods.RemoveOptions{
		Force: &force,
	})
	if err != nil {
		return fmt.Errorf("pods.Remove(): %v", err)
	}

	if rep.Err != nil {
		return fmt.Errorf("pods.Remove() PodRmReport: %s: %v", rep.Id, rep.Err)
	}

	log.Infof("deleted pod %s", name)
	return nil
}

func podmanPodExists(server, name string) (bool, error) {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return false, err
	}

	exists, err := pods.Exists(ctx, name, &pods.ExistsOptions{})
	if err != nil {
		return false, fmt.Errorf("pods.Exists(): %v", err)
	}

	return exists, nil
}

func podmanPodInspect(server, name string) (*entities.PodInspectReport, error) {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return nil, err
	}

	rep, err := pods.Inspect(ctx, name, &pods.InspectOptions{})
	if err != nil {
		return nil, fmt.Errorf("pods.Inspect(): %v", err)
	}

	return rep, nil
}

func podmanPodList(server string) ([]*entities.ListPodsReport, error) {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return nil, err
	}

	rep, err := pods.List(ctx, &pods.ListOptions{})
	if err != nil {
		return nil, fmt.Errorf("pods.List(): %v", err)
	}

	return rep, nil
}

func podmanPodStart(server, name string) error {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	rep, err := pods.Start(ctx, name, &pods.StartOptions{})
	if err != nil {
		return fmt.Errorf("pods.Start(): %v", err)
	}

	if len(rep.Errs) > 0 {
		return fmt.Errorf("pods.Start() PodStartReport: %s: %v", rep.Id, rep.Errs)
	}

	log.Infof("started pod %s", name)
	return nil
}

func podmanPodRestart(server, name string) error {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	rep, err := pods.Restart(ctx, name, &pods.RestartOptions{})
	if err != nil {
		return fmt.Errorf("pods.Restart(): %v", err)
	}

	if len(rep.Errs) > 0 {
		return fmt.Errorf("pods.Restart() PodRestartReport: %s: %v", rep.Id, rep.Errs)
	}

	log.Infof("restarted pod %s", name)
	return nil
}

func podmanPodUnpause(server, name string) error {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	rep, err := pods.Unpause(ctx, name, &pods.UnpauseOptions{})
	if err != nil {
		return fmt.Errorf("pods.Unpause(): %v", err)
	}

	if len(rep.Errs) > 0 {
		return fmt.Errorf("pods.Unpause() PodUnpauseReport: %s: %v", rep.Id, rep.Errs)
	}

	log.Infof("unpaused pod %s", name)
	return nil
}

func podmanContainerCreate(server string, spec *specgen.SpecGenerator) error {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	rep, err := containers.CreateWithSpec(ctx, spec, &containers.CreateOptions{})
	if err != nil {
		return fmt.Errorf("pods.CreatePodFromSpec(): %v", err)
	}

	for _, w := range rep.Warnings {
		log.Warnf("pods.CreatePodFromSpec(): %s", w)
	}

	log.Infof("created container %s", spec.Name)
	return nil
}

func podmanContainerInspect(server, name string) (*define.InspectContainerData, error) {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return nil, err
	}

	data, err := containers.Inspect(ctx, name, &containers.InspectOptions{})
	if err != nil {
		return nil, fmt.Errorf("containers.Inspect(): %v", err)
	}

	return data, nil
}

func podmanNetworkList(server string) ([]string, error) {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return nil, err
	}

	reps, err := network.List(ctx, &network.ListOptions{})
	if err != nil {
		return nil, fmt.Errorf("network.List(): %v", err)
	}

	var nets []string
	for _, rep := range reps {
		nets = append(nets, rep.Name)
	}

	return nets, nil
}

func podmanPullImage(server, name string, noVerify bool) error {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	policy := "newer"
	resp, err := images.Pull(ctx, name, &images.PullOptions{
		Policy:        &policy,
		SkipTLSVerify: &noVerify,
	})
	if err != nil {
		return fmt.Errorf("images.Pull(): %v", err)
	}

	for _, i := range resp {
		log.Debugf("pulled image %s", i)
	}

	return nil
}

func podmanNetworkRemove(server, name string) error {
	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	v := true
	_, err = network.Remove(ctx, name, &network.RemoveOptions{
		Force: &v,
	})

	if err != nil {
		return fmt.Errorf("remove podman network: %s: %w", name, err)
	}

	return nil
}
