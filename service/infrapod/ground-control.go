package main

import (
	"context"
	_ "crypto/tls"
	"fmt"
	_ "github.com/sirupsen/logrus"

	"google.golang.org/grpc"
	_ "google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/ops/ground-control/pkg/api"
)

func groundControlClient(host string) (*grpc.ClientConn, groundcontrol.GroundControlClient, error) {
	/*
		creds := credentials.NewTLS(&tls.Config{
			//XXX XXX XXX
			InsecureSkipVerify: false,
		})
		conn, err := grpc.Dial(fmt.Sprintf("%s:%d", host, groundcontrol.GRPCEndpoint),
			grpc.WithTransportCredentials(creds),
		)
	*/
	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", host, groundcontrol.GRPCEndpoint),
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, nil, fmt.Errorf("ground-control dial: %v", err)
	}

	return conn, groundcontrol.NewGroundControlClient(conn), nil
}

func withGroundControl(host string, f func(gcc groundcontrol.GroundControlClient) error) error {
	conn, cli, err := groundControlClient(host)
	if err != nil {
		return err
	}
	defer conn.Close()

	return f(cli)
}

func GCRegisterInfrapod(mac, ipv4 string, names []string) error {
	err := withGroundControl("ground-control", func(gcc groundcontrol.GroundControlClient) error {
		_, err := gcc.AddDance4Entry(
			context.TODO(),
			&groundcontrol.Dance4Request{
				Mac:   mac,
				Ipv4:  ipv4,
				Names: names,
			},
		)

		if err != nil {
			return fmt.Errorf("ground-control status: %v", err)
		}

		return nil
	})

	return err
}

func GCUnregisterInfrapod(mac, ipv4 string, names []string) error {
	err := withGroundControl("ground-control", func(gcc groundcontrol.GroundControlClient) error {
		_, err := gcc.DelDance4Entry(
			context.TODO(),
			&groundcontrol.Dance4Request{
				Mac:   mac,
				Ipv4:  ipv4,
				Names: names,
			},
		)

		if err != nil {
			return fmt.Errorf("ground-control status: %v", err)
		}

		return nil
	})

	return err
}

func GCGetIpv4() (string, uint8, error) {
	var ipv4 string
	var bits uint8

	err := withGroundControl("ground-control", func(gcc groundcontrol.GroundControlClient) error {
		resp, err := gcc.NewIP4(
			context.TODO(),
			&groundcontrol.IP4Request{},
		)

		if err != nil {
			return fmt.Errorf("ground-control status: %v", err)
		}

		ipv4 = resp.Ipv4
		bits = uint8(resp.Bits)
		return nil
	})

	return ipv4, bits, err
}

func GCFreeIpv4(addr string) error {
	err := withGroundControl("ground-control", func(gcc groundcontrol.GroundControlClient) error {
		_, err := gcc.FreeIP4(
			context.TODO(),
			&groundcontrol.IP4Request{
				Ipv4: addr,
			},
		)

		if err != nil {
			return fmt.Errorf("ground-control status: %v", err)
		}

		return nil
	})

	return err
}
