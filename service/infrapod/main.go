package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	// "net/http"
	// _ "net/http/pprof"

	"gitlab.com/mergetb/facility/mars/internal"
	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/tech/reconcile"
)

var (
	hostname    string
)

func init() {
	internal.InitLogging()
	internal.InitReconciler()
}

func main() {
	log.Infof("starting infrapod reconciler")

	err := storage.InitMarsEtcdClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	err = storage.InitMarsMinIOClient()
	if err != nil {
		log.Fatalf("etcd init: %v", err)
	}

	fqdn, err := os.Hostname()
	if err != nil {
		log.Fatalf("query hostname: %v", err)
	}
	hostname = strings.Split(fqdn, ".")[0]
	log.Infof("hostname: %s", hostname)

	// run a one-time infrapod check at startup time to reconcile target state against current
	// runPollCheck()

	// cleanup stale CNI networks
	cleanupNetworks()

	// enable IPv4 forwarding
	err = enableIpv4Forwarding()
	if err != nil {
		log.Fatal(err)
	}

	// go func() {
	// 	log.Println(http.ListenAndServe("localhost:6060", nil))
	// }()

	t := &reconcile.ReconcilerManager{
		Manager:                    "infrapod",
		EtcdCli:                    storage.EtcdClient,
		ServerHeartbeatGracePeriod: config.HeartbeatGracePeriodFromConfig(),
		Reconcilers: []*reconcile.Reconciler{{
			Name:            hostname,
			Prefix:          "/infrapod/",
			Desc:            fmt.Sprintf("Manage infrapods on %s", hostname),
			EnsureFrequency: 60 * time.Second,
			Actions:         &InfrapodTask{},
		}},
	}

	t.Run()
}
