package main

import (
	"crypto/rand"
	"fmt"
	"net"
	"os"
	"time"

	"github.com/containers/common/libnetwork/types"
	"github.com/containers/podman/v4/libpod/define"
	"github.com/containers/podman/v4/pkg/bindings/network"
	"github.com/containers/podman/v4/pkg/domain/entities"
	"github.com/containers/podman/v4/pkg/specgen"

	spec "github.com/opencontainers/runtime-spec/specs-go"
	log "github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"

	"gitlab.com/mergetb/api/facility/v1/go/state"
	portal "gitlab.com/mergetb/api/portal/v1/go"
	rec "gitlab.com/mergetb/tech/reconcile"

	"gitlab.com/mergetb/facility/mars/internal/storage"
	"gitlab.com/mergetb/facility/mars/pkg/canopy"
	"gitlab.com/mergetb/facility/mars/pkg/config"
	"gitlab.com/mergetb/facility/mars/pkg/infrapod"
)

const (
	mars_gateway    string = "mars-gateway"
	mars_control    string = "mars-control"
	default_network string = "podman"
	restart_policy  string = "always"
	harbor          string = "harbor.system.marstb"
)

type InfrapodTask struct {
	host        string
	realization string
	experiment  string
	project     string
}

// Actions interface implementation ===========================================

func (t *InfrapodTask) Parse(key string) bool {
	tkns := storage.InfrapodKey.FindAllStringSubmatch(key, -1)

	if len(tkns) == 0 || len(tkns[0]) != 5 {
		return false
	}

	t.realization = tkns[0][1]
	t.experiment = tkns[0][2]
	t.project = tkns[0][3]
	t.host = tkns[0][4]

	if t.host != hostname {
		log.Debugf("key matched, but different hostname (%s != %s)", t.host, hostname)
		return false
	}

	return true
}

func (t *InfrapodTask) Create(buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] infrapod create @ %s", t.mzid(), t.host)

	config := &state.InfrapodConfig{
		Mzid: t.mzid(),
	}

	err := proto.Unmarshal(buf, config)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal mz params: %v", err)
	}

	return rec.CheckErrorToMessage(setupInfrapod(t.host, t.mzid(), config, td))
}

func (t *InfrapodTask) Update(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] infrapod update @ %s", t.mzid(), t.host)
	log.Warnf("[%s] infrapod update handler not implemented", t.mzid())
	return nil
}

func (t *InfrapodTask) Ensure(prev, buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	log.Debugf("[%s] infrapod ensure @ %s", t.mzid(), t.host)

	mzid := t.mzid()

	config := &state.InfrapodConfig{
		Mzid: mzid,
	}

	err := proto.Unmarshal(buf, config)
	if err != nil {
		return rec.TaskMessageErrorf("unmarshal mz params: %v", err)
	}

	server := hostname

	found, err := podmanPodExists(server, t.mzid())
	if err != nil {
		return rec.TaskMessageErrorf("podmanPodExists: %v", err)
	}

	if !found {
		err := releasePodDNS(server, t.mzid())
		if err != nil {
			return rec.TaskMessageErrorf("releasePod: %v", err)
		}
	} else {
		report, err := podmanPodInspect(server, t.mzid())

		if err != nil {
			return rec.TaskMessageErrorf("podmanPodInspect: %v", err)
		}

		if report == nil {
			// report == nil but found == true seems like an error
			return rec.TaskMessageErrorf("somehow, the pod status is nil, but we previously found the pod")
		}

		up, err := ensureInfrapodUp(server, t.mzid(), config.Config.Vni, report)
		if err != nil {
			return rec.TaskMessageErrorf("ensureInfrapodUp: %v", err)
		}

		if up {
			return rec.TaskMessageInfof("infrapod is up")
		}

		// infrapod isn't up, so tear it down and try again
		log.Infof("infrapod isn't up, so tearing it down and retrying...")
		err = teardownInfrapod(server, mzid, false, config)
		if err != nil {
			return rec.TaskMessageError(err)
		}
	}

	// if we got here, then the infrapod isn't up for any reason, so recreate it
	log.Infof("infrapod for mzid %s not present: creating now", mzid)
	err = setupInfrapod(server, mzid, config, td)
	if err != nil {
		return rec.TaskMessageErrorf("setupInfrapod: %+v", err)
	}

	return rec.TaskMessageWarningf("recreated infrapod: %s", mzid)

}

func (t *InfrapodTask) Delete(buf []byte, ver int64, td *rec.TaskData) *rec.TaskMessage {
	log.Infof("[%s] infrapod delete @ %s", t.mzid(), t.host)

	config := &state.InfrapodConfig{
		Mzid: t.mzid(),
	}

	// can be nil on service startup
	if buf == nil {
		log.Error("nil buf, nothing to do")
		return nil
	}

	err := proto.Unmarshal(buf, config)
	if err != nil {
		log.Errorf("unmarshal mz params: %v, nothing to do", err)
		return nil
	}

	err = teardownInfrapod(t.host, t.mzid(), true, config)
	if err != nil {
		log.Errorf("error tearing down: %+v, assuming nothing to do", err)
	}

	return nil
}

func (t *InfrapodTask) mzid() string {
	return fmt.Sprintf("%s.%s.%s", t.realization, t.experiment, t.project)
}

// Helpers ====================================================================

// clean up any stale CNI networks that could be lying around from a previous boot
func cleanupNetworks() {
	log.Infof("cleaning up networks")

	inUse := make(map[string]bool)

	// get all networks in use
	server := hostname
	infrapods := infrapodStates(server)

	for _, report := range infrapods {
		if report == nil {
			log.Warn("cleanupNetworks: report is nil, skipping")
			continue
		}

		if report.InfraConfig == nil {
			log.Warnf("cleanupNetworks: %s report.InfraConfig is nil, skipping", report.Name)
			continue
		}

		for _, n := range report.InfraConfig.Networks {
			inUse[n] = true
		}
	}

	// get all networks
	nets, err := podmanNetworkList(server)
	if err != nil {
		log.Error(err)
		return
	}

	for _, n := range nets {
		// don't cleanup the default networks
		if n == default_network || n == mars_control || n == mars_gateway {
			continue
		}
		if _, ok := inUse[n]; !ok {
			log.Warnf("cleaning up stale network %s", n)
			err := podmanNetworkRemove(server, n)
			if err != nil {
				log.Error(err)
			}
		}
	}
}

/*
func runPollCheck() {
	log.Infof("running infrapod check")

	resp, err := storage.EtcdClient.Get(
		context.TODO(),
		"/infrapod/",
		clientv3.WithPrefix(),
	)
	if err != nil {
		log.Fatalf("failed to get data for infrapod check: %v", err)
	}

	// iterate through the KVS remembering mzid of all infrapods
	to_reconcile := make(map[string]*state.InfrapodConfig)
	for _, kv := range resp.Kvs {
		t := &InfrapodTask{}
		if !t.Parse(string(kv.Key)) {
			continue
		}

		config := &state.InfrapodConfig{
			Mzid: t.mzid(),
		}

		err = proto.Unmarshal(kv.Value, config)
		if err != nil {
			log.Errorf("unmarshal state.InfrapodConfig for %s: %v", string(kv.Key), err)
			continue
		}

		to_reconcile[config.Mzid] = config
	}

	// query status of all current infrapods on the system
	server := hostname
	infrapods := infrapodStates(server)

	for mzid, report := range infrapods {
		if state, ok := to_reconcile[mzid]; ok {
			// infrapod exists and is in KVS; ensure it is up
			up, err := ensureInfrapodUp(server, mzid, state.Config.Vni, report)
			if err != nil {
				log.Error(err)
			}

			if !up {
				// we tried to bring it up but failed; delete it and start again from scratch below
				teardownInfrapod(server, mzid, false, state)
			} else {
				// it's up and thus reconciled
				delete(to_reconcile, mzid)
			}
		} else {
			// infrapod exists but is not in KVS; remove it
			err = teardownInfrapod(server, mzid, true, state)
			if err != nil {
				log.Error(err)
			}
		}
	}

	// Before we create new infrapod, ensure that any old DNS state is cleared out. Do
	// this by querying storage and removing any records that exist but don't have an
	// mzid in the 'infrapod' map
	entries, err := storage.GetInfrapodDNSConfigs(server)
	if err != nil {
		log.Fatal(err)
	}

	for _, e := range entries {
		if _, ok := infrapods[e.Mzid]; !ok {
			// record without infrapod; remove it now
			log.Warnf("found stale DNS config for infrapod %s; releasing now", e)
			err = releasePod(server, e.Mzid)
			if err != nil {
				log.Error(err)
			}
		}
	}

	// anything else to be reconciled needs to be created
	for mzid, state := range to_reconcile {
		log.Infof("infrapod for mzid %s not present: creating now", mzid)
		err = setupInfrapod(server, mzid, state, nil)
		if err != nil {
			log.Error(err)
		}
	}
}
*/

func setupInfrapod(server, mzid string, config *state.InfrapodConfig, td *rec.TaskData) error {
	err := createVtep(server, config.Config.Vni)
	if err != nil {
		return fmt.Errorf("create vtep: %v", err)
	}

	// see if it's already up; this can happen if runPollCheck() brings up a pod
	// after a service failure
	rep, err := podmanPodInspect(server, mzid)
	if err == nil && rep.State == define.PodStateRunning {
		log.Infof("infrapod %s already up and running", mzid)
		return nil
	}

	err = createInfraNetwork(server, mzid, config.Config)
	if err != nil {
		return fmt.Errorf("create podman networks: %w", err)
	}
	defer func() {
		if err != nil {
			deleteNetworks(server, mzid)
		}
	}()

	td.SetStatusClearMessages(
		rec.TaskStatus_Processing,
		rec.TaskMessageInfo("creating containers..."),
	)
	_, err = td.Status.Update(td.EtcdClient())

	if err != nil {
		log.Warnf("unable to set status to processing: %+v", err)
	}

	err = createInfrapod(server, mzid, config.Config.Addr)
	if err != nil {
		return fmt.Errorf("create infrapod: %v", err)
	}
	defer func() {
		if err != nil {
			deleteInfrapod(server, mzid)
		}
	}()

	err = createPodinitContainer(server, mzid)
	if err != nil {
		return fmt.Errorf("create podinit container: %v", err)
	}

	err = createEtcdContainer(server, mzid)
	if err != nil {
		return fmt.Errorf("create etcd container: %v", err)
	}

	err = createFoundryContainer(server, mzid)
	if err != nil {
		return fmt.Errorf("create foundry container: %v", err)
	}

	err = createDanceContainer(server, mzid)
	if err != nil {
		return fmt.Errorf("create dance container: %v", err)
	}

	//harbor
	if config.Config.Vni == 3 {
		err = createMinioContainer(server, mzid)
		if err != nil {
			return fmt.Errorf("create minio container: %v", err)
		}

		err = createTftpContainer(server, mzid)
		if err != nil {
			return fmt.Errorf("create tftp container: %v", err)
		}

		err = createSledContainer(server, mzid)
		if err != nil {
			return fmt.Errorf("create sled container: %v", err)
		}

	} else {
		// only create the moa infrapod if we need it
		mz, err := storage.FetchMaterialization(mzid)
		if err != nil {
			return fmt.Errorf("fetch materialization: %v", err)
		}

		if mz.LinkEmulation != nil {
			err = createMoactlContainer(server, mzid)
			if err != nil {
				return fmt.Errorf("create moactl container: %v", err)
			}
		}
	}

	// start the infrapod
	err = podmanPodStart(server, mzid)
	if err != nil {
		return fmt.Errorf("start pod: %v", err)
	}

	td.SetStatusClearMessages(
		rec.TaskStatus_Processing,
		rec.TaskMessageInfo("waiting for infrapod to come up..."),
	)
	_, err = td.Status.Update(td.EtcdClient())

	if err != nil {
		log.Warnf("unable to set status to processing: %+v", err)
	}

	// wait for it to come up
	err = waitForInfrapodUp(server, mzid, 60)
	if err != nil {
		return fmt.Errorf("wait for pod: %v", err)
	}

	return nil
}

func teardownInfrapod(server, mzid string, delete_vtep bool, config *state.InfrapodConfig) error {
	var nets []string
	var errs []error

	// get the pod's networks
	rep, err := podmanPodInspect(server, mzid)
	if err != nil {
		errs = append(errs, fmt.Errorf("pod inspect: %v", err))
	} else if rep.InfraConfig == nil {
		log.Warnf("teardownInfrapod: %s report.InfraConfig is nil, skipping nets", mzid)
	} else {
		for _, n := range rep.InfraConfig.Networks {
			if n == mars_gateway || n == mars_control {
				continue
			}

			nets = append(nets, n)
		}
	}

	// implicitly deletes containers within pod
	err = deleteInfrapod(server, mzid)
	if err != nil {
		errs = append(errs, fmt.Errorf("delete infrapod: %v", err))
	}

	// teardown the vtep for the infrapod
	if delete_vtep && config.Config != nil {
		err = deleteVtep(server, config.Config.Vni)
		if err != nil {
			errs = append(errs, fmt.Errorf("delete vtep: %v", err))
		}
	}

	// remove the podman networks for this pod
	for _, n := range nets {
		err = podmanNetworkRemove(server, n)
		if err != nil {
			errs = append(errs, fmt.Errorf("delete CNI spec: %v", err))
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return fmt.Errorf("%+v", errs)
}

func createInfraNetwork(server, name string, config *portal.InfrapodConfig) error {
	a := config.Addr + "/16"
	n, err := types.ParseCIDR(a)
	if err != nil {
		return fmt.Errorf("parse cidr: %s: %w", a, err)
	}

	ctx, err := initPodmanConn(server)
	defer closePodmanConn(ctx)
	if err != nil {
		return err
	}

	_, err = network.Create(ctx, &types.Network{
		Name:             name,
		Driver:           types.MacVLANNetworkDriver,
		NetworkInterface: fmt.Sprintf("vtep%d", int(config.Vni)),
		Subnets: []types.Subnet{
			{
				Subnet: n,
			},
		},
		Options: map[string]string{
			types.NoDefaultRoute: "true",
		},
	})
	if err != nil {
		return fmt.Errorf("create infranet podman net: %w", err)
	}

	log.Infof("created network for %s", name)

	return nil
}

func deleteNetworks(server, mzid string) error {
	var errs []error

	err := podmanNetworkRemove(server, mzid)
	if err != nil {
		errs = append(errs, fmt.Errorf("remove infra network:%s:%w", mzid, err))
	}

	log.Infof("deleted network for %s", mzid)

	if len(errs) == 0 {
		return nil
	}

	return fmt.Errorf("%+v", errs)
}

func generateUnicastMacAddr() (types.HardwareAddr, error) {

	buf := make([]byte, 6)
	_, err := rand.Read(buf)
	if err != nil {
		return nil, fmt.Errorf("failed to generate random macaddr bytes: %v", err)
	}

	// ensure local+unicast address
	buf[0] = (buf[0] | 2) & 0xfe

	//ar := fmt.Sprintf("%02x:%02x:%02x:%02x:%02x:%02x", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5])
	return buf, nil
}

func createInfrapod(server, mzid, infraaddr string) error {
	log.Debugf("creating infrapod on %s for %s", server, mzid)

	exists, err := podmanPodExists(server, mzid)
	if err != nil {
		return err
	}

	if exists {
		return fmt.Errorf("infrapod %s already exists in local storage", mzid)
	}

	var macaddr types.HardwareAddr

	// special handling for the harbor
	// eventually this can go away once the netavark dhcp proxy sends the pod name in the dhcp discover/request message
	if mzid == harbor {
		macaddr, err = generateUnicastMacAddr()
		if err != nil {
			return err
		}

		err = registerPodDNS(server, mzid, macaddr.String())
		if err != nil {
			return err
		}
	}

	infraname := fmt.Sprintf("%s-infra", mzid)
	spec := &entities.PodSpec{
		PodSpecGen: specgen.PodSpecGenerator{
			PodBasicConfig: specgen.PodBasicConfig{
				Name:      mzid,
				InfraName: infraname,
			},
			PodNetworkConfig: specgen.PodNetworkConfig{
				Networks: map[string]types.PerNetworkOptions{
					mzid: {
						InterfaceName: "eth0",
						StaticIPs:     []net.IP{net.ParseIP(infraaddr)},
					},
					mars_control: {
						InterfaceName: "eth1",
						StaticMAC:     macaddr,
					},
					mars_gateway: {
						InterfaceName: "eth2",
					},
				},
			},
			InfraContainerSpec: &specgen.SpecGenerator{
				ContainerBasicConfig: specgen.ContainerBasicConfig{
					Name:          infraname,
					Pod:           mzid,
					RestartPolicy: restart_policy,
				},
			},
		},
	}

	err = podmanPodCreate(server, spec)

	if err != nil && mzid == harbor {
		e := releasePodDNS(server, mzid)
		if e != nil {
			log.WithFields(log.Fields{"err": e}).Error("release pod")
		}
	}

	return err
}

func deleteInfrapod(server, mzid string) error {
	log.Debugf("deleting infrapod on %s for %s", server, mzid)

	exists, err := podmanPodExists(server, mzid)
	if err != nil {
		return err
	}

	if !exists {
		log.Warningf("infrapod %s does not exist in local storage", mzid)
		return nil
	}

	// release resource allocated by ground-control
	// can be removed once netavark dhcp proxy supports sending the client host name in the dhcp discover
	if mzid == harbor {
		err = releasePodDNS(server, mzid)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("release pod dns")
		}
	}

	err = podmanPodRemove(server, mzid, true)

	return err
}

func createEtcdContainer(server, mzid string) error {
	//TODO consider a volume mount for data?
	//     can see it both ways, the container is tied to the lifetime of the
	//     materialization, and podman keeps the image snapshot in tact
	//     across container restarts so this may be sufficient, but it could be
	//     nice to have a more decoupled data volume.

	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	image := cfg.Images.Infrapod.Etcd.RegUrl()
	noVerify := cfg.Images.Infrapod.Etcd.Registry.NoVerify

	err = podmanPullImage(server, image, noVerify)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s-etcd", mzid)

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name: name,
			Pod:  mzid,
			Env: map[string]string{
				"ETCD_LISTEN_CLIENT_URLS":    "http://0.0.0.0:2379",
				"ETCD_ADVERTISE_CLIENT_URLS": "http://etcd:2379",
				"ETCD_MAX_REQUEST_BYTES":     fmt.Sprintf("%d", storage.MaxMessageSize),
				"ETCD_MAX_TXN_OPS":           "32768",
			},
			RestartPolicy: restart_policy,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: image,
		},
	}

	return podmanContainerCreate(server, spec)
}

func createMinioContainer(server, mzid string) error {
	err := os.MkdirAll("/var/vol/minio-img", 0755)
	if err != nil {
		return fmt.Errorf("create minio data dir: %v", err)
	}

	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	image := cfg.Images.Infrapod.Minio.RegUrl()
	noVerify := cfg.Images.Infrapod.Minio.Registry.NoVerify

	err = podmanPullImage(server, image, noVerify)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s-minio", mzid)
	username := cfg.Services.Infrapod.Minio.Credentials.Username
	password := cfg.Services.Infrapod.Minio.Credentials.Password

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name: name,
			Pod:  mzid,
			Command: []string{
				"minio", "server", "/data",
			},
			Env: map[string]string{
				"MINIO_ROOT_USER":     username,
				"MINIO_ROOT_PASSWORD": password,
			},
			RestartPolicy: restart_policy,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: image,
			Mounts: []spec.Mount{
				{
					Type:        define.TypeBind,
					Source:      "/var/vol/minio-img",
					Destination: "/data",
					Options:     []string{"rbind", "rw", "z"},
				},
			},
		},
	}

	return podmanContainerCreate(server, spec)
}

func createPodinitContainer(server, mzid string) error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	image := cfg.Images.Infrapod.Podinit.RegUrl()
	noVerify := cfg.Images.Infrapod.Podinit.Registry.NoVerify

	err = podmanPullImage(server, image, noVerify)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s-podinit", mzid)
	is_harbor := "0"
	if mzid == "harbor.system.marstb" {
		is_harbor = "1"
	}

	env := map[string]string{
		"PODINIT_GATEWAY_SUBNET": "10.254.0.0/16",
		"PODINIT_MZID":           mzid,
		"PODINIT_IS_HARBOR":      is_harbor,
		"PODINIT_HOST":           hostname,
	}

	if cfg.Images.Infrapod.Podinit.LogLevel != "" {
		env["LOGLEVEL"] = cfg.Images.Infrapod.Podinit.LogLevel
	}

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name: name,
			Pod:  mzid,
			Env:  env,
			Sysctl: map[string]string{
				"net.ipv4.ip_forward": "1", // route from infranet to gateway network
			},
			RestartPolicy: restart_policy,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: image,
			Mounts: []spec.Mount{
				{
					Type:        define.TypeBind,
					Source:      "/var/vol/certs/",
					Destination: "/certs/",
					Options:     []string{"rbind", "ro", "z"},
				},
			},
		},
		ContainerSecurityConfig: specgen.ContainerSecurityConfig{
			CapAdd: []string{
				"NET_ADMIN", "NET_RAW", // needed for masquerading onto gateway network
			},
		},
	}

	return podmanContainerCreate(server, spec)
}

func createTftpContainer(server, mzid string) error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	image := cfg.Images.Infrapod.Tftp.RegUrl()
	noVerify := cfg.Images.Infrapod.Tftp.Registry.NoVerify

	err = podmanPullImage(server, image, noVerify)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s-tftp", mzid)

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name:          name,
			Pod:           mzid,
			RestartPolicy: restart_policy,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: image,
		},
	}

	return podmanContainerCreate(server, spec)
}

func createSledContainer(server, mzid string) error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	image := cfg.Images.Infrapod.Sled.RegUrl()
	noVerify := cfg.Images.Infrapod.Sled.Registry.NoVerify

	err = podmanPullImage(server, image, noVerify)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s-sled", mzid)

	env := make(map[string]string)
	if cfg.Images.Infrapod.Dance.LogLevel != "" {
		env["LOGLEVEL"] = cfg.Images.Infrapod.Sled.LogLevel
	}

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name:          name,
			Pod:           mzid,
			Env:           env,
			RestartPolicy: restart_policy,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: image,
			Mounts: []spec.Mount{
				{
					Type:        define.TypeBind,
					Source:      "/var/vol/certs/",
					Destination: "/certs/",
					Options:     []string{"rbind", "ro", "z"},
				},
			},
		},
	}

	return podmanContainerCreate(server, spec)
}

func createDanceContainer(server, mzid string) error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	image := cfg.Images.Infrapod.Dance.RegUrl()
	noVerify := cfg.Images.Infrapod.Dance.Registry.NoVerify

	err = podmanPullImage(server, image, noVerify)
	if err != nil {
		return err
	}

	env := make(map[string]string)
	if cfg.Images.Infrapod.Dance.LogLevel != "" {
		env["LOGLEVEL"] = cfg.Images.Infrapod.Dance.LogLevel
	}

	name := fmt.Sprintf("%s-dance", mzid)

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name:          name,
			Pod:           mzid,
			Env:           env,
			RestartPolicy: restart_policy,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: image,
		},
	}

	return podmanContainerCreate(server, spec)
}

func createFoundryContainer(server, mzid string) error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	image := cfg.Images.Infrapod.Foundry.RegUrl()
	noVerify := cfg.Images.Infrapod.Foundry.Registry.NoVerify

	err = podmanPullImage(server, image, noVerify)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s-foundry", mzid)

	env := make(map[string]string)
	if cfg.Images.Infrapod.Dance.LogLevel != "" {
		env["LOGLEVEL"] = cfg.Images.Infrapod.Foundry.LogLevel
	}

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name: name,
			Pod:  mzid,
			Env:  env,
			Entrypoint: []string{
				"/usr/bin/foundryd",
			},
			Command: []string{
				// foundry maxsize is in MB
				"--maxsize", "512",
			},
			RestartPolicy: restart_policy,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: image,
			Mounts: []spec.Mount{
				{
					Type:        define.TypeBind,
					Source:      "/var/vol/certs/foundry.pem",
					Destination: "/etc/foundry/manage.pem",
					Options:     []string{"rbind", "ro", "z"},
				},
				{
					Type:        define.TypeBind,
					Source:      "/var/vol/certs/foundry-key.pem",
					Destination: "/etc/foundry/manage-key.pem",
					Options:     []string{"rbind", "ro", "z"},
				},
			},
		},
	}

	return podmanContainerCreate(server, spec)
}

func createMoactlContainer(server, mzid string) error {
	cfg, err := config.GetConfig()
	if err != nil {
		return fmt.Errorf("get config: %v", err)
	}

	image := cfg.Images.Infrapod.Moactl.RegUrl()
	noVerify := cfg.Images.Infrapod.Moactl.Registry.NoVerify

	err = podmanPullImage(server, image, noVerify)
	if err != nil {
		return err
	}

	name := fmt.Sprintf("%s-moa", mzid)

	env := make(map[string]string)
	if cfg.Images.Infrapod.Dance.LogLevel != "" {
		env["LOGLEVEL"] = cfg.Images.Infrapod.Moactl.LogLevel
	}

	spec := &specgen.SpecGenerator{
		ContainerBasicConfig: specgen.ContainerBasicConfig{
			Name:          name,
			Pod:           mzid,
			Env:           env,
			RestartPolicy: restart_policy,
		},
		ContainerStorageConfig: specgen.ContainerStorageConfig{
			Image: image,
		},
	}

	return podmanContainerCreate(server, spec)
}

func createVtep(host string, vni uint32) error {
	device := fmt.Sprintf("vtep%d", vni) // hacky
	vtep, err := storage.GetVtep(host, device)
	if err != nil {
		return fmt.Errorf("get vtep from storage: %v", err)
	}

	return canopy.CreateVtep(vtep)
}

func deleteVtep(host string, vni uint32) error {
	device := fmt.Sprintf("vtep%d", vni) // hacky
	return canopy.DeleteVtep(device)
}

func infrapodStates(server string) map[string]*entities.PodInspectReport {
	pods := make(map[string]*entities.PodInspectReport)

	rep, err := podmanPodList(server)
	if err != nil {
		log.Error(err)
		return pods
	}

	for _, pod := range rep {
		repInspect, err := podmanPodInspect(server, pod.Name)
		if err != nil {
			log.Error(err)
			continue
		}

		pods[pod.Name] = repInspect
	}

	return pods
}

func ensureInfrapodUp(server, mzid string, vni uint32, report *entities.PodInspectReport) (bool, error) {
	// create vtep for the infrapod
	err := createVtep(server, vni)
	if err != nil {
		return false, fmt.Errorf("create vtep: %v", err)
	}

	// nil report is possible, which happens if the infrapod exists but we fail to inspect it
	if report == nil {
		return false, fmt.Errorf("no inspect report for mzid %s", mzid)
	}

	switch report.State {
	case define.PodStateCreated:
		// pods that are only created need to be destroyed as opposed to started/restarted. The
		// reason is that, without more advanced inspection here, we don't know how many of the
		// containers for the pod were actually created successfully. So to be safe, just delete
		// and restart from scratch
		log.Warnf("pod %s in state PodStateCreated; cannot bring up", mzid)
		return false, nil

	case define.PodStateErrored, define.PodStateExited, define.PodStateDegraded, define.PodStateStopped:
		err = podmanPodRestart(server, mzid)
		if err != nil {
			return false, err
		}

	case define.PodStatePaused:
		err = podmanPodUnpause(server, mzid)
		if err != nil {
			return false, err
		}

	case define.PodStateRunning:
		return true, nil

	default:
		return false, fmt.Errorf("unknown pod state for %s: %s", mzid, report.State)
	}

	err = waitForInfrapodUp(server, mzid, 60)
	if err != nil {
		return true, nil
	}

	return false, err
}

func waitForInfrapodUp(server, mzid string, seconds int) error {
	for {
		report, err := podmanPodInspect(server, mzid)
		if err != nil {
			return err
		}

		if report.State == define.PodStateRunning {
			break
		}

		if seconds == 0 {
			return fmt.Errorf("giving up on pod %s. it never came up", mzid)
		}

		log.Infof("waiting %d seconds for %s pod to come up, currently %s", seconds, mzid, report.State)
		seconds--
		time.Sleep(time.Second * 1)
	}

	return nil
}

// The following register/release DNS entries for infrapods at the facility ground-control server.
// In order to make sure that we don't leak addresses or DNS entries across failures/reboots of the
// infrapod service, these functions record entries in storage. Then, at each startup of this
// service, any entries that are no longer needed (due to, e.g., a podman pod being forcibly deleted
// by an operator or something) can be queried and relinquished

func registerPodDNS(server, mzid, mac string) error {

	ip, _, err := GCGetIpv4()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			e := GCFreeIpv4(ip)
			if e != nil {
				log.WithFields(log.Fields{"err": e}).Error("free ipv4")
			}
		}
	}()

	err = storage.SetInfrapodDNSConfig(
		server,
		mzid,
		&infrapod.InfrapodDNSConfig{
			Mzid: mzid,
			Ipv4: ip,
			Mac:  mac,
		},
	)
	if err != nil {
		return fmt.Errorf("set infrapod dns config: %w", err)
	}
	defer func() {
		if err != nil {
			e:= storage.DeleteInfrapodDNSConfig(
				server,
				mzid,
			)
			if e != nil {
				log.WithFields(log.Fields{"err": e}).Error("delete infrapod dns config")
			}
		}
	}()

	err = GCRegisterInfrapod(
		mac,
		ip,
		[]string{fmt.Sprintf("infrapod.%s", mzid)},
	)

	return err
}

// free the allocated ip and dns entries from ground-control
func releasePodDNS(server, mzid string) error {
	var errs []error

	// get control IP from storage
	config, err := storage.GetInfrapodDNSConfig(server, mzid)
	if err != nil {
		return err
	}

	if config != nil {
		// if we allocated an IP, release it
		if config.Ipv4 != "" {
			err = GCFreeIpv4(config.Ipv4)
			if err != nil {
				errs = append(errs, err)
			}
		}

		// if we registered a name, release it
		if config.Mzid != "" {
			err = GCUnregisterInfrapod(
				config.Mac,
				config.Ipv4,
				[]string{fmt.Sprintf("infrapod.%s", config.Mzid)},
			)
			if err != nil {
				errs = append(errs, err)
			}
		}

		err = storage.DeleteInfrapodDNSConfig(
			server,
			mzid,
		)
		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) == 0 {
		return nil
	}

	return fmt.Errorf("%+v", errs)
}
