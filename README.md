# Mars

Merge Automated Resource Space

## Well Known Default Ports

See [service/ports.go](service/ports.go)

### MinIO Notes

Get the client

```
curl -OL https://dl.min.io/client/mc/release/linux-amd64/mc
```

Setup the client

```
pw=`cat /var/moss/genconf.yml | grep miniopassword | awk '{print $2}'`
./mc alias set minio http://minio:9000 mars $pw
```
